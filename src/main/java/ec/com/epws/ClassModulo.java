package ec.com.epws;

import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.Modulo;
import ec.com.epws.entity.Rol;
import ec.com.epws.entity.RolModulo;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.sistema.Sistema;

public class ClassModulo {
	
	
	private String btGuardarEditar = Sistema.idioma.getGuardar();
	private List<Modulo> listaModulo;
	private Modulo modulo;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaModulo = InstanciaDAO.getModuloDAO().getListaModulo();
		listaModulo = InstanciaDAO.getModuloDAO().getListaModulo();
		modulo = new Modulo();
	}

	@Command
	@NotifyChange("listaModulo")
	public void guardarEditar() {
		if (modulo.getDescripcion() == null || modulo.getDescripcion().equals("") || modulo.getCodigo() == null
				|| modulo.getCodigo().equals("") || modulo.getAbreviacionCodigo() == null
				|| modulo.getAbreviacionCodigo().equals("")) {
			Messagebox.show("Debe llenar todos los campos");
			return;
		}

		if (modulo.getId() == null) {
			listaModulo = InstanciaDAO.getModuloDAO().getListaModulo();
			for (Modulo modulo : listaModulo) {
				if (modulo.getDescripcion().trim().equals(this.modulo.getDescripcion().trim())) {
					Messagebox.show("La Descripcion ya ha sido asignado");
					return;
				}
			}
			listaModulo.clear();
			Modulo moduloDevuelto = InstanciaDAO.getModuloDAO().saveorUpdate(modulo);
			List<Rol> listaRol = InstanciaDAO.getRolDAO().getListaRol();
			for (Rol rol : listaRol) {
				RolModulo rolModulo = new RolModulo();
				rolModulo.setRol(rol);
				rolModulo.setModulo(moduloDevuelto);
				rolModulo.setEstado(false);
				InstanciaDAO.getRolModuloDAO().saveorUpdate(rolModulo);
			}
			Clients.showNotification("Modulo ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		} else {
			InstanciaDAO.getModuloDAO().saveorUpdate(modulo);
			Clients.showNotification("Modulo Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
		listaModulo = InstanciaDAO.getModuloDAO().getListaModulo();
	}

	@Command
	@NotifyChange({ "listaModulo", "modulo","btGuardarEditar" })
	public void nuevo() {
		modulo = new Modulo();
		btGuardarEditar = Sistema.idioma.getGuardar();
	}
	
	@Command
	@NotifyChange("btGuardarEditar")
	public void select(){
		btGuardarEditar = Sistema.idioma.getEditar();
	}
	
	/*
	 * GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Modulo> getListaModulo() {
		return listaModulo;
	}

	public void setListaModulo(List<Modulo> listaModulo) {
		this.listaModulo = listaModulo;
	}

	public Modulo getModulo() {
		return modulo;
	}

	public void setModulo(Modulo modulo) {
		this.modulo = modulo;
	}

	public String getBtGuardarEditar() {
		return btGuardarEditar;
	}

	public void setBtGuardarEditar(String btGuardarEditar) {
		this.btGuardarEditar = btGuardarEditar;
	}

}
