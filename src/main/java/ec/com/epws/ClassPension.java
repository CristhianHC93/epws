package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;

import ec.com.epws.entity.Pension;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Mes;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.models.Modelos;
import ec.com.epws.sistema.Sistema;

public class ClassPension {

	private List<Modelos<Pension, List<Pension>, Object, Object>> listaPensionPropio = new ArrayList<>();
	private List<Pension> listaPension;

	private List<Curso> listaCurso;
	private Curso curso;

	private Matricula matricula;
	// private List<Estado> listaEstadoPension;
	// private List<Estado> listaEstado;
	// private Estado estado;

	// private boolean obtenMes = false;
	private List<Mes> ListaMes;
	// private Mes mes;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista) {
		Selectors.wireComponents(vista, this, false);
		listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
		// listaEstado = Sistema.getListaEstadoPension();
		ListaMes = InstanciaDAO.mesDAO.getListaMes();
		curso = listaCurso.get(0);
		// estado = listaEstado.get(0);
		// mes = ListaMes.get(0);
		// listaEstadoPension = Sistema.getListaEstadoPension();
		buscar();
	}

	@NotifyChange({ "listaPensionPropio" })
	@Command
	public void buscar() {
		listaPensionPropio.clear();
		if (matricula != null) {
			listaPension = InstanciaDAO.pensionDAO.getListaPensionByMesMatricula(1, matricula.getId(),
					Sistema.getAnioLectivoActivoEstatico().getId());
		} else {
			listaPension = InstanciaDAO.pensionDAO.getListaPensionByMesCurso(1, curso.getId(),
					Sistema.getAnioLectivoActivoEstatico().getId());
		}
		listaPension.forEach(x -> {
			List<Pension> lista = new ArrayList<>();
			lista = InstanciaDAO.pensionDAO.getListaPensionByMatricula(x.getMatricula().getId());
			listaPensionPropio.add(new Modelos<Pension, List<Pension>, Object, Object>(x, lista, null, null, true));
		});
	}

	@Command
	public void agregarAlumno() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Pension");
		parametros.put("curso", curso.getId());
		Executions.createComponents("/vista/formMatricula.zul", null, parametros);
	}

	@NotifyChange({ "alumno", "listaPensionPropio" })
	@GlobalCommand
	public void cargarAlumno(@BindingParam("objeto") Matricula matricula) {
		this.matricula = matricula;
		buscar();
	}

	@NotifyChange({ "alumno", "listaPensionPropio" })
	@Command
	public void limpiar() {
		this.matricula = null;
		buscar();
	}

	@NotifyChange("listaPension")
	@Command
	public void guardar(@BindingParam("item") Pension pension) {
		/*
		 * if (pension.getEstado().getId() != Sistema.ESTADO_CON_DEUDA) {
		 * pension.setValorAdeudado(0); }
		 */
		InstanciaDAO.pensionDAO.saveorUpdate(pension);
		Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
	}

	@NotifyChange("listaPensionPropio")
	@Command
	public void select(@BindingParam("item") Pension pension) {
		if (pension.getEstado().getId() != Sistema.ESTADO_PENSION_DEUDA) {
			// pension.setValorAdeudado(0);
		}
	}

	@NotifyChange("listaPensionPropio")
	@Command
	public void pagarCuota(@BindingParam("item") Pension pension) {
		if (pension.getValorPagado() <= 0.0) {
			pension.setValorPagado(0.0);
			pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_NO_PAGADA));
		} else {
			if (pension.getMes().getId() == 1) {
				if (pension.getValorPagado() >= pension.getValorReferencia().getValorMatricula()) {
					pension.setValorPagado(pension.getValorReferencia().getValorMatricula());
					pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_PAGADA));
				} else {
					if (pension.getValorPagado() < pension.getValorReferencia().getValorMatricula()
							&& pension.getValorPagado() > 0) {
						pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_DEUDA));
					}
				}
			}
			if (pension.getValorPagado() >= pension.getValorReferencia().getValorPension()) {
				pension.setValorPagado(pension.getValorReferencia().getValorPension());
				pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_PAGADA));
			} else {
				if (pension.getValorPagado() < pension.getValorReferencia().getValorPension()
						&& pension.getValorPagado() > 0) {
					pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_DEUDA));
				}
			}
		}
		pension = InstanciaDAO.pensionDAO.saveorUpdate(pension);
		buscar();
	}

	@NotifyChange("listaPensionPropio")
	@Command
	public void pagarCuotaPdf(@BindingParam("item") Pension pension) {
		if (pension.getValorPagado() <= 0.0) {
			pension.setValorPagado(0.0);
			pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_NO_PAGADA));
		} else {
			if (pension.getMes().getId() == 1) {
				if (pension.getValorPagado() >= pension.getValorReferencia().getValorMatricula()) {
					pension.setValorPagado(pension.getValorReferencia().getValorMatricula());
					pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_PAGADA));
				} else {
					if (pension.getValorPagado() < pension.getValorReferencia().getValorMatricula()
							&& pension.getValorPagado() > 0) {
						pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_DEUDA));
					}
				}
			}
			if (pension.getValorPagado() >= pension.getValorReferencia().getValorPension()) {
				pension.setValorPagado(pension.getValorReferencia().getValorPension());
				pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_PAGADA));
			} else {
				if (pension.getValorPagado() < pension.getValorReferencia().getValorPension()
						&& pension.getValorPagado() > 0) {
					pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_DEUDA));
				}
			}
		}
		pension = InstanciaDAO.pensionDAO.saveorUpdate(pension);
		buscar();
		if (pension.getEstado().getId() != Sistema.ESTADO_PENSION_NO_PAGADA) {
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("pension", pension);
			Executions.createComponents("/vista/formDetallePago.zul", null, parameters);
		}
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Pension> getListaPension() {
		return listaPension;
	}

	public void setListaPension(List<Pension> listaPension) {
		this.listaPension = listaPension;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}

	public List<Mes> getListaMes() {
		return ListaMes;
	}

	public void setListaMes(List<Mes> listaMes) {
		ListaMes = listaMes;
	}

	public String getConstraintPrecio() {
		return Sistema.CONSTRAINT_PRECIO;
	}

	public List<Modelos<Pension, List<Pension>, Object, Object>> getListaPensionPropio() {
		return listaPensionPropio;
	}

	public void setListaPensionPropio(List<Modelos<Pension, List<Pension>, Object, Object>> listaPensionPropio) {
		this.listaPensionPropio = listaPensionPropio;
	}

	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

}
