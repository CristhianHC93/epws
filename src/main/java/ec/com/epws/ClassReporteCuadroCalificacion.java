package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;

import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Matricula;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.models.CuadroCalificacion;
import ec.com.epws.sistema.Sistema;

public class ClassReporteCuadroCalificacion {

	private List<Curso> listaCurso;
	private Curso curso;
	private List<Asignatura> listaAsignatura;
	List<Matricula> listaMatricula;
	private List<CuadroCalificacion> listaCuadroCalificacion = new ArrayList<>();
	double promedioAnual = 0.0;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("curso") Curso curso,
			@ExecutionArgParam("lista") List<CuadroCalificacion> listaCuadroCalificacion) {
		Selectors.wireComponents(vista, this, false);
		if (listaCuadroCalificacion != null) {
			this.curso = curso;
			this.listaCuadroCalificacion = listaCuadroCalificacion;
		} else {
			listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
			this.curso = listaCurso.get(0);
			listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignaturaBasicaInicial(this.curso.getId());
			listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), this.curso.getId());
			buscar();
		}		
	}

	List<Asignatura> listaAsignaturaAlt = new ArrayList<>();

	@NotifyChange("listaCuadroCalificacion")
	@Command
	public void buscar() {
			listaCuadroCalificacion.clear();
				listaMatricula.forEach(x -> {
					List<ec.com.epws.entity.CuadroCalificacion> lista = new ArrayList<>();
					CuadroCalificacion cc= new CuadroCalificacion();
					listaAsignaturaAlt = InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(), true);
					listaAsignaturaAlt.forEach(z -> {
						ec.com.epws.entity.CuadroCalificacion cdc = InstanciaDAO.cuadroCalificacionDAO.getCuadroCalificacion(z.getId(), x.getId());
						//CertificadoPromocion calificacion = new CertificadoPromocion();
						//calificacion.setAsignatura(z.getDescripcion());
						if (cdc != null) {
							//calificacion.setQuimestre1(cdc.getQuimestre1());
							//calificacion.setQuimestre2(cdc.getQuimestre2());							
								if (cdc.getAsignatura().getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_COMPORTAMIENTO) {
									cc.setComportamiento(InstanciaDAO.cualitativaDAO.getCualitativa((int) cdc.getPromedioFinal()).getSiglas());
								}
								if (cdc.getAsignatura().getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_BASICO) {
									//calificacion.setValorNota(cdc.getPromedioFinal());
									lista.add(cdc);
								}
								if (cdc.getAsignatura().getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_MATERIA) {
									cc.setClubes(InstanciaDAO.cualitativaDAO.getCualitativa((int) cdc.getPromedioFinal()).getSiglas());
									
								}
								if (cdc.getAsignatura().getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_INICIAL) {
									cc.setClubes(InstanciaDAO.cualitativaDAO.getCualitativa((int) cdc.getPromedioFinal()).getSiglas());
									
								}
						}else {
							lista.add(new ec.com.epws.entity.CuadroCalificacion());
						}

					});
					promedioAnual = 0.0;
					lista.forEach(c -> {
						promedioAnual += c.getPromedioFinal();
					});
					promedioAnual = Sistema.redondear(promedioAnual / lista.size());
					cc.setListaCalificacion(lista);
					cc.setAlumno(x.getAlumno().getApellido()+" "+x.getAlumno().getNombre());
					cc.setPromedioAnual(promedioAnual);
					if (promedioAnual >= 7) {
						cc.setObservacion("APROBADO");
					}else {
						cc.setObservacion("REPROBADO");
					}
					listaCuadroCalificacion.add(cc);
				});
	}
	
	@NotifyChange({"asignatura","listaAsignatura","listaMatricula","listaCuadroCalificacion" })
	@Command
	public void selectCurso(){
		listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignaturaBasicaInicial(curso.getId());
		listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), this.curso.getId());
		buscar();
	}
	

	@Command
	public void generarExcel() {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("curso", curso.getId());
		Sistema.reporteEXCEL("cuadro_calificacion.jasper", "Informe Quimestre", mapreporte,
				null,Sistema.Conexion(),true);
	}
/*
	@Command
	public void generarPdf() {
		if (listaCuadroCalificacion.size() == 0) {
			Messagebox.show(Sistema.idioma.getNoExisteInformacion());
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaCuadroCalificacion);
		parametros.put("asignatura", asignatura);
		parametros.put("quimestre", quimestre);
		Executions.createComponents("/reporte/impresionQuimestreAsignatura.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), asignatura.getId());
		List<String> lista = new ArrayList<>();
		InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId()).forEach(x -> lista.add(x.getDescripcion()));
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("lista", lista);
		mapreporte.put("curso", asignatura.getCurso().getDescripcion());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		mapreporte.put("asignatura", asignatura.getDescripcion());
		mapreporte.put("quimestre",quimestre.getDescripcion());
		mapreporte.put("docente", ad.getDocente().getUsuario().getNombre()+" "+ad.getDocente().getUsuario().getApellido());
		iframe.setContent(Sistema.reportePDF(Sistema.QUIMESTRE_ASIGNATURA_JASPER, mapreporte,
				Sistema.getInformeCalificacionQuimestreDataSource(listaCuadroCalificacion)));
	}
	
	/*
	 * 
	 */
	
	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<CuadroCalificacion> getListaCuadroCalificacion() {
		return listaCuadroCalificacion;
	}

	public void setListaCuadroCalificacion(List<CuadroCalificacion> listaCuadroCalificacion) {
		this.listaCuadroCalificacion = listaCuadroCalificacion;
	}

	public List<Asignatura> getListaAsignatura() {
		return listaAsignatura;
	}

	public void setListaAsignatura(List<Asignatura> listaAsignatura) {
		this.listaAsignatura = listaAsignatura;
	}
	
	
}
