package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Parcial;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassParcial {
	@Wire("#winParcial")
	private Window winParcial;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Parcial> listaParcial;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaParcial = InstanciaDAO.parcialDAO.getListaParcial();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formParcialCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaParcial")
	public void eliminar(@BindingParam("parcial") final Parcial parcial) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + parcial.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.parcialDAO.eliminar(parcial.getId());
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaParcial.clear();
							listaParcial = InstanciaDAO.parcialDAO.getListaParcial();
							BindUtils.postNotifyChange(null, null, ClassParcial.this, "listaParcial");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassParcial.this, "listaParcial");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("parcial") Parcial parcial) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", parcial);
		BindUtils.postGlobalCommand(null, null, "cargarParcial", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaParcial")
	public void refrescarlista() {
		listaParcial = InstanciaDAO.parcialDAO.getListaParcial();
	}

	@Command
	public void editar(@BindingParam("parcial") Parcial parcial) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", parcial);
		Executions.createComponents("/vista/formParcialCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaParcial")
	public void filtrar() {
		listaParcial.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaParcial.addAll(InstanciaDAO.parcialDAO.getListaParcial());
		} else {
			for (Parcial item : InstanciaDAO.parcialDAO.getListaParcial()) {
				if (item.getDescripcion().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
						|| item.getQuimestre().getDescripcion().trim().toLowerCase()
								.indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaParcial.add(item);
				}
			}
		}
	}

	@Command
	public void salir() {
		winParcial.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Parcial> getListaParcial() {
		return listaParcial;
	}

	public void setListaParcial(List<Parcial> listaParcial) {
		this.listaParcial = listaParcial;
	}
}
