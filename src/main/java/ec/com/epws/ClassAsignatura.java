package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.Curso;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassAsignatura {
	@Wire("#winAsignatura")
	private Window winAsignatura;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Asignatura> listaAsignatura;
	private List<Curso> listaCursoBuscar;
	private Curso curso;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaCursoBuscar = InstanciaDAO.getCursoDAO().getListaCurso();
		curso = listaCursoBuscar.get(0);
		listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(),true);
	}

	@NotifyChange("listaAsignatura")
	@Command
	public void select() {
		listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(),true);
	}

	@Command
	public void nuevo() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("curso", curso);
		Executions.createComponents("/vista/formAsignaturaCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaAsignatura")
	public void eliminar(@BindingParam("asignatura") final Asignatura asignatura) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + asignatura.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.asignaturaDAO.eliminar(asignatura);
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaAsignatura.clear();
							listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(),true);
							BindUtils.postNotifyChange(null, null, ClassAsignatura.this, "listaAsignatura");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassAsignatura.this, "listaAsignatura");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("asignatura") Asignatura asignatura) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", asignatura);
		BindUtils.postGlobalCommand(null, null, "cargarAsignatura", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaAsignatura")
	public void refrescarlista() {
		listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(),true);
	}

	@Command
	public void editar(@BindingParam("asignatura") Asignatura asignatura) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", asignatura);
		Executions.createComponents("/vista/formAsignaturaCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaAsignatura")
	public void filtrar() {
		listaAsignatura.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaAsignatura.addAll(InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(),true));
		} else {
			for (Asignatura item : InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(),true)) {
				if (item.getDescripcion().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
						|| item.getCurso().getDescripcion().trim().toLowerCase()
								.indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaAsignatura.add(item);
				}
			}
		}
	}

	@Command
	public void salir() {
		winAsignatura.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public Window getWinAsignatura() {
		return winAsignatura;
	}

	public void setWinAsignatura(Window winAsignatura) {
		this.winAsignatura = winAsignatura;
	}

	public List<Asignatura> getListaAsignatura() {
		return listaAsignatura;
	}

	public void setListaAsignatura(List<Asignatura> listaAsignatura) {
		this.listaAsignatura = listaAsignatura;
	}

	public List<Curso> getListaCursoBuscar() {
		return listaCursoBuscar;
	}

	public void setListaCursoBuscar(List<Curso> listaCursoBuscar) {
		this.listaCursoBuscar = listaCursoBuscar;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

}
