package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Alumno;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassInscripcion {
	@Wire("#winInscripcion")
	private Window winInscripcion;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private boolean inscripcion;
	private Alumno alumno;

	private List<Alumno> listaAlumno = new ArrayList<>();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase,
			@ExecutionArgParam("alumno") Alumno alumno) {
		Selectors.wireComponents(view, this, false);
		if (clase == null) {
			clase = "this";
		}

		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
			inscripcion = false;
		}
		if (clase.equals("Matricula")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "93%";
			inscripcion = false;
		}
		if (clase.equals("Inscripcion")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "93%";
			inscripcion = true;
		}
		this.alumno = alumno;
		listaAlumno = (inscripcion) ? InstanciaDAO.getAlumnoDAO().getListaAlumno(true)
				: InstanciaDAO.getAlumnoDAO().getListaAlumno();
		listaAlumno.forEach(x -> {
			x.getAlumnoFamilias()
					.add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_PADRE));
			x.getAlumnoFamilias()
					.add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_MADRE));
			x.getAlumnoFamilias().add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId()));
		});
	}

	@Command
	public void nuevo() {
		HashMap<String, String> mimap = new HashMap<String, String>();
		mimap.put("clase", "inscripcion");
		Executions.createComponents("/vista/formAlumnoCE.zul", null, mimap);
	}

	@Command
	@NotifyChange("listaAlumno")
	public void eliminar(@BindingParam("alumno") final Alumno alumno) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + alumno.getNombre() + " "
				+ alumno.getApellido() + "\" esta accion no podra deshacerse si se confirma";

		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						alumno.setEstado(Sistema.getEstado(Sistema.ESTADO_ELIMINADO));
						InstanciaDAO.getAlumnoDAO().saveorUpdate(alumno);
						Clients.showNotification("Registro eliminado satisfactoriamente",
								Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
						BindUtils.postNotifyChange(null, null, ClassInscripcion.this, "listaAlumno");
						listaAlumno.clear();
						listaAlumno = (inscripcion) ? InstanciaDAO.getAlumnoDAO().getListaAlumno(true)
								: InstanciaDAO.getAlumnoDAO().getListaAlumno();
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("alumno") Alumno alumno) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", alumno);
		BindUtils.postGlobalCommand(null, null, "cargarAlumno", parametros);
		salir();
	}

	@Command
	@NotifyChange("alumno")
	public void editar(@BindingParam("alumno") Alumno alumno) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", alumno);
		parametros.put("clase", "inscripcion");
		Executions.createComponents("/vista/formAlumnoCE.zul", null, parametros);
	}

	@Command
	@NotifyChange("listaAlumno")
	public void filtrar() {
		listaAlumno.clear();
		if (datoBusqueda.trim().equals("")) {
			if (inscripcion) {
				listaAlumno = InstanciaDAO.getAlumnoDAO().getListaAlumno(inscripcion);
				listaAlumno.forEach(x -> {
					x.getAlumnoFamilias().add(
							InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_PADRE));
					x.getAlumnoFamilias().add(
							InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_MADRE));
					x.getAlumnoFamilias().add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId()));
				});
			} else {
				listaAlumno = InstanciaDAO.getAlumnoDAO().getListaAlumno();
				listaAlumno.forEach(x -> {
					x.getAlumnoFamilias().add(
							InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_PADRE));
					x.getAlumnoFamilias().add(
							InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_MADRE));
					x.getAlumnoFamilias().add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId()));
				});
			}
		} else {
			if (inscripcion) {
				listaAlumno = InstanciaDAO.getAlumnoDAO().getListaAlumnoFiltroInscripcion(datoBusqueda.toUpperCase());
				listaAlumno.forEach(x -> {
					x.getAlumnoFamilias().add(
							InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_PADRE));
					x.getAlumnoFamilias().add(
							InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_MADRE));
					x.getAlumnoFamilias().add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId()));
				});
			} else {
				listaAlumno = InstanciaDAO.getAlumnoDAO().getListaAlumnoFiltro(datoBusqueda.toUpperCase());
				listaAlumno.forEach(x -> {
					x.getAlumnoFamilias().add(
							InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_PADRE));
					x.getAlumnoFamilias().add(
							InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_MADRE));
					x.getAlumnoFamilias().add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId()));
				});
			}
		}
	}

	@GlobalCommand
	@NotifyChange("listaAlumno")
	public void refrescarlistaInscritos() {
		listaAlumno = (inscripcion) ? InstanciaDAO.getAlumnoDAO().getListaAlumno(true)
				: InstanciaDAO.getAlumnoDAO().getListaAlumno();
		listaAlumno.forEach(x -> {
			x.getAlumnoFamilias()
					.add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_PADRE));
			x.getAlumnoFamilias()
					.add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId(), Sistema.PARENTESCO_MADRE));
			x.getAlumnoFamilias().add(InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(x.getId()));
		});
	}

	@NotifyChange("listaAlumno")
	@Command
	public void inscripcion(@BindingParam("item") Alumno alumno) {
		InstanciaDAO.getAlumnoDAO().saveorUpdate(alumno);
	}

	@Command
	public void generarExcel(@BindingParam("alumno") Alumno alumno) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("ficha", "FICHA ESTUDIANTIL");
		mapreporte.put("cedula", alumno.getCedula());
		mapreporte.put("nombre", alumno.getNombre());
		mapreporte.put("apellido", alumno.getApellido());
		mapreporte.put("padre", alumno.getAlumnoFamilias().get(0).getFamilia().getNombre() + " "
				+ alumno.getAlumnoFamilias().get(0).getFamilia().getApellido());
		mapreporte.put("madre", alumno.getAlumnoFamilias().get(1).getFamilia().getNombre() + " "
				+ alumno.getAlumnoFamilias().get(1).getFamilia().getApellido());
		mapreporte.put("representante", alumno.getAlumnoFamilias().get(2).getFamilia().getNombre() + " "
				+ alumno.getAlumnoFamilias().get(2).getFamilia().getApellido());
		mapreporte.put("lugar_nacimiento", alumno.getLugarNacimiento());
		mapreporte.put("fecha_nacimiento", alumno.getFechaNacimiento());
		mapreporte.put("direccion", alumno.getDomicilio());
		mapreporte.put("telefono", alumno.getAlumnoFamilias().get(2).getFamilia().getTelefono());
		mapreporte.put("escuela_anterior", alumno.getEscuelaAnterior());
		mapreporte.put("observacion",
				(alumno.getObservacionMedica() != null) ? alumno.getObservacionMedica().getObservacion() : "");
		mapreporte.put("foto", (alumno.getFoto() == null || alumno.getFoto().equals("")
				? Util.getPathRaiz() + "/jasperReport/sin_foto.png" : Util.getPathRaiz() + alumno.getFoto()));
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		Sistema.reporteEXCEL(Sistema.FICHA_ESTUDIANTE, "ficha Estudiante", mapreporte, null,null, false);
	}

	@Command
	public void generarPdf(@BindingParam("alumno") Alumno alumno) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("alumno", alumno);
		Executions.createComponents("/reporte/impresionFichaEstudiante.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		// List<String> lista = new ArrayList<>();
		// listaCalificacionParcial.forEach(x ->
		// lista.add(x.getCalificacionQuimestre().getAsignatura().getDescripcion()));

		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("ficha", "FICHA ESTUDIANTIL");
		mapreporte.put("cedula", alumno.getCedula());
		mapreporte.put("nombre", alumno.getNombre());
		mapreporte.put("apellido", alumno.getApellido());
		mapreporte.put("padre", alumno.getAlumnoFamilias().get(0).getFamilia().getNombre() + " "
				+ alumno.getAlumnoFamilias().get(0).getFamilia().getApellido());
		mapreporte.put("madre", alumno.getAlumnoFamilias().get(1).getFamilia().getNombre() + " "
				+ alumno.getAlumnoFamilias().get(1).getFamilia().getApellido());
		mapreporte.put("representante", alumno.getAlumnoFamilias().get(2).getFamilia().getNombre() + " "
				+ alumno.getAlumnoFamilias().get(2).getFamilia().getApellido());
		mapreporte.put("lugar_nacimiento", alumno.getLugarNacimiento());
		mapreporte.put("fecha_nacimiento", alumno.getFechaNacimiento());
		mapreporte.put("direccion", alumno.getDomicilio());
		mapreporte.put("telefono", alumno.getAlumnoFamilias().get(2).getFamilia().getTelefono());
		mapreporte.put("escuela_anterior", alumno.getEscuelaAnterior());
		mapreporte.put("observacion",
				(alumno.getObservacionMedica() != null) ? alumno.getObservacionMedica().getObservacion() : "");
		mapreporte.put("foto", (alumno.getFoto() == null || alumno.getFoto().equals("")
				? Util.getPathRaiz() + "/jasperReport/sin_foto.png" : Util.getPathRaiz() + alumno.getFoto()));
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		iframe.setContent(Sistema.reportePDF(Sistema.FICHA_ESTUDIANTE, mapreporte, null));
	}

	@Command
	public void salir() {
		winInscripcion.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Alumno> getListaAlumno() {
		return listaAlumno;
	}

	public void setListaAlumno(List<Alumno> listaAlumno) {
		this.listaAlumno = listaAlumno;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

}
