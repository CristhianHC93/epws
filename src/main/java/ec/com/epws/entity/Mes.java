package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the mes database table.
 * 
 */
@Entity
@NamedQuery(name="Mes.findAll", query="SELECT m FROM Mes m")
public class Mes implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to Pension
	@OneToMany(mappedBy="mes")
	private List<Pension> pensions;

	public Mes() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Pension> getPensions() {
		return this.pensions;
	}

	public void setPensions(List<Pension> pensions) {
		this.pensions = pensions;
	}

	public Pension addPension(Pension pension) {
		getPensions().add(pension);
		pension.setMes(this);

		return pension;
	}

	public Pension removePension(Pension pension) {
		getPensions().remove(pension);
		pension.setMes(null);

		return pension;
	}

}