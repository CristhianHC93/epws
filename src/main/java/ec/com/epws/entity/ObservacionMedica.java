package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the observacion_medica database table.
 * 
 */
@Entity
@Table(name="observacion_medica")
@NamedQuery(name="ObservacionMedica.findAll", query="SELECT o FROM ObservacionMedica o")
public class ObservacionMedica implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="certificado_medico")
	private String certificadoMedico;

	private Integer discapacidad;

	private Integer enfermedad;

	private String observacion;

	private Integer vacuna;

	//bi-directional many-to-one association to Alumno
	@OneToMany(mappedBy="observacionMedica")
	private List<Alumno> alumnos;

	//bi-directional many-to-one association to MedicaDiscapacidad
	@OneToMany(mappedBy="observacionMedica")
	private List<MedicaDiscapacidad> medicaDiscapacidads;

	//bi-directional many-to-one association to MedicaEnfermedad
	@OneToMany(mappedBy="observacionMedica")
	private List<MedicaEnfermedad> medicaEnfermedads;

	//bi-directional many-to-one association to MedicaVacuna
	@OneToMany(mappedBy="observacionMedica")
	private List<MedicaVacuna> medicaVacunas;

	public ObservacionMedica() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCertificadoMedico() {
		return this.certificadoMedico;
	}

	public void setCertificadoMedico(String certificadoMedico) {
		this.certificadoMedico = certificadoMedico;
	}

	public Integer getDiscapacidad() {
		return this.discapacidad;
	}

	public void setDiscapacidad(Integer discapacidad) {
		this.discapacidad = discapacidad;
	}

	public Integer getEnfermedad() {
		return this.enfermedad;
	}

	public void setEnfermedad(Integer enfermedad) {
		this.enfermedad = enfermedad;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Integer getVacuna() {
		return this.vacuna;
	}

	public void setVacuna(Integer vacuna) {
		this.vacuna = vacuna;
	}

	public List<Alumno> getAlumnos() {
		return this.alumnos;
	}

	public void setAlumnos(List<Alumno> alumnos) {
		this.alumnos = alumnos;
	}

	public Alumno addAlumno(Alumno alumno) {
		getAlumnos().add(alumno);
		alumno.setObservacionMedica(this);

		return alumno;
	}

	public Alumno removeAlumno(Alumno alumno) {
		getAlumnos().remove(alumno);
		alumno.setObservacionMedica(null);

		return alumno;
	}

	public List<MedicaDiscapacidad> getMedicaDiscapacidads() {
		return this.medicaDiscapacidads;
	}

	public void setMedicaDiscapacidads(List<MedicaDiscapacidad> medicaDiscapacidads) {
		this.medicaDiscapacidads = medicaDiscapacidads;
	}

	public MedicaDiscapacidad addMedicaDiscapacidad(MedicaDiscapacidad medicaDiscapacidad) {
		getMedicaDiscapacidads().add(medicaDiscapacidad);
		medicaDiscapacidad.setObservacionMedica(this);

		return medicaDiscapacidad;
	}

	public MedicaDiscapacidad removeMedicaDiscapacidad(MedicaDiscapacidad medicaDiscapacidad) {
		getMedicaDiscapacidads().remove(medicaDiscapacidad);
		medicaDiscapacidad.setObservacionMedica(null);

		return medicaDiscapacidad;
	}

	public List<MedicaEnfermedad> getMedicaEnfermedads() {
		return this.medicaEnfermedads;
	}

	public void setMedicaEnfermedads(List<MedicaEnfermedad> medicaEnfermedads) {
		this.medicaEnfermedads = medicaEnfermedads;
	}

	public MedicaEnfermedad addMedicaEnfermedad(MedicaEnfermedad medicaEnfermedad) {
		getMedicaEnfermedads().add(medicaEnfermedad);
		medicaEnfermedad.setObservacionMedica(this);

		return medicaEnfermedad;
	}

	public MedicaEnfermedad removeMedicaEnfermedad(MedicaEnfermedad medicaEnfermedad) {
		getMedicaEnfermedads().remove(medicaEnfermedad);
		medicaEnfermedad.setObservacionMedica(null);

		return medicaEnfermedad;
	}

	public List<MedicaVacuna> getMedicaVacunas() {
		return this.medicaVacunas;
	}

	public void setMedicaVacunas(List<MedicaVacuna> medicaVacunas) {
		this.medicaVacunas = medicaVacunas;
	}

	public MedicaVacuna addMedicaVacuna(MedicaVacuna medicaVacuna) {
		getMedicaVacunas().add(medicaVacuna);
		medicaVacuna.setObservacionMedica(this);

		return medicaVacuna;
	}

	public MedicaVacuna removeMedicaVacuna(MedicaVacuna medicaVacuna) {
		getMedicaVacunas().remove(medicaVacuna);
		medicaVacuna.setObservacionMedica(null);

		return medicaVacuna;
	}

}