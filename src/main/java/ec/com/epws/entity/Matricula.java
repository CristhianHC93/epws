package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Timestamp;
import java.util.List;


/**
 * The persistent class for the matricula database table.
 * 
 */
@Entity
@NamedQuery(name="Matricula.findAll", query="SELECT m FROM Matricula m")
public class Matricula implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private Timestamp fecha;

	private String numero;

	private String observacion;

	//bi-directional many-to-one association to CalificacionQuimestre
	@OneToMany(mappedBy="matricula")
	private List<CalificacionQuimestre> calificacionQuimestres;

	//bi-directional many-to-one association to CalificacionQuimestreCualitativa
	@OneToMany(mappedBy="matricula")
	private List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas;

	//bi-directional many-to-one association to Alumno
	@ManyToOne
	@JoinColumn(name="id_alumno")
	private Alumno alumno;

	//bi-directional many-to-one association to AnioLectivo
	@ManyToOne
	@JoinColumn(name="id_aniolectivo")
	private AnioLectivo anioLectivo;

	//bi-directional many-to-one association to Curso
	@ManyToOne
	@JoinColumn(name="id_curso")
	private Curso curso;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="id_estado")
	private Estado estado;

	//bi-directional many-to-one association to Pension
	@OneToMany(mappedBy="matricula")
	private List<Pension> pensions;

	//bi-directional many-to-one association to CuadroCalificacion
	@OneToMany(mappedBy="matricula")
	private List<CuadroCalificacion> cuadroCalificacions;

	public Matricula() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Timestamp getFecha() {
		return this.fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}

	public String getNumero() {
		return this.numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public List<CalificacionQuimestre> getCalificacionQuimestres() {
		return this.calificacionQuimestres;
	}

	public void setCalificacionQuimestres(List<CalificacionQuimestre> calificacionQuimestres) {
		this.calificacionQuimestres = calificacionQuimestres;
	}

	public CalificacionQuimestre addCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		getCalificacionQuimestres().add(calificacionQuimestre);
		calificacionQuimestre.setMatricula(this);

		return calificacionQuimestre;
	}

	public CalificacionQuimestre removeCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		getCalificacionQuimestres().remove(calificacionQuimestre);
		calificacionQuimestre.setMatricula(null);

		return calificacionQuimestre;
	}

	public List<CalificacionQuimestreCualitativa> getCalificacionQuimestreCualitativas() {
		return this.calificacionQuimestreCualitativas;
	}

	public void setCalificacionQuimestreCualitativas(List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas) {
		this.calificacionQuimestreCualitativas = calificacionQuimestreCualitativas;
	}

	public CalificacionQuimestreCualitativa addCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		getCalificacionQuimestreCualitativas().add(calificacionQuimestreCualitativa);
		calificacionQuimestreCualitativa.setMatricula(this);

		return calificacionQuimestreCualitativa;
	}

	public CalificacionQuimestreCualitativa removeCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		getCalificacionQuimestreCualitativas().remove(calificacionQuimestreCualitativa);
		calificacionQuimestreCualitativa.setMatricula(null);

		return calificacionQuimestreCualitativa;
	}

	public Alumno getAlumno() {
		return this.alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public AnioLectivo getAnioLectivo() {
		return this.anioLectivo;
	}

	public void setAnioLectivo(AnioLectivo anioLectivo) {
		this.anioLectivo = anioLectivo;
	}

	public Curso getCurso() {
		return this.curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Pension> getPensions() {
		return this.pensions;
	}

	public void setPensions(List<Pension> pensions) {
		this.pensions = pensions;
	}

	public Pension addPension(Pension pension) {
		getPensions().add(pension);
		pension.setMatricula(this);

		return pension;
	}

	public Pension removePension(Pension pension) {
		getPensions().remove(pension);
		pension.setMatricula(null);

		return pension;
	}

	public List<CuadroCalificacion> getCuadroCalificacions() {
		return this.cuadroCalificacions;
	}

	public void setCuadroCalificacions(List<CuadroCalificacion> cuadroCalificacions) {
		this.cuadroCalificacions = cuadroCalificacions;
	}

	public CuadroCalificacion addCuadroCalificacion(CuadroCalificacion cuadroCalificacion) {
		getCuadroCalificacions().add(cuadroCalificacion);
		cuadroCalificacion.setMatricula(this);

		return cuadroCalificacion;
	}

	public CuadroCalificacion removeCuadroCalificacion(CuadroCalificacion cuadroCalificacion) {
		getCuadroCalificacions().remove(cuadroCalificacion);
		cuadroCalificacion.setMatricula(null);

		return cuadroCalificacion;
	}

}