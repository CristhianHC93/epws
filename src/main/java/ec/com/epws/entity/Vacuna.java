package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the vacuna database table.
 * 
 */
@Entity
@NamedQuery(name="Vacuna.findAll", query="SELECT v FROM Vacuna v")
public class Vacuna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to MedicaVacuna
	@OneToMany(mappedBy="vacuna")
	private List<MedicaVacuna> medicaVacunas;

	public Vacuna() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<MedicaVacuna> getMedicaVacunas() {
		return this.medicaVacunas;
	}

	public void setMedicaVacunas(List<MedicaVacuna> medicaVacunas) {
		this.medicaVacunas = medicaVacunas;
	}

	public MedicaVacuna addMedicaVacuna(MedicaVacuna medicaVacuna) {
		getMedicaVacunas().add(medicaVacuna);
		medicaVacuna.setVacuna(this);

		return medicaVacuna;
	}

	public MedicaVacuna removeMedicaVacuna(MedicaVacuna medicaVacuna) {
		getMedicaVacunas().remove(medicaVacuna);
		medicaVacuna.setVacuna(null);

		return medicaVacuna;
	}

}