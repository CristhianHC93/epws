package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the calificacion_parcial_cualitativa database table.
 * 
 */
@Entity
@Table(name="calificacion_parcial_cualitativa")
@NamedQuery(name="CalificacionParcialCualitativa.findAll", query="SELECT c FROM CalificacionParcialCualitativa c")
public class CalificacionParcialCualitativa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to CalificacionQuimestreCualitativa
	@ManyToOne
	@JoinColumn(name="id_calificacion_quimestre_cualitativa")
	private CalificacionQuimestreCualitativa calificacionQuimestreCualitativa;

	//bi-directional many-to-one association to Cualitativa
	@ManyToOne
	@JoinColumn(name="id_cualitativa")
	private Cualitativa cualitativa;

	//bi-directional many-to-one association to Observacion
	@ManyToOne
	@JoinColumn(name="id_observacion")
	private Observacion observacion;

	//bi-directional many-to-one association to Parcial
	@ManyToOne
	@JoinColumn(name="id_parcial")
	private Parcial parcial;

	public CalificacionParcialCualitativa() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public CalificacionQuimestreCualitativa getCalificacionQuimestreCualitativa() {
		return this.calificacionQuimestreCualitativa;
	}

	public void setCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		this.calificacionQuimestreCualitativa = calificacionQuimestreCualitativa;
	}

	public Cualitativa getCualitativa() {
		return this.cualitativa;
	}

	public void setCualitativa(Cualitativa cualitativa) {
		this.cualitativa = cualitativa;
	}

	public Observacion getObservacion() {
		return this.observacion;
	}

	public void setObservacion(Observacion observacion) {
		this.observacion = observacion;
	}

	public Parcial getParcial() {
		return this.parcial;
	}

	public void setParcial(Parcial parcial) {
		this.parcial = parcial;
	}

}