package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the medica_discapacidad database table.
 * 
 */
@Entity
@Table(name="medica_discapacidad")
@NamedQuery(name="MedicaDiscapacidad.findAll", query="SELECT m FROM MedicaDiscapacidad m")
public class MedicaDiscapacidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to Discapacidad
	@ManyToOne
	@JoinColumn(name="id_discapacidad")
	private Discapacidad discapacidad;

	//bi-directional many-to-one association to ObservacionMedica
	@ManyToOne
	@JoinColumn(name="id_observacion_medica")
	private ObservacionMedica observacionMedica;

	public MedicaDiscapacidad() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Discapacidad getDiscapacidad() {
		return this.discapacidad;
	}

	public void setDiscapacidad(Discapacidad discapacidad) {
		this.discapacidad = discapacidad;
	}

	public ObservacionMedica getObservacionMedica() {
		return this.observacionMedica;
	}

	public void setObservacionMedica(ObservacionMedica observacionMedica) {
		this.observacionMedica = observacionMedica;
	}

}