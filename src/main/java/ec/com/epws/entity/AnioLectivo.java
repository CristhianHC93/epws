package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the anio_lectivo database table.
 * 
 */
@Entity
@Table(name="anio_lectivo")
@NamedQuery(name="AnioLectivo.findAll", query="SELECT a FROM AnioLectivo a")
public class AnioLectivo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	private Boolean estado;

	//bi-directional many-to-one association to AsignacionDocente
	@OneToMany(mappedBy="anioLectivo")
	private List<AsignacionDocente> asignacionDocentes;

	//bi-directional many-to-one association to AsignacionTutor
	@OneToMany(mappedBy="anioLectivo")
	private List<AsignacionTutor> asignacionTutors;

	//bi-directional many-to-one association to Asistencia
	@OneToMany(mappedBy="anioLectivo")
	private List<Asistencia> asistencias;

	//bi-directional many-to-one association to Matricula
	@OneToMany(mappedBy="anioLectivo")
	private List<Matricula> matriculas;

	public AnioLectivo() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEstado() {
		return this.estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public List<AsignacionDocente> getAsignacionDocentes() {
		return this.asignacionDocentes;
	}

	public void setAsignacionDocentes(List<AsignacionDocente> asignacionDocentes) {
		this.asignacionDocentes = asignacionDocentes;
	}

	public AsignacionDocente addAsignacionDocente(AsignacionDocente asignacionDocente) {
		getAsignacionDocentes().add(asignacionDocente);
		asignacionDocente.setAnioLectivo(this);

		return asignacionDocente;
	}

	public AsignacionDocente removeAsignacionDocente(AsignacionDocente asignacionDocente) {
		getAsignacionDocentes().remove(asignacionDocente);
		asignacionDocente.setAnioLectivo(null);

		return asignacionDocente;
	}

	public List<AsignacionTutor> getAsignacionTutors() {
		return this.asignacionTutors;
	}

	public void setAsignacionTutors(List<AsignacionTutor> asignacionTutors) {
		this.asignacionTutors = asignacionTutors;
	}

	public AsignacionTutor addAsignacionTutor(AsignacionTutor asignacionTutor) {
		getAsignacionTutors().add(asignacionTutor);
		asignacionTutor.setAnioLectivo(this);

		return asignacionTutor;
	}

	public AsignacionTutor removeAsignacionTutor(AsignacionTutor asignacionTutor) {
		getAsignacionTutors().remove(asignacionTutor);
		asignacionTutor.setAnioLectivo(null);

		return asignacionTutor;
	}

	public List<Asistencia> getAsistencias() {
		return this.asistencias;
	}

	public void setAsistencias(List<Asistencia> asistencias) {
		this.asistencias = asistencias;
	}

	public Asistencia addAsistencia(Asistencia asistencia) {
		getAsistencias().add(asistencia);
		asistencia.setAnioLectivo(this);

		return asistencia;
	}

	public Asistencia removeAsistencia(Asistencia asistencia) {
		getAsistencias().remove(asistencia);
		asistencia.setAnioLectivo(null);

		return asistencia;
	}

	public List<Matricula> getMatriculas() {
		return this.matriculas;
	}

	public void setMatriculas(List<Matricula> matriculas) {
		this.matriculas = matriculas;
	}

	public Matricula addMatricula(Matricula matricula) {
		getMatriculas().add(matricula);
		matricula.setAnioLectivo(this);

		return matricula;
	}

	public Matricula removeMatricula(Matricula matricula) {
		getMatriculas().remove(matricula);
		matricula.setAnioLectivo(null);

		return matricula;
	}

}