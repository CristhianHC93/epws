package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the curso database table.
 * 
 */
@Entity
@NamedQuery(name="Curso.findAll", query="SELECT c FROM Curso c")
public class Curso implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String abreviado;

	private String descripcion;

	//bi-directional many-to-one association to AsignacionTutor
	@OneToMany(mappedBy="curso")
	private List<AsignacionTutor> asignacionTutors;

	//bi-directional many-to-one association to Asignatura
	@OneToMany(mappedBy="curso")
	private List<Asignatura> asignaturas;

	//bi-directional many-to-one association to Asistencia
	@OneToMany(mappedBy="curso")
	private List<Asistencia> asistencias;

	//bi-directional many-to-one association to Nivel
	@ManyToOne
	@JoinColumn(name="id_nivel")
	private Nivel nivel;

	//bi-directional many-to-one association to Matricula
	@OneToMany(mappedBy="curso")
	private List<Matricula> matriculas;

	public Curso() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAbreviado() {
		return this.abreviado;
	}

	public void setAbreviado(String abreviado) {
		this.abreviado = abreviado;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<AsignacionTutor> getAsignacionTutors() {
		return this.asignacionTutors;
	}

	public void setAsignacionTutors(List<AsignacionTutor> asignacionTutors) {
		this.asignacionTutors = asignacionTutors;
	}

	public AsignacionTutor addAsignacionTutor(AsignacionTutor asignacionTutor) {
		getAsignacionTutors().add(asignacionTutor);
		asignacionTutor.setCurso(this);

		return asignacionTutor;
	}

	public AsignacionTutor removeAsignacionTutor(AsignacionTutor asignacionTutor) {
		getAsignacionTutors().remove(asignacionTutor);
		asignacionTutor.setCurso(null);

		return asignacionTutor;
	}

	public List<Asignatura> getAsignaturas() {
		return this.asignaturas;
	}

	public void setAsignaturas(List<Asignatura> asignaturas) {
		this.asignaturas = asignaturas;
	}

	public Asignatura addAsignatura(Asignatura asignatura) {
		getAsignaturas().add(asignatura);
		asignatura.setCurso(this);

		return asignatura;
	}

	public Asignatura removeAsignatura(Asignatura asignatura) {
		getAsignaturas().remove(asignatura);
		asignatura.setCurso(null);

		return asignatura;
	}

	public List<Asistencia> getAsistencias() {
		return this.asistencias;
	}

	public void setAsistencias(List<Asistencia> asistencias) {
		this.asistencias = asistencias;
	}

	public Asistencia addAsistencia(Asistencia asistencia) {
		getAsistencias().add(asistencia);
		asistencia.setCurso(this);

		return asistencia;
	}

	public Asistencia removeAsistencia(Asistencia asistencia) {
		getAsistencias().remove(asistencia);
		asistencia.setCurso(null);

		return asistencia;
	}

	public Nivel getNivel() {
		return this.nivel;
	}

	public void setNivel(Nivel nivel) {
		this.nivel = nivel;
	}

	public List<Matricula> getMatriculas() {
		return this.matriculas;
	}

	public void setMatriculas(List<Matricula> matriculas) {
		this.matriculas = matriculas;
	}

	public Matricula addMatricula(Matricula matricula) {
		getMatriculas().add(matricula);
		matricula.setCurso(this);

		return matricula;
	}

	public Matricula removeMatricula(Matricula matricula) {
		getMatriculas().remove(matricula);
		matricula.setCurso(null);

		return matricula;
	}

}