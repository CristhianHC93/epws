package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the calificacion_quimestre_cualitativa database table.
 * 
 */
@Entity
@Table(name="calificacion_quimestre_cualitativa")
@NamedQuery(name="CalificacionQuimestreCualitativa.findAll", query="SELECT c FROM CalificacionQuimestreCualitativa c")
public class CalificacionQuimestreCualitativa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to CalificacionParcialCualitativa
	@OneToMany(mappedBy="calificacionQuimestreCualitativa")
	private List<CalificacionParcialCualitativa> calificacionParcialCualitativas;

	//bi-directional many-to-one association to Asignatura
	@ManyToOne
	@JoinColumn(name="id_asignatura")
	private Asignatura asignatura;

	//bi-directional many-to-one association to Cualitativa
	@ManyToOne
	@JoinColumn(name="id_cualitativa")
	private Cualitativa cualitativa1;

	//bi-directional many-to-one association to Cualitativa
	@ManyToOne
	@JoinColumn(name="nota_quimestral")
	private Cualitativa cualitativa2;

	//bi-directional many-to-one association to Matricula
	@ManyToOne
	@JoinColumn(name="id_matricula")
	private Matricula matricula;

	//bi-directional many-to-one association to Observacion
	@ManyToOne
	@JoinColumn(name="id_observacion")
	private Observacion observacion;

	//bi-directional many-to-one association to Quimestre
	@ManyToOne
	@JoinColumn(name="id_quimestre")
	private Quimestre quimestre;

	public CalificacionQuimestreCualitativa() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public List<CalificacionParcialCualitativa> getCalificacionParcialCualitativas() {
		return this.calificacionParcialCualitativas;
	}

	public void setCalificacionParcialCualitativas(List<CalificacionParcialCualitativa> calificacionParcialCualitativas) {
		this.calificacionParcialCualitativas = calificacionParcialCualitativas;
	}

	public CalificacionParcialCualitativa addCalificacionParcialCualitativa(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		getCalificacionParcialCualitativas().add(calificacionParcialCualitativa);
		calificacionParcialCualitativa.setCalificacionQuimestreCualitativa(this);

		return calificacionParcialCualitativa;
	}

	public CalificacionParcialCualitativa removeCalificacionParcialCualitativa(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		getCalificacionParcialCualitativas().remove(calificacionParcialCualitativa);
		calificacionParcialCualitativa.setCalificacionQuimestreCualitativa(null);

		return calificacionParcialCualitativa;
	}

	public Asignatura getAsignatura() {
		return this.asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public Cualitativa getCualitativa1() {
		return this.cualitativa1;
	}

	public void setCualitativa1(Cualitativa cualitativa1) {
		this.cualitativa1 = cualitativa1;
	}

	public Cualitativa getCualitativa2() {
		return this.cualitativa2;
	}

	public void setCualitativa2(Cualitativa cualitativa2) {
		this.cualitativa2 = cualitativa2;
	}

	public Matricula getMatricula() {
		return this.matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	public Observacion getObservacion() {
		return this.observacion;
	}

	public void setObservacion(Observacion observacion) {
		this.observacion = observacion;
	}

	public Quimestre getQuimestre() {
		return this.quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

}