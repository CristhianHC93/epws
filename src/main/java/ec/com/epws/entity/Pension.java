package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the pension database table.
 * 
 */
@Entity
@NamedQuery(name="Pension.findAll", query="SELECT p FROM Pension p")
public class Pension implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String detalle;

	@Column(name="valor_pagado")
	private double valorPagado;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="id_estado")
	private Estado estado;

	//bi-directional many-to-one association to Matricula
	@ManyToOne
	@JoinColumn(name="id_matricula")
	private Matricula matricula;

	//bi-directional many-to-one association to Mes
	@ManyToOne
	@JoinColumn(name="id_mes")
	private Mes mes;

	//bi-directional many-to-one association to ValorReferencia
	@ManyToOne
	@JoinColumn(name="id_valor_referencia")
	private ValorReferencia valorReferencia;

	public Pension() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDetalle() {
		return this.detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public double getValorPagado() {
		return this.valorPagado;
	}

	public void setValorPagado(double valorPagado) {
		this.valorPagado = valorPagado;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Matricula getMatricula() {
		return this.matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	public Mes getMes() {
		return this.mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public ValorReferencia getValorReferencia() {
		return this.valorReferencia;
	}

	public void setValorReferencia(ValorReferencia valorReferencia) {
		this.valorReferencia = valorReferencia;
	}

}