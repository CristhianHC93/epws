package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the codigo_cualitativa database table.
 * 
 */
@Entity
@Table(name="codigo_cualitativa")
@NamedQuery(name="CodigoCualitativa.findAll", query="SELECT c FROM CodigoCualitativa c")
public class CodigoCualitativa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to Asignatura
	@OneToMany(mappedBy="codigoCualitativa")
	private List<Asignatura> asignaturas;

	//bi-directional many-to-one association to Cualitativa
	@OneToMany(mappedBy="codigoCualitativa")
	private List<Cualitativa> cualitativas;

	public CodigoCualitativa() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Asignatura> getAsignaturas() {
		return this.asignaturas;
	}

	public void setAsignaturas(List<Asignatura> asignaturas) {
		this.asignaturas = asignaturas;
	}

	public Asignatura addAsignatura(Asignatura asignatura) {
		getAsignaturas().add(asignatura);
		asignatura.setCodigoCualitativa(this);

		return asignatura;
	}

	public Asignatura removeAsignatura(Asignatura asignatura) {
		getAsignaturas().remove(asignatura);
		asignatura.setCodigoCualitativa(null);

		return asignatura;
	}

	public List<Cualitativa> getCualitativas() {
		return this.cualitativas;
	}

	public void setCualitativas(List<Cualitativa> cualitativas) {
		this.cualitativas = cualitativas;
	}

	public Cualitativa addCualitativa(Cualitativa cualitativa) {
		getCualitativas().add(cualitativa);
		cualitativa.setCodigoCualitativa(this);

		return cualitativa;
	}

	public Cualitativa removeCualitativa(Cualitativa cualitativa) {
		getCualitativas().remove(cualitativa);
		cualitativa.setCodigoCualitativa(null);

		return cualitativa;
	}

}