package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the docente database table.
 * 
 */
@Entity
@NamedQuery(name="Docente.findAll", query="SELECT d FROM Docente d")
public class Docente implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String celular;

	private String direccion;

	private String foto;

	private String observacion;

	private String titulo;

	//bi-directional many-to-one association to AsignacionDocente
	@OneToMany(mappedBy="docente")
	private List<AsignacionDocente> asignacionDocentes;

	//bi-directional many-to-one association to AsignacionTutor
	@OneToMany(mappedBy="docente")
	private List<AsignacionTutor> asignacionTutors;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="id_estado")
	private Estado estado;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;

	public Docente() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCelular() {
		return this.celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getDireccion() {
		return this.direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getFoto() {
		return this.foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public String getObservacion() {
		return this.observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public List<AsignacionDocente> getAsignacionDocentes() {
		return this.asignacionDocentes;
	}

	public void setAsignacionDocentes(List<AsignacionDocente> asignacionDocentes) {
		this.asignacionDocentes = asignacionDocentes;
	}

	public AsignacionDocente addAsignacionDocente(AsignacionDocente asignacionDocente) {
		getAsignacionDocentes().add(asignacionDocente);
		asignacionDocente.setDocente(this);

		return asignacionDocente;
	}

	public AsignacionDocente removeAsignacionDocente(AsignacionDocente asignacionDocente) {
		getAsignacionDocentes().remove(asignacionDocente);
		asignacionDocente.setDocente(null);

		return asignacionDocente;
	}

	public List<AsignacionTutor> getAsignacionTutors() {
		return this.asignacionTutors;
	}

	public void setAsignacionTutors(List<AsignacionTutor> asignacionTutors) {
		this.asignacionTutors = asignacionTutors;
	}

	public AsignacionTutor addAsignacionTutor(AsignacionTutor asignacionTutor) {
		getAsignacionTutors().add(asignacionTutor);
		asignacionTutor.setDocente(this);

		return asignacionTutor;
	}

	public AsignacionTutor removeAsignacionTutor(AsignacionTutor asignacionTutor) {
		getAsignacionTutors().remove(asignacionTutor);
		asignacionTutor.setDocente(null);

		return asignacionTutor;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

}