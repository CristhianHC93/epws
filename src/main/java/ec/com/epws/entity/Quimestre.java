package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the quimestre database table.
 * 
 */
@Entity
@NamedQuery(name="Quimestre.findAll", query="SELECT q FROM Quimestre q")
public class Quimestre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to CalificacionQuimestre
	@OneToMany(mappedBy="quimestre")
	private List<CalificacionQuimestre> calificacionQuimestres;

	//bi-directional many-to-one association to CalificacionQuimestreCualitativa
	@OneToMany(mappedBy="quimestre")
	private List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas;

	//bi-directional many-to-one association to Parcial
	@OneToMany(mappedBy="quimestre")
	private List<Parcial> parcials;

	public Quimestre() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<CalificacionQuimestre> getCalificacionQuimestres() {
		return this.calificacionQuimestres;
	}

	public void setCalificacionQuimestres(List<CalificacionQuimestre> calificacionQuimestres) {
		this.calificacionQuimestres = calificacionQuimestres;
	}

	public CalificacionQuimestre addCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		getCalificacionQuimestres().add(calificacionQuimestre);
		calificacionQuimestre.setQuimestre(this);

		return calificacionQuimestre;
	}

	public CalificacionQuimestre removeCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		getCalificacionQuimestres().remove(calificacionQuimestre);
		calificacionQuimestre.setQuimestre(null);

		return calificacionQuimestre;
	}

	public List<CalificacionQuimestreCualitativa> getCalificacionQuimestreCualitativas() {
		return this.calificacionQuimestreCualitativas;
	}

	public void setCalificacionQuimestreCualitativas(List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas) {
		this.calificacionQuimestreCualitativas = calificacionQuimestreCualitativas;
	}

	public CalificacionQuimestreCualitativa addCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		getCalificacionQuimestreCualitativas().add(calificacionQuimestreCualitativa);
		calificacionQuimestreCualitativa.setQuimestre(this);

		return calificacionQuimestreCualitativa;
	}

	public CalificacionQuimestreCualitativa removeCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		getCalificacionQuimestreCualitativas().remove(calificacionQuimestreCualitativa);
		calificacionQuimestreCualitativa.setQuimestre(null);

		return calificacionQuimestreCualitativa;
	}

	public List<Parcial> getParcials() {
		return this.parcials;
	}

	public void setParcials(List<Parcial> parcials) {
		this.parcials = parcials;
	}

	public Parcial addParcial(Parcial parcial) {
		getParcials().add(parcial);
		parcial.setQuimestre(this);

		return parcial;
	}

	public Parcial removeParcial(Parcial parcial) {
		getParcials().remove(parcial);
		parcial.setQuimestre(null);

		return parcial;
	}

}