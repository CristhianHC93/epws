package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the asignacion_tutor database table.
 * 
 */
@Entity
@Table(name="asignacion_tutor")
@NamedQuery(name="AsignacionTutor.findAll", query="SELECT a FROM AsignacionTutor a")
public class AsignacionTutor implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_asignacion_tutor")
	private Integer idAsignacionTutor;

	//bi-directional many-to-one association to AnioLectivo
	@ManyToOne
	@JoinColumn(name="id_anio_lectivo")
	private AnioLectivo anioLectivo;

	//bi-directional many-to-one association to Curso
	@ManyToOne
	@JoinColumn(name="id_curso")
	private Curso curso;

	//bi-directional many-to-one association to Docente
	@ManyToOne
	@JoinColumn(name="id_docente")
	private Docente docente;

	public AsignacionTutor() {
	}

	public Integer getIdAsignacionTutor() {
		return this.idAsignacionTutor;
	}

	public void setIdAsignacionTutor(Integer idAsignacionTutor) {
		this.idAsignacionTutor = idAsignacionTutor;
	}

	public AnioLectivo getAnioLectivo() {
		return this.anioLectivo;
	}

	public void setAnioLectivo(AnioLectivo anioLectivo) {
		this.anioLectivo = anioLectivo;
	}

	public Curso getCurso() {
		return this.curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public Docente getDocente() {
		return this.docente;
	}

	public void setDocente(Docente docente) {
		this.docente = docente;
	}

}