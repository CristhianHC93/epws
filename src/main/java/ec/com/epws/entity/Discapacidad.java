package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the discapacidad database table.
 * 
 */
@Entity
@NamedQuery(name="Discapacidad.findAll", query="SELECT d FROM Discapacidad d")
public class Discapacidad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String certificado;

	private String descripcion;

	//bi-directional many-to-one association to MedicaDiscapacidad
	@OneToMany(mappedBy="discapacidad")
	private List<MedicaDiscapacidad> medicaDiscapacidads;

	public Discapacidad() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCertificado() {
		return this.certificado;
	}

	public void setCertificado(String certificado) {
		this.certificado = certificado;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<MedicaDiscapacidad> getMedicaDiscapacidads() {
		return this.medicaDiscapacidads;
	}

	public void setMedicaDiscapacidads(List<MedicaDiscapacidad> medicaDiscapacidads) {
		this.medicaDiscapacidads = medicaDiscapacidads;
	}

	public MedicaDiscapacidad addMedicaDiscapacidad(MedicaDiscapacidad medicaDiscapacidad) {
		getMedicaDiscapacidads().add(medicaDiscapacidad);
		medicaDiscapacidad.setDiscapacidad(this);

		return medicaDiscapacidad;
	}

	public MedicaDiscapacidad removeMedicaDiscapacidad(MedicaDiscapacidad medicaDiscapacidad) {
		getMedicaDiscapacidads().remove(medicaDiscapacidad);
		medicaDiscapacidad.setDiscapacidad(null);

		return medicaDiscapacidad;
	}

}