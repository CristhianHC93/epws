package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the medica_vacuna database table.
 * 
 */
@Entity
@Table(name="medica_vacuna")
@NamedQuery(name="MedicaVacuna.findAll", query="SELECT m FROM MedicaVacuna m")
public class MedicaVacuna implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to ObservacionMedica
	@ManyToOne
	@JoinColumn(name="id_observacion_medica")
	private ObservacionMedica observacionMedica;

	//bi-directional many-to-one association to Vacuna
	@ManyToOne
	@JoinColumn(name="id_vacuna")
	private Vacuna vacuna;

	public MedicaVacuna() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public ObservacionMedica getObservacionMedica() {
		return this.observacionMedica;
	}

	public void setObservacionMedica(ObservacionMedica observacionMedica) {
		this.observacionMedica = observacionMedica;
	}

	public Vacuna getVacuna() {
		return this.vacuna;
	}

	public void setVacuna(Vacuna vacuna) {
		this.vacuna = vacuna;
	}

}