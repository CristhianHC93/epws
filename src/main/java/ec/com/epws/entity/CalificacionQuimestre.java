package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the calificacion_quimestre database table.
 * 
 */
@Entity
@Table(name="calificacion_quimestre")
@NamedQuery(name="CalificacionQuimestre.findAll", query="SELECT c FROM CalificacionQuimestre c")
public class CalificacionQuimestre implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private double examen;

	@Column(name="examen_20")
	private double examen20;

	private double nota;

	@Column(name="promedio_80")
	private double promedio80;

	@Column(name="promedio_parciales")
	private double promedioParciales;

	//bi-directional many-to-one association to CalificacionParcial
	@OneToMany(mappedBy="calificacionQuimestre")
	private List<CalificacionParcial> calificacionParcials;

	//bi-directional many-to-one association to Asignatura
	@ManyToOne
	@JoinColumn(name="id_asignatura")
	private Asignatura asignatura;

	//bi-directional many-to-one association to Matricula
	@ManyToOne
	@JoinColumn(name="id_matricula")
	private Matricula matricula;

	//bi-directional many-to-one association to Observacion
	@ManyToOne
	@JoinColumn(name="id_observacion")
	private Observacion observacion;

	//bi-directional many-to-one association to Quimestre
	@ManyToOne
	@JoinColumn(name="id_quimestre")
	private Quimestre quimestre;

	public CalificacionQuimestre() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getExamen() {
		return this.examen;
	}

	public void setExamen(double examen) {
		this.examen = examen;
	}

	public double getExamen20() {
		return this.examen20;
	}

	public void setExamen20(double examen20) {
		this.examen20 = examen20;
	}

	public double getNota() {
		return this.nota;
	}

	public void setNota(double nota) {
		this.nota = nota;
	}

	public double getPromedio80() {
		return this.promedio80;
	}

	public void setPromedio80(double promedio80) {
		this.promedio80 = promedio80;
	}

	public double getPromedioParciales() {
		return this.promedioParciales;
	}

	public void setPromedioParciales(double promedioParciales) {
		this.promedioParciales = promedioParciales;
	}

	public List<CalificacionParcial> getCalificacionParcials() {
		return this.calificacionParcials;
	}

	public void setCalificacionParcials(List<CalificacionParcial> calificacionParcials) {
		this.calificacionParcials = calificacionParcials;
	}

	public CalificacionParcial addCalificacionParcial(CalificacionParcial calificacionParcial) {
		getCalificacionParcials().add(calificacionParcial);
		calificacionParcial.setCalificacionQuimestre(this);

		return calificacionParcial;
	}

	public CalificacionParcial removeCalificacionParcial(CalificacionParcial calificacionParcial) {
		getCalificacionParcials().remove(calificacionParcial);
		calificacionParcial.setCalificacionQuimestre(null);

		return calificacionParcial;
	}

	public Asignatura getAsignatura() {
		return this.asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public Matricula getMatricula() {
		return this.matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	public Observacion getObservacion() {
		return this.observacion;
	}

	public void setObservacion(Observacion observacion) {
		this.observacion = observacion;
	}

	public Quimestre getQuimestre() {
		return this.quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

}