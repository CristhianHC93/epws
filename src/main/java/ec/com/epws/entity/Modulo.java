package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the modulo database table.
 * 
 */
@Entity
@NamedQuery(name="Modulo.findAll", query="SELECT m FROM Modulo m")
public class Modulo implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="abreviacion_codigo")
	private String abreviacionCodigo;

	private String codigo;

	private String descripcion;

	//bi-directional many-to-one association to ModuloDetalle
	@OneToMany(mappedBy="modulo")
	private List<ModuloDetalle> moduloDetalles;

	//bi-directional many-to-one association to RolModulo
	@OneToMany(mappedBy="modulo")
	private List<RolModulo> rolModulos;

	public Modulo() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAbreviacionCodigo() {
		return this.abreviacionCodigo;
	}

	public void setAbreviacionCodigo(String abreviacionCodigo) {
		this.abreviacionCodigo = abreviacionCodigo;
	}

	public String getCodigo() {
		return this.codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<ModuloDetalle> getModuloDetalles() {
		return this.moduloDetalles;
	}

	public void setModuloDetalles(List<ModuloDetalle> moduloDetalles) {
		this.moduloDetalles = moduloDetalles;
	}

	public ModuloDetalle addModuloDetalle(ModuloDetalle moduloDetalle) {
		getModuloDetalles().add(moduloDetalle);
		moduloDetalle.setModulo(this);

		return moduloDetalle;
	}

	public ModuloDetalle removeModuloDetalle(ModuloDetalle moduloDetalle) {
		getModuloDetalles().remove(moduloDetalle);
		moduloDetalle.setModulo(null);

		return moduloDetalle;
	}

	public List<RolModulo> getRolModulos() {
		return this.rolModulos;
	}

	public void setRolModulos(List<RolModulo> rolModulos) {
		this.rolModulos = rolModulos;
	}

	public RolModulo addRolModulo(RolModulo rolModulo) {
		getRolModulos().add(rolModulo);
		rolModulo.setModulo(this);

		return rolModulo;
	}

	public RolModulo removeRolModulo(RolModulo rolModulo) {
		getRolModulos().remove(rolModulo);
		rolModulo.setModulo(null);

		return rolModulo;
	}

}