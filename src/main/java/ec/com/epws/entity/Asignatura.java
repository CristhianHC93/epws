package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the asignatura database table.
 * 
 */
@Entity
@NamedQuery(name="Asignatura.findAll", query="SELECT a FROM Asignatura a")
public class Asignatura implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to AsignacionDocente
	@OneToMany(mappedBy="asignatura")
	private List<AsignacionDocente> asignacionDocentes;

	//bi-directional many-to-one association to CodigoCualitativa
	@ManyToOne
	@JoinColumn(name="id_codigo_cualitativa")
	private CodigoCualitativa codigoCualitativa;

	//bi-directional many-to-one association to Curso
	@ManyToOne
	@JoinColumn(name="id_curso")
	private Curso curso;

	//bi-directional many-to-one association to CalificacionQuimestre
	@OneToMany(mappedBy="asignatura")
	private List<CalificacionQuimestre> calificacionQuimestres;

	//bi-directional many-to-one association to CalificacionQuimestreCualitativa
	@OneToMany(mappedBy="asignatura")
	private List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas;

	//bi-directional many-to-one association to CuadroCalificacion
	@OneToMany(mappedBy="asignatura")
	private List<CuadroCalificacion> cuadroCalificacions;

	public Asignatura() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<AsignacionDocente> getAsignacionDocentes() {
		return this.asignacionDocentes;
	}

	public void setAsignacionDocentes(List<AsignacionDocente> asignacionDocentes) {
		this.asignacionDocentes = asignacionDocentes;
	}

	public AsignacionDocente addAsignacionDocente(AsignacionDocente asignacionDocente) {
		getAsignacionDocentes().add(asignacionDocente);
		asignacionDocente.setAsignatura(this);

		return asignacionDocente;
	}

	public AsignacionDocente removeAsignacionDocente(AsignacionDocente asignacionDocente) {
		getAsignacionDocentes().remove(asignacionDocente);
		asignacionDocente.setAsignatura(null);

		return asignacionDocente;
	}

	public CodigoCualitativa getCodigoCualitativa() {
		return this.codigoCualitativa;
	}

	public void setCodigoCualitativa(CodigoCualitativa codigoCualitativa) {
		this.codigoCualitativa = codigoCualitativa;
	}

	public Curso getCurso() {
		return this.curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<CalificacionQuimestre> getCalificacionQuimestres() {
		return this.calificacionQuimestres;
	}

	public void setCalificacionQuimestres(List<CalificacionQuimestre> calificacionQuimestres) {
		this.calificacionQuimestres = calificacionQuimestres;
	}

	public CalificacionQuimestre addCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		getCalificacionQuimestres().add(calificacionQuimestre);
		calificacionQuimestre.setAsignatura(this);

		return calificacionQuimestre;
	}

	public CalificacionQuimestre removeCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		getCalificacionQuimestres().remove(calificacionQuimestre);
		calificacionQuimestre.setAsignatura(null);

		return calificacionQuimestre;
	}

	public List<CalificacionQuimestreCualitativa> getCalificacionQuimestreCualitativas() {
		return this.calificacionQuimestreCualitativas;
	}

	public void setCalificacionQuimestreCualitativas(List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas) {
		this.calificacionQuimestreCualitativas = calificacionQuimestreCualitativas;
	}

	public CalificacionQuimestreCualitativa addCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		getCalificacionQuimestreCualitativas().add(calificacionQuimestreCualitativa);
		calificacionQuimestreCualitativa.setAsignatura(this);

		return calificacionQuimestreCualitativa;
	}

	public CalificacionQuimestreCualitativa removeCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		getCalificacionQuimestreCualitativas().remove(calificacionQuimestreCualitativa);
		calificacionQuimestreCualitativa.setAsignatura(null);

		return calificacionQuimestreCualitativa;
	}

	public List<CuadroCalificacion> getCuadroCalificacions() {
		return this.cuadroCalificacions;
	}

	public void setCuadroCalificacions(List<CuadroCalificacion> cuadroCalificacions) {
		this.cuadroCalificacions = cuadroCalificacions;
	}

	public CuadroCalificacion addCuadroCalificacion(CuadroCalificacion cuadroCalificacion) {
		getCuadroCalificacions().add(cuadroCalificacion);
		cuadroCalificacion.setAsignatura(this);

		return cuadroCalificacion;
	}

	public CuadroCalificacion removeCuadroCalificacion(CuadroCalificacion cuadroCalificacion) {
		getCuadroCalificacions().remove(cuadroCalificacion);
		cuadroCalificacion.setAsignatura(null);

		return cuadroCalificacion;
	}

}