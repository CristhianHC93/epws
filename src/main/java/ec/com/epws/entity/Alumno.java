package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import java.util.List;


/**
 * The persistent class for the alumno database table.
 * 
 */
@Entity
@NamedQuery(name="Alumno.findAll", query="SELECT a FROM Alumno a")
public class Alumno implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String apellido;

	private String cedula;

	private String domicilio;

	@Column(name="escuela_anterior")
	private String escuelaAnterior;

	@Temporal(TemporalType.DATE)
	@Column(name="fecha_nacimiento")
	private Date fechaNacimiento;

	private String foto;

	private Boolean inscripcion;

	@Column(name="lugar_nacimiento")
	private String lugarNacimiento;

	private String nombre;

	private Integer numero;

	//bi-directional many-to-one association to Estado
	@ManyToOne
	@JoinColumn(name="id_estado")
	private Estado estado;

	//bi-directional many-to-one association to ObservacionMedica
	@ManyToOne
	@JoinColumn(name="id_observacion_medica")
	private ObservacionMedica observacionMedica;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="id_usuario")
	private Usuario usuario;

	//bi-directional many-to-one association to AlumnoFamilia
	@OneToMany(mappedBy="alumno")
	private List<AlumnoFamilia> alumnoFamilias;

	//bi-directional many-to-one association to Asistencia
	@OneToMany(mappedBy="alumno")
	private List<Asistencia> asistencias;

	//bi-directional many-to-one association to Matricula
	@OneToMany(mappedBy="alumno")
	private List<Matricula> matriculas;

	public Alumno() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getDomicilio() {
		return this.domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getEscuelaAnterior() {
		return this.escuelaAnterior;
	}

	public void setEscuelaAnterior(String escuelaAnterior) {
		this.escuelaAnterior = escuelaAnterior;
	}

	public Date getFechaNacimiento() {
		return this.fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getFoto() {
		return this.foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public Boolean getInscripcion() {
		return this.inscripcion;
	}

	public void setInscripcion(Boolean inscripcion) {
		this.inscripcion = inscripcion;
	}

	public String getLugarNacimiento() {
		return this.lugarNacimiento;
	}

	public void setLugarNacimiento(String lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getNumero() {
		return this.numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}

	public Estado getEstado() {
		return this.estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public ObservacionMedica getObservacionMedica() {
		return this.observacionMedica;
	}

	public void setObservacionMedica(ObservacionMedica observacionMedica) {
		this.observacionMedica = observacionMedica;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<AlumnoFamilia> getAlumnoFamilias() {
		return this.alumnoFamilias;
	}

	public void setAlumnoFamilias(List<AlumnoFamilia> alumnoFamilias) {
		this.alumnoFamilias = alumnoFamilias;
	}

	public AlumnoFamilia addAlumnoFamilia(AlumnoFamilia alumnoFamilia) {
		getAlumnoFamilias().add(alumnoFamilia);
		alumnoFamilia.setAlumno(this);

		return alumnoFamilia;
	}

	public AlumnoFamilia removeAlumnoFamilia(AlumnoFamilia alumnoFamilia) {
		getAlumnoFamilias().remove(alumnoFamilia);
		alumnoFamilia.setAlumno(null);

		return alumnoFamilia;
	}

	public List<Asistencia> getAsistencias() {
		return this.asistencias;
	}

	public void setAsistencias(List<Asistencia> asistencias) {
		this.asistencias = asistencias;
	}

	public Asistencia addAsistencia(Asistencia asistencia) {
		getAsistencias().add(asistencia);
		asistencia.setAlumno(this);

		return asistencia;
	}

	public Asistencia removeAsistencia(Asistencia asistencia) {
		getAsistencias().remove(asistencia);
		asistencia.setAlumno(null);

		return asistencia;
	}

	public List<Matricula> getMatriculas() {
		return this.matriculas;
	}

	public void setMatriculas(List<Matricula> matriculas) {
		this.matriculas = matriculas;
	}

	public Matricula addMatricula(Matricula matricula) {
		getMatriculas().add(matricula);
		matricula.setAlumno(this);

		return matricula;
	}

	public Matricula removeMatricula(Matricula matricula) {
		getMatriculas().remove(matricula);
		matricula.setAlumno(null);

		return matricula;
	}

}