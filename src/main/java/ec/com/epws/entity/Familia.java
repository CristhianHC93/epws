package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the familia database table.
 * 
 */
@Entity
@NamedQuery(name="Familia.findAll", query="SELECT f FROM Familia f")
public class Familia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String apellido;

	private String cedula;

	private String nombre;

	private String profesion;

	private String telefono;

	//bi-directional many-to-one association to AlumnoFamilia
	@OneToMany(mappedBy="familia")
	private List<AlumnoFamilia> alumnoFamilias;

	//bi-directional many-to-one association to Parentesco
	@ManyToOne
	@JoinColumn(name="id_parentesco")
	private Parentesco parentesco;

	public Familia() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getApellido() {
		return this.apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getCedula() {
		return this.cedula;
	}

	public void setCedula(String cedula) {
		this.cedula = cedula;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getProfesion() {
		return this.profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public List<AlumnoFamilia> getAlumnoFamilias() {
		return this.alumnoFamilias;
	}

	public void setAlumnoFamilias(List<AlumnoFamilia> alumnoFamilias) {
		this.alumnoFamilias = alumnoFamilias;
	}

	public AlumnoFamilia addAlumnoFamilia(AlumnoFamilia alumnoFamilia) {
		getAlumnoFamilias().add(alumnoFamilia);
		alumnoFamilia.setFamilia(this);

		return alumnoFamilia;
	}

	public AlumnoFamilia removeAlumnoFamilia(AlumnoFamilia alumnoFamilia) {
		getAlumnoFamilias().remove(alumnoFamilia);
		alumnoFamilia.setFamilia(null);

		return alumnoFamilia;
	}

	public Parentesco getParentesco() {
		return this.parentesco;
	}

	public void setParentesco(Parentesco parentesco) {
		this.parentesco = parentesco;
	}

}