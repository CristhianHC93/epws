package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the cualitativa database table.
 * 
 */
@Entity
@NamedQuery(name="Cualitativa.findAll", query="SELECT c FROM Cualitativa c")
public class Cualitativa implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	private String siglas;

	@Column(name="valor_maximo")
	private double valorMaximo;

	@Column(name="valor_minimo")
	private double valorMinimo;

	//bi-directional many-to-one association to CalificacionParcial
	@OneToMany(mappedBy="cualitativa")
	private List<CalificacionParcial> calificacionParcials;

	//bi-directional many-to-one association to CalificacionParcialCualitativa
	@OneToMany(mappedBy="cualitativa")
	private List<CalificacionParcialCualitativa> calificacionParcialCualitativas;

	//bi-directional many-to-one association to CalificacionQuimestreCualitativa
	@OneToMany(mappedBy="cualitativa1")
	private List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas1;

	//bi-directional many-to-one association to CalificacionQuimestreCualitativa
	@OneToMany(mappedBy="cualitativa2")
	private List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas2;

	//bi-directional many-to-one association to CodigoCualitativa
	@ManyToOne
	@JoinColumn(name="id_codigo_cualitativa")
	private CodigoCualitativa codigoCualitativa;

	public Cualitativa() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getSiglas() {
		return this.siglas;
	}

	public void setSiglas(String siglas) {
		this.siglas = siglas;
	}

	public double getValorMaximo() {
		return this.valorMaximo;
	}

	public void setValorMaximo(double valorMaximo) {
		this.valorMaximo = valorMaximo;
	}

	public double getValorMinimo() {
		return this.valorMinimo;
	}

	public void setValorMinimo(double valorMinimo) {
		this.valorMinimo = valorMinimo;
	}

	public List<CalificacionParcial> getCalificacionParcials() {
		return this.calificacionParcials;
	}

	public void setCalificacionParcials(List<CalificacionParcial> calificacionParcials) {
		this.calificacionParcials = calificacionParcials;
	}

	public CalificacionParcial addCalificacionParcial(CalificacionParcial calificacionParcial) {
		getCalificacionParcials().add(calificacionParcial);
		calificacionParcial.setCualitativa(this);

		return calificacionParcial;
	}

	public CalificacionParcial removeCalificacionParcial(CalificacionParcial calificacionParcial) {
		getCalificacionParcials().remove(calificacionParcial);
		calificacionParcial.setCualitativa(null);

		return calificacionParcial;
	}

	public List<CalificacionParcialCualitativa> getCalificacionParcialCualitativas() {
		return this.calificacionParcialCualitativas;
	}

	public void setCalificacionParcialCualitativas(List<CalificacionParcialCualitativa> calificacionParcialCualitativas) {
		this.calificacionParcialCualitativas = calificacionParcialCualitativas;
	}

	public CalificacionParcialCualitativa addCalificacionParcialCualitativa(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		getCalificacionParcialCualitativas().add(calificacionParcialCualitativa);
		calificacionParcialCualitativa.setCualitativa(this);

		return calificacionParcialCualitativa;
	}

	public CalificacionParcialCualitativa removeCalificacionParcialCualitativa(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		getCalificacionParcialCualitativas().remove(calificacionParcialCualitativa);
		calificacionParcialCualitativa.setCualitativa(null);

		return calificacionParcialCualitativa;
	}

	public List<CalificacionQuimestreCualitativa> getCalificacionQuimestreCualitativas1() {
		return this.calificacionQuimestreCualitativas1;
	}

	public void setCalificacionQuimestreCualitativas1(List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas1) {
		this.calificacionQuimestreCualitativas1 = calificacionQuimestreCualitativas1;
	}

	public CalificacionQuimestreCualitativa addCalificacionQuimestreCualitativas1(CalificacionQuimestreCualitativa calificacionQuimestreCualitativas1) {
		getCalificacionQuimestreCualitativas1().add(calificacionQuimestreCualitativas1);
		calificacionQuimestreCualitativas1.setCualitativa1(this);

		return calificacionQuimestreCualitativas1;
	}

	public CalificacionQuimestreCualitativa removeCalificacionQuimestreCualitativas1(CalificacionQuimestreCualitativa calificacionQuimestreCualitativas1) {
		getCalificacionQuimestreCualitativas1().remove(calificacionQuimestreCualitativas1);
		calificacionQuimestreCualitativas1.setCualitativa1(null);

		return calificacionQuimestreCualitativas1;
	}

	public List<CalificacionQuimestreCualitativa> getCalificacionQuimestreCualitativas2() {
		return this.calificacionQuimestreCualitativas2;
	}

	public void setCalificacionQuimestreCualitativas2(List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas2) {
		this.calificacionQuimestreCualitativas2 = calificacionQuimestreCualitativas2;
	}

	public CalificacionQuimestreCualitativa addCalificacionQuimestreCualitativas2(CalificacionQuimestreCualitativa calificacionQuimestreCualitativas2) {
		getCalificacionQuimestreCualitativas2().add(calificacionQuimestreCualitativas2);
		calificacionQuimestreCualitativas2.setCualitativa2(this);

		return calificacionQuimestreCualitativas2;
	}

	public CalificacionQuimestreCualitativa removeCalificacionQuimestreCualitativas2(CalificacionQuimestreCualitativa calificacionQuimestreCualitativas2) {
		getCalificacionQuimestreCualitativas2().remove(calificacionQuimestreCualitativas2);
		calificacionQuimestreCualitativas2.setCualitativa2(null);

		return calificacionQuimestreCualitativas2;
	}

	public CodigoCualitativa getCodigoCualitativa() {
		return this.codigoCualitativa;
	}

	public void setCodigoCualitativa(CodigoCualitativa codigoCualitativa) {
		this.codigoCualitativa = codigoCualitativa;
	}

}