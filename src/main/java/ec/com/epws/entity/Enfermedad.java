package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the enfermedad database table.
 * 
 */
@Entity
@NamedQuery(name="Enfermedad.findAll", query="SELECT e FROM Enfermedad e")
public class Enfermedad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to MedicaEnfermedad
	@OneToMany(mappedBy="enfermedad")
	private List<MedicaEnfermedad> medicaEnfermedads;

	public Enfermedad() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<MedicaEnfermedad> getMedicaEnfermedads() {
		return this.medicaEnfermedads;
	}

	public void setMedicaEnfermedads(List<MedicaEnfermedad> medicaEnfermedads) {
		this.medicaEnfermedads = medicaEnfermedads;
	}

	public MedicaEnfermedad addMedicaEnfermedad(MedicaEnfermedad medicaEnfermedad) {
		getMedicaEnfermedads().add(medicaEnfermedad);
		medicaEnfermedad.setEnfermedad(this);

		return medicaEnfermedad;
	}

	public MedicaEnfermedad removeMedicaEnfermedad(MedicaEnfermedad medicaEnfermedad) {
		getMedicaEnfermedads().remove(medicaEnfermedad);
		medicaEnfermedad.setEnfermedad(null);

		return medicaEnfermedad;
	}

}