package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the observacion database table.
 * 
 */
@Entity
@NamedQuery(name="Observacion.findAll", query="SELECT o FROM Observacion o")
public class Observacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to Asistencia
	@OneToMany(mappedBy="observacion")
	private List<Asistencia> asistencias;

	//bi-directional many-to-one association to CalificacionParcial
	@OneToMany(mappedBy="observacion")
	private List<CalificacionParcial> calificacionParcials;

	//bi-directional many-to-one association to CalificacionParcialCualitativa
	@OneToMany(mappedBy="observacion")
	private List<CalificacionParcialCualitativa> calificacionParcialCualitativas;

	//bi-directional many-to-one association to CalificacionQuimestre
	@OneToMany(mappedBy="observacion")
	private List<CalificacionQuimestre> calificacionQuimestres;

	//bi-directional many-to-one association to CalificacionQuimestreCualitativa
	@OneToMany(mappedBy="observacion")
	private List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas;

	public Observacion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Asistencia> getAsistencias() {
		return this.asistencias;
	}

	public void setAsistencias(List<Asistencia> asistencias) {
		this.asistencias = asistencias;
	}

	public Asistencia addAsistencia(Asistencia asistencia) {
		getAsistencias().add(asistencia);
		asistencia.setObservacion(this);

		return asistencia;
	}

	public Asistencia removeAsistencia(Asistencia asistencia) {
		getAsistencias().remove(asistencia);
		asistencia.setObservacion(null);

		return asistencia;
	}

	public List<CalificacionParcial> getCalificacionParcials() {
		return this.calificacionParcials;
	}

	public void setCalificacionParcials(List<CalificacionParcial> calificacionParcials) {
		this.calificacionParcials = calificacionParcials;
	}

	public CalificacionParcial addCalificacionParcial(CalificacionParcial calificacionParcial) {
		getCalificacionParcials().add(calificacionParcial);
		calificacionParcial.setObservacion(this);

		return calificacionParcial;
	}

	public CalificacionParcial removeCalificacionParcial(CalificacionParcial calificacionParcial) {
		getCalificacionParcials().remove(calificacionParcial);
		calificacionParcial.setObservacion(null);

		return calificacionParcial;
	}

	public List<CalificacionParcialCualitativa> getCalificacionParcialCualitativas() {
		return this.calificacionParcialCualitativas;
	}

	public void setCalificacionParcialCualitativas(List<CalificacionParcialCualitativa> calificacionParcialCualitativas) {
		this.calificacionParcialCualitativas = calificacionParcialCualitativas;
	}

	public CalificacionParcialCualitativa addCalificacionParcialCualitativa(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		getCalificacionParcialCualitativas().add(calificacionParcialCualitativa);
		calificacionParcialCualitativa.setObservacion(this);

		return calificacionParcialCualitativa;
	}

	public CalificacionParcialCualitativa removeCalificacionParcialCualitativa(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		getCalificacionParcialCualitativas().remove(calificacionParcialCualitativa);
		calificacionParcialCualitativa.setObservacion(null);

		return calificacionParcialCualitativa;
	}

	public List<CalificacionQuimestre> getCalificacionQuimestres() {
		return this.calificacionQuimestres;
	}

	public void setCalificacionQuimestres(List<CalificacionQuimestre> calificacionQuimestres) {
		this.calificacionQuimestres = calificacionQuimestres;
	}

	public CalificacionQuimestre addCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		getCalificacionQuimestres().add(calificacionQuimestre);
		calificacionQuimestre.setObservacion(this);

		return calificacionQuimestre;
	}

	public CalificacionQuimestre removeCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		getCalificacionQuimestres().remove(calificacionQuimestre);
		calificacionQuimestre.setObservacion(null);

		return calificacionQuimestre;
	}

	public List<CalificacionQuimestreCualitativa> getCalificacionQuimestreCualitativas() {
		return this.calificacionQuimestreCualitativas;
	}

	public void setCalificacionQuimestreCualitativas(List<CalificacionQuimestreCualitativa> calificacionQuimestreCualitativas) {
		this.calificacionQuimestreCualitativas = calificacionQuimestreCualitativas;
	}

	public CalificacionQuimestreCualitativa addCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		getCalificacionQuimestreCualitativas().add(calificacionQuimestreCualitativa);
		calificacionQuimestreCualitativa.setObservacion(this);

		return calificacionQuimestreCualitativa;
	}

	public CalificacionQuimestreCualitativa removeCalificacionQuimestreCualitativa(CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		getCalificacionQuimestreCualitativas().remove(calificacionQuimestreCualitativa);
		calificacionQuimestreCualitativa.setObservacion(null);

		return calificacionQuimestreCualitativa;
	}

}