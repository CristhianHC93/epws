package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the parcial database table.
 * 
 */
@Entity
@NamedQuery(name="Parcial.findAll", query="SELECT p FROM Parcial p")
public class Parcial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to CalificacionParcial
	@OneToMany(mappedBy="parcial")
	private List<CalificacionParcial> calificacionParcials;

	//bi-directional many-to-one association to CalificacionParcialCualitativa
	@OneToMany(mappedBy="parcial")
	private List<CalificacionParcialCualitativa> calificacionParcialCualitativas;

	//bi-directional many-to-one association to Quimestre
	@ManyToOne
	@JoinColumn(name="id_quimestre")
	private Quimestre quimestre;

	public Parcial() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<CalificacionParcial> getCalificacionParcials() {
		return this.calificacionParcials;
	}

	public void setCalificacionParcials(List<CalificacionParcial> calificacionParcials) {
		this.calificacionParcials = calificacionParcials;
	}

	public CalificacionParcial addCalificacionParcial(CalificacionParcial calificacionParcial) {
		getCalificacionParcials().add(calificacionParcial);
		calificacionParcial.setParcial(this);

		return calificacionParcial;
	}

	public CalificacionParcial removeCalificacionParcial(CalificacionParcial calificacionParcial) {
		getCalificacionParcials().remove(calificacionParcial);
		calificacionParcial.setParcial(null);

		return calificacionParcial;
	}

	public List<CalificacionParcialCualitativa> getCalificacionParcialCualitativas() {
		return this.calificacionParcialCualitativas;
	}

	public void setCalificacionParcialCualitativas(List<CalificacionParcialCualitativa> calificacionParcialCualitativas) {
		this.calificacionParcialCualitativas = calificacionParcialCualitativas;
	}

	public CalificacionParcialCualitativa addCalificacionParcialCualitativa(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		getCalificacionParcialCualitativas().add(calificacionParcialCualitativa);
		calificacionParcialCualitativa.setParcial(this);

		return calificacionParcialCualitativa;
	}

	public CalificacionParcialCualitativa removeCalificacionParcialCualitativa(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		getCalificacionParcialCualitativas().remove(calificacionParcialCualitativa);
		calificacionParcialCualitativa.setParcial(null);

		return calificacionParcialCualitativa;
	}

	public Quimestre getQuimestre() {
		return this.quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

}