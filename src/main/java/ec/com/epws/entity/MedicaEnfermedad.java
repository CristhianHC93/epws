package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the medica_enfermedad database table.
 * 
 */
@Entity
@Table(name="medica_enfermedad")
@NamedQuery(name="MedicaEnfermedad.findAll", query="SELECT m FROM MedicaEnfermedad m")
public class MedicaEnfermedad implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	//bi-directional many-to-one association to Enfermedad
	@ManyToOne
	@JoinColumn(name="id_enfermedad")
	private Enfermedad enfermedad;

	//bi-directional many-to-one association to ObservacionMedica
	@ManyToOne
	@JoinColumn(name="id_observacion_medica")
	private ObservacionMedica observacionMedica;

	public MedicaEnfermedad() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Enfermedad getEnfermedad() {
		return this.enfermedad;
	}

	public void setEnfermedad(Enfermedad enfermedad) {
		this.enfermedad = enfermedad;
	}

	public ObservacionMedica getObservacionMedica() {
		return this.observacionMedica;
	}

	public void setObservacionMedica(ObservacionMedica observacionMedica) {
		this.observacionMedica = observacionMedica;
	}

}