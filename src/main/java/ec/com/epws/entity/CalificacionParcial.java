package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the calificacion_parcial database table.
 * 
 */
@Entity
@Table(name="calificacion_parcial")
@NamedQuery(name="CalificacionParcial.findAll", query="SELECT c FROM CalificacionParcial c")
public class CalificacionParcial implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private double agc;

	private double aic;

	private double esumativa;

	private double l;

	private double promedio;

	private double tai;

	//bi-directional many-to-one association to CalificacionQuimestre
	@ManyToOne
	@JoinColumn(name="id_calificacion_quimestre")
	private CalificacionQuimestre calificacionQuimestre;

	//bi-directional many-to-one association to Cualitativa
	@ManyToOne
	@JoinColumn(name="id_cualitativa")
	private Cualitativa cualitativa;

	//bi-directional many-to-one association to Observacion
	@ManyToOne
	@JoinColumn(name="id_observacion")
	private Observacion observacion;

	//bi-directional many-to-one association to Parcial
	@ManyToOne
	@JoinColumn(name="id_parcial")
	private Parcial parcial;

	public CalificacionParcial() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public double getAgc() {
		return this.agc;
	}

	public void setAgc(double agc) {
		this.agc = agc;
	}

	public double getAic() {
		return this.aic;
	}

	public void setAic(double aic) {
		this.aic = aic;
	}

	public double getEsumativa() {
		return this.esumativa;
	}

	public void setEsumativa(double esumativa) {
		this.esumativa = esumativa;
	}

	public double getL() {
		return this.l;
	}

	public void setL(double l) {
		this.l = l;
	}

	public double getPromedio() {
		return this.promedio;
	}

	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}

	public double getTai() {
		return this.tai;
	}

	public void setTai(double tai) {
		this.tai = tai;
	}

	public CalificacionQuimestre getCalificacionQuimestre() {
		return this.calificacionQuimestre;
	}

	public void setCalificacionQuimestre(CalificacionQuimestre calificacionQuimestre) {
		this.calificacionQuimestre = calificacionQuimestre;
	}

	public Cualitativa getCualitativa() {
		return this.cualitativa;
	}

	public void setCualitativa(Cualitativa cualitativa) {
		this.cualitativa = cualitativa;
	}

	public Observacion getObservacion() {
		return this.observacion;
	}

	public void setObservacion(Observacion observacion) {
		this.observacion = observacion;
	}

	public Parcial getParcial() {
		return this.parcial;
	}

	public void setParcial(Parcial parcial) {
		this.parcial = parcial;
	}

}