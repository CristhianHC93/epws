package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the parentesco database table.
 * 
 */
@Entity
@NamedQuery(name="Parentesco.findAll", query="SELECT p FROM Parentesco p")
public class Parentesco implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String descripcion;

	//bi-directional many-to-one association to Familia
	@OneToMany(mappedBy="parentesco")
	private List<Familia> familias;

	public Parentesco() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Familia> getFamilias() {
		return this.familias;
	}

	public void setFamilias(List<Familia> familias) {
		this.familias = familias;
	}

	public Familia addFamilia(Familia familia) {
		getFamilias().add(familia);
		familia.setParentesco(this);

		return familia;
	}

	public Familia removeFamilia(Familia familia) {
		getFamilias().remove(familia);
		familia.setParentesco(null);

		return familia;
	}

}