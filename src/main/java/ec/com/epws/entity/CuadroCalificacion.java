package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the cuadro_calificacion database table.
 * 
 */
@Entity
@Table(name="cuadro_calificacion")
@NamedQuery(name="CuadroCalificacion.findAll", query="SELECT c FROM CuadroCalificacion c")
public class CuadroCalificacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	private String letra;

	private String nota;

	private double promedio;

	@Column(name="promedio_final")
	private double promedioFinal;

	private double quimestre1;

	private double quimestre2;

	//bi-directional many-to-one association to Asignatura
	@ManyToOne
	@JoinColumn(name="id_asignatura")
	private Asignatura asignatura;

	//bi-directional many-to-one association to Matricula
	@ManyToOne
	@JoinColumn(name="id_matricula")
	private Matricula matricula;

	public CuadroCalificacion() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLetra() {
		return this.letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public String getNota() {
		return this.nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public double getPromedio() {
		return this.promedio;
	}

	public void setPromedio(double promedio) {
		this.promedio = promedio;
	}

	public double getPromedioFinal() {
		return this.promedioFinal;
	}

	public void setPromedioFinal(double promedioFinal) {
		this.promedioFinal = promedioFinal;
	}

	public double getQuimestre1() {
		return this.quimestre1;
	}

	public void setQuimestre1(double quimestre1) {
		this.quimestre1 = quimestre1;
	}

	public double getQuimestre2() {
		return this.quimestre2;
	}

	public void setQuimestre2(double quimestre2) {
		this.quimestre2 = quimestre2;
	}

	public Asignatura getAsignatura() {
		return this.asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public Matricula getMatricula() {
		return this.matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

}