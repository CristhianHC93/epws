package ec.com.epws.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.List;


/**
 * The persistent class for the valor_referencia database table.
 * 
 */
@Entity
@Table(name="valor_referencia")
@NamedQuery(name="ValorReferencia.findAll", query="SELECT v FROM ValorReferencia v")
public class ValorReferencia implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="numero_matricula")
	private Integer numeroMatricula;

	@Column(name="valor_matricula")
	private double valorMatricula;

	@Column(name="valor_pension")
	private double valorPension;

	//bi-directional many-to-one association to Pension
	@OneToMany(mappedBy="valorReferencia")
	private List<Pension> pensions;

	public ValorReferencia() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getNumeroMatricula() {
		return this.numeroMatricula;
	}

	public void setNumeroMatricula(Integer numeroMatricula) {
		this.numeroMatricula = numeroMatricula;
	}

	public double getValorMatricula() {
		return this.valorMatricula;
	}

	public void setValorMatricula(double valorMatricula) {
		this.valorMatricula = valorMatricula;
	}

	public double getValorPension() {
		return this.valorPension;
	}

	public void setValorPension(double valorPension) {
		this.valorPension = valorPension;
	}

	public List<Pension> getPensions() {
		return this.pensions;
	}

	public void setPensions(List<Pension> pensions) {
		this.pensions = pensions;
	}

	public Pension addPension(Pension pension) {
		getPensions().add(pension);
		pension.setValorReferencia(this);

		return pension;
	}

	public Pension removePension(Pension pension) {
		getPensions().remove(pension);
		pension.setValorReferencia(null);

		return pension;
	}

}