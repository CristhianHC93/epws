package ec.com.epws.controlUsuario;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;

import ec.com.epws.service.Autenticacion;
import ec.com.epws.service.impl.AutenticacionImpl;

public class Logout extends SelectorComposer<Component>{
private static final long serialVersionUID = 1L;
	
	//services
	Autenticacion authService = new AutenticacionImpl();
	
	@Listen("onClick=#logout")
	public void doLogout(){
		authService.logout();		
		Executions.sendRedirect("index.zul");
	}

}
