package ec.com.epws.controlUsuario;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;

import ec.com.epws.entity.Email;
import ec.com.epws.entity.Usuario;
import ec.com.epws.service.Autenticacion;
import ec.com.epws.service.impl.AutenticacionImpl;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.sistema.Sistema;

public class login {
	
	
	private List<String> listaIdioma = new ArrayList<String>();
	private String idioma = "Latino";

	private String username;
	private String password;
	private String mensaje;

	Usuario usuario;

	Autenticacion authService = new AutenticacionImpl();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaIdioma = Sistema.getListaIdioma();
		Usuario usuario = authService.getUserCredential();
		if (usuario != null) {
			Executions.sendRedirect("/index.zul");
			return;
		}

	}

	@Command
	public void ingresar() {
		Autenticacion authService = AutenticacionImpl.getIntance(Sessions.getCurrent());
		String nm = username;
		String pd = Sistema.Encriptar(password);

		if (!authService.login(nm, pd)) {
			mensaje = "Cuenta Incorrecta";
			Clients.showNotification("Credenciales Incorrectas", Clients.NOTIFICATION_TYPE_WARNING, null,
					"middle_center", 1500);
			return;
		}
		Usuario usuario = authService.getUserCredential();
		mensaje = "Bienvenido, " + usuario.getNombre();
		mensaje = "";
		Sessions.getCurrent().setAttribute("Usuario", usuario);
		Executions.sendRedirect("index.zul");
		Clients.showNotification("Bienvenido " + usuario.getNombre(), Clients.NOTIFICATION_TYPE_INFO, null,
				"middle_center", 1500);
		Sistema.idioma = Sistema.getIdioma(idioma);
		Email email = InstanciaDAO.getEmailDAO().getEmail(1);
		Sistema.datosEmail(email.getCorreo(), email.getClave(), email.getHost(), email.getPuerto());
	}
	
	@Command
	@NotifyChange("idioma")
	public void select() {
	//	Sistema.idioma = Sistema.getIdioma(idioma);
	}

	/*
	 * Get and Set
	 */

	public List<String> getListaIdioma(){
		return Sistema.getListaIdioma();
	}
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getIdioma() {
		return idioma;
	}

	public void setIdioma(String idioma) {
		this.idioma = idioma;
	}

	public void setListaIdioma(List<String> listaIdioma) {
		this.listaIdioma = listaIdioma;
	}
	
	
}
