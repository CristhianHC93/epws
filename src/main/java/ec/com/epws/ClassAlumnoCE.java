package ec.com.epws;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Alumno;
import ec.com.epws.entity.AlumnoFamilia;
import ec.com.epws.entity.Familia;
import ec.com.epws.entity.MedicaDiscapacidad;
import ec.com.epws.entity.MedicaEnfermedad;
import ec.com.epws.entity.MedicaVacuna;
import ec.com.epws.entity.ObservacionMedica;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;
import ec.com.epws.idioma.Idioma;

public class ClassAlumnoCE {
	@Wire("#winAlumnoCE")
	private Window winAlumnoCE;

	private String cedulaAlumno;

	private List<MedicaVacuna> listaVacuna;
	private List<MedicaEnfermedad> listaEnfermedad;
	private List<MedicaDiscapacidad> listaDiscapacidad;

	private Alumno alumno;
	private AlumnoFamilia padre;
	private AlumnoFamilia madre;
	private AlumnoFamilia representante;

	private boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("objeto") Alumno alumno,
			@ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(view, this, false);
		this.alumno = (alumno == null) ? new Alumno() : alumno;
		this.alumno.setInscripcion((clase.equals("inscripcion") ? true : false));
		this.alumno.setEstado(Sistema.getEstado(Sistema.ESTADO_INGRESADO));
		if (alumno != null) {
			padre = InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(alumno.getId(), Sistema.PARENTESCO_PADRE);
			madre = InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(alumno.getId(), Sistema.PARENTESCO_MADRE);
			representante = InstanciaDAO.getAlumnoFamiliaDAO().getAlumnoFamilia(alumno.getId());
		} else {
			padre = new AlumnoFamilia();
			madre = new AlumnoFamilia();
			representante = new AlumnoFamilia();
		}
	}

	@Command
	@NotifyChange({ "alumno", "padre", "madre", "representante" })
	public void guardarEditar() {
		String cedulaProvicional = alumno.getCedula();
		alumno.setCedula(cedulaAlumno);
		if (!Sistema.validarCedula(alumno.getCedula())) {
			return;
		}
		if (representante.getId() == null) {
			Messagebox.show("Escoger Representante");
			return;
		}
		if (Sistema.validarRepetido(InstanciaDAO.getAlumnoDAO().getListaAlumno(), alumno)) {
			alumno.setCedula(cedulaProvicional);
			return;
		} else {
			if (alumno.getId() == null) {
				alumno = InstanciaDAO.getAlumnoDAO().saveorUpdate(alumno);
				if (listaDiscapacidad != null) {
					listaDiscapacidad.forEach(x -> {
						x.setObservacionMedica(alumno.getObservacionMedica());
						InstanciaDAO.medicaDiscapacidadDAO.saveorUpdate(x);
					});
				}
				if (listaEnfermedad != null) {
					listaEnfermedad.forEach(x -> {
						x.setObservacionMedica(alumno.getObservacionMedica());
						InstanciaDAO.medicaEnfermedadDAO.saveorUpdate(x);
					});
				}
				if (listaVacuna != null) {
					listaVacuna.forEach(x -> {
						x.setObservacionMedica(alumno.getObservacionMedica());
						InstanciaDAO.medicaVacunaDAO.saveorUpdate(x);
					});
				}
				Clients.showNotification("Alumno ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
						"middle_center", 1500);
				bandera = true;
			} else {
				InstanciaDAO.getAlumnoDAO().saveorUpdate(alumno);
				Clients.showNotification("Alumno Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
						"middle_center", 1500);
			}
			padre.setAlumno(alumno);
			madre.setAlumno(alumno);
			padre.setRepresentante(false);
			madre.setRepresentante(false);
//			padre.setParentesco(Sistema.PADRE);
//			madre.setParentesco(Sistema.MADRE);
			representante.setAlumno(alumno);
//			representante.setParentesco(representante.getParentesco());
			InstanciaDAO.getAlumnoFamiliaDAO().saveorUpdate(padre);
			InstanciaDAO.getAlumnoFamiliaDAO().saveorUpdate(madre);
			InstanciaDAO.getAlumnoFamiliaDAO().saveorUpdate(representante);
			if (bandera) {
				alumno = new Alumno();
				padre = new AlumnoFamilia();
				madre = new AlumnoFamilia();
				representante = new AlumnoFamilia();
			}
		}
	}

	@Command
	public void agregarPadre() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Alumno");
		parametros.put("parentesco", Sistema.PARENTESCO_PADRE);
		parametros.put("objeto", padre);
		Executions.createComponents("/vista/formFamilia.zul", null, parametros);
	}

	@Command
	public void agregarMadre() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Alumno");
		parametros.put("parentesco", Sistema.PARENTESCO_MADRE);
		parametros.put("objeto", madre);
		Executions.createComponents("/vista/formFamilia.zul", null, parametros);
	}

	@Command
	public void agregarRepresentante() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Alumno");
		parametros.put("parentesco", Sistema.PARENTESCO_REPRESENTANTE);
		parametros.put("objeto", representante);
		Executions.createComponents("/vista/formFamilia.zul", null, parametros);
	}

	@NotifyChange({ "padre", "madre", "representante" })
	@GlobalCommand
	public void cargarFamilia(@BindingParam("objeto") Familia familia, @BindingParam("parentesco") Integer parentesco) {
		if (parentesco == Sistema.PARENTESCO_PADRE) {
			padre.setFamilia(familia);
		}
		if (parentesco == Sistema.PARENTESCO_MADRE) {
			madre.setFamilia(familia);
		}
		if (parentesco == Sistema.PARENTESCO_REPRESENTANTE) {
			representante.setFamilia(familia);
			representante.setRepresentante(true);
		}
	}

	@Command
	public void agregarObservacionMedica() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", alumno.getObservacionMedica());
		Executions.createComponents("/vista/formObservacionMedicaCE.zul", null, parametros);
	}

	@NotifyChange({ "alumno" })
	@GlobalCommand
	public void cargarObservacionMedica(@BindingParam("objeto") ObservacionMedica observacionMedica,
			@BindingParam("listaVacuna") List<MedicaVacuna> listaVacuna,
			@BindingParam("listaEnfermedad") List<MedicaEnfermedad> listaEnfermedad,
			@BindingParam("listaDiscapacidad") List<MedicaDiscapacidad> listaDiscapacidad) {
		alumno.setObservacionMedica(observacionMedica);
		this.listaVacuna = (listaVacuna != null) ? listaVacuna : null;
		this.listaEnfermedad = (listaEnfermedad != null) ? listaEnfermedad : null;
		this.listaDiscapacidad = (listaDiscapacidad != null) ? listaDiscapacidad : null;
	}

	@Command
	public void validar(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("nombre")) {
			alumno.setNombre(cadena.toUpperCase());
		}
		if (campo.equals("apellido")) {
			alumno.setApellido(cadena.toUpperCase());
		}
		if (campo.equals("domicilio")) {
			alumno.setDomicilio(cadena.toUpperCase());
		}
	}

	@NotifyChange("alumno")
	@Command
	public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {
		if (alumno.getCedula() == null || alumno.getCedula().equals("")) {
			Messagebox.show("Es necesario ingresar cedula");
			return;
		}
		String cedulaProvicional = alumno.getCedula();
		if (Sistema.validarRepetido(InstanciaDAO.getAlumnoDAO().getListaAlumno(), alumno)) {
			alumno.setCedula(cedulaProvicional);
			return;
		}
		Media media;
		media = event.getMedia();
		int aleatorio = ThreadLocalRandom.current().nextInt(1, 100000);
		if (Util.uploadFile(media, alumno.getCedula() + aleatorio, Sistema.carpetaFotoAlumno)) {
			new File(Util.getPathRaiz() + alumno.getFoto()).delete();
			alumno.setFoto("/" + Sistema.carpetaFotoAlumno + "/" + alumno.getCedula() + aleatorio);
		}
	}

	@Command
	public void salir() {
		BindUtils.postGlobalCommand(null, null, "refrescarlistaInscritos", null);
		winAlumnoCE.detach();
	}

	// --
	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintEntero() {
		return Sistema.CONSTRAINT_ENTERO;
	}

	public String getConstraintEnteroVacio() {
		return Sistema.CONSTRAINT_ENTERO_VACIO;
	}

	public String getConstraintLetra() {
		return Sistema.CONSTRAINT_LETRA;
	}

	public String getConstraintLetraVacio() {
		return Sistema.CONSTRAINT_LETRA_VACIO;
	}

	public String getCedulaAlumno() {
		return cedulaAlumno;
	}

	public void setCedulaAlumno(String cedulaAlumno) {
		this.cedulaAlumno = cedulaAlumno;
	}

	public Alumno getAlumno() {
		return alumno;
	}

	public void setAlumno(Alumno alumno) {
		this.alumno = alumno;
	}

	public AlumnoFamilia getPadre() {
		return padre;
	}

	public void setPadre(AlumnoFamilia padre) {
		this.padre = padre;
	}

	public AlumnoFamilia getMadre() {
		return madre;
	}

	public void setMadre(AlumnoFamilia madre) {
		this.madre = madre;
	}

	public AlumnoFamilia getRepresentante() {
		return representante;
	}

	public void setRepresentante(AlumnoFamilia representante) {
		this.representante = representante;
	}

}
