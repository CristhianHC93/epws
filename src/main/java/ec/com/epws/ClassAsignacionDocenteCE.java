package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.AsignacionDocente;
import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Docente;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassAsignacionDocenteCE {
	@Wire("#winAsignacionDocenteCE")
	private Window winAsignacionDocenteCE;

	private List<Asignatura> listaAsignatura;
	private AsignacionDocente asignacionDocente;

	private boolean asignatura;

	boolean bandera = false;

	Curso cursoThis;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") AsignacionDocente asignacionDocente, @ExecutionArgParam("curso") Curso curso) {
		Selectors.wireComponents(view, this, false);
		this.asignacionDocente = (asignacionDocente == null) ? new AsignacionDocente() : asignacionDocente;
		asignatura = (curso.getNivel().getId() == Sistema.NIVEL_MEDIO) ? true : false;
		listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(),false);
		cursoThis = curso;
	}

	@Command
	@NotifyChange("asignacionDocente")
	public void guardarEditar() {
		if (asignacionDocente.getDocente() == null) {
			Messagebox.show("Debe Escojer un docente");
			return;
		}
		bandera = true;
		if (asignacionDocente.getId() == null) {
			asignacionDocente.setAnioLectivo(Sistema.getAnioLectivoActivoEstatico());
			// Si asignatura es true significa que es nivel medio es decir
			// maestros por materias
			if (asignatura) {
				if (asignacionDocente.getAsignatura() == null) {
					Messagebox.show("Debe Escojer una Asignatura");
					return;
				}
				AsignacionDocente asignacionDoc = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(
						asignacionDocente.getAnioLectivo().getId(), asignacionDocente.getAsignatura().getId());
				if (asignacionDoc == null) {
					InstanciaDAO.asignacionDocenteDAO.saveorUpdate(asignacionDocente);
					asignacionDocente = new AsignacionDocente();
				} else {
					asd(asignacionDoc, asignacionDocente.getDocente());
					return;
				}
			} else {
				List<AsignacionDocente> lista = InstanciaDAO.asignacionDocenteDAO.getListaAsignacionDocenteByCurso(
						asignacionDocente.getAnioLectivo().getId(), cursoThis.getId());
				if (lista.size() > 0) {
					lista.forEach(x -> {
						x.setDocente(asignacionDocente.getDocente());
						InstanciaDAO.asignacionDocenteDAO.saveorUpdate(x);
					});
					List<Asignatura> listapro = listaAsignatura;
					lista.forEach(y -> {
						listapro.removeIf(x -> x.getId() == y.getAsignatura().getId());
					});
					listapro.forEach(x -> {
						AsignacionDocente ad = new AsignacionDocente();
						ad.setAnioLectivo(asignacionDocente.getAnioLectivo());
						ad.setDocente(asignacionDocente.getDocente());
						ad.setAsignatura(x);
						InstanciaDAO.asignacionDocenteDAO.saveorUpdate(ad);
					});
				} else {
					listaAsignatura.forEach(x -> {
						AsignacionDocente ad = new AsignacionDocente();
						ad.setAnioLectivo(asignacionDocente.getAnioLectivo());
						ad.setDocente(asignacionDocente.getDocente());
						ad.setAsignatura(x);
						InstanciaDAO.asignacionDocenteDAO.saveorUpdate(ad);
					});
				}
			}
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		} else {
			InstanciaDAO.asignacionDocenteDAO.saveorUpdate(asignacionDocente);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	private void asd(AsignacionDocente asignacionDoc, Docente docente) {
		String msj = "Esta Asignatura ya tiene un Docente asignado desea reemplazarlo";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						asignacionDoc.setDocente(docente);
						InstanciaDAO.asignacionDocenteDAO.saveorUpdate(asignacionDoc);
						BindUtils.postNotifyChange(null, null, ClassAsignacionDocenteCE.this, "asignacionDocente");
						Clients.showNotification("Dato modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
								"middle_center", 1500);
					}
				});
	}

	@Command
	public void agregarDocente() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Asignacion");
		Executions.createComponents("/vista/formDocente.zul", null, parametros);
	}

	@NotifyChange("asignacionDocente")
	@GlobalCommand
	public void cargarDocente(@BindingParam("objeto") Docente docente) {
		asignacionDocente.setDocente(docente);
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winAsignacionDocenteCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Window getWinAsignacionDocenteCE() {
		return winAsignacionDocenteCE;
	}

	public void setWinAsignacionDocenteCE(Window winAsignacionDocenteCE) {
		this.winAsignacionDocenteCE = winAsignacionDocenteCE;
	}

	public List<Asignatura> getListaAsignatura() {
		return listaAsignatura;
	}

	public void setListaAsignatura(List<Asignatura> listaAsignatura) {
		this.listaAsignatura = listaAsignatura;
	}

	public AsignacionDocente getAsignacionDocente() {
		return asignacionDocente;
	}

	public void setAsignacionDocente(AsignacionDocente asignacionDocente) {
		this.asignacionDocente = asignacionDocente;
	}

	public boolean isAsignatura() {
		return asignatura;
	}

	public void setAsignatura(boolean asignatura) {
		this.asignatura = asignatura;
	}

}
