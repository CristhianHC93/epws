package ec.com.epws;

import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Parcial;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassParcialCE {
	@Wire("#winParcialCE")
	private Window winParcialCE;
	private List<Quimestre> listaQuimestre;
	private Parcial parcial;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Parcial parcial) {
		Selectors.wireComponents(view, this, false);
		listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
		this.parcial = (parcial == null) ? new Parcial() : parcial;
	}

	@Command
	@NotifyChange("parcial")
	public void guardarEditar() {
		bandera = true;
		if (parcial.getId() == null) {
			InstanciaDAO.parcialDAO.saveorUpdate(parcial);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			parcial = new Parcial();
		} else {
			InstanciaDAO.parcialDAO.saveorUpdate(parcial);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("parcial")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			parcial.setDescripcion((cadena.toUpperCase()));
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winParcialCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}

	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}

	public Parcial getParcial() {
		return parcial;
	}

	public void setParcial(Parcial parcial) {
		this.parcial = parcial;
	}

}
