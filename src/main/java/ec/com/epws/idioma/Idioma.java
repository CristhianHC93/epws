package ec.com.epws.idioma;

public interface Idioma {
	//Para Modulos
	public String getSecretaria();
	public String getProfesor();
	public String getRepresentante();
	public String getUsuario();
	public String getConfiguracion();
	public String getReporte();
	public String getCerrarSesion();
	//Para item's de Modulos
	public String getSistemaFacturacion();
	public String getProducto();
	public String getCategoria();
	public String getProveedor();
	public String getNotaCompra();
	public String getCliente();
	public String getModulo();
	public String getRol();
	public String getDetalleModulo();
	public String getRolModulo();
	public String getEmail();
	public String getReporteVenta();
	
	/*
	 * Ventana Alumno
	 */
	public String getPadre();
	public String getMadre();
	public String getLugarNacimiento();
	public String getFechaNacimiento();
	public String getEscuelaAnterior();
	public String getRepresentanteMin();
	
	
	/*
	 * Ventana Familia
	 */
	public String getProfesion();
	public String getParentescoMin();
	public String getParentesco();
	
	/*
	 * Ventana Curso
	 */
	

	
	/*
	 * Ventana Observacion Medica
	 */
	public String getVacuna();
	public String getAlergia();
	public String getFractura();
	public String getCertificadoMedico();

	
	public String getHost();
	public String getPuerto();	
	public String getGuardar();
	public String getEditar();
	public String getNuevo();
	public String getAceptar();
	public String getSalir();
	public String getGenerarPdf();
	public String getGenerarPdfCurso();
	public String getGenerarExcel();
	public String getCodigo();
	public String getDescripcion();
	public String getBuscar();
	public String getOpcion();
	public String getProveedorMin();
	public String getCategoriaMin();
	public String getImpuesto();
	public String getPrecioVenta();
	public String getPrecioCompra();
	public String getStock();
	public String getStockAlerta();
	public String getStockCritico();
	public String getNoExisteInformacion();	
	public String getEmpresa();
	public String getRuc();
	public String getNombre();
	public String getTitulo();
	public String getInscripcion();
	public String getObservacionMedica();
	public String getSubirImagen();
	public String getApellido();
	public String getDireccion();
	public String getTelefono();
	public String getCelular();
	public String getCorreoPersonal();
	public String getCorreoEmpresarial();
	public String getObservacion();
	public String getAnular();	
	public String getNotaCompraMin();
	public String getHora();
	public String getFecha();
	public String getFechaRecepcion();
	public String getCantidad();
	public String getPrecio();
	public String getSubTotalMin();
	public String getAgregarItem();
	public String getSubTotal();
	public String getIva();
	public String getTotal();
	public String getFechaEmision();
	public String getCedula();
	public String getCorreo();
	public String getProforma();
	public String getClienteMin();
	public String getDescuentoPorcentaje();
	public String getFuncion();
	public String getCargo();
	public String getRolMin();
	public String getUsuarioMin();
	public String getClave();
	public String getActivo();
	public String getFechaCreacion();
	public String getEstado();	
	public String getAbreviacion();
	public String getModuloMin();
	public String getRuta();
	public String getIvaMin();
	public String getTotalMin();
	public String getListaCompra();
	public String getListaNotaCompra();
	public String getListaVenta();
	
	/*
	 * Mensajes de Pantalla
	 */

	public String getMensageCodigoRepetido();
	public String getMensageRucRepetido();
	public String getMensageCedulaRepetido();
	public String getMensageNumeroRepetido();
	public String getMensageAlumnoRepetido();
	
	public String getCalcularPromedios();
	public String getValorInvalido();
	public String getAlumno();
	public String getAlumnoMin();
	public String getAsignacion();
	public String getAsignacionMin();
	public String getTutor();
	public String getAgregarTutor();
	public String getDocente();
	public String getDocenteMin();
	public String getAsignatura();
	public String getAsignaturaMin();
	public String getAsistencia();
	public String getAsistenciaMin();
	public String getCalificacionParcial();
	public String getCalificacionParcialMin();
	public String getPromedio();
	public String getDesarrollo();
	public String getNota();
	public String getCualitativa();
	public String getCualitativaMin();
	public String getCalificacionQuimestre();
	public String getCalificacionQuimestreMin();
	public String getComportamiento();
	public String getComportamientoMin();
	public String getMaximo();
	public String getMinimo();
	public String getSiglas();
	public String getCurso();
	public String getCursoMin();
	public String getDiscapacidad();
	public String getDiscapacidadMin();
	public String getEnfermedad();
	public String getEnfermedadMin();
	public String getFamilia();
	public String getFamiliaMin();
	public String getMatricula();
	public String getMatriculaMin();
	public String getNumeroMatricula();
	public String getMes();
	public String getMesMin();
	public String getNivel();
	public String getNivelMin();
	public String getNoticia();
	public String getNoticiaMin();
	public String getResumen();
	public String getObservacionMedicaMin();
	public String getParcial();
	public String getParcialMin();
	public String getQuimestre();
	public String getQuimestreMin();
	public String getPension();
	public String getPensionMin();
	public String getCertificadoPromocion();
	public String getCertificadoPromocionMin();
}
