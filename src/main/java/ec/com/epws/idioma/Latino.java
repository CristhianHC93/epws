package ec.com.epws.idioma;

public class Latino implements Idioma {

	// Label SubMenu's
	public static final String PRODUCTO = "PRODUCTO";
	public static final String CATEGORIA = "CATEGORÍA";
	public static final String PROVEEDOR = "PROVEEDOR";
	public static final String NOTA_COMPRA = "NOTA COMPRA";
	public static final String CLIENTE = "CLIENTE";
	public static final String MODULO = "MÓDULO";
	public static final String ROL = "ROL";
	public static final String DETALLE_MODULO = "DETALLE MÓDULO";
	public static final String ROL_MODULO = "ROL MÓDULO";
	public static final String EMAIL = "EMAIL";
	public static final String REPORTE_VENTA = "REPORTE VENTA";
	// Label Botones
	public static final String HOST = "Host";
	public static final String PUERTO = "Puerto";
	public static final String GUARDAR = "Guardar";
	public static final String EDITAR = "Guardar Cambios";
	public static final String NUEVO = "Nuevo";
	public static final String ACEPTAR = "Aceptar";
	public static final String SALIR = "Salir";
	public static final String GENERAR_PDF = "Generar PDF";
	public static final String GENERAR_PDF_CURSO = "Generar PDF Curso";
	public static final String GENERAR_EXCEL = "Generar EXCEL";
	// Label generales
	public static final String CODIGO = "Código *";
	public static final String DESCRIPCION = "Descripción *";
	public static final String BUSCAR = "Buscar";
	public static final String OPCION = "Opción";
	public static final String PROVEEDOR_MIN = "Proveedor *";
	public static final String CATEGORIA_MIN = "Categoría *";
	public static final String IMPUESTO = "Impuesto *";
	public static final String PRECIO_VENTA = "Precio Venta *";
	public static final String PRECIO_COMPRA = "Precio Compra *";
	public static final String STOCK = "Stock *";
	public static final String STOCK_ALERTA = "StockAlerta *";
	public static final String STOCK_CRITICO = "StockCritico *";
	public static final String NO_EXISTE_INFORMACION = "No existe Información";
	public static final String EMPRESA = "Empresa *";
	public static final String RUC = "RUC *";
	public static final String NOMBRE = "Nombre *";
	public static final String APELLIDO = "Apellido *";
	public static final String DIRECCION = "Dirección";
	public static final String TELEFONO = "Teléfono";
	public static final String CELULAR = "Celular";
	public static final String CORREO_PERSONAL = "Correo Personal";
	public static final String CORREO_EMPRESARIAL = "Correo Empresarial";
	public static final String OBSERVACION = "Observación";
	public static final String ANULAR = "Anular";
	public static final String NOTA_COMPRA_MIN = "Nota Compra";
	public static final String HORA = "Hora";
	public static final String FECHA = "Fecha";
	public static final String FECHA_RECEPCION = "Fecha Recepción";
	public static final String FECHA_EMISION = "Fecha Emisión";
	public static final String CANTIDAD = "Cantidad";
	public static final String PRECIO = "Precio";
	public static final String SUB_TOTAL_MIN = "SubTotal";
	public static final String AGREGAR_ITEM = "Agregar Item";
	public static final String SUB_TOTAL = "SUBTOTAL";
	public static final String IVA = "IVA";
	public static final String TOTAL = "TOTAL";
	public static final String CEDULA = "Cédula *";
	public static final String CORREO = "Correo *";
	public static final String PROFORMA = "Proforma";
	public static final String CLIENTE_MIN = "Cliente";
	public static final String DESCUENTO_PORCENTAJE = "Descuento %";
	public static final String FUNCION = "Función";
	public static final String CARGO = "Cargo";
	public static final String ROL_MIN = "Rol *";
	public static final String USUARIO_MIN = "Usuario *";
	public static final String CLAVE = "Clave *";
	public static final String ACTIVO = "Activo";
	public static final String FECHA_CREACION = "Fecha Creación";
	public static final String ESTADO = "Estado";
	public static final String ABREVIACION = "Abreviación";
	public static final String MODULO_MIN = "Módulo";
	public static final String RUTA = "Ruta";
	public static final String IVA_MIN = "Iva";
	public static final String TOTAL_MIN = "Total";

	public static final String LISTA_COMPRA = "LISTA COMPRA";
	public static final String LISTA_NOTA_COMPRA = "LISTA NOTA COMPRA";
	public static final String LISTA_VENTA = "LISTA VENTA";

	public static final String MENSAJE_CODIGO_REPETIDO = "Este Código ya ha sido asignado";
	public static final String MENSAJE_RUC_REPETIDO = "Este Ruc ya ha sido asignado";
	public static final String MENSAJE_CEDULA_REPETIDO = "Esta Cedula ya ha sido asignado";

	public static final String CALCULAR_PROMEDIOS = "Calcular Promedios";
	public static final String TITULO = "Título *";
	public static final String INSCRIPCION = "Inscripción ";
	public static final String ASIGNACION = "ASIGNACIÓN ";
	public static final String ASIGNACION_MIN = "Asignación ";
	public static final String TUTOR = "Tutor ";
	public static final String AGREGAR_TUTOR = "Agregar Tutor ";
	public static final String CURSO = "CURSO ";
	public static final String CURSO_MIN = "Curso ";
	public static final String DOCENTE = "DOCENTE ";
	public static final String DOCENTE_MIN = "Docente ";
	public static final String ASIGNATURA = "ASIGNATURA ";
	public static final String ASIGNATURA_MIN = "Asignatura ";
	public static final String ASISTENCIA = "ASISTENCIA ";
	public static final String ASISTENCIA_MIN = "Asistencia ";
	public static final String ALUMNO = "ALUMNO ";
	public static final String ALUMNO_MIN = "Alumno ";
	public static final String CALIFICACION_PARCIAL = "CALIFICACIÓN PARCIAL ";
	public static final String CALIFICACION_PARCIAL_MIN = "Calificación Parcial ";
	public static final String PROMEDIO = "Promedio ";
	public static final String DESARROLLO = "Desarrollo ";
	public static final String CUALITATIVA = "CUALITATIVA ";
	public static final String CUALITATIVAMIN = "Cualitativa ";
	public static final String NOTA = "Nota ";
	public static final String MAXIMO = "Máximo * ";
	public static final String MINIMO = "Mínimo * ";
	public static final String SIGLAS = "Siglas * ";
	public static final String CALIFICACION_QUIMESTRE = "CALIFICACIÓN QUIMESTRE ";
	public static final String CALIFICACION_QUIMESTRE_MIN = "Calificación Quimestre ";
	public static final String COMPORTAMIENTO = "COMPORTAMIENTO ";
	public static final String COMPORTAMIENTO_MIN = "Comportamiento ";
	public static final String DISCAPACIDAD = "DISCAPACIDAD ";
	public static final String DISCAPACIDAD_MIN = "Discapacidad ";
	public static final String ENFERMEDAD = "ENFERMEDAD ";
	public static final String ENFERMEDAD_MIN = "Enfermedad ";
	public static final String FAMILIA = "FAMILIA ";
	public static final String FAMILIA_MIN = "Familia ";
	public static final String MATRICULA = "MATRICULA ";
	public static final String MATRICULA_MIN = "Matricula ";
	public static final String NUMERO_MATRICULA = "Número de Matricula ";
	public static final String MES = "MES ";
	public static final String MES_MIN = "Mes ";
	public static final String NOTICIA = "NOTICIA ";
	public static final String NOTICIA_MIN = "Noticia ";
	public static final String RESUMEN = "Resumen ";
	public static final String PARCIAL = "PARCIAL ";
	public static final String PARCIAL_MIN = "Parcial ";
	public static final String QUIMESTRE = "QUIMESTRE ";
	public static final String QUIMESTRE_MIN = "Quimestre ";
	public static final String PENSION = "PENSIÓN ";
	public static final String PENSION_MIN = "Pensión ";
	public static final String CERTIFICADO_PROMOCION = "CERTIFICADO PROMOCIÓN ";
	public static final String CERTIFICADO_PROMOCION_MIN = "Certificado Promoción ";

	// NOMBRE DEL SISTEMA
	@Override
	public String getSistemaFacturacion() {
		return "William Shakespeare ";
	}

	// MENU'S
	@Override
	public String getSecretaria() {
		return "SECRETARIA";
	}

	@Override
	public String getProfesor() {
		return "PROFESOR";
	}

	@Override
	public String getRepresentante() {
		return "REPRESENTANTE";
	}

	@Override
	public String getSubirImagen() {
		return "Subir Imagen";
	}

	@Override
	public String getObservacionMedica() {
		return "OBSERVACIÓN MÉDICA";
	}

	@Override
	public String getObservacionMedicaMin() {
		return "Observación Médica";
	}

	@Override
	public String getUsuario() {
		return "USUARIO";
	}

	@Override
	public String getConfiguracion() {
		return "CONFIGURACIÓN";
	}

	@Override
	public String getReporte() {
		return "REPORTE";
	}

	@Override
	public String getCerrarSesion() {
		return "CERRAR SESIÓN";
	}

	@Override
	public String getLugarNacimiento() {
		return "Lugar de Nacimiento *";
	}

	@Override
	public String getFechaNacimiento() {
		return "Fecha de Nacimiento *";
	}

	@Override
	public String getValorInvalido() {
		return "Valor Inválido";
	}

	@Override
	public String getEscuelaAnterior() {
		return "Escuela Anterior";
	}

	@Override
	public String getProfesion() {
		return "Profesión";
	}

	@Override
	public String getParentescoMin() {
		return "Parentesco";
	}

	@Override
	public String getParentesco() {
		return "PARENTESCO";
	}

	@Override
	public String getPadre() {
		return "Padre";
	}

	@Override
	public String getMadre() {
		return "Madre";
	}

	@Override
	public String getRepresentanteMin() {
		return "Representante";
	}

	@Override
	public String getNivel() {
		return "NIVEL";
	}

	@Override
	public String getNivelMin() {
		return "Nivel";
	}

	@Override
	public String getVacuna() {
		return "Vacuna";
	}

	@Override
	public String getAlergia() {
		return "Alergia";
	}

	@Override
	public String getFractura() {
		return "Fractura";
	}

	@Override
	public String getCertificadoMedico() {
		return "Certificado médico";
	}

	@Override
	public String getCalcularPromedios() {
		return CALCULAR_PROMEDIOS;
	}

	@Override
	public String getTitulo() {
		return TITULO;
	}

	@Override
	public String getInscripcion() {
		return INSCRIPCION;
	}

	@Override
	public String getAlumno() {
		return ALUMNO;
	}

	@Override
	public String getAlumnoMin() {
		return ALUMNO_MIN;
	}

	@Override
	public String getAsignacion() {
		return ASIGNACION;
	}

	@Override
	public String getAsignacionMin() {
		return ASIGNACION_MIN;
	}

	@Override
	public String getTutor() {
		return TUTOR;
	}

	@Override
	public String getAgregarTutor() {
		return AGREGAR_TUTOR;
	}

	@Override
	public String getCurso() {
		return CURSO;
	}

	@Override
	public String getCursoMin() {
		return CURSO_MIN;
	}

	@Override
	public String getDocente() {
		return DOCENTE;
	}

	@Override
	public String getDocenteMin() {
		return DOCENTE_MIN;
	}

	@Override
	public String getAsignatura() {
		return ASIGNATURA;
	}

	@Override
	public String getAsignaturaMin() {
		return ASIGNATURA_MIN;
	}

	@Override
	public String getAsistencia() {
		return ASISTENCIA;
	}

	@Override
	public String getAsistenciaMin() {
		return ASISTENCIA_MIN;
	}

	@Override
	public String getCalificacionParcial() {
		return CALIFICACION_PARCIAL;
	}

	@Override
	public String getCalificacionParcialMin() {
		return CALIFICACION_PARCIAL_MIN;
	}

	@Override
	public String getPromedio() {
		return PROMEDIO;
	}

	@Override
	public String getDesarrollo() {
		return DESARROLLO;
	}

	@Override
	public String getCualitativa() {
		return CUALITATIVA;
	}

	@Override
	public String getCualitativaMin() {
		return CUALITATIVAMIN;
	}

	@Override
	public String getNota() {
		return NOTA;
	}

	public String getMaximo() {
		return MAXIMO;
	}

	public String getMinimo() {
		return MINIMO;
	}

	public String getSiglas() {
		return SIGLAS;
	}

	@Override
	public String getCalificacionQuimestre() {
		return CALIFICACION_QUIMESTRE;
	}

	@Override
	public String getCalificacionQuimestreMin() {
		return CALIFICACION_QUIMESTRE_MIN;
	}

	@Override
	public String getComportamiento() {
		return COMPORTAMIENTO;
	}

	@Override
	public String getComportamientoMin() {
		return COMPORTAMIENTO_MIN;
	}

	@Override
	public String getDiscapacidad() {
		return DISCAPACIDAD;
	}

	@Override
	public String getDiscapacidadMin() {
		return DISCAPACIDAD_MIN;
	}

	@Override
	public String getEnfermedad() {
		return ENFERMEDAD;
	}

	@Override
	public String getEnfermedadMin() {
		return ENFERMEDAD_MIN;
	}

	@Override
	public String getFamilia() {
		return FAMILIA;
	}

	@Override
	public String getFamiliaMin() {
		return FAMILIA_MIN;
	}

	@Override
	public String getMatricula() {
		return MATRICULA;
	}

	@Override
	public String getMatriculaMin() {
		return MATRICULA_MIN;
	}

	@Override
	public String getNumeroMatricula() {
		return NUMERO_MATRICULA;
	}

	@Override
	public String getMes() {
		return MES;
	}

	@Override
	public String getMesMin() {
		return MES_MIN;
	}

	@Override
	public String getNoticia() {
		return NOTICIA;
	}

	@Override
	public String getNoticiaMin() {
		return NOTICIA_MIN;
	}

	@Override
	public String getResumen() {
		return RESUMEN;
	}

	@Override
	public String getParcial() {
		return PARCIAL;
	}

	@Override
	public String getParcialMin() {
		return PARCIAL_MIN;
	}

	@Override
	public String getQuimestre() {
		return QUIMESTRE;
	}

	@Override
	public String getQuimestreMin() {
		return QUIMESTRE_MIN;
	}

	@Override
	public String getPension() {
		return PENSION;
	}

	@Override
	public String getPensionMin() {
		return PENSION_MIN;
	}

	@Override
	public String getCertificadoPromocion() {
		return CERTIFICADO_PROMOCION;
	}

	@Override
	public String getCertificadoPromocionMin() {
		return CERTIFICADO_PROMOCION_MIN;
	}

	// --
	@Override
	public String getProducto() {
		return PRODUCTO;
	}

	public String getCategoria() {
		return CATEGORIA;
	}

	@Override
	public String getProveedor() {
		return PROVEEDOR;
	}

	@Override
	public String getNotaCompra() {
		return NOTA_COMPRA;
	}

	@Override
	public String getCliente() {
		return CLIENTE;
	}

	@Override
	public String getModulo() {
		return MODULO;
	}

	@Override
	public String getRol() {
		return ROL;
	}

	@Override
	public String getDetalleModulo() {
		return DETALLE_MODULO;
	}

	@Override
	public String getRolModulo() {
		return ROL_MODULO;
	}

	@Override
	public String getEmail() {
		return EMAIL;
	}

	@Override
	public String getHost() {
		return HOST;
	}

	@Override
	public String getPuerto() {
		return PUERTO;
	}

	@Override
	public String getGuardar() {
		return GUARDAR;
	}

	@Override
	public String getEditar() {
		return EDITAR;
	}

	@Override
	public String getNuevo() {
		return NUEVO;
	}

	@Override
	public String getAceptar() {
		return ACEPTAR;
	}

	@Override
	public String getSalir() {
		return SALIR;
	}

	@Override
	public String getGenerarPdf() {
		return GENERAR_PDF;
	}

	@Override
	public String getGenerarPdfCurso() {
		return GENERAR_PDF_CURSO;
	}

	@Override
	public String getGenerarExcel() {
		return GENERAR_EXCEL;
	}

	@Override
	public String getCodigo() {
		return CODIGO;
	}

	@Override
	public String getDescripcion() {
		return DESCRIPCION;
	}

	@Override
	public String getBuscar() {
		return BUSCAR;
	}

	@Override
	public String getOpcion() {
		return OPCION;
	}

	@Override
	public String getProveedorMin() {
		return PROVEEDOR_MIN;
	}

	@Override
	public String getCategoriaMin() {
		return CATEGORIA_MIN;
	}

	@Override
	public String getImpuesto() {
		return IMPUESTO;
	}

	@Override
	public String getPrecioVenta() {
		return PRECIO_VENTA;
	}

	@Override
	public String getPrecioCompra() {
		return PRECIO_COMPRA;
	}

	@Override
	public String getStock() {
		return STOCK;
	}

	@Override
	public String getStockAlerta() {
		return STOCK_ALERTA;
	}

	@Override
	public String getStockCritico() {
		return STOCK_CRITICO;
	}

	@Override
	public String getNoExisteInformacion() {
		return NO_EXISTE_INFORMACION;
	}

	@Override
	public String getEmpresa() {
		return EMPRESA;
	}

	@Override
	public String getRuc() {
		return RUC;
	}

	@Override
	public String getNombre() {
		return NOMBRE;
	}

	@Override
	public String getApellido() {
		return APELLIDO;
	}

	@Override
	public String getDireccion() {
		return DIRECCION;
	}

	@Override
	public String getTelefono() {
		return TELEFONO;
	}

	@Override
	public String getCelular() {
		return CELULAR;
	}

	@Override
	public String getCorreoPersonal() {
		return CORREO_PERSONAL;
	}

	@Override
	public String getCorreoEmpresarial() {
		return CORREO_EMPRESARIAL;
	}

	@Override
	public String getObservacion() {
		return OBSERVACION;
	}

	@Override
	public String getAnular() {
		return ANULAR;
	}

	@Override
	public String getNotaCompraMin() {
		return NOTA_COMPRA_MIN;
	}

	@Override
	public String getHora() {
		return HORA;
	}

	@Override
	public String getFecha() {
		return FECHA;
	}

	@Override
	public String getFechaRecepcion() {
		return FECHA_RECEPCION;
	}

	@Override
	public String getCantidad() {
		return CANTIDAD;
	}

	@Override
	public String getPrecio() {
		return PRECIO;
	}

	@Override
	public String getSubTotalMin() {
		return SUB_TOTAL_MIN;
	}

	@Override
	public String getAgregarItem() {
		return AGREGAR_ITEM;
	}

	@Override
	public String getSubTotal() {
		return SUB_TOTAL;
	}

	@Override
	public String getIva() {
		return IVA;
	}

	@Override
	public String getTotal() {
		return TOTAL;
	}

	@Override
	public String getFechaEmision() {
		return FECHA_EMISION;
	}

	@Override
	public String getCedula() {
		return CEDULA;
	}

	@Override
	public String getCorreo() {
		return CORREO;
	}

	@Override
	public String getProforma() {
		return PROFORMA;
	}

	@Override
	public String getClienteMin() {
		return CLIENTE_MIN;
	}

	@Override
	public String getDescuentoPorcentaje() {
		return DESCUENTO_PORCENTAJE;
	}

	@Override
	public String getFuncion() {
		return FUNCION;
	}

	@Override
	public String getCargo() {
		return CARGO;
	}

	@Override
	public String getRolMin() {
		return ROL_MIN;
	}

	@Override
	public String getUsuarioMin() {
		return USUARIO_MIN;
	}

	@Override
	public String getClave() {
		return CLAVE;
	}

	@Override
	public String getActivo() {
		return ACTIVO;
	}

	@Override
	public String getFechaCreacion() {
		return FECHA_CREACION;
	}

	@Override
	public String getEstado() {
		return ESTADO;
	}

	@Override
	public String getAbreviacion() {
		return ABREVIACION;
	}

	@Override
	public String getModuloMin() {
		return MODULO_MIN;
	}

	@Override
	public String getRuta() {
		return RUTA;
	}

	@Override
	public String getIvaMin() {
		return IVA_MIN;
	}

	@Override
	public String getTotalMin() {
		return TOTAL_MIN;
	}

	@Override
	public String getListaCompra() {
		return LISTA_COMPRA;
	}

	@Override
	public String getListaNotaCompra() {
		return LISTA_NOTA_COMPRA;
	}

	@Override
	public String getListaVenta() {
		return LISTA_VENTA;
	}

	@Override
	public String getMensageCodigoRepetido() {
		return MENSAJE_CODIGO_REPETIDO;
	}

	@Override
	public String getMensageRucRepetido() {
		return MENSAJE_RUC_REPETIDO;
	}

	@Override
	public String getMensageCedulaRepetido() {
		return MENSAJE_CEDULA_REPETIDO;
	}

	@Override
	public String getMensageNumeroRepetido() {
		return "Este Número de Matrícula ya ha sido asignado";
	}

	@Override
	public String getMensageAlumnoRepetido() {
		return "Este Alumno ya ha sido matriculado";
	}

	@Override
	public String getReporteVenta() {
		return REPORTE_VENTA;
	}

}
