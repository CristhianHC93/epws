package ec.com.epws.idioma;

public class Ingles  {

	/*
	 * Label de Menu's
	 */
	public static final String INVENTARIO = "Inventory";
	public static final String COMPRA = "Purchase";
	public static final String VENTA = "Sale";
	public static final String USUARIO = "User";
	public static final String CERRARSESION = "Sign off";
	public static final String SISTEMAfACTURACION = "Facturation system";
}
