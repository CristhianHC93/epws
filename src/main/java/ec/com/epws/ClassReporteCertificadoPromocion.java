package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.CuadroCalificacion;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Matricula;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
//import ec.com.epws.models.CertificadoPromocion;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassReporteCertificadoPromocion {

	private List<Curso> listaCurso;
	private Curso curso;
	private List<CuadroCalificacion> listaCertificadoPromocion = new ArrayList<>();

	private Matricula matricula;
	private List<Matricula> listaMatricula;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("curso") Curso curso,
			@ExecutionArgParam("matricula") Matricula matricula,
			@ExecutionArgParam("lista") List<CuadroCalificacion> listaCertificadoPromocion,
			@ExecutionArgParam("listaMatricula") List<Matricula> listaMatricula) {
		Selectors.wireComponents(vista, this, false);
		if (curso != null) {
			this.curso = curso;
			this.matricula = matricula;
			this.listaMatricula = listaMatricula;
			if (listaCertificadoPromocion != null) {
				this.listaCertificadoPromocion = listaCertificadoPromocion;
			}
		} else {
			listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
			this.curso = listaCurso.get(0);
		}
	}

	double nota = 0.0;
	double promedioCualitativa = 0.0;
	boolean inicial = false;
	boolean basico = false;
	boolean materia = false;
	boolean comportamiento = false;

	@NotifyChange("listaCertificadoPromocion")
	@Command
	public List<CuadroCalificacion> buscar(Matricula matricula) {
		listaCertificadoPromocion.clear();
		listaCertificadoPromocion = InstanciaDAO.cuadroCalificacionDAO
				.getListaCuadroCalificacionByMatricula(matricula.getId());
		return listaCertificadoPromocion;
	}

	@Command
	public void agregarAlumno() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Pension");
		parametros.put("curso", curso.getId());
		Executions.createComponents("/vista/formMatricula.zul", null, parametros);
	}

	@NotifyChange({ "matricula", "listaCertificadoPromocion" })
	@GlobalCommand
	public void cargarAlumno(@BindingParam("objeto") Matricula matricula) {
		this.matricula = matricula;
		buscar(matricula);
	}

	@Command
	public void generarExcel() {
		if (listaCertificadoPromocion.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}

		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("cursoPromovido", InstanciaDAO.getCursoDAO().getCursoById(curso.getId() + 1).getDescripcion());
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		mapreporte.put("fecha", "Manta, " + Sistema.getFechaActual());
		mapreporte.put("alumno", matricula.getAlumno().getNombre() + " " + matricula.getAlumno().getApellido());
		calcular(listaCertificadoPromocion);
		mapreporte.put("aprobado", (promedioNota >= 7.0) ? true : false);
		mapreporte.put("nota", String.valueOf(promedioNota));
		mapreporte.put("letra", Sistema.numeroAletras(promedioNota));
		Sistema.reporteEXCEL(Sistema.CERTIFICADO_PROMOCION_JASPER, "Certificado", mapreporte,
				Sistema.getCertificadoPromocionDataSource(listaCertificadoPromocion), null, false);
	}

	boolean bandera = false;

	@Command
	public void generarPdf() {
		if (listaCertificadoPromocion.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaCertificadoPromocion);
		parametros.put("curso", curso);
		parametros.put("matricula", matricula);
		Executions.createComponents("/reporte/impresionCertificadoPromocion.zul", null, parametros);
	}

	@Command
	public void generarPdfTodo() {
		List<Matricula> listaMatricula = InstanciaDAO.matriculaDAO
				.getListaMatriculaAnioActivoByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());
		if (listaMatricula.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}

		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("listaMatricula", listaMatricula);
		parametros.put("curso", curso);
		Executions.createComponents("/reporte/impresionCertificadoPromocion.zul", null, parametros);
	}

	AMedia amedia;
	double promedioNota = 0.0;

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());

		if (curso.getId() < 8) {
			bandera = true;
		}
		mapreporte.put("cursoPromovido",
				(bandera) ? InstanciaDAO.getCursoDAO().getCursoById(curso.getId() + 1).getDescripcion() : "");
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		mapreporte.put("fecha", "Manta, " + Sistema.getFechaActual());
		if (listaMatricula != null) {
			List<AMedia> lista = new ArrayList<>();
			listaMatricula.forEach(x -> {
				mapreporte.put("alumno", x.getAlumno().getNombre() + " " + x.getAlumno().getApellido());
				listaCertificadoPromocion = buscar(x);
				calcular(listaCertificadoPromocion);
				mapreporte.put("aprobado", (bandera) ? ((promedioNota >= 7.0) ? true : false) : false);
				mapreporte.put("nota", String.valueOf(promedioNota));
				mapreporte.put("letra", Sistema.numeroAletras(promedioNota));
				amedia = Sistema.reportePDF(Sistema.CERTIFICADO_PROMOCION_JASPER, mapreporte,
						Sistema.getCertificadoPromocionDataSource((listaCertificadoPromocion)));
				lista.add(amedia);
			});
			iframe.setContent(Sistema.reportePDF(lista));
		} else {
			mapreporte.put("alumno", matricula.getAlumno().getNombre() + " " + matricula.getAlumno().getApellido());
			calcular(listaCertificadoPromocion);
			mapreporte.put("aprobado", (bandera) ? ((promedioNota >= 7.0) ? true : false) : false);
			mapreporte.put("nota", String.valueOf(promedioNota));
			mapreporte.put("letra", Sistema.numeroAletras(promedioNota));
			amedia = Sistema.reportePDF(Sistema.CERTIFICADO_PROMOCION_JASPER, mapreporte,
					Sistema.getCertificadoPromocionDataSource(listaCertificadoPromocion));
			iframe.setContent(amedia);
		}
	}

	private void calcular(List<CuadroCalificacion> lista) {
		lista.forEach(x -> {
			if (x.getAsignatura().getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_BASICO && x.getPromedio() > 0.0) {
				promedioNota += x.getPromedio();
			}
		});
		// List<Asignatura> l =
		// InstanciaDAO.asignaturaDAO.getListaAsignaturaBasica(curso.getId());
		double ad = InstanciaDAO.asignaturaDAO.countAsignatura(curso.getId());
		promedioNota = Sistema.redondear(promedioNota / ad);
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<CuadroCalificacion> getListaCertificadoPromocion() {
		return listaCertificadoPromocion;
	}

	public void setListaCertificadoPromocion(List<CuadroCalificacion> listaCertificadoPromocion) {
		this.listaCertificadoPromocion = listaCertificadoPromocion;
	}

	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

}
