package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Parentesco;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassParentesco {
	@Wire("#winParentesco")
	private Window winParentesco;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Parentesco> listaParentesco;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaParentesco = InstanciaDAO.getParentescoDAO().getListaParentesco();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formParentescoCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaParentesco")
	public void eliminar(@BindingParam("parentesco") final Parentesco parentesco) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + parentesco.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.getParentescoDAO().eliminar(parentesco);
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaParentesco.clear();
							listaParentesco = InstanciaDAO.getParentescoDAO().getListaParentesco();
							BindUtils.postNotifyChange(null, null, ClassParentesco.this, "listaParentesco");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassParentesco.this, "listaParentesco");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("parentesco") Parentesco parentesco) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", parentesco);
		BindUtils.postGlobalCommand(null, null, "cargarParentesco", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaParentesco")
	public void refrescarlista() {
		listaParentesco = InstanciaDAO.getParentescoDAO().getListaParentesco();
	}

	@Command
	public void editar(@BindingParam("parentesco") Parentesco parentesco) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", parentesco);
		Executions.createComponents("/vista/formParentescoCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaParentesco")
	public void filtrar() {
		listaParentesco.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaParentesco.addAll(InstanciaDAO.getParentescoDAO().getListaParentesco());
		} else {
			for (Parentesco item : InstanciaDAO.getParentescoDAO().getListaParentesco()) {
				if (item.getDescripcion().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
						) {
					listaParentesco.add(item);
				}
			}
		}
	}

	@Command
	public void salir() {
		winParentesco.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Parentesco> getListaParentesco() {
		return listaParentesco;
	}

	public void setListaParentesco(List<Parentesco> listaParentesco) {
		this.listaParentesco = listaParentesco;
	}

	
}
