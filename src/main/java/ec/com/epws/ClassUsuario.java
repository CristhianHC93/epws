package ec.com.epws;

import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Checkbox;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.Usuario;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.entity.Rol;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.sistema.Sistema;

public class ClassUsuario {

	private String btGuardarEditar = Sistema.idioma.getGuardar();
	private List<Usuario> listaUsuario;
	private Usuario usuario;
	private List<Rol> listaRol;
	private String password;
	private String passwordRepetir;
	private String claveRepetida;
	boolean validarRepetido = false;

	// private Estado estado = InstanciaEntity.getInstanciaEstado();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaUsuario = InstanciaDAO.usuarioDAO.getListaUsuario();
		listaRol = InstanciaDAO.getRolDAO().getListaRol(Sistema.ESTADO_ACTIVAR_USUARIO);
		usuario = new Usuario();
	}

	@Command
	@NotifyChange({ "listaUsuario", "claveRepetida", "password", "passwordRepetir", "listaUsuario", "usuario",
			"btGuardarEditar" })
	public void guardarEditar() {
		validarPassword();
		if (!validarRepetido) {
			Messagebox.show("Claves no coinciden");
			return;
		}
		if (Sistema.validarRepetido(InstanciaDAO.usuarioDAO.getListaUsuario(), usuario)) {
			return;
		}
		usuario.setPassword(Sistema.Encriptar(password));
		usuario.setToken(Sistema.generarToken(usuario.getUsername()));
		if (usuario.getId() == null) {
			usuario.setEstado(true);
			listaUsuario.clear();
			InstanciaDAO.usuarioDAO.saveorUpdate(usuario);
			Clients.showNotification("Usuario ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		} else {
			InstanciaDAO.usuarioDAO.saveorUpdate(usuario);
			Clients.showNotification("Usuario Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
		listaUsuario = InstanciaDAO.usuarioDAO.getListaUsuario();
		nuevo();
	}

	@Command
	@NotifyChange({ "password", "passwordRepetir", "listaUsuario", "usuario", "btGuardarEditar" })
	public void nuevo() {
		usuario = new Usuario();
		btGuardarEditar = Sistema.idioma.getGuardar();
		password = "";
		passwordRepetir = "";
	}

	@Command
	@NotifyChange({ "password", "passwordRepetir", "btGuardarEditar" })
	public void desencriptar(@BindingParam("item") Usuario usuario) {
		try {
			password = Sistema.desencriptar(usuario.getPassword());
			passwordRepetir = password;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btGuardarEditar = Sistema.idioma.getEditar();
	}

	@Command
	public void check(@BindingParam("item") Usuario usuario) {
		InstanciaDAO.usuarioDAO.saveorUpdate(usuario);
	}

	@Command
	public void admin(@BindingParam("item") Usuario usuario, @BindingParam("check") Checkbox check) {
		if (usuario.getId() == 1) {
			check.setDisabled(true);
		}
	}

	@NotifyChange("claveRepetida")
	@Command
	public void validarPassword() {
		if (passwordRepetir != null && password != null && !passwordRepetir.trim().equals("")
				&& !password.trim().equals("")) {
			if (!password.trim().equals(passwordRepetir.trim())) {
				validarRepetido = false;
				claveRepetida = "Claves no coinciden";
			} else {
				validarRepetido = true;
				claveRepetida = "";
			}
		}
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public List<Rol> getListaRol() {
		return listaRol;
	}

	public void setListaRol(List<Rol> listaRol) {
		this.listaRol = listaRol;
	}

	public String getPasswordRepetir() {
		return passwordRepetir;
	}

	public void setPasswordRepetir(String passwordRepetir) {
		this.passwordRepetir = passwordRepetir;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getBtGuardarEditar() {
		return btGuardarEditar;
	}

	public void setBtGuardarEditar(String btGuardarEditar) {
		this.btGuardarEditar = btGuardarEditar;
	}

	public String getClaveRepetida() {
		return claveRepetida;
	}

	public void setClaveRepetida(String claveRepetida) {
		this.claveRepetida = claveRepetida;
	}

}
