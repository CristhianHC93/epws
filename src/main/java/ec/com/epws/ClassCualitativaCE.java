package ec.com.epws;

import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.CodigoCualitativa;
import ec.com.epws.entity.Cualitativa;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassCualitativaCE {
	@Wire("#winCualitativaCE")
	private Window winCualitativaCE;
	private Cualitativa cualitativa;
	private List<CodigoCualitativa> listaCodigoCualitativa;
	private CodigoCualitativa codigoCualitativa;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Cualitativa cualitativa) {
		Selectors.wireComponents(view, this, false);
		listaCodigoCualitativa = Sistema.getListaCodigoCualitativa();
		if (cualitativa == null) {
			this.cualitativa = new Cualitativa();
			codigoCualitativa = listaCodigoCualitativa.get(Sistema.FILTRO_CUALITATIVO_BASICO - 1);
		} else {
			this.cualitativa = cualitativa;
			codigoCualitativa = listaCodigoCualitativa.get(cualitativa.getCodigoCualitativa().getId() - 1);
		}
	}

	@Command
	@NotifyChange("cualitativa")
	public void guardarEditar() {
		bandera = true;
		cualitativa.setCodigoCualitativa(codigoCualitativa);
		if (cualitativa.getId() == null) {
			InstanciaDAO.cualitativaDAO.saveorUpdate(cualitativa);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			cualitativa = new Cualitativa();
		} else {
			InstanciaDAO.cualitativaDAO.saveorUpdate(cualitativa);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("cualitativa")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			cualitativa.setDescripcion((cadena.toUpperCase()));
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winCualitativaCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintEntero() {
		return Sistema.CONSTRAINT_ENTERO;
	}

	public String getConstraintEnteroVacio() {
		return Sistema.CONSTRAINT_ENTERO_VACIO;
	}

	public String getConstraintLetra() {
		return Sistema.CONSTRAINT_LETRA;
	}

	public String getConstraintLetraVacio() {
		return Sistema.CONSTRAINT_LETRA_VACIO;
	}

	public Cualitativa getCualitativa() {
		return cualitativa;
	}

	public void setCualitativa(Cualitativa cualitativa) {
		this.cualitativa = cualitativa;
	}

	public List<CodigoCualitativa> getListaCodigoCualitativa() {
		return listaCodigoCualitativa;
	}

	public void setListaCodigoCualitativa(List<CodigoCualitativa> listaCodigoCualitativa) {
		this.listaCodigoCualitativa = listaCodigoCualitativa;
	}

	public CodigoCualitativa getCodigoCualitativa() {
		return codigoCualitativa;
	}

	public void setCodigoCualitativa(CodigoCualitativa codigoCualitativa) {
		this.codigoCualitativa = codigoCualitativa;
	}

}
