package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Discapacidad;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassDiscapacidad {
	@Wire("#winDiscapacidad")
	private Window winDiscapacidad;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Discapacidad> listaDiscapacidad;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaDiscapacidad = InstanciaDAO.discapacidadDAO.getListaDiscapacidad();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formDiscapacidadCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaDiscapacidad")
	public void eliminar(@BindingParam("discapacidad") final Discapacidad discapacidad) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + discapacidad.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.discapacidadDAO.eliminar(discapacidad);
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaDiscapacidad.clear();
							listaDiscapacidad = InstanciaDAO.discapacidadDAO.getListaDiscapacidad();
							BindUtils.postNotifyChange(null, null, ClassDiscapacidad.this, "listaDiscapacidad");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassDiscapacidad.this, "listaDiscapacidad");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("discapacidad") Discapacidad discapacidad) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", discapacidad);
		BindUtils.postGlobalCommand(null, null, "cargarDiscapacidad", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaDiscapacidad")
	public void refrescarlista() {
		listaDiscapacidad = InstanciaDAO.discapacidadDAO.getListaDiscapacidad();
	}

	@Command
	public void editar(@BindingParam("discapacidad") Discapacidad discapacidad) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", discapacidad);
		Executions.createComponents("/vista/formDiscapacidadCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaDiscapacidad")
	public void filtrar() {
		listaDiscapacidad.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaDiscapacidad.addAll(InstanciaDAO.discapacidadDAO.getListaDiscapacidad());
		} else {
			for (Discapacidad item : InstanciaDAO.discapacidadDAO.getListaDiscapacidad()) {
				if (item.getDescripcion().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaDiscapacidad.add(item);
				}
			}
		}
	}

	@Command
	public void salir() {
		winDiscapacidad.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Discapacidad> getListaDiscapacidad() {
		return listaDiscapacidad;
	}

	public void setListaDiscapacidad(List<Discapacidad> listaDiscapacidad) {
		this.listaDiscapacidad = listaDiscapacidad;
	}

}