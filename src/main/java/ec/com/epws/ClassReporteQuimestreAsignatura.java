package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.AsignacionDocente;
import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.CalificacionQuimestre;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.models.Modelos;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassReporteQuimestreAsignatura {

	private List<Curso> listaCurso;
	private Curso curso;
	private List<Asignatura> listaAsignatura;
	private Asignatura asignatura;
	private List<Quimestre> listaQuimestre;
	private Quimestre quimestre;
	private List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacionQuimestre = new ArrayList<>();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("asignatura") Asignatura asignatura,
			@ExecutionArgParam("quimestre") Quimestre quimestre,
			@ExecutionArgParam("lista") List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacionParcial) {
		Selectors.wireComponents(vista, this, false);
		if (listaCalificacionParcial != null) {
			this.asignatura = asignatura;
			this.quimestre = quimestre;
			this.listaCalificacionQuimestre = listaCalificacionParcial;
		} else {
			listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
			curso = listaCurso.get(0);
			listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(), false);
			this.asignatura = listaAsignatura.get(0);
			listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
			this.quimestre = listaQuimestre.get(0);
			buscar();
		}		
	}
	
	@NotifyChange("listaCalificacionQuimestre")
	@Command
	public void buscar() {
			listaCalificacionQuimestre.clear();
			if (asignatura.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_BASICO) {
				List<CalificacionQuimestre> listaQuimestre = InstanciaDAO.calificacionQuimestreDAO
						.getListaCalificacionQuimestreByAsignaturaAndQuimestre(asignatura.getId(), quimestre.getId());

				listaQuimestre.forEach(x -> {
					x.setCalificacionParcials(InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByCalificacionQuimestre(x.getId()));				
					listaCalificacionQuimestre
						.add(new Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>(x, null,
								null, null, true));});
			}
			if (asignatura.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_INICIAL || asignatura.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_MATERIA) {
				List<CalificacionQuimestreCualitativa> listaQuimestreCualitativa = InstanciaDAO.calificacionQuimestreCualitativaDAO
						.getListaCalificacionQuimestreCualitativaByAsignaturaAndQuimestre(asignatura.getId(), quimestre.getId());

				listaQuimestreCualitativa.forEach(x -> {
					x.setCalificacionParcialCualitativas(InstanciaDAO.calificacionParcialCualitativaDAO.getListaCalificacionParcialCualitativaByCalificacionQuimestre(x.getId()));
					listaCalificacionQuimestre
						.add(new Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>(null, x,
								null, null, true));});	
			}
	}
	
	@NotifyChange({"asignatura", "listaAsignatura","listaCalificacionQuimestre" })
	@Command
	public void selectCurso(){
		listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(this.curso.getId(), false);
		asignatura = listaAsignatura.get(0);
		buscar();
	}
	
	@NotifyChange({ "listaCalificacionQuimestre" })
	@Command
	public void selectAsignatura(){
		buscar();
	}
	
	@NotifyChange({ "listaCalificacionQuimestre" })
	@Command
	public void selectQuimestre(){
			buscar();
	}
	
	@Command
	public void generarExcel() {
		if (listaCalificacionQuimestre.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), asignatura.getId());
		List<String> lista = new ArrayList<>();
		InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId()).forEach(x -> lista.add(x.getDescripcion()));
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("lista", lista);
		mapreporte.put("curso", asignatura.getCurso().getDescripcion());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		mapreporte.put("asignatura", asignatura.getDescripcion());
		mapreporte.put("quimestre",quimestre.getDescripcion());
		mapreporte.put("docente", ad.getDocente().getUsuario().getNombre()+" "+ad.getDocente().getUsuario().getApellido());
		Sistema.reporteEXCEL(Sistema.QUIMESTRE_ASIGNATURA_JASPER, "Informe Quimestre", mapreporte,
				Sistema.getInformeCalificacionQuimestreDataSource(listaCalificacionQuimestre),null,true);
	}

	@Command
	public void generarPdf() {
		if (listaCalificacionQuimestre.size() == 0) {
			Messagebox.show(Sistema.idioma.getNoExisteInformacion());
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaCalificacionQuimestre);
		parametros.put("asignatura", asignatura);
		parametros.put("quimestre", quimestre);
		Executions.createComponents("/reporte/impresionQuimestreAsignatura.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), asignatura.getId());
		List<String> lista = new ArrayList<>();
		InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId()).forEach(x -> lista.add(x.getDescripcion()));
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("lista", lista);
		mapreporte.put("curso", asignatura.getCurso().getDescripcion());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		mapreporte.put("asignatura", asignatura.getDescripcion());
		mapreporte.put("quimestre",quimestre.getDescripcion());
		mapreporte.put("docente", ad.getDocente().getUsuario().getNombre()+" "+ad.getDocente().getUsuario().getApellido());
		iframe.setContent(Sistema.reportePDF(Sistema.QUIMESTRE_ASIGNATURA_JASPER, mapreporte,
				Sistema.getInformeCalificacionQuimestreDataSource(listaCalificacionQuimestre)));
	}
	
	/*
	 * 
	 */
	
	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Asignatura> getListaAsignatura() {
		return listaAsignatura;
	}

	public void setListaAsignatura(List<Asignatura> listaAsignatura) {
		this.listaAsignatura = listaAsignatura;
	}

	public Asignatura getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}

	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}

	public Quimestre getQuimestre() {
		return quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

	public List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> getListaCalificacionQuimestre() {
		return listaCalificacionQuimestre;
	}

	public void setListaCalificacionQuimestre(
			List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacionQuimestre) {
		this.listaCalificacionQuimestre = listaCalificacionQuimestre;
	}
	
}
