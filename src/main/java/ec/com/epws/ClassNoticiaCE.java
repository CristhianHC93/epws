package ec.com.epws;

import java.io.File;
import java.sql.Timestamp;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Noticia;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassNoticiaCE {
	@Wire("#winNoticiaCE")
	private Window winNoticiaCE;
	
	private Noticia noticia;
	private Timestamp fecha;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("objeto") Noticia noticia) {
		Selectors.wireComponents(view, this, false);
		if (noticia == null) {
			this.noticia = new Noticia();
			this.noticia.setFecha(new Timestamp(new Date().getTime()));
			fecha = new Timestamp(new Date().getTime());
		}else{
		this.noticia = noticia;
		fecha = this.noticia.getFecha();
		}
	}

	@SuppressWarnings("deprecation")
	@Command
	@NotifyChange("noticia")
	public void guardarEditar() {
		bandera = true;
		noticia.getFecha().setYear(fecha.getYear());
		noticia.getFecha().setMonth(fecha.getMonth());
		noticia.getFecha().setDate(fecha.getDate());
		
		if (noticia.getId() == null) {
			InstanciaDAO.noticiaDAO.saveorUpdate(noticia);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			noticia = new Noticia();
		} else {
			InstanciaDAO.noticiaDAO.saveorUpdate(noticia);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("noticia")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			noticia.setDescripcion((cadena.toUpperCase()));
		}
		if (campo.equals("titulo")) {
			noticia.setTitulo((cadena.toUpperCase()));
		}
		if (campo.equals("resumen")) {
			noticia.setResumen((cadena.toUpperCase()));
		}
	}

	@NotifyChange("noticia")
	@Command
	public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {
		if (noticia.getTitulo() == null || noticia.getTitulo().equals("")) {
			Messagebox.show("Es necesario ingresar Titulo");
			return;
		}
		Media media;
		media = event.getMedia();
		int aleatorio = ThreadLocalRandom.current().nextInt(1, 100000);
		if (Util.uploadFile(media, noticia.getTitulo().replaceAll(" ", "_") + aleatorio+".png", Sistema.carpetaNoticia)) {
			new File(Util.getPathRaiz() + noticia.getTitulo().replaceAll(" ", "_") + aleatorio+".png").delete();
			noticia.setImagen("/" + Sistema.carpetaNoticia + "/" + noticia.getTitulo().replaceAll(" ", "_") + aleatorio+".png");
		}
	}
	
	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winNoticiaCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Noticia getNoticia() {
		return noticia;
	}

	public void setNoticia(Noticia noticia) {
		this.noticia = noticia;
	}

	public Timestamp getFecha() {
		return fecha;
	}

	public void setFecha(Timestamp fecha) {
		this.fecha = fecha;
	}
	
}
