package ec.com.epws;

import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;

import ec.com.epws.entity.Rol;
import ec.com.epws.entity.RolModulo;
import ec.com.epws.entity.Usuario;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.sistema.Sistema;

public class ClassRolModulo {

	private Rol rol;
	private List<Rol> listaRol;
	private List<RolModulo> listaRolModulo;
	private RolModulo rolModulo;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		Usuario usuario = (Usuario) Sessions.getCurrent().getAttribute("Usuario");
		rol = usuario.getRol();
		listaRol = InstanciaDAO.getRolDAO().getListaRol(Sistema.ESTADO_ACTIVAR_USUARIO);
		listaRolModulo = InstanciaDAO.getRolModuloDAO().getListaRolModulo(rol);
	}

	@Command
	public void check(@BindingParam("item") RolModulo rolModulo) {
		InstanciaDAO.getRolModuloDAO().saveorUpdate(rolModulo);
	}

	@Command
	@NotifyChange("listaRolModulo")
	public void select(@BindingParam("item") Rol rol) {
		listaRolModulo = InstanciaDAO.getRolModuloDAO().getListaRolModulo(rol);
	}
	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<RolModulo> getListaRolModulo() {
		return listaRolModulo;
	}

	public void setListaRolModulo(List<RolModulo> listaRolModulo) {
		this.listaRolModulo = listaRolModulo;
	}

	public RolModulo getRolModulo() {
		return rolModulo;
	}

	public void setRolModulo(RolModulo rolModulo) {
		this.rolModulo = rolModulo;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public List<Rol> getListaRol() {
		return listaRol;
	}

	public void setListaRol(List<Rol> listaRol) {
		this.listaRol = listaRol;
	}

}
