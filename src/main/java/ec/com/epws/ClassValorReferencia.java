package ec.com.epws;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;

import ec.com.epws.entity.ValorReferencia;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassValorReferencia {

	private ValorReferencia valorReferencia;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		valorReferencia = Sistema.getValorReferencia();
	}

	@NotifyChange("valorReferencia")
	@Command
	public void guardarEditar() {
		InstanciaDAO.valorReferenciaDAO.saveUpdate(valorReferencia);
		Clients.showNotification("Datos modificados correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
				"middle_center", 1500);
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintPrecio() {
		return Sistema.CONSTRAINT_PRECIO;
	}

	public ValorReferencia getValorReferencia() {
		return valorReferencia;
	}

	public void setValorReferencia(ValorReferencia valorReferencia) {
		this.valorReferencia = valorReferencia;
	}

}
