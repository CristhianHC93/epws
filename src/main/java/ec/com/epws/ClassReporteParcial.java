package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.AsignacionDocente;
import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.CalificacionParcial;
import ec.com.epws.entity.CalificacionParcialCualitativa;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Parcial;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.models.Modelos;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassReporteParcial {
	private List<Curso> listaCurso;
	private Curso curso;
	private List<Quimestre> listaQuimestre;
	private Quimestre quimestre;
	private List<Parcial> listaParcial;
	private Parcial parcial;
	private List<Asignatura> listaAsignatura;
	private Asignatura asignatura;
	private List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> listaCalificacionParcial = new ArrayList<>();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista,
			@ExecutionArgParam("parcial") Parcial parcial, @ExecutionArgParam("asignatura") Asignatura asignatura,
			@ExecutionArgParam("lista") List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> listaCalificacionParcial) {
		Selectors.wireComponents(vista, this, false);
		if (listaCalificacionParcial != null) {
			this.asignatura = asignatura;
			this.parcial = parcial;
			this.listaCalificacionParcial = listaCalificacionParcial;
		} else {
			listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
			this.curso = listaCurso.get(0);
			listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(this.curso.getId(), true);
			this.asignatura = listaAsignatura.get(0);
			listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
			this.quimestre = listaQuimestre.get(0);
			listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
			this.parcial = listaParcial.get(0);
			buscar();
		}
	}

	@NotifyChange("listaCalificacionParcial")
	@Command
	public void buscar() {
		listaCalificacionParcial.clear();
		List<CalificacionParcial> listaParcial = InstanciaDAO.calificacionParcialDAO
				.getListaCalificacionParcialByAsignaturaAndParcial(asignatura.getId(), parcial.getId());
		List<CalificacionParcialCualitativa> listaQuimestreCualitativa = InstanciaDAO.calificacionParcialCualitativaDAO
				.getListaCalificacionParcialCualitativaByAsignaturaAndParcial(asignatura.getId(), parcial.getId());
		listaParcial.forEach(x -> {
			// x.setCalificacionParcials(InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByCalificacionQuimestre(x.getId()));
			listaCalificacionParcial
					.add(new Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>(x, null, null,
							null, true));
		});
		listaQuimestreCualitativa.forEach(x -> {
			// x.setCalificacionParcialCualitativas(InstanciaDAO.calificacionParcialCualitativaDAO.getListaCalificacionParcialCualitativaByCalificacionQuimestre(x.getId()));
			listaCalificacionParcial
					.add(new Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>(null, x, null,
							null, true));
		});
	}

	@NotifyChange({ "asignatura", "listaAsignatura", "listaCalificacionParcial" })
	@Command
	public void selectCurso() {
		listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignatura(curso.getId(), true);
		asignatura = listaAsignatura.get(0);
		buscar();
	}

	@NotifyChange({ "listaCalificacionParcial" })
	@Command
	public void selectAsignatura() {
		buscar();
	}

	@NotifyChange({ "parcial", "listaParcial", "listaCalificacionParcial" })
	@Command
	public void selectQuimestre() {
		listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
		parcial = listaParcial.get(0);
		buscar();
	}

	@NotifyChange({ "listaCalificacionParcial" })
	@Command
	public void selectParcial() {
		buscar();
	}

	@NotifyChange("listaCalificacionParcial")
	@Command
	public void calcularPromedio(@BindingParam("item") CalificacionParcial item, @BindingParam("valor") Double valor) {
		if (valor > 10) {
			valor = 0.0;
			Messagebox.show("Valor Invalido");
			return;
		}

		item.setPromedio(
				(Sistema.redondear(item.getTai() + item.getAic() + item.getAgc() + item.getL() + item.getEsumativa())
						/ 5));
		Sistema.getListaCualitativaBasico().forEach(x -> {
			if (item.getPromedio() >= x.getValorMinimo() && item.getPromedio() <= x.getValorMaximo()) {
				item.setCualitativa(x);
			}
		});
	}

	@Command
	public void generarExcel() {
		if (listaCalificacionParcial.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}

		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), asignatura.getId());
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("parcial", parcial.getDescripcion());
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		mapreporte.put("asignatura", asignatura.getDescripcion());
		mapreporte.put("docente", ad.getDocente().getUsuario().getNombre()+" "+ad.getDocente().getUsuario().getApellido());
		Sistema.reporteEXCEL(Sistema.PARCIAL_ASIGNATURA_JASPER, "Parcial", mapreporte,
				Sistema.getParcialDataSource(listaCalificacionParcial),null,true);
	}

	@Command
	public void generarPdf() {
		if (listaCalificacionParcial.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaCalificacionParcial);
		parametros.put("asignatura", asignatura);
		parametros.put("parcial", parcial);
		Executions.createComponents("/reporte/impresionParcialAsignatura.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), asignatura.getId());
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", asignatura.getCurso().getDescripcion());
		mapreporte.put("parcial", parcial.getDescripcion());
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		mapreporte.put("asignatura",asignatura.getDescripcion());
		mapreporte.put("docente", ad.getDocente().getUsuario().getNombre()+" "+ad.getDocente().getUsuario().getApellido());

		iframe.setContent(Sistema.reportePDF(Sistema.PARCIAL_ASIGNATURA_JASPER, mapreporte,
				Sistema.getParcialDataSource(listaCalificacionParcial)));
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintPrecio() {
		return Sistema.CONSTRAINT_PRECIO;
	}

	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}

	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}

	public Quimestre getQuimestre() {
		return quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

	public List<Parcial> getListaParcial() {
		return listaParcial;
	}

	public void setListaParcial(List<Parcial> listaParcial) {
		this.listaParcial = listaParcial;
	}

	public Parcial getParcial() {
		return parcial;
	}

	public void setParcial(Parcial parcial) {
		this.parcial = parcial;
	}

	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Asignatura> getListaAsignatura() {
		return listaAsignatura;
	}

	public void setListaAsignatura(List<Asignatura> listaAsignatura) {
		this.listaAsignatura = listaAsignatura;
	}

	public Asignatura getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> getListaCalificacionParcial() {
		return listaCalificacionParcial;
	}

	public void setListaCalificacionParcial(
			List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> listaCalificacionParcial) {
		this.listaCalificacionParcial = listaCalificacionParcial;
	}

}
