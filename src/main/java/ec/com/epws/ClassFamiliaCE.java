package ec.com.epws;

import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Familia;
import ec.com.epws.entity.Parentesco;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassFamiliaCE {
	@Wire("#winFamiliaCE")
	private Window winFamiliaCE;
	private List<Parentesco> listaParentesco;
	private Familia familia;
	private boolean parentesco;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("objeto") Familia familia,
			@ExecutionArgParam("parentesco") Integer parentesco) {
		Selectors.wireComponents(view, this, false);
		listaParentesco = InstanciaDAO.getParentescoDAO().getListaParentesco();
		if (familia != null) {
			this.familia = familia;
			this.familia.setParentesco(familia.getParentesco());
		} else {
			this.familia = new Familia();
			this.familia
					.setParentesco(listaParentesco.get(((parentesco == null || parentesco == 0) ? 0 : parentesco - 1)));
			this.parentesco = false;
		}
		this.parentesco = ((parentesco == null || parentesco == Sistema.PARENTESCO_REPRESENTANTE) ? false : true);
	}

	@Command
	@NotifyChange("familia")
	public void guardarEditar() {
		bandera = true;
		if (!Sistema.validarCedula(familia.getCedula())) {
			return;
		}
		if (Sistema.validarRepetido(InstanciaDAO.getFamiliaDAO().getListaFamilia(), familia)) {
			return;
		}

		if (familia.getId() == null) {
			InstanciaDAO.getFamiliaDAO().saveorUpdate(familia);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			Parentesco parentesco = familia.getParentesco();
			familia = new Familia();
			familia.setParentesco(parentesco);
		} else {
			InstanciaDAO.getFamiliaDAO().saveorUpdate(familia);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("familia")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("nombre")) {
			familia.setNombre(cadena.toUpperCase());
		}
		if (campo.equals("apellido")) {
			familia.setApellido(cadena.toUpperCase());
		}
		if (campo.equals("profesion")) {
			familia.setProfesion(cadena.toUpperCase());
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winFamiliaCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintEntero() {
		return Sistema.CONSTRAINT_ENTERO;
	}

	public String getConstraintEnteroVacio() {
		return Sistema.CONSTRAINT_ENTERO_VACIO;
	}

	public String getConstraintLetra() {
		return Sistema.CONSTRAINT_LETRA;
	}

	public String getConstraintLetraVacio() {
		return Sistema.CONSTRAINT_LETRA_VACIO;
	}

	public Familia getFamilia() {
		return familia;
	}

	public void setFamilia(Familia familia) {
		this.familia = familia;
	}

	public List<Parentesco> getListaParentesco() {
		return listaParentesco;
	}

	public void setListaParentesco(List<Parentesco> listaParentesco) {
		this.listaParentesco = listaParentesco;
	}

	public boolean isParentesco() {
		return parentesco;
	}

	public void setParentesco(boolean parentesco) {
		this.parentesco = parentesco;
	}

}
