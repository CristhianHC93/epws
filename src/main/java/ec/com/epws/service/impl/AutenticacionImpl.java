package ec.com.epws.service.impl;

import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;

import ec.com.epws.entity.Usuario;
import ec.com.epws.service.*;
import ec.com.epws.factory.InstanciaDAO;

public class AutenticacionImpl implements Autenticacion {
	private static final String KEY_USER_MODEL = Autenticacion.class.getName() + "_MODEL";

	@Override
	public boolean login(String username, String password) {
		Usuario usuario = InstanciaDAO.usuarioDAO.getUsuario(username, password);
		// a simple plan text password verification
		if (usuario == null) {
			return false;
		} else {
			Session sess = Sessions.getCurrent();
			sess.setAttribute("CredencialUsuario", usuario);
			return true;
		}
	}

	@Override
	public void logout() {
		Session sess = Sessions.getCurrent();
		sess.removeAttribute("CredencialUsuario");
	}

	@Override
	public Usuario getUserCredential() {
		return (Usuario) Sessions.getCurrent().getAttribute("CredencialUsuario");
	}

	public static Autenticacion getIntance(Session zkSession) {

		synchronized (zkSession) {

			Autenticacion userModel = (Autenticacion) zkSession.getAttribute(KEY_USER_MODEL);

			if (userModel == null) {

				System.out.println("desde credencial");

				zkSession.setAttribute(KEY_USER_MODEL, userModel = new AutenticacionImpl());

			}
			return userModel;
		}
	}
}
