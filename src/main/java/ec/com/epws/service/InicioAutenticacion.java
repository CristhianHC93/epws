package ec.com.epws.service;

import java.util.Map;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.util.Initiator;

import ec.com.epws.entity.Usuario;
import ec.com.epws.service.impl.AutenticacionImpl;

public class InicioAutenticacion implements Initiator{

	Autenticacion authService = new AutenticacionImpl();
	
	@Override
	public void doInit(Page arg0, Map<String, Object> arg1) throws Exception {
		Usuario usuario = authService.getUserCredential();
		if(usuario==null){
			Executions.sendRedirect("/login.zul");
			return;
		}
	}

}
