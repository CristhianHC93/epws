package ec.com.epws.service;

import ec.com.epws.entity.Usuario;

public interface Autenticacion {
	
	/**login with account and password**/
	public boolean login(String account, String password);
	
	/**logout current user**/
	public void logout();
	
	/**get current user credential**/
	public Usuario getUserCredential();

}
