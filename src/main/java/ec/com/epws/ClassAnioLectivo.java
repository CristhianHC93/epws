package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;

import ec.com.epws.entity.AnioLectivo;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassAnioLectivo {

	private String datoBusqueda;
	private List<AnioLectivo> listaAnioLectivo;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista) {
		Selectors.wireComponents(vista, this, false);
		listaAnioLectivo = InstanciaDAO.anioLectivoDAO.getListaAnioLectivo();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formAnioLectivoCE.zul", null, null);
	}

	@GlobalCommand
	@NotifyChange("listaAnioLectivo")
	public void refrescarlista() {
		listaAnioLectivo = InstanciaDAO.anioLectivoDAO.getListaAnioLectivo();
	}

	@Command
	public void editar(@BindingParam("anioLectivo") AnioLectivo anioLectivo) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", anioLectivo);
		Executions.createComponents("/vista/formAnioLectivoCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaAnioLectivo")
	public void filtrar() {
		listaAnioLectivo.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaAnioLectivo.addAll(InstanciaDAO.anioLectivoDAO.getListaAnioLectivo());
		} else {
			for (AnioLectivo item : InstanciaDAO.anioLectivoDAO.getListaAnioLectivo()) {
				if (item.getDescripcion().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaAnioLectivo.add(item);
				}
			}
		}
	}

	@NotifyChange("listaAnioLectivo")
	@Command
	public void check(@BindingParam("item") AnioLectivo anioLectivo) {
		if (anioLectivo.getEstado()) {
			listaAnioLectivo.remove(anioLectivo);
			listaAnioLectivo.forEach(x -> {
				if (x.getEstado()) {
					x.setEstado(false);
					InstanciaDAO.anioLectivoDAO.saveorUpdate(x);
				}
			});
			InstanciaDAO.anioLectivoDAO.saveorUpdate(anioLectivo);
			listaAnioLectivo = InstanciaDAO.anioLectivoDAO.getListaAnioLectivo();
			Sistema.getAnioLectivoActivo();
			return;
		} else {
			anioLectivo.setEstado(true);
			InstanciaDAO.anioLectivoDAO.saveorUpdate(anioLectivo);
		}
		listaAnioLectivo = InstanciaDAO.anioLectivoDAO.getListaAnioLectivo();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<AnioLectivo> getListaAnioLectivo() {
		return listaAnioLectivo;
	}

	public void setListaAnioLectivo(List<AnioLectivo> listaAnioLectivo) {
		this.listaAnioLectivo = listaAnioLectivo;
	}

}
