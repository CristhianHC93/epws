package ec.com.epws;

import java.util.Date;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.Estado;
import ec.com.epws.entity.Modulo;
import ec.com.epws.entity.Rol;
import ec.com.epws.entity.RolModulo;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.factory.InstanciaEntity;
import ec.com.epws.sistema.Sistema;

public class ClassRol {

	private String btGuardarEditar = Sistema.idioma.getGuardar();;
	private List<Rol> listaRol;
	private List<Estado> listaEstado;
	private Rol rol;
	Estado estado = InstanciaEntity.getInstanciaEstado();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaRol = InstanciaDAO.getRolDAO().getListaRol();
		listaEstado = Sistema.getListaEstadoUsuario();
		rol = new Rol();
	}

	@Command
	@NotifyChange("listaRol")
	public void guardarEditar() {
		if (rol.getDescripcion() == null || rol.getDescripcion().equals("")) {
			Messagebox.show("Ingresar Descripcion");
			return;
		}
		if (rol.getId() == null) {
			listaRol = InstanciaDAO.getRolDAO().getListaRol();
			for (Rol rol : listaRol) {
				if (rol.getDescripcion().trim().equals(this.rol.getDescripcion().trim())) {
					Messagebox.show("El rol ya ha sido asignado");
					return;
				}
			}
			estado.setId(Sistema.ESTADO_ACTIVAR_USUARIO);
			rol.setFechaCreacion(new Date());
			rol.setEstado(estado);
			listaRol.clear();
			Rol rolDevuelto = InstanciaDAO.getRolDAO().saveorUpdate(rol);
			List<Modulo> listaModulo = InstanciaDAO.getModuloDAO().getListaModulo();
			for (Modulo modulo : listaModulo) {
				RolModulo rolModulo = new RolModulo();
				rolModulo.setRol(rolDevuelto);
				rolModulo.setModulo(modulo);
				rolModulo.setEstado(false);
				InstanciaDAO.getRolModuloDAO().saveorUpdate(rolModulo);
			}
			Clients.showNotification("Rol ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		} else {
			InstanciaDAO.getRolDAO().saveorUpdate(rol);
			Clients.showNotification("Rol Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
		listaRol = InstanciaDAO.getRolDAO().getListaRol();
	}

	@Command
	@NotifyChange({ "listaRol", "rol", "btGuardarEditar" })
	public void nuevo() {
		rol = new Rol();
		btGuardarEditar = Sistema.idioma.getGuardar();
	}

	@Command
	public void select(@BindingParam("item") Rol rol) {
		InstanciaDAO.getRolDAO().saveorUpdate(rol);
	}

	@Command
	public void admin(@BindingParam("item") Rol rol, @BindingParam("combo") Combobox combo) {
		if (rol.getId() == 1) {
			combo.setDisabled(true);
		}
	}

	@Command
	@NotifyChange({ "btGuardarEditar" })
	public void btselect() {
		btGuardarEditar = Sistema.idioma.getEditar();
	}

	/*
	 * METODO GET AND SET
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Rol> getListaRol() {
		return listaRol;
	}

	public void setListaRol(List<Rol> listaRol) {
		this.listaRol = listaRol;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public String getBtGuardarEditar() {
		return btGuardarEditar;
	}

	public void setBtGuardarEditar(String btGuardarEditar) {
		this.btGuardarEditar = btGuardarEditar;
	}

	public List<Estado> getListaEstado() {
		return listaEstado;
	}

	public void setListaEstado(List<Estado> listaEstado) {
		this.listaEstado = listaEstado;
	}

}
