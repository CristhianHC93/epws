package ec.com.epws;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Vacuna;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassVacunaCE {
	@Wire("#winVacunaCE")
	private Window winVacunaCE;

	private Vacuna vacuna;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("objeto") Vacuna vacuna) {
		Selectors.wireComponents(view, this, false);
		this.vacuna = (vacuna == null) ? new Vacuna() : vacuna;
	}

	@Command
	@NotifyChange("vacuna")
	public void guardarEditar() {
		bandera = true;
		if (vacuna.getId() == null) {
			InstanciaDAO.vacunaDAO.saveorUpdate(vacuna);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			vacuna = new Vacuna();
		} else {
			InstanciaDAO.vacunaDAO.saveorUpdate(vacuna);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("vacuna")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			vacuna.setDescripcion((cadena.toUpperCase()));
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winVacunaCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Vacuna getVacuna() {
		return vacuna;
	}

	public void setVacuna(Vacuna vacuna) {
		this.vacuna = vacuna;
	}

}
