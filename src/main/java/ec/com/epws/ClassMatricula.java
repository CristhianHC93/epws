package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.AnioLectivo;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Pension;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassMatricula {
	@Wire("#winMatricula")
	private Window winMatricula;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	AnioLectivo anioLectivo;
	List<Pension> listaPension;

	int curso = 0;

	private List<Matricula> listaMatricula = new ArrayList<>();
	
	private Matricula matricula;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("clase") String clase,
			@ExecutionArgParam("curso") Integer curso,@ExecutionArgParam("matricula") Matricula matricula) {
		Selectors.wireComponents(view, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (clase.equals("Pension")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "93%";
			this.curso = curso;
		}
		anioLectivo = Sistema.getAnioLectivoActivoEstatico();
		if (anioLectivo == null) {
			Messagebox.show("No existe Año Lectivo Activo");
			return;
		} else {
			listaMatricula = (this.curso == 0) ? InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivo(anioLectivo.getId())
					: InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(anioLectivo.getId(), curso);
		}
		this.matricula = matricula;
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formMatriculaCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaMatricula")
	public void eliminar(
			@BindingParam("matricula") final Matricula matricula) {
		String msj = "Esta seguro(a) que desea eliminar el registro \""
				+ matricula.getAlumno().getNombre()+ " " +matricula.getAlumno().getApellido()
				+ "\" esta accion no podra deshacerse si se confirma";

		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						listaPension = InstanciaDAO.pensionDAO.getListaPensionByMatricula(matricula.getId());
						listaPension.forEach(x -> {
							x.setEstado(Sistema.getEstado(Sistema.ESTADO_ELIMINADO));
							InstanciaDAO.pensionDAO.saveorUpdate(x);
						});
						matricula.setEstado(Sistema.getEstado(Sistema.ESTADO_ELIMINADO));
						InstanciaDAO.matriculaDAO.saveorUpdate(matricula);
						Clients.showNotification("Registro eliminado satisfactoriamente",
								Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
						BindUtils.postNotifyChange(null, null, ClassMatricula.this, "listaMatricula");
						listaMatricula.clear();
						listaMatricula = (curso == 0)
								? InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivo(anioLectivo.getId())
								: InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(anioLectivo.getId(),
										curso);
					}
				});
	}

	@Command
	@NotifyChange("alumno")
	public void editar(@BindingParam("matricula") Matricula matricula) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", matricula);
		Executions.createComponents("/vista/formMatriculaCE.zul", null, parametros);
	}

	@Command
	public void aceptar(@BindingParam("matricula") Matricula matricula) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", matricula);
		BindUtils.postGlobalCommand(null, null, "cargarAlumno", parametros);
		salir();
	}

	@Command
	@NotifyChange("listaMatricula")
	public void filtrar() {
		listaMatricula.clear();
		if (datoBusqueda.trim().equals("")) {
			listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivo(Sistema.getAnioLectivoActivoEstatico().getId());
		} else {
			listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoFiltro(Sistema.getAnioLectivoActivoEstatico().getId(),datoBusqueda.toUpperCase());
		}
	}

	@GlobalCommand
	@NotifyChange("listaMatricula")
	public void refrescarlistaMatricula() {
		listaMatricula = (curso == 0) ? InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivo(anioLectivo.getId())
				: InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(anioLectivo.getId(), curso);
	}
	
	@Command
	public void generarExcel(@BindingParam("matricula") Matricula matricula) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("ficha", "FICHA DE MATRÍCULA");
		mapreporte.put("cedula", matricula.getAlumno().getCedula());
		mapreporte.put("nombre", matricula.getAlumno().getNombre());
		mapreporte.put("apellido", matricula.getAlumno().getApellido());
		mapreporte.put("numero", matricula.getNumero());
		mapreporte.put("representante", matricula.getAlumno().getAlumnoFamilias().get(2).getFamilia().getNombre() + " "
				+ matricula.getAlumno().getAlumnoFamilias().get(2).getFamilia().getApellido());
		mapreporte.put("fecha", matricula.getFecha());
		mapreporte.put("curso", matricula.getCurso().getDescripcion());
		mapreporte.put("docente", "");
		mapreporte.put("valor", matricula.getEstado().getDescripcion());
		mapreporte.put("observacion",matricula.getObservacion());
		mapreporte.put("foto", (matricula.getAlumno().getFoto()==null || matricula.getAlumno().getFoto().equals("")? Util.getPathRaiz()+"/jasperReport/sin_foto.png":Util.getPathRaiz()+matricula.getAlumno().getFoto()));
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		Sistema.reporteEXCEL(Sistema.FICHA_MATRICULA, "ficha Matricula", mapreporte, null,null,false);
	}

	@Command
	public void generarPdf(@BindingParam("matricula") Matricula matricula) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("matricula", matricula);
		Executions.createComponents("/reporte/impresionFichaMatricula.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("ficha", "FICHA DE MATRÍCULA");
		mapreporte.put("cedula", matricula.getAlumno().getCedula());
		mapreporte.put("nombre", matricula.getAlumno().getNombre());
		mapreporte.put("apellido", matricula.getAlumno().getApellido());
		mapreporte.put("numero", matricula.getNumero());
		mapreporte.put("representante", matricula.getAlumno().getAlumnoFamilias().get(2).getFamilia().getNombre() + " "
				+ matricula.getAlumno().getAlumnoFamilias().get(2).getFamilia().getApellido());
		mapreporte.put("fecha", matricula.getFecha());
		mapreporte.put("curso", matricula.getCurso().getDescripcion());
		mapreporte.put("docente", (InstanciaDAO.asignacionTutorDAO.getAsignacionTutor(Sistema.getAnioLectivoActivoEstatico().getId(), matricula.getCurso().getId()).getDocente().getUsuario().getNombre()+" "+
				(InstanciaDAO.asignacionTutorDAO.getAsignacionTutor(Sistema.getAnioLectivoActivoEstatico().getId(), matricula.getCurso().getId()).getDocente().getUsuario().getApellido())));
		mapreporte.put("valor", InstanciaDAO.pensionDAO.getListaPensionByMesMatricula(1, matricula.getId(),Sistema.getAnioLectivoActivoEstatico().getId()).get(0).getEstado().getDescripcion());
		mapreporte.put("observacion",matricula.getObservacion());
		mapreporte.put("foto", (matricula.getAlumno().getFoto()==null || matricula.getAlumno().getFoto().equals("")? Util.getPathRaiz()+"/jasperReport/sin_foto.png":Util.getPathRaiz()+matricula.getAlumno().getFoto()));
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		iframe.setContent(Sistema.reportePDF(Sistema.FICHA_MATRICULA, mapreporte, null));
	}

	@Command
	public void salir() {
		winMatricula.detach();
	}

	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Matricula> getListaMatricula() {
		return listaMatricula;
	}

	public void setListaMatricula(List<Matricula> listaMatricula) {
		this.listaMatricula = listaMatricula;
	}

	public Window getWinMatricula() {
		return winMatricula;
	}

	public void setWinMatricula(Window winMatricula) {
		this.winMatricula = winMatricula;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

}
