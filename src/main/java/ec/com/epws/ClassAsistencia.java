package ec.com.epws;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.AsignacionTutor;
import ec.com.epws.entity.Asistencia;
import ec.com.epws.entity.Docente;
import ec.com.epws.entity.Estado;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Observacion;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassAsistencia {

	private List<Asistencia> listaAsistencia = new ArrayList<>();
	private List<Matricula> listaMatricula;
	private List<Estado> listaEstadoAsistencia;
	private List<Estado> listaEstadoAsistenciaCabecera;
	private Estado estadoAsistencia;
	private Timestamp fechaAsistencia = new Timestamp(new Date().getTime());

	AsignacionTutor tutor;

	@SuppressWarnings("deprecation")
	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista) {
		Selectors.wireComponents(vista, this, false);
		if (Sistema.getUsuario().getRol().getId() != 2) {
			Messagebox.show("Usuario no es Docente");
			return;
		}
		listaEstadoAsistencia = Sistema.getListaEstadoAsistencia();
		listaEstadoAsistenciaCabecera = Sistema.getListaEstadoAsistencia();
		estadoAsistencia = listaEstadoAsistencia.get(0);
		Docente docente = InstanciaDAO.docenteDAO.getDocente(Sistema.getUsuario().getId());
		tutor = InstanciaDAO.asignacionTutorDAO
				.getAsignacionTutorByDocente(Sistema.getAnioLectivoActivoEstatico().getId(), docente.getId());
		if (tutor != null && tutor.getCurso() != null) {
			listaAsistencia = InstanciaDAO.asistenciaDAO.getListaAsistencia(
					Sistema.getAnioLectivoActivoEstatico().getId(), tutor.getCurso().getId(),
					new Date(fechaAsistencia.getYear(), fechaAsistencia.getMonth(), fechaAsistencia.getDate()));
			if (listaAsistencia == null || listaAsistencia.size() == 0) {
				llenarListaAlumno();
			}
		}
	}

	private void llenarListaAlumno() {
		listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(
				Sistema.getAnioLectivoActivoEstatico().getId(), tutor.getCurso().getId());
		listaMatricula.forEach(x -> {
			Asistencia asistencia = new Asistencia();
			asistencia.setAlumno(x.getAlumno());
			asistencia.setAnioLectivo(Sistema.getAnioLectivoActivoEstatico());
			asistencia.setCurso(tutor.getCurso());
			asistencia.setEstado(estadoAsistencia);
			listaAsistencia.add(asistencia);
		});
	}

	@NotifyChange("listaAsistencia")
	@Command
	public void guardar() {
		listaAsistencia.forEach(x -> {
			if (x.getId() != null && x.getObservacion() != null && x.getObservacion().getId() != null) {
				InstanciaDAO.observacionDAO.saveorUpdate(x.getObservacion());
			}
			x.setFecha(fechaAsistencia);
			InstanciaDAO.asistenciaDAO.saveorUpdate(x);
		});
		Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
	}

	@NotifyChange("listaAsistencia")
	@Command
	public void select() {
		listaAsistencia.forEach(x -> x.setEstado(estadoAsistencia));
	}

	@NotifyChange("listaAsistencia")
	@Command
	public void buscar() {
		listaAsistencia = InstanciaDAO.asistenciaDAO.getListaAsistencia(Sistema.getAnioLectivoActivoEstatico().getId(),
				tutor.getCurso().getId(), fechaAsistencia);
		if (listaAsistencia == null || listaAsistencia.size() == 0) {
			llenarListaAlumno();
		}
	}

	@NotifyChange("listaAsistencia")
	@Command
	public void nuevo(@BindingParam("asistencia") Asistencia asistencia) {
		asistencia.setObservacion(new Observacion());
	}

	@NotifyChange("listaAsistencia")
	@Command
	public void eliminar(@BindingParam("asistencia") Asistencia asistencia) {
		String msj = "Esta seguro(a) que desea eliminar el registro.";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						if (asistencia.getObservacion().getId() != null) {
							Observacion observacion = asistencia.getObservacion();
							asistencia.setObservacion(null);
							InstanciaDAO.asistenciaDAO.saveorUpdate(asistencia);
							int a = InstanciaDAO.observacionDAO.eliminar(observacion);
							if (a == 1) {
								Clients.showNotification("Registro eliminado satisfactoriamente",
										Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassAsistencia.this, "listaAsistencia");
							} else {
								Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
										null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassAsistencia.this, "listaAsistencia");
							}
						} else {
							asistencia.setObservacion(null);
							BindUtils.postNotifyChange(null, null, ClassAsistencia.this, "listaAsistencia");
						}
					}
				});
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Asistencia> getListaAsistencia() {
		return listaAsistencia;
	}

	public void setListaAsistencia(List<Asistencia> listaAsistencia) {
		this.listaAsistencia = listaAsistencia;
	}

	public List<Estado> getListaEstadoAsistencia() {
		return listaEstadoAsistencia;
	}

	public void setListaEstadoAsistencia(List<Estado> listaEstadoAsistencia) {
		this.listaEstadoAsistencia = listaEstadoAsistencia;
	}

	public Estado getEstadoAsistencia() {
		return estadoAsistencia;
	}

	public void setEstadoAsistencia(Estado estadoAsistencia) {
		this.estadoAsistencia = estadoAsistencia;
	}

	public List<Estado> getListaEstadoAsistenciaCabecera() {
		return listaEstadoAsistenciaCabecera;
	}

	public void setListaEstadoAsistenciaCabecera(List<Estado> listaEstadoAsistenciaCabecera) {
		this.listaEstadoAsistenciaCabecera = listaEstadoAsistenciaCabecera;
	}

	public Timestamp getFechaAsistencia() {
		return fechaAsistencia;
	}

	public void setFechaAsistencia(Timestamp fechaAsistencia) {
		this.fechaAsistencia = fechaAsistencia;
	}

}
