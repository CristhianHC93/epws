package ec.com.epws;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassQuimestreCE {
	@Wire("#winQuimestreCE")
	private Window winQuimestreCE;
	
	private Quimestre quimestre;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("objeto") Quimestre quimestre) {
		Selectors.wireComponents(view, this, false);
		this.quimestre = (quimestre == null) ? new Quimestre() : quimestre;
	}

	@Command
	@NotifyChange("quimestre")
	public void guardarEditar() {
		bandera = true;
		if (quimestre.getId() == null) {
			InstanciaDAO.quimestreDAO.saveorUpdate(quimestre);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			quimestre = new Quimestre();
		} else {
			InstanciaDAO.quimestreDAO.saveorUpdate(quimestre);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("quimestre")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			quimestre.setDescripcion((cadena.toUpperCase()));
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winQuimestreCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Quimestre getQuimestre() {
		return quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}
	
}
