package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.CalificacionParcial;
import ec.com.epws.entity.CalificacionParcialCualitativa;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Parcial;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.models.Modelos;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassReporteParcialAlumno {

	private List<Curso> listaCurso;
	private Curso curso;
	private List<Quimestre> listaQuimestre;
	private Quimestre quimestre;
	private List<Parcial> listaParcial;
	private Parcial parcial;
	private List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> listaCalificacionParcial = new ArrayList<>();
	
	private Matricula matricula;
	private List<Matricula> listaMatricula = new ArrayList<>();
	
	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("curso") Curso curso,
			@ExecutionArgParam("parcial") Parcial parcial,@ExecutionArgParam("matricula") Matricula matricula,
			@ExecutionArgParam("lista") List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> listaCalificacionParcial,
			@ExecutionArgParam("listaMatricula") List<Matricula> listaMatricula) {
		Selectors.wireComponents(vista, this, false);
		if (parcial != null) {
			this.curso = curso;
			this.parcial = parcial;
			this.matricula = matricula;
			this.listaMatricula = listaMatricula;
			if (listaCalificacionParcial != null) {
				this.listaCalificacionParcial = listaCalificacionParcial;
			}
		} else {
			listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
			this.curso = listaCurso.get(0);
			listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
			this.quimestre = listaQuimestre.get(0);
			listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
			this.parcial = listaParcial.get(0);
		}
	}
	
	@NotifyChange("listaCalificacionParcial")
	@Command
	public List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> buscar(Matricula matricula) {
		if (matricula != null) {
			listaCalificacionParcial.clear();
			List<CalificacionParcial> listaParcial = InstanciaDAO.calificacionParcialDAO
					.getListaCalificacionParcialByMatriculaAndParcial(matricula.getId(), parcial.getId());
			List<CalificacionParcialCualitativa> listaQuimestreCualitativa = InstanciaDAO.calificacionParcialCualitativaDAO
					.getListaCalificacionParcialCualitativaByMatriculaAndParcial(matricula.getId(), parcial.getId());
			listaParcial.forEach(x -> {
				//x.setCalificacionParcials(InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByCalificacionQuimestre(x.getId()));				
				listaCalificacionParcial
					.add(new Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>(x, null,
							null, null, true));});
			listaQuimestreCualitativa.forEach(x -> {
				//x.setCalificacionParcialCualitativas(InstanciaDAO.calificacionParcialCualitativaDAO.getListaCalificacionParcialCualitativaByCalificacionQuimestre(x.getId()));
				listaCalificacionParcial
					.add(new Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>(null, x,
							null, null, true));});
		}
		return listaCalificacionParcial;
	}
	
	@Command
	public void agregarAlumno() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Pension");
		parametros.put("curso", curso.getId());
		Executions.createComponents("/vista/formMatricula.zul", null, parametros);
	}

	@NotifyChange({ "matricula", "listaCalificacionParcial" })
	@GlobalCommand
	public void cargarAlumno(@BindingParam("objeto") Matricula matricula) {
		this.matricula = matricula;
		buscar(this.matricula);
	}
	
	@NotifyChange({ "matricula", "listaCalificacionParcial" })
	@Command
	public void selectCurso(){
		matricula = null;
		listaCalificacionParcial.clear();
	}
	
	@NotifyChange({"parcial", "listaParcial", "listaCalificacionParcial" })
	@Command
	public void selectQuimestre(){
		listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
		parcial = listaParcial.get(0);
		if (matricula != null) {
			buscar(matricula);
		}
	}
	
	@NotifyChange({ "matricula", "listaCalificacionParcial" })
	@Command
	public void selectParcial(){
		if (matricula != null) {
			buscar(matricula);
		}
	}
	
	@Command
	public void generarExcel() {
		if (listaCalificacionParcial.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}

		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("parcial", parcial.getDescripcion());
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		mapreporte.put("alumno", matricula.getAlumno().getNombre()+" "+matricula.getAlumno().getApellido());
		Sistema.reporteEXCEL(Sistema.PARCIAL_ALUMNO_JASPER, "Parcial", mapreporte,
				Sistema.getParcialDataSource(listaCalificacionParcial),null,true);
	}

	@Command
	public void generarPdf() {
		if (listaCalificacionParcial.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaCalificacionParcial);
		parametros.put("curso", curso);
		parametros.put("matricula", matricula);
		parametros.put("parcial", parcial);
		Executions.createComponents("/reporte/impresionParcialAlumno.zul", null, parametros);
	}
	
	@Command
	public void generarPdfTodo() {
		List<Matricula> listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());		
		if (listaMatricula.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("listaMatricula", listaMatricula);
		parametros.put("curso", curso);
		parametros.put("matricula", matricula);
		parametros.put("parcial", parcial);
		Executions.createComponents("/reporte/impresionParcialAlumno.zul", null, parametros);
	}

	AMedia amedia;
	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("parcial", parcial.getDescripcion());
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		if (listaMatricula != null) {
			List<AMedia> lista = new ArrayList<>();
			listaMatricula.forEach(x -> {
				mapreporte.put("alumno", x.getAlumno().getNombre()+" "+x.getAlumno().getApellido());
				amedia = Sistema.reportePDF(Sistema.PARCIAL_ALUMNO_JASPER, mapreporte,
						Sistema.getParcialDataSource(buscar(x)));
				lista.add(amedia);
			});
			iframe.setContent(Sistema.reportePDF(lista));
		}else{
			mapreporte.put("alumno", matricula.getAlumno().getNombre()+" "+matricula.getAlumno().getApellido());
			amedia = Sistema.reportePDF(Sistema.PARCIAL_ALUMNO_JASPER, mapreporte,
					Sistema.getParcialDataSource(listaCalificacionParcial));
			iframe.setContent(amedia);
		}
	}
	
	
	/*
	 * 
	 */
	
	public Idioma getIdioma() {
		return Sistema.idioma;
	}
	public List<Curso> getListaCurso() {
		return listaCurso;
	}
	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}
	public Curso getCurso() {
		return curso;
	}
	public void setCurso(Curso curso) {
		this.curso = curso;
	}
	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}
	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}
	public Quimestre getQuimestre() {
		return quimestre;
	}
	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}
	public List<Parcial> getListaParcial() {
		return listaParcial;
	}
	public void setListaParcial(List<Parcial> listaParcial) {
		this.listaParcial = listaParcial;
	}
	public Parcial getParcial() {
		return parcial;
	}
	public void setParcial(Parcial parcial) {
		this.parcial = parcial;
	}
	public List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> getListaCalificacionParcial() {
		return listaCalificacionParcial;
	}
	public void setListaCalificacionParcial(
			List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> listaCalificacionParcial) {
		this.listaCalificacionParcial = listaCalificacionParcial;
	}
	public Matricula getMatricula() {
		return matricula;
	}
	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}
	
	
}
