package ec.com.epws;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Menuitem;

import ec.com.epws.entity.InformacionNegocio;
import ec.com.epws.entity.ModuloDetalle;
import ec.com.epws.entity.Noticia;
import ec.com.epws.entity.RolModulo;
import ec.com.epws.entity.Usuario;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.sistema.Sistema;

//esta clase interactua con dos vista index.zul y /template/banner.zul
public class Index {

	private List<RolModulo> listaRolModulo = new ArrayList<>();

	// lista de moduloDetalle para llenar listbox que mostrara los detalles
	private List<ModuloDetalle> listaModuloDetalle = new ArrayList<>();
	private ModuloDetalle moduloDetalle = new ModuloDetalle();
	// variable para la ruta
	private String src = "main.zul";
	// variables para efecto de ocultar bara
	private String anchoBarra = "190px";
	private boolean visibleBarra = true;
	private String btDesplegarBarra = "z-icon-angle-double-left";

	private Usuario usuario;

	private List<Noticia> listaNoticia = InstanciaDAO.noticiaDAO.getListaNoticia();
	private InformacionNegocio informacionNegocio = Sistema.getInformacionNegocio();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		usuario = (Usuario) Sessions.getCurrent().getAttribute("Usuario");
		if (usuario != null) {
			listaRolModulo = InstanciaDAO.getRolModuloDAO().getListaRolModulo(usuario.getRol().getId(),
					Sistema.ESTADO_ACTIVAR_USUARIO);
		}
	}

	@Command
	@NotifyChange({ "anchoBarra", "visibleBarra", "btDesplegarBarra" })
	public void ocultarBarra() {
		if (visibleBarra) {
			visibleBarra = false;
			anchoBarra = "20px";
			btDesplegarBarra = "z-icon-angle-double-right";
		} else {
			visibleBarra = true;
			anchoBarra = "190px";
			btDesplegarBarra = "z-icon-angle-double-left";
		}
	}

	// Metodo que se activa cuando damos click en uno de los detalles y cambiara
	// la variable src la cual esta bindeada
	// en index.zul y nos redirije a la ruta del detalle
	@Command
	@NotifyChange("src")
	public void select(@BindingParam("item") String ruta) {
		src = ruta;
	}

	@Command
	@NotifyChange("src")
	public void src(@BindingParam("item") Menuitem item) {
		setSrc(item.getId());
	}

	@Command
	public void acercaDe() {
		Executions.createComponents("/vista/formAcercaDe.zul", null, null);
	}

	/*
	 * Metodos GET and SET
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getUrlLogin(){
		return Sistema.URL_SISTEMA;
	}
	
	public String getUrlMapa(){
		return Sistema.URL_MAPA;
	}
	
	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}

	public List<ModuloDetalle> getListaModuloDetalle() {
		return listaModuloDetalle;
	}

	public void setListaModuloDetalle(List<ModuloDetalle> listaModuloDetalle) {
		this.listaModuloDetalle = listaModuloDetalle;
	}

	public ModuloDetalle getModuloDetalle() {
		return moduloDetalle;
	}

	public void setModuloDetalle(ModuloDetalle moduloDetalle) {
		this.moduloDetalle = moduloDetalle;
	}

	public String getAnchoBarra() {
		return anchoBarra;
	}

	public void setAnchoBarra(String anchoBarra) {
		this.anchoBarra = anchoBarra;
	}

	public boolean isVisibleBarra() {
		return visibleBarra;
	}

	public void setVisibleBarra(boolean visibleBarra) {
		this.visibleBarra = visibleBarra;
	}

	public String getBtDesplegarBarra() {
		return btDesplegarBarra;
	}

	public void setBtDesplegarBarra(String btDesplegarBarra) {
		this.btDesplegarBarra = btDesplegarBarra;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public String getMensajeUsuario() {
		return usuario.getNombre();
	}

	public List<RolModulo> getListaRolModulo() {
		return listaRolModulo;
	}

	public void setListaRolModulo(List<RolModulo> listaRolModulo) {
		this.listaRolModulo = listaRolModulo;
	}

	public InformacionNegocio getInformacionNegocio() {
		return informacionNegocio;
	}

	public void setInformacionNegocio(InformacionNegocio informacionNegocio) {
		this.informacionNegocio = informacionNegocio;
	}

	public List<Noticia> getListaNoticia() {
		return listaNoticia;
	}

	public void setListaNoticia(List<Noticia> listaNoticia) {
		this.listaNoticia = listaNoticia;
	}

}
