package ec.com.epws;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Alumno;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Pension;
import ec.com.epws.entity.Rol;
import ec.com.epws.entity.Usuario;
import ec.com.epws.entity.ValorReferencia;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassMatriculaCE {
	@Wire("#winMatriculaCE")
	private Window winMatriculaCE;

	private String numeroMatricula;

	private Matricula matricula;

	private boolean visibleBuscar;

	private String password;
	private String passwordRepetir;
	private String claveRepetida;
	boolean validarRepetido = false;

	// private Estado estado = InstanciaEntity.getInstanciaEstado();
	// private MatriculaDAO matriculaDAO =
	// InstanciaDAO.getInstanciaMatriculaDAO();

	private boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Matricula matricula) {
		Selectors.wireComponents(view, this, false);
		if (matricula == null) {
			this.matricula = new Matricula();
			this.matricula.setFecha(new Timestamp(new Date().getTime()));
			this.matricula.setAlumno(new Alumno());
			this.matricula.getAlumno().setUsuario(new Usuario());
			visibleBuscar = true;
		} else {
			this.matricula = matricula;
			visibleBuscar = false;
			this.matricula.getAlumno().setUsuario(matricula.getAlumno().getUsuario());
			desencriptar(this.matricula.getAlumno().getUsuario());
		}
		this.matricula.setEstado(Sistema.getEstado(Sistema.ESTADO_INGRESADO));
	}

	@Command
	@NotifyChange({ "matricula" })
	public void guardarEditar() {
		validarPassword();
		if (matricula.getAlumno() == null) {
			Messagebox.show("Debe Escojer un alumno a matricular");
			return;
		}
		if (matricula.getCurso() == null) {
			Messagebox.show("Debe Escojer un curso a matricular");
			return;
		}
		if (!validarRepetido) {
			Messagebox.show("Claves no coinciden");
			return;
		}
		matricula.getAlumno().getUsuario().setCedula(matricula.getAlumno().getCedula());
		matricula.getAlumno().getUsuario().setNombre(matricula.getAlumno().getNombre());
		matricula.getAlumno().getUsuario().setApellido(matricula.getAlumno().getApellido());
		matricula.getAlumno().getUsuario().setPassword(Sistema.Encriptar(password));
		matricula.getAlumno().getUsuario().setEstado(true);
		if (Sistema.validarRepetido(InstanciaDAO.usuarioDAO.getListaUsuario(), matricula.getAlumno().getUsuario())) {
			return;
		}
		List<Matricula> lm = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByAlumnoByEstado(Sistema.getAnioLectivoActivoEstatico().getId(), matricula.getAlumno().getId(), Sistema.ESTADO_INGRESADO);
		if (lm.size() > 0) {
			Messagebox.show("Alumno ya ha sido matriculado, si desea modificar por favor dirijirse a la ventana anterior");
			return;
		}
		if (matricula.getId() == null) {
			matricula.setAnioLectivo(InstanciaDAO.anioLectivoDAO.getAnioLectivoActivo());
			ValorReferencia vr = InstanciaDAO.valorReferenciaDAO.getValorReferencia();
			vr.setNumeroMatricula(vr.getNumeroMatricula() + 1);
			InstanciaDAO.valorReferenciaDAO.saveUpdate(vr);
			matricula.setNumero(String.valueOf(vr.getNumeroMatricula()));
			Rol rol = new Rol();
			rol.setId(Sistema.ID_ROL_REPRESENTANTE);
			matricula.getAlumno().getUsuario().setRol(rol);
			InstanciaDAO.getAlumnoDAO().saveorUpdate(matricula.getAlumno());
			matricula = InstanciaDAO.matriculaDAO.saveorUpdate(matricula);
			Clients.showNotification("Matricula ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			InstanciaDAO.mesDAO.getListaMes().forEach(x -> {
				Pension pension = new Pension();
				pension.setMatricula(matricula);
				pension.setMes(x);
				pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_NO_PAGADA));
				pension.setValorReferencia(Sistema.getValorReferencia());
				if (x.getId() == 1) {
					pension.setValorPagado(Sistema.getValorReferencia().getValorMatricula());
					pension.setEstado(Sistema.getEstado(Sistema.ESTADO_PENSION_PAGADA));
				}
				InstanciaDAO.pensionDAO.saveorUpdate(pension);
			});
			bandera = true;
		} else {
			InstanciaDAO.usuarioDAO.saveorUpdate(matricula.getAlumno().getUsuario());
			InstanciaDAO.getAlumnoDAO().saveorUpdate(matricula.getAlumno());
			InstanciaDAO.matriculaDAO.saveorUpdate(matricula);
			Clients.showNotification("Matricula Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
		if (bandera) {
			matricula = new Matricula();
			matricula.setEstado(Sistema.getEstado(Sistema.ESTADO_INGRESADO));
			password = "";
			passwordRepetir = "";
		}
	}

	public void desencriptar(Usuario usuario) {
		try {
			password = Sistema.desencriptar(usuario.getPassword());
			passwordRepetir = password;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@NotifyChange("claveRepetida")
	@Command
	public void validarPassword() {
		if (passwordRepetir != null && password != null && !passwordRepetir.trim().equals("")
				&& !password.trim().equals("")) {
			if (!password.trim().equals(passwordRepetir.trim())) {
				validarRepetido = false;
				claveRepetida = "Claves no coinciden";
			} else {
				validarRepetido = true;
				claveRepetida = "";
			}
		}
	}

	@Command
	public void agregarAlumno() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Matricula");
		Executions.createComponents("/vista/formInscripcion.zul", null, parametros);
	}

	@Command
	public void agregarInscripcion() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Inscripcion");
		Executions.createComponents("/vista/formInscripcion.zul", null, parametros);
	}

	@NotifyChange("matricula")
	@GlobalCommand
	public void cargarAlumno(@BindingParam("objeto") Alumno alumno) {
		matricula.setAlumno(alumno);
		matricula.getAlumno().setUsuario((alumno.getUsuario() == null)? new Usuario(): alumno.getUsuario());
		desencriptar(this.matricula.getAlumno().getUsuario());
	}

	@Command
	public void agregarCurso() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Alumno");
		Executions.createComponents("/vista/formCurso.zul", null, parametros);
	}

	@NotifyChange({ "matricula" })
	@GlobalCommand
	public void cargarCurso(@BindingParam("objeto") Curso curso) {
		matricula.setCurso(curso);
	}

	@Command
	public void salir() {
		BindUtils.postGlobalCommand(null, null, "refrescarlistaMatricula", null);
		winMatriculaCE.detach();
	}

	// --
	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Window getWinMatriculaCE() {
		return winMatriculaCE;
	}

	public void setWinMatriculaCE(Window winMatriculaCE) {
		this.winMatriculaCE = winMatriculaCE;
	}

	public String getNumeroMatricula() {
		return numeroMatricula;
	}

	public void setNumeroMatricula(String numeroMatricula) {
		this.numeroMatricula = numeroMatricula;
	}

	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	public boolean isVisibleBuscar() {
		return visibleBuscar;
	}

	public void setVisibleBuscar(boolean visibleBuscar) {
		this.visibleBuscar = visibleBuscar;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepetir() {
		return passwordRepetir;
	}

	public void setPasswordRepetir(String passwordRepetir) {
		this.passwordRepetir = passwordRepetir;
	}

	public String getClaveRepetida() {
		return claveRepetida;
	}

	public void setClaveRepetida(String claveRepetida) {
		this.claveRepetida = claveRepetida;
	}

}
