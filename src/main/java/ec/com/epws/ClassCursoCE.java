package ec.com.epws;

import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Nivel;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassCursoCE {
	@Wire("#winCursoCE")
	private Window winCursoCE;
	private List<Nivel> listaNivel;
	private Curso curso;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("objeto") Curso curso) {
		Selectors.wireComponents(view, this, false);
		listaNivel = InstanciaDAO.getNivelDAO().getListaNivel();
		this.curso = (curso == null) ? new Curso() : curso;
	}

	@Command
	@NotifyChange("curso")
	public void guardarEditar() {
		bandera = true;
		if (curso.getId() == null) {
			InstanciaDAO.getCursoDAO().saveorUpdate(curso);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			curso = new Curso();
		} else {
			InstanciaDAO.getCursoDAO().saveorUpdate(curso);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("curso")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			curso.setDescripcion((cadena.toUpperCase()));
		}
		if (campo.equals("abreviado")) {
			curso.setAbreviado((cadena.toUpperCase()));
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winCursoCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintEntero() {
		return Sistema.CONSTRAINT_ENTERO;
	}

	public String getConstraintEnteroVacio() {
		return Sistema.CONSTRAINT_ENTERO_VACIO;
	}

	public String getConstraintLetra() {
		return Sistema.CONSTRAINT_LETRA;
	}

	public String getConstraintLetraVacio() {
		return Sistema.CONSTRAINT_LETRA_VACIO;
	}

	public List<Nivel> getListaNivel() {
		return listaNivel;
	}

	public void setListaNivel(List<Nivel> listaNivel) {
		this.listaNivel = listaNivel;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

}
