package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.AsignacionDocente;
import ec.com.epws.entity.AsignacionTutor;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Docente;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassAsignacionDocente {
	@Wire("#winAsignacionDocente")
	private Window winAsignacionDocente;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<AsignacionDocente> listaAsignacionDocente;
	private List<Curso> listaCursoBuscar;
	private Curso curso;
	private AsignacionTutor tutor;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaCursoBuscar = InstanciaDAO.getCursoDAO().getListaCurso();
		curso = listaCursoBuscar.get(0);
		listaAsignacionDocente = InstanciaDAO.asignacionDocenteDAO
				.getListaAsignacionDocenteByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());
		if (InstanciaDAO.asignacionTutorDAO.getAsignacionTutor(Sistema.getAnioLectivoActivoEstatico().getId(),
				curso.getId()) == null) {
			tutor = new AsignacionTutor();
		}else{
			tutor = InstanciaDAO.asignacionTutorDAO.getAsignacionTutor(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());
		}
	}

	@NotifyChange({ "listaAsignacionDocente", "tutor" })
	@Command
	public void select() {
		listaAsignacionDocente = InstanciaDAO.asignacionDocenteDAO
				.getListaAsignacionDocenteByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());
		tutor = (InstanciaDAO.asignacionTutorDAO.getAsignacionTutor(Sistema.getAnioLectivoActivoEstatico().getId(),
				curso.getId()) == null) ? new AsignacionTutor()
						: InstanciaDAO.asignacionTutorDAO.getAsignacionTutor(Sistema.getAnioLectivoActivoEstatico().getId(),
								curso.getId());
	}

	@Command
	public void nuevo() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("curso", curso);
		Executions.createComponents("/vista/formAsignacionDocenteCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaAsignacionDocente")
	public void eliminar(@BindingParam("asignacionDocente") final AsignacionDocente asignacionDocente) {
		String msj = "Esta seguro(a) que desea este registro esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.asignacionDocenteDAO.eliminar(asignacionDocente);
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaAsignacionDocente.clear();
							listaAsignacionDocente = InstanciaDAO.asignacionDocenteDAO
									.getListaAsignacionDocenteByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());
							BindUtils.postNotifyChange(null, null, ClassAsignacionDocente.this,
									"listaAsignacionDocente");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassAsignacionDocente.this,
									"listaAsignacionDocente");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("asignacionDocente") AsignacionDocente asignacionDocente) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", asignacionDocente);
		BindUtils.postGlobalCommand(null, null, "cargarAsignacionDocente", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaAsignacionDocente")
	public void refrescarlista() {
		listaAsignacionDocente = InstanciaDAO.asignacionDocenteDAO
				.getListaAsignacionDocenteByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());
	}

	@Command
	public void editar(@BindingParam("asignacionDocente") AsignacionDocente asignacionDocente) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", asignacionDocente);
		parameters.put("curso", curso);
		Executions.createComponents("/vista/formAsignacionDocenteCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaAsignacionDocente")
	public void filtrar() {
		listaAsignacionDocente.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaAsignacionDocente.addAll(InstanciaDAO.asignacionDocenteDAO
					.getListaAsignacionDocenteByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId()));
		} else {
			for (AsignacionDocente item : InstanciaDAO.asignacionDocenteDAO
					.getListaAsignacionDocenteByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId())) {
				if (item.getDocente().getUsuario().getCedula().toLowerCase()
						.indexOf(datoBusqueda.trim().toLowerCase()) == 0
						|| item.getDocente().getUsuario().getNombre().trim().toLowerCase()
								.indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaAsignacionDocente.add(item);
				}
			}
		}
	}

	@Command
	public void agregarTutor() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "AsignacionDocente");
		Executions.createComponents("/vista/formDocente.zul", null, parametros);
	}

	private boolean bandera = false;
	String cursso = "";

	@NotifyChange("tutor")
	@GlobalCommand
	public void cargarTutor(@BindingParam("objeto") Docente docente) {
		InstanciaDAO.asignacionTutorDAO.getListaAsignacionTutor(Sistema.getAnioLectivoActivoEstatico().getId()).forEach(x -> {
			if (x != null && x.getDocente().getId() == docente.getId()) {
				bandera = true;
				cursso = x.getCurso().getDescripcion();
			}
		});
		if (bandera) {
			Messagebox.show("Este Docente " + docente.getUsuario().getNombre() + " ya ha sido designado como tutor de "
					+ cursso);
			bandera = false;
			return;
		}
		tutor.setAnioLectivo(Sistema.getAnioLectivoActivoEstatico());
		tutor.setDocente(docente);
		tutor.setCurso(curso);
		InstanciaDAO.asignacionTutorDAO.saveorUpdate(tutor);
	}

	@Command
	public void salir() {
		winAsignacionDocente.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public Window getWinAsignacionDocente() {
		return winAsignacionDocente;
	}

	public void setWinAsignacionDocente(Window winAsignacionDocente) {
		this.winAsignacionDocente = winAsignacionDocente;
	}

	public List<AsignacionDocente> getListaAsignacionDocente() {
		return listaAsignacionDocente;
	}

	public void setListaAsignacionDocente(List<AsignacionDocente> listaAsignacionDocente) {
		this.listaAsignacionDocente = listaAsignacionDocente;
	}

	public List<Curso> getListaCursoBuscar() {
		return listaCursoBuscar;
	}

	public void setListaCursoBuscar(List<Curso> listaCursoBuscar) {
		this.listaCursoBuscar = listaCursoBuscar;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public AsignacionTutor getTutor() {
		return tutor;
	}

	public void setTutor(AsignacionTutor tutor) {
		this.tutor = tutor;
	}
}
