package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Curso;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassCurso {
	@Wire("#winCurso")
	private Window winCurso;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Curso> listaCurso;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formCursoCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaCurso")
	public void eliminar(@BindingParam("curso") final Curso curso) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + curso.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.getCursoDAO().eliminar(curso);
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaCurso.clear();
							listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
							BindUtils.postNotifyChange(null, null, ClassCurso.this, "listaCurso");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassCurso.this, "listaCurso");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("curso") Curso curso) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", curso);
		BindUtils.postGlobalCommand(null, null, "cargarCurso", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaCurso")
	public void refrescarlista() {
		listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
	}

	@Command
	public void editar(@BindingParam("curso") Curso curso) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", curso);
		Executions.createComponents("/vista/formCursoCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaCurso")
	public void filtrar() {
		listaCurso.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaCurso.addAll(InstanciaDAO.getCursoDAO().getListaCurso());
		} else {
			for (Curso item : InstanciaDAO.getCursoDAO().getListaCurso()) {
				if (item.getDescripcion().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
						|| item.getNivel().getDescripcion().trim().toLowerCase()
								.indexOf(datoBusqueda.trim().toLowerCase()) == 0
						|| item.getAbreviado().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaCurso.add(item);
				}
			}
		}
	}

	@Command
	public void salir() {
		winCurso.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}

}
