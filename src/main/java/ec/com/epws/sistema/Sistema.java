package ec.com.epws.sistema;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Arrays;
import java.util.Date;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.codec.binary.Base64;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Messagebox;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfCopyFields;
import com.lowagie.text.pdf.PdfReader;

import ec.com.epws.data.source.CertificadoPromocionDataSource;
import ec.com.epws.data.source.InformeQuimestreDataSource;
import ec.com.epws.data.source.MatriculaDataSource;
import ec.com.epws.data.source.ParcialAsignaturaDataSource;
import ec.com.epws.data.source.PensionDataSource;
import ec.com.epws.data.source.PensionMesDataSource;
import ec.com.epws.entity.Alumno;
import ec.com.epws.entity.AnioLectivo;
import ec.com.epws.entity.CalificacionParcial;
import ec.com.epws.entity.CalificacionParcialCualitativa;
import ec.com.epws.entity.CalificacionQuimestre;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;
import ec.com.epws.entity.CodigoCualitativa;
import ec.com.epws.entity.CuadroCalificacion;
import ec.com.epws.entity.Cualitativa;
import ec.com.epws.entity.Estado;
import ec.com.epws.entity.Familia;
import ec.com.epws.entity.InformacionNegocio;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Parentesco;
import ec.com.epws.entity.Pension;
import ec.com.epws.entity.Usuario;
import ec.com.epws.entity.ValorReferencia;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.models.Modelos;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleXlsReportConfiguration;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.factory.InstanciaIdioma;

public class Sistema {

	private Sistema() {
	}

	public static final String PAGINA = "http://www.uewilliamshakespeare.edu.ec";
	// public static final String PAGINA = "http://epws.eastus.cloudapp.azure.com";
	public static final String URL = PAGINA;// +":8080/epws";
	public static final String URL_SISTEMA = URL + "/login.zul";
	public static final String URL_MAPA = URL + "/mapa.zul";

	public static final String ME = "/jasperReport/me.png";
	public static final String LOGO = "/jasperReport/logo.png";

	public static Usuario getUsuario() {
		return (Usuario) Sessions.getCurrent().getAttribute("Usuario");
	}

	static AnioLectivo anioLectivo;

	public static AnioLectivo getAnioLectivoActivo() {
		anioLectivo = InstanciaDAO.anioLectivoDAO.getAnioLectivoActivo();
		return anioLectivo;
	}

	public static AnioLectivo getAnioLectivoActivoEstatico() {
		if (anioLectivo == null) {
			anioLectivo = getAnioLectivoActivo();
		}
		return anioLectivo;
	}

	public static Idioma idioma;

	public static String setIdioma(String letra) {
		// idioma = letra;
		return null;
	}

	public static Idioma getIdioma(String idioma) {
		Idioma devuelto = null;
		if (idioma.trim().equals("Latino")) {
			devuelto = InstanciaIdioma.getLatino();
		}
		return devuelto;
	}

	private static List<String> listaIdioma = new ArrayList<String>();

	static {
		listaIdioma.add("Latino");
		listaIdioma.add("Ingles");
	}

	public static final List<String> getListaIdioma() {
		return new ArrayList<String>(listaIdioma);
	}

	public static final String carpetaCertificadoMedico = "certificado_medico";
	public static final String carpetaFotoAlumno = "foto_alumno";
	public static final String carpetaFotoDocente = "foto_docente";
	public static final String carpetaFacturaPDF = "facturaPDF";
	public static final String carpetaNoticia = "noticia";

	public static final double COMPENSACION_PORCENTAJE = 0.02;

	public static final String footerMessage = "Un Total de %d Items";
	public static final String formatCantidad = "#,###.##";
	public static final String formatNumero = "#########";
	public static final String formatPrecio = "#,##0.00##";

	// Expresion Regular
	public static final String CONSTRAINT_PRECIO = "no empty,/^[0-9]*.[0-9]*";
	public static final String CONSTRAINT_ENTERO = "no empty,/^[0-9]+$/";
	public static final String CONSTRAINT_ENTERO_VACIO = "/^[0-9]*$/";
	public static final String CONSTRAINT_LETRA = "/^[a-z A-ZÃ‘Ã±Ã€Ã�ÃˆÃ‰ÃŒÃ�Ã’Ã“Ã Ã¡Ã¨Ã©Ã¬Ã­Ã²Ã³Ã¹ÃºÃ¼ ]+$/";
	public static final String CONSTRAINT_LETRA_VACIO = "/^[a-z A-Z]*$/";
	public static final String CONSTRAINT_CORREO = "/.+@.+\\.[a-z]+";

	public static final String CODIGO = "CÃ³digo";
	/*
	 * Para Calorimetria de Productos
	 */
	public static final String STOCK_ALERTA = "background-color:yellow;";
	public static final String STOCK_CRITICO = "background-color:red;";

	/*
	 * Estados del Sistema
	 */

	public static final int ESTADO_ACTIVAR_USUARIO = 1;
	public static final int ESTADO_DESACTIVAR_USUARIO = 2;
	public static final int ESTADO_INGRESADO = 3;
	public static final int ESTADO_ELIMINADO = 4;
	public static final int ESTADO_PENSION_PAGADA = 5;
	public static final int ESTADO_PENSION_NO_PAGADA = 6;
	public static final int ESTADO_PENSION_DEUDA = 7;

	public static final int NIVEL_INICIAL = 1;
	public static final int NIVEL_BASICO = 2;
	public static final int NIVEL_MEDIO = 3;

	public static final int ID_ROL_DOCENTE = 2;
	public static final int ID_ROL_REPRESENTANTE = 4;

	public static Estado estado;

	public static Estado getEstado(int id) {
		if (estado == null) {
			estado = new Estado();
		}
		estado.setId(id);
		return estado;
	}

	/*
	 * Id's de Representantes
	 */
	public static final int PARENTESCO_PADRE = 1;
	public static final int PARENTESCO_MADRE = 2;
	public static final int PARENTESCO_REPRESENTANTE = 3;

	/*
	 * Filtron para Estado
	 */

	public static final int FILTRO_ESTADO_TODAS = 1;
	public static final int FILTRO_ESTADO_VENTA = 2;
	public static final int FILTRO_ESTADO_USUARIO = 3;
	public static final int FILTRO_ESTADO_PENSION = 4;
	public static final int FILTRO_ESTADO_ASISTENCIA = 5;

	/*
	 * Filtron para Cualitativo
	 */

	public static final int FILTRO_CUALITATIVO_INICIAL = 1;
	public static final int FILTRO_CUALITATIVO_BASICO = 2;
	public static final int FILTRO_CUALITATIVO_MATERIA = 3;
	public static final int FILTRO_CUALITATIVO_COMPORTAMIENTO = 4;

	/*
	 * Tab del Menu
	 */

	public static final String INFORMACION = "tabInformacion";
	public static final String ACADEMICA = "tabAcademica";
	public static final String SECRETARIA = "tabSecretaria";
	public static final String DOCENTE = "tabDocente";
	public static final String REPORTE = "tabReporte";
	public static final String CONFIGURACION = "tabConfiguracion";
	public static final String PARAMETRO = "tabParametro";
	public static final String REPRESENTATE = "tabRepresentante";
	public static final String USUARIO = "tabUsuario";

	public static final Parentesco PADRE = InstanciaDAO.getParentescoDAO().getParentesco(PARENTESCO_PADRE);
	public static final Parentesco MADRE = InstanciaDAO.getParentescoDAO().getParentesco(PARENTESCO_MADRE);;

	/*
	 * Convertir numeros a Equivalente a Letras
	 */

	public static String numeroAletras(double num) {
		n2t numero = new n2t();
		int p_ent = (int) num;
		int p_dec = (int) Sistema.redondear((num - p_ent) * 100);
		return numero.convertirLetras(p_ent).toUpperCase() + " COMA " + numero.convertirLetras(p_dec).toUpperCase();
	}

	/*
	 * Obtener Fecha Actual Con Formato
	 */
	public static String getFechaActual() {
		Date today = new Date();
		String dateOut;
		Locale currentLocale = new Locale("es", "ES");
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.LONG, currentLocale);
		dateOut = dateFormatter.format(today);
		return dateOut;
	}

	/*
	 * Metodo para encriptar
	 */

	public static String Encriptar(String texto) {

		String secretKey = "qualityinfosolutions";
		String base64EncryptedString = "";

		try {

			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
			byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);

			SecretKey key = new SecretKeySpec(keyBytes, "DESede");
			Cipher cipher = Cipher.getInstance("DESede");
			cipher.init(Cipher.ENCRYPT_MODE, key);

			byte[] plainTextBytes = texto.getBytes("utf-8");
			byte[] buf = cipher.doFinal(plainTextBytes);
			byte[] base64Bytes = Base64.encodeBase64(buf);
			base64EncryptedString = new String(base64Bytes);

		} catch (Exception ex) {
		}
		return base64EncryptedString;
	}
	/*
	 * Metodo para Desencriptar
	 */

	public static String desencriptar(String textoEncriptado) throws Exception {

		String secretKey = "qualityinfosolutions";
		String base64EncryptedString = "";

		try {
			byte[] message = Base64.decodeBase64(textoEncriptado.getBytes("utf-8"));
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digestOfPassword = md.digest(secretKey.getBytes("utf-8"));
			byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
			SecretKey key = new SecretKeySpec(keyBytes, "DESede");

			Cipher decipher = Cipher.getInstance("DESede");
			decipher.init(Cipher.DECRYPT_MODE, key);

			byte[] plainText = decipher.doFinal(message);

			base64EncryptedString = new String(plainText, "UTF-8");

		} catch (Exception ex) {
		}
		return base64EncryptedString;
	}

	public static double redondear(double numero) {
		double resultado;
		resultado = numero * Math.pow(10, 2);
		resultado = Math.round(resultado);
		resultado = resultado / Math.pow(10, 2);
		return resultado;
	}

	public static double redondearEntero(double numero) {
		double resultado;
		resultado = numero * Math.pow(10, 0);
		resultado = Math.round(resultado);
		resultado = resultado / Math.pow(10, 0);
		return resultado;
	}

	private static List<Estado> listaEstadoUsuario;
	private static List<Estado> listaEstadoPension;
	private static List<Estado> listaEstadoAsistencia;
	private static List<Cualitativa> listaCualitativaInicial;
	private static List<Cualitativa> listaCualitativaBasico;
	private static List<Cualitativa> listaCualitativaMateria;
	private static List<Cualitativa> listaCualitativaComportamiento;
	private static List<CodigoCualitativa> listaCodigoCualitativa;

	public static List<Estado> getListaEstadoUsuario() {
		if (listaEstadoUsuario == null) {
			listaEstadoUsuario = InstanciaDAO.getEstadoDAO().getListaEstadoByModulo(FILTRO_ESTADO_USUARIO);
		}
		return listaEstadoUsuario;
	}

	public static List<Estado> getListaEstadoPension() {
		if (listaEstadoPension == null) {
			listaEstadoPension = InstanciaDAO.getEstadoDAO().getListaEstadoByModulo(FILTRO_ESTADO_PENSION);
		}
		return listaEstadoPension;
	}

	public static List<Estado> getListaEstadoAsistencia() {
		if (listaEstadoAsistencia == null) {
			listaEstadoAsistencia = InstanciaDAO.getEstadoDAO().getListaEstadoByModulo(FILTRO_ESTADO_ASISTENCIA);
		}
		return listaEstadoAsistencia;
	}

	public static List<Cualitativa> getListaCualitativaInicial() {
		if (listaCualitativaInicial == null) {
			listaCualitativaInicial = InstanciaDAO.cualitativaDAO
					.getListaCualitativaByCodigo(FILTRO_CUALITATIVO_INICIAL);
		}
		return listaCualitativaInicial;
	}

	public static List<Cualitativa> getListaCualitativaBasico() {
		if (listaCualitativaBasico == null) {
			listaCualitativaBasico = InstanciaDAO.cualitativaDAO.getListaCualitativaByCodigo(FILTRO_CUALITATIVO_BASICO);
		}
		return listaCualitativaBasico;
	}

	public static List<Cualitativa> getListaCualitativaMateria() {
		if (listaCualitativaMateria == null) {
			listaCualitativaMateria = InstanciaDAO.cualitativaDAO
					.getListaCualitativaByCodigo(FILTRO_CUALITATIVO_MATERIA);
		}
		return listaCualitativaMateria;
	}

	public static List<Cualitativa> getListaCualitativaComportamiento() {
		if (listaCualitativaComportamiento == null) {
			listaCualitativaComportamiento = InstanciaDAO.cualitativaDAO
					.getListaCualitativaByCodigo(FILTRO_CUALITATIVO_COMPORTAMIENTO);
		}
		return listaCualitativaComportamiento;
	}

	public static List<CodigoCualitativa> getListaCodigoCualitativa() {
		if (listaCodigoCualitativa == null) {
			listaCodigoCualitativa = InstanciaDAO.codigoCualitativaDAO.getListaCodigoCualitativa();
		}
		return listaCodigoCualitativa;
	}

	public static boolean validarRepetido(List<?> lista, Object objecto) {
		boolean devuelto = false;
		for (Object o : lista) {
			if (objecto instanceof Alumno) {
				if (((Alumno) objecto).getId() != ((Alumno) o).getId()
						&& ((Alumno) objecto).getCedula().trim().equals(((Alumno) o).getCedula().trim())) {
					Messagebox.show(idioma.getMensageCedulaRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof Familia) {
				if (((Familia) objecto).getId() != ((Familia) o).getId()
						&& ((Familia) objecto).getCedula().trim().equals(((Familia) o).getCedula().trim())) {
					Messagebox.show(idioma.getMensageCedulaRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof Usuario) {
				if (((Usuario) objecto).getId() != ((Usuario) o).getId()
						&& ((Usuario) objecto).getUsername().trim().equals(((Usuario) o).getUsername().trim())) {
					Messagebox.show("Este usuario ya ha sido asignado");
					devuelto = true;
				}
				if (((Usuario) objecto).getId() != ((Usuario) o).getId()
						&& ((Usuario) objecto).getCedula().trim().equals(((Usuario) o).getCedula().trim())) {
					Messagebox.show(idioma.getMensageCedulaRepetido());
					devuelto = true;
				}
			}
			if (objecto instanceof Matricula) {
				if (((Matricula) objecto).getId() != ((Matricula) o).getId()
						&& (((Matricula) objecto).getNumero().trim().equals(((Matricula) o).getNumero().trim()))) {
					Messagebox.show(idioma.getMensageNumeroRepetido());
					devuelto = true;
				}
				if (((Matricula) objecto).getAlumno().getCedula().trim()
						.equals(((Matricula) o).getAlumno().getCedula().trim())) {
					Messagebox.show(idioma.getMensageAlumnoRepetido());
					devuelto = true;
				}
			}
		}
		return devuelto;
	}

	// Codigo para consulta total de cedula 'por finalizar'
	/*
	 * private static List<String> lista = new ArrayList<>();
	 * 
	 * public static List<String> getTodasCedula(){ lista.clear();
	 * InstanciaDAO.getAlumnoDAO().getListaAlumno().forEach(x ->
	 * lista.add(x.getCedula()));
	 * InstanciaDAO.getFamiliaDAO().getListaFamilia().forEach(x ->
	 * lista.add(x.getCedula()));
	 * InstanciaDAO.usuarioDAO.getListaUsuario().forEach(x ->
	 * lista.add(x.getCedula())); return lista; }
	 */

	public static boolean validarCorreo(String cadena) {
		if (cadena == null || !cadena.matches(".+@.+\\.[a-z]+")) {
			return false;
		} else {
			return true;
		}
	}

	public static boolean validarNumero(String cadena) {
		if (cadena.matches("\\d*"))
			return true;
		else
			return false;
	}

	public static boolean validarCedula(String cedula) {
		boolean cedulaCorrecta = false;

		try {

			if (cedula.length() == 10) // ConstantesApp.LongitudCedula
			{
				int tercerDigito = Integer.parseInt(cedula.substring(2, 3));
				if (tercerDigito < 6) {
					// Coeficientes de validaciÃ³n cÃ©dula
					// El decimo digito se lo considera dÃ­gito verificador
					int[] coefValCedula = { 2, 1, 2, 1, 2, 1, 2, 1, 2 };
					int verificador = Integer.parseInt(cedula.substring(9, 10));
					int suma = 0;
					int digito = 0;
					for (int i = 0; i < (cedula.length() - 1); i++) {
						digito = Integer.parseInt(cedula.substring(i, i + 1)) * coefValCedula[i];
						suma += ((digito % 10) + (digito / 10));
					}

					if ((suma % 10 == 0) && (suma % 10 == verificador)) {
						cedulaCorrecta = true;
					} else if ((10 - (suma % 10)) == verificador) {
						cedulaCorrecta = true;
					} else {
						cedulaCorrecta = false;
					}
				} else {
					cedulaCorrecta = false;
				}
			} else {
				cedulaCorrecta = false;
			}
		} catch (NumberFormatException nfe) {
			cedulaCorrecta = false;
		} catch (Exception err) {
			System.out.println("Una excepcion ocurrio en el proceso de validadcion");
			cedulaCorrecta = false;
		}
		if (!cedulaCorrecta) {
			Messagebox.show("La CÃ©dula ingresada es Incorrecta");
		}
		return cedulaCorrecta;
	}
	/*
	 * Codigo para enviar correo
	 */

	// Credenciales para correo
	public static String correoEnvia;
	public static String claveCorreo;
	public static String host;
	public static String puerto;

	public static void datosEmail(String correoEnvia, String claveCorreo, String host, String puerto) {
		Sistema.correoEnvia = correoEnvia;
		Sistema.claveCorreo = claveCorreo;
		Sistema.host = host;
		Sistema.puerto = puerto;
	}

	public static void enviarCorreo(String correo, String asunto, String mensaje, String archivo) {
		// La configuraciÃ³n para enviar correo
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", puerto);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.user", correoEnvia);
		properties.put("mail.password", claveCorreo);

		// Obtener la sesion
		Session session = Session.getInstance(properties, null);

		try {
			// Crear el cuerpo del mensaje
			MimeMessage mimeMessage = new MimeMessage(session);

			// Agregar quien envÃ­a el correo
			mimeMessage.setFrom(new InternetAddress(correoEnvia, "Sistema Realizado por DesigntTechx"));

			// Los destinatarios
			InternetAddress[] internetAddresses = { new InternetAddress(correo) };

			// Agregar los destinatarios al mensaje
			mimeMessage.setRecipients(Message.RecipientType.TO, internetAddresses);

			// Agregar el asunto al correo
			mimeMessage.setSubject(asunto);

			// Creo la parte del mensaje
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(mensaje);
			mimeBodyPart.attachFile(archivo);

			// Crear el multipart para agregar la parte del mensaje anterior
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);

			// Agregar el multipart al cuerpo del mensaje
			mimeMessage.setContent(multipart);

			// Enviar el mensaje
			Transport transport = session.getTransport("smtp");
			transport.connect(correoEnvia, claveCorreo);
			transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
			transport.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Correo enviado");
	}

	public static void enviarCorreoPrueba(String correo, String asunto, String mensaje) {
		// La configuraciÃ³n para enviar correo
		Properties properties = new Properties();
		properties.put("mail.smtp.host", host);
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.port", puerto);
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.user", correoEnvia);
		properties.put("mail.password", claveCorreo);

		// Obtener la sesion
		Session session = Session.getInstance(properties, null);

		try {
			// Crear el cuerpo del mensaje
			MimeMessage mimeMessage = new MimeMessage(session);

			// Agregar quien envÃ­a el correo
			mimeMessage.setFrom(new InternetAddress(correoEnvia, "Sistema Realizado por DesigntTechx"));

			// Los destinatarios
			InternetAddress[] internetAddresses = { new InternetAddress(correo) };

			// Agregar los destinatarios al mensaje
			mimeMessage.setRecipients(Message.RecipientType.TO, internetAddresses);

			// Agregar el asunto al correo
			mimeMessage.setSubject(asunto);

			// Creo la parte del mensaje
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setText(mensaje);
			// mimeBodyPart.attachFile(archivo);

			// Crear el multipart para agregar la parte del mensaje anterior
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(mimeBodyPart);

			// Agregar el multipart al cuerpo del mensaje
			mimeMessage.setContent(multipart);

			// Enviar el mensaje
			Transport transport = session.getTransport("smtp");
			transport.connect(correoEnvia, claveCorreo);
			transport.sendMessage(mimeMessage, mimeMessage.getAllRecipients());
			transport.close();

		} catch (Exception ex) {
			ex.printStackTrace();
		}
		System.out.println("Correo enviado");
	}

	// Codigo para comprbar si existe conexion ha internet

	@SuppressWarnings("resource")
	public static boolean verificarInternet() {
		boolean respuesta = false;
		String dirWeb = "www.google.com";
		int puerto = 80;
		try {
			Socket s = new Socket(dirWeb, puerto);
			if (s.isConnected()) {
				System.out.println(
						"ConexiÃ³n establecida con la direcciÃ³n: " + dirWeb + " a travÃ©z del puerto: " + puerto);
				respuesta = true;
			}
		} catch (Exception e) {
			respuesta = false;
			System.err.println("No se pudo establecer conexiÃ³n con: " + dirWeb + " a travez del puerto: " + puerto);
		}
		return respuesta;
	}

	public static ValorReferencia getValorReferencia() {
		return InstanciaDAO.valorReferenciaDAO.getValorReferencia();
	}

	public static InformacionNegocio getInformacionNegocio() {
		return InstanciaDAO.getInformacionNegocioDAO().getInformacionNegocio(1);
	}

	/*
	 * Metodos para todos los reportes
	 */

	private static String getFileName(String nombreJasper) {
		String fileName = Executions.getCurrent().getDesktop().getWebApp().getRealPath("/jasperReport").toString()
				+ System.getProperty("file.separator") + nombreJasper;
		return fileName;
	}

	public static AMedia reportePDF(String nombreJaspre, Map<String, Object> map, JRDataSource dataSource) {
		AMedia amedia = null;
		try {
			OutputStream outputStream = new ByteArrayOutputStream();
			JasperPrint jasperPrint = JasperFillManager.fillReport(getFileName(nombreJaspre), map, dataSource);
			JRPdfExporter exp = new JRPdfExporter();
			exp.setExporterInput(new SimpleExporterInput(jasperPrint));
			exp.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
			SimplePdfExporterConfiguration conf = new SimplePdfExporterConfiguration();
			exp.setConfiguration(conf);
			exp.exportReport();
			amedia = new AMedia(nombreJaspre.replace("jasper", "pdf"), "pdf", "application/pdf",
					((ByteArrayOutputStream) outputStream).toByteArray());
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
		return amedia;
	}

	public static AMedia reportePDF(List<AMedia> listAmedia) {
		AMedia amedia1 = null;
		try {
			OutputStream outputStream = new ByteArrayOutputStream();
			PdfCopyFields pcf = new PdfCopyFields(outputStream);
			listAmedia.forEach(x -> {
				PdfReader pdfReader;
				try {
					pdfReader = new PdfReader(x.getStreamData());
					pcf.addDocument(pdfReader);

				} catch (IOException | DocumentException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			});
			pcf.close();
			amedia1 = new AMedia(listAmedia.get(0).getName().replace("jasper", "pdf"), "pdf", "application/pdf",
					((ByteArrayOutputStream) outputStream).toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return amedia1;
	}

	public static Connection Conexion() {
		try {
			Class.forName("org.postgresql.Driver");
			Connection conexion = DriverManager.getConnection("jdbc:postgresql://localhost:5432/epws", "postgres",
					"PostgreSQL");
			return conexion;
		} catch (Exception e) {
			return null;
		}

	}

	public static void reporteEXCEL(String nombreJaspre, String nombreArchivo, Map<String, Object> map,
			JRDataSource dataSource,Connection conexion, boolean configuracion) {
		AMedia amedia = null;
		try {
			JasperPrint jasperPrint = null;
			if (dataSource != null) {
				jasperPrint = JasperFillManager.fillReport(getFileName(nombreJaspre), map, dataSource);
			}
			if (conexion != null) {
				jasperPrint = JasperFillManager.fillReport(getFileName(nombreJaspre), map, conexion);
			}
			if (dataSource == null && conexion == null) {
				jasperPrint = JasperFillManager.fillReport(getFileName(nombreJaspre), map);
			}


			ByteArrayOutputStream out = new ByteArrayOutputStream();

			SimpleXlsReportConfiguration configuration = new SimpleXlsReportConfiguration();

			// coding For Excel:
			JRXlsExporter exporterXLS = new JRXlsExporter();
			exporterXLS.setExporterInput(new SimpleExporterInput(jasperPrint));
			exporterXLS.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
			// configuration.setOnePagePerSheet(true);
			configuration.setDetectCellType(false);
			configuration.setWhitePageBackground(false);
			configuration.setRemoveEmptySpaceBetweenRows(true);
			// //agregadas
			configuration.setIgnorePageMargins(true);
			configuration.setWhitePageBackground(false);
			configuration.setIgnoreCellBorder(false);
			configuration.setRemoveEmptySpaceBetweenColumns(true);
			configuration.setRemoveEmptySpaceBetweenRows(true);
			configuration.setCollapseRowSpan(false);
			if (configuracion) {
				exporterXLS.setConfiguration(configuration);
			}
			exporterXLS.exportReport();
			Date fechaActual = new Date();
			String nombre = "";
			String hora = "";
			SimpleDateFormat format = new SimpleDateFormat("MMM-dd-yyyy-HH:mm:ss");
			nombre = format.format(fechaActual);
			hora = nombre.replace(":", "-");
			amedia = new AMedia(nombreArchivo + "-" + hora + ".xls", "xls", "application/file", out.toByteArray());
			Filedownload.save(amedia);
		} catch (Exception e) {
			System.out.print(e.getMessage());
		}
	}

	/*
	 * Jasper Reportes
	 */

	public static final String PENSION_JASPER = "pension.jasper";
	public static final String PENSION_MES_JASPER = "pensionMes.jasper";
	public static final String MATRICULA_JASPER = "matricula.jasper";
	public static final String PARCIAL_ALUMNO_JASPER = "parcialAlumno.jasper";
	public static final String CERTIFICADO_PROMOCION_JASPER = "certificado_promocion.jasper";
	public static final String PARCIAL_ASIGNATURA_JASPER = "parcialAsignatura.jasper";
	public static final String PARCIAL_CURSO_JASPER = "parcialCurso.jasper";
	public static final String QUIMESTRE_ALUMNO_JASPER = "quimestreAlumno.jasper";
	public static final String QUIMESTRE_ASIGNATURA_JASPER = "quimestreAsignatura.jasper";
	public static final String FICHA_ESTUDIANTE = "ficha_alumno.jasper";
	public static final String FICHA_DOCENTE = "ficha_docente.jasper";
	public static final String FICHA_MATRICULA = "ficha_matricula.jasper";
	public static final String FICHA_DETALLE_PAGO = "detalle_pago.jasper";

	public static JRDataSource getMatriculaDataSource(List<Matricula> listaMatricual) {
		MatriculaDataSource datasource = new MatriculaDataSource();
		listaMatricual.forEach(x -> datasource.add(x));
		return datasource;
	}

	public static JRDataSource getPensionDataSource(
			List<Modelos<Pension, List<Pension>, Object, Object>> listaPensionPropio, boolean mes) {
		if (mes) {
			PensionMesDataSource datasource = new PensionMesDataSource();
			listaPensionPropio.forEach(x -> datasource.add(x));
			return datasource;
		} else {
			PensionDataSource datasource = new PensionDataSource();
			listaPensionPropio.forEach(x -> datasource.add(x));
			return datasource;
		}
	}

	public static JRDataSource getCertificadoPromocionDataSource(List<CuadroCalificacion> listaCertificadoPromocion) {
		CertificadoPromocionDataSource certificadoPromocionDataSource = new CertificadoPromocionDataSource();
		listaCertificadoPromocion.forEach(x -> certificadoPromocionDataSource.add(x));
		return certificadoPromocionDataSource;
	}

	public static JRDataSource getParcialDataSource(
			List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> listaCalificacionParcial) {
		ParcialAsignaturaDataSource parcialAsignaturaDataSource = new ParcialAsignaturaDataSource();
		listaCalificacionParcial.forEach(x -> parcialAsignaturaDataSource.add(x));
		return parcialAsignaturaDataSource;
	}

	public static JRDataSource getInformeCalificacionQuimestreDataSource(
			List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacion) {
		InformeQuimestreDataSource informeQuimestreDataSource = new InformeQuimestreDataSource();
		listaCalificacion.forEach(x -> informeQuimestreDataSource.add(x));
		return informeQuimestreDataSource;
	}

	protected static SecureRandom random = new SecureRandom();

	public static synchronized String generarToken(String username) {
		long longToken = Math.abs(random.nextLong());
		String random = Long.toString(longToken, 16);
		return (username + ":" + random);
	}

}