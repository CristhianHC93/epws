package ec.com.epws.models;

public class CertificadoPromocion {

	private String asignatura;
	private String nota;
	private double quimestre1;
	private double quimestre2;
	private double valorNota;
	private String letra;

	public CertificadoPromocion() {}
	
	public CertificadoPromocion(String asignatura, String nota, double valorNota, String letra) {
		this.asignatura = asignatura;
		this.nota = nota;
		this.valorNota = valorNota;
		this.letra = letra;
	}

	public CertificadoPromocion(String asignatura, double valorNota, double quimestre1, double quimestre2) {
		this.asignatura = asignatura;
		this.valorNota = valorNota;
		this.quimestre1 = quimestre1;
		this.quimestre2 = quimestre2;

	}

	public double getValorNota() {
		return valorNota;
	}

	public void setValorNota(double valorNota) {
		this.valorNota = valorNota;
	}

	public String getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(String asignatura) {
		this.asignatura = asignatura;
	}

	public String getNota() {
		return nota;
	}

	public void setNota(String nota) {
		this.nota = nota;
	}

	public String getLetra() {
		return letra;
	}

	public void setLetra(String letra) {
		this.letra = letra;
	}

	public double getQuimestre1() {
		return quimestre1;
	}

	public void setQuimestre1(double quimestre1) {
		this.quimestre1 = quimestre1;
	}

	public double getQuimestre2() {
		return quimestre2;
	}

	public void setQuimestre2(double quimestre2) {
		this.quimestre2 = quimestre2;
	}

}
