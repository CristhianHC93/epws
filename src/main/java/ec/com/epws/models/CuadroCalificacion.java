package ec.com.epws.models;

import java.util.List;

public class CuadroCalificacion {
	private String alumno;
	private List<ec.com.epws.entity.CuadroCalificacion> listaCalificacion;
	private double promedioAnual;
	private String clubes;
	private String comportamiento;
	private String observacion;

	public CuadroCalificacion() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CuadroCalificacion(String alumno, List<ec.com.epws.entity.CuadroCalificacion> listaCalificacion, double promedioAnual,
			String clubes, String comportamiento, String observacion) {
		super();
		this.alumno = alumno;
		this.listaCalificacion = listaCalificacion;
		this.promedioAnual = promedioAnual;
		this.clubes = clubes;
		this.comportamiento = comportamiento;
		this.observacion = observacion;
	}

	public String getAlumno() {
		return alumno;
	}

	public void setAlumno(String alumno) {
		this.alumno = alumno;
	}

	public List<ec.com.epws.entity.CuadroCalificacion> getListaCalificacion() {
		return listaCalificacion;
	}

	public void setListaCalificacion(List<ec.com.epws.entity.CuadroCalificacion> listaCalificacion) {
		this.listaCalificacion = listaCalificacion;
	}

	public double getPromedioAnual() {
		return promedioAnual;
	}

	public void setPromedioAnual(double promedioAnual) {
		this.promedioAnual = promedioAnual;
	}

	public String getClubes() {
		return clubes;
	}

	public void setClubes(String clubes) {
		this.clubes = clubes;
	}

	public String getComportamiento() {
		return comportamiento;
	}

	public void setComportamiento(String comportamiento) {
		this.comportamiento = comportamiento;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

}
