package ec.com.epws.models;

public class Modelos<T, T1, T2, T3> {

	private T objetoT;
	private T1 objetoT1;
	private T2 objetoT2;
	private T3 objetoT3;
	private boolean estado;

	public Modelos(T objetoT, T1 objetoT1, T2 objetoT2, T3 objetoT3, boolean estado) {
		this.objetoT = objetoT;
		this.objetoT1 = objetoT1;
		this.objetoT2 = objetoT2;
		this.objetoT3 = objetoT3;
		this.estado = estado;
	}
	public Modelos() {
	}

	public T1 getObjetoT1() {
		return objetoT1;
	}

	public void setObjetoT1(T1 objetoT1) {
		this.objetoT1 = objetoT1;
	}

	public T getObjetoT() {
		return objetoT;
	}

	public void setObjetoT(T objetoT) {
		this.objetoT = objetoT;
	}

	public T2 getObjetoT2() {
		return objetoT2;
	}

	public void setObjetoT2(T2 objetoT2) {
		this.objetoT2 = objetoT2;
	}

	public T3 getObjetoT3() {
		return objetoT3;
	}

	public void setObjetoT3(T3 objetoT3) {
		this.objetoT3 = objetoT3;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}

}
