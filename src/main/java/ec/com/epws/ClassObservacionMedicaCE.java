package ec.com.epws;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Discapacidad;
import ec.com.epws.entity.Enfermedad;
import ec.com.epws.entity.MedicaDiscapacidad;
import ec.com.epws.entity.MedicaEnfermedad;
import ec.com.epws.entity.MedicaVacuna;
import ec.com.epws.entity.ObservacionMedica;
import ec.com.epws.entity.Vacuna;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassObservacionMedicaCE {
	@Wire("#winObservacionMedicaCE")
	private Window winObservacionMedicaCE;
	private ObservacionMedica observacionMedica;

	private List<MedicaVacuna> listaVacuna = new ArrayList<>();
	private List<MedicaEnfermedad> listaEnfermedad = new ArrayList<>();
	private List<MedicaDiscapacidad> listaDiscapacidad = new ArrayList<>();

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") ObservacionMedica observacionMedica) {
		Selectors.wireComponents(view, this, false);
		if (observacionMedica == null) {
			this.observacionMedica =new ObservacionMedica();
		} else {
			this.observacionMedica = observacionMedica;
			if (observacionMedica.getId() == null) {
				listaVacuna.addAll(observacionMedica.getMedicaVacunas());
				listaDiscapacidad = observacionMedica.getMedicaDiscapacidads();
				listaEnfermedad = observacionMedica.getMedicaEnfermedads();
			}else{
			listaVacuna = InstanciaDAO.medicaVacunaDAO
					.getListaMedicaVacunaByObservacionMedica(this.observacionMedica.getId());
			listaDiscapacidad = InstanciaDAO.medicaDiscapacidadDAO
					.getListaMedicaDiscapacidadByObservacionMedica(this.observacionMedica.getId());
			listaEnfermedad = InstanciaDAO.medicaEnfermedadDAO
					.getListaMedicaEnfermedadByObservacionMedica(this.observacionMedica.getId());
		}}
	}

	@Command
	@NotifyChange("observacionMedica")
	public void guardarEditar() {
		if (observacionMedica.getId() == null) {
			InstanciaDAO.getObservacionMedicaDAO().saveorUpdate(observacionMedica);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			// observacionMedica = new ObservacionMedica();
		} else {
			InstanciaDAO.getObservacionMedicaDAO().saveorUpdate(observacionMedica);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("observacionMedica")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("observacion")) {
			observacionMedica.setObservacion((cadena.toUpperCase()));
		}
	}

	@NotifyChange("observacionMedica")
	@Command
	public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {

		Media media;
		media = event.getMedia();
		int aleatorio = ThreadLocalRandom.current().nextInt(1, 100000);
		if (Util.uploadFile(media, String.valueOf(aleatorio), Sistema.carpetaCertificadoMedico)) {
			new File(Util.getPathRaiz() + observacionMedica.getCertificadoMedico()).delete();
			observacionMedica
					.setCertificadoMedico("/" + Sistema.carpetaCertificadoMedico + "/" + String.valueOf(aleatorio));
		}
	}

	@Command
	public void aceptar() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", observacionMedica);
		parametros.put("listaVacuna", listaVacuna);
		parametros.put("listaEnfermedad", listaEnfermedad);
		parametros.put("listaDiscapacidad", listaDiscapacidad);
		BindUtils.postGlobalCommand(null, null, "cargarObservacionMedica", parametros);
		salir();
	}

	boolean banVacuna = false;
	boolean banEnfermedad = false;
	boolean banDiscapacidad = false;

	@Command
	public void agregarVacuna() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "ObservacionMedica");
		Executions.createComponents("/vista/formVacuna.zul", null, parametros);
	}

	@NotifyChange({ "alumno", "listaVacuna" })
	@GlobalCommand
	public void cargarVacuna(@BindingParam("objeto") Vacuna vacuna) {
		listaVacuna.forEach(x -> {
			if (x.getVacuna().getId() == vacuna.getId()) {
				banVacuna = true;
				return;
			}
		});
		if (banVacuna) {
			banVacuna = false;
			return;
		}
		MedicaVacuna medicoVacuna = new MedicaVacuna();
		medicoVacuna.setObservacionMedica(observacionMedica);
		medicoVacuna.setVacuna(vacuna);
		listaVacuna.add(medicoVacuna);
	}

	@Command
	public void agregarEnfermedad() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "ObservacionMedica");
		Executions.createComponents("/vista/formEnfermedad.zul", null, parametros);
	}

	@NotifyChange({ "alumno", "listaEnfermedad" })
	@GlobalCommand
	public void cargarEnfermedad(@BindingParam("objeto") Enfermedad enfermedad) {
		listaEnfermedad.forEach(x -> {
			if (x.getEnfermedad().getId() == enfermedad.getId()) {
				banEnfermedad = true;
				return;
			}
		});
		if (banEnfermedad) {
			banEnfermedad = false;
			return;
		}
		MedicaEnfermedad medicaEnfermedad = new MedicaEnfermedad();
		medicaEnfermedad.setObservacionMedica(observacionMedica);
		medicaEnfermedad.setEnfermedad(enfermedad);
		listaEnfermedad.add(medicaEnfermedad);
	}

	@Command
	public void agregarDiscapacidad() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "ObservacionMedica");
		Executions.createComponents("/vista/formDiscapacidad.zul", null, parametros);
	}

	@NotifyChange({ "alumno", "listaDiscapacidad" })
	@GlobalCommand
	public void cargarDiscapacidad(@BindingParam("objeto") Discapacidad discapacidad) {
		listaDiscapacidad.forEach(x -> {
			if (x.getDiscapacidad().getId() == discapacidad.getId()) {
				banDiscapacidad = true;
				return;
			}
		});
		if (banDiscapacidad) {
			banDiscapacidad = false;
			return;
		}
		MedicaDiscapacidad medicaDiscapacidad = new MedicaDiscapacidad();
		medicaDiscapacidad.setObservacionMedica(observacionMedica);
		medicaDiscapacidad.setDiscapacidad(discapacidad);
		listaDiscapacidad.add(medicaDiscapacidad);
	}

	@NotifyChange({ "listaVacuna", "listaDiscapacidad", "listaEnfermedad" })
	@Command
	public void eliminar(@BindingParam("item") Object item) {
		if (item instanceof MedicaVacuna) {
			listaVacuna.remove(item);
		}
		if (item instanceof MedicaDiscapacidad) {
			listaDiscapacidad.remove(item);
		}
		if (item instanceof MedicaEnfermedad) {
			listaEnfermedad.remove(item);
		}
	}

	@Command
	public void salir() {
		winObservacionMedicaCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public ObservacionMedica getObservacionMedica() {
		return observacionMedica;
	}

	public void setObservacionMedica(ObservacionMedica observacionMedica) {
		this.observacionMedica = observacionMedica;
	}

	public List<MedicaVacuna> getListaVacuna() {
		return listaVacuna;
	}

	public void setListaVacuna(List<MedicaVacuna> listaVacuna) {
		this.listaVacuna = listaVacuna;
	}

	public List<MedicaEnfermedad> getListaEnfermedad() {
		return listaEnfermedad;
	}

	public void setListaEnfermedad(List<MedicaEnfermedad> listaEnfermedad) {
		this.listaEnfermedad = listaEnfermedad;
	}

	public List<MedicaDiscapacidad> getListaDiscapacidad() {
		return listaDiscapacidad;
	}

	public void setListaDiscapacidad(List<MedicaDiscapacidad> listaDiscapacidad) {
		this.listaDiscapacidad = listaDiscapacidad;
	}

}
