package ec.com.epws.jpa;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class JPAPersistenceManager {

	public static final boolean DEBUG = true;
	private static final JPAPersistenceManager singleton = new JPAPersistenceManager();
	protected EntityManagerFactory emf;
	// private static String vDBMS = (String)
	// Sessions.getCurrent().getAttribute("PostgreSQL"); //PostgreSQL MSSQL

	public static JPAPersistenceManager getInstance() {
		return singleton;
	}

	public JPAPersistenceManager() {

	}

	public EntityManagerFactory getEntityManagerFactory() {
		if (emf == null)
			createEntityManagerFactory();
		return emf;
	}


	@SuppressWarnings("rawtypes")
	protected void createEntityManagerFactory() {
		this.emf = Persistence.createEntityManagerFactory("epws", new java.util.HashMap());
		if (DEBUG)
			System.out.println(new java.util.Date());
	}
}
