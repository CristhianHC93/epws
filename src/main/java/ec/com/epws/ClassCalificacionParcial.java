package ec.com.epws;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.AsignacionDocente;
import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.CalificacionParcial;
import ec.com.epws.entity.CalificacionParcialCualitativa;
import ec.com.epws.entity.CalificacionQuimestre;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;
import ec.com.epws.entity.CuadroCalificacion;
import ec.com.epws.entity.Cualitativa;
import ec.com.epws.entity.Docente;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Observacion;
import ec.com.epws.entity.Parcial;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassCalificacionParcial {
	private List<CalificacionParcial> listaCalificacionParcial = new ArrayList<>();
	private List<CalificacionParcialCualitativa> listaCalificacionParcialCualitativa = new ArrayList<>();
	private List<CalificacionQuimestre> listaCalificacionQuimestre = new ArrayList<>();
	private List<CalificacionQuimestreCualitativa> listaCalificacionQuimestreCualitativa = new ArrayList<>();
	private List<Matricula> listaMatricula = new ArrayList<>();

	private List<Cualitativa> listaCualitativa = Sistema.getListaCualitativaMateria();

	private List<Quimestre> listaQuimestre;
	private Quimestre quimestre;
	private List<Parcial> listaParcial;
	private Parcial parcial;
	private List<AsignacionDocente> listaAsignacionDocente;
	private AsignacionDocente asignacionDocente;

	private boolean basico;
	private boolean materia;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista) {
		Selectors.wireComponents(vista, this, false);
		if (Sistema.getUsuario().getRol().getId() != 2) {
			Messagebox.show("Usuario no es Docente");
			return;
		}
		Docente docente = InstanciaDAO.docenteDAO.getDocente(Sistema.getUsuario().getId());
		listaAsignacionDocente = InstanciaDAO.asignacionDocenteDAO
				.getListaAsignacionDocenteByDocente(Sistema.getAnioLectivoActivoEstatico().getId(), docente.getId());
		if (listaAsignacionDocente == null || listaAsignacionDocente.size() <= 0) {
			Messagebox.show("Docente no tiene asignaturas asignadas");
			return;
		}
		if (listaAsignacionDocente.get(0).getAsignatura().getCodigoCualitativa()
				.getId() == Sistema.FILTRO_CUALITATIVO_INICIAL) {
			Messagebox.show("Docente de inicial no califica Parciales");
			return;
		}
		asignacionDocente = listaAsignacionDocente.get(0);
		listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
		quimestre = listaQuimestre.get(0);
		listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
		parcial = listaParcial.get(0);
		buscar();
	}

	private void metodoCalificacion(Asignatura asignatura) {
		// listaCualitativa.clear();
		if (asignatura.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_BASICO) {
			basico = true;
			materia = false;
		}
		if (asignatura.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_MATERIA) {

			basico = false;
			materia = true;
		}
	}

	private void llenarListaAlumno() {
		listaCalificacionParcial.clear();
		listaCalificacionQuimestre.clear();
		listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(
				Sistema.getAnioLectivoActivoEstatico().getId(), asignacionDocente.getAsignatura().getCurso().getId());

		List<CalificacionQuimestre> pro = InstanciaDAO.calificacionQuimestreDAO
				.getListaCalificacionQuimestreByAsignaturaAndQuimestre(asignacionDocente.getAsignatura().getId(),
						quimestre.getId());
		if (pro == null || pro.size() == 0) {
			listaMatricula.forEach(x -> {
				CalificacionQuimestre calificacionQuimestre = new CalificacionQuimestre();
				calificacionQuimestre.setMatricula(x);
				listaCalificacionQuimestre.add(calificacionQuimestre);
			});
		} else {
			listaCalificacionQuimestre.addAll(pro);
		}

		listaCalificacionQuimestre.forEach(x -> {
			CalificacionParcial calificacionParcial = new CalificacionParcial();
			calificacionParcial.setCalificacionQuimestre(x);
			calificacionParcial.setParcial(parcial);
			listaCalificacionParcial.add(calificacionParcial);
		});
	}

	private void llenarListaAlumnoCualitativa() {
		listaCalificacionParcialCualitativa.clear();
		listaCalificacionQuimestreCualitativa.clear();
		listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(
				Sistema.getAnioLectivoActivoEstatico().getId(), asignacionDocente.getAsignatura().getCurso().getId());

		List<CalificacionQuimestreCualitativa> pro = InstanciaDAO.calificacionQuimestreCualitativaDAO
				.getListaCalificacionQuimestreCualitativaByAsignaturaAndQuimestre(
						asignacionDocente.getAsignatura().getId(), quimestre.getId());
		if (pro == null || pro.size() == 0) {
			listaMatricula.forEach(x -> {
				CalificacionQuimestreCualitativa calificacionQuimestreCualitativa = new CalificacionQuimestreCualitativa();
				calificacionQuimestreCualitativa.setMatricula(x);
				listaCalificacionQuimestreCualitativa.add(calificacionQuimestreCualitativa);
			});
		} else {
			listaCalificacionQuimestreCualitativa.addAll(pro);
		}

		listaCalificacionQuimestreCualitativa.forEach(x -> {
			CalificacionParcialCualitativa calificacionParcialCualitativa = new CalificacionParcialCualitativa();
			calificacionParcialCualitativa.setCalificacionQuimestreCualitativa(x);
			calificacionParcialCualitativa.setParcial(parcial);
			calificacionParcialCualitativa.setCualitativa(listaCualitativa.get(2));
			listaCalificacionParcialCualitativa.add(calificacionParcialCualitativa);
		});
	}

	@NotifyChange({ "listaCalificacionParcialCualitativa", "listaCalificacionParcial", "parcial", "basico", "materia" })
	@Command
	public void guardar() {
		if (basico) {
			calcularPromedioTodos();
			listaCalificacionParcial.forEach(x -> {
				if (x.getCualitativa() == null) {
					Messagebox.show("Hay Valor(es) sin calificar");
					return;
				}
			});
			listaCalificacionParcial.forEach(x -> {
				if (x.getId() != null && x.getObservacion() != null && x.getObservacion().getId() != null) {
					InstanciaDAO.observacionDAO.saveorUpdate(x.getObservacion());
				}
				//Inicia proceso para guardar Cuadro Calificacion
				CuadroCalificacion cc = InstanciaDAO.cuadroCalificacionDAO
						.getCuadroCalificacion(x.getCalificacionQuimestre().getAsignatura().getId(), x.getCalificacionQuimestre().getMatricula().getId());
				if (cc == null) {
					cc = new CuadroCalificacion();
					cc.setAsignatura(x.getCalificacionQuimestre().getAsignatura());
					cc.setMatricula(x.getCalificacionQuimestre().getMatricula());
				}
				if (x.getCalificacionQuimestre().getQuimestre().getId() == 1) {
					cc.setQuimestre1(x.getCalificacionQuimestre().getNota());
				}
				if (x.getCalificacionQuimestre().getQuimestre().getId() == 2) {
					cc.setQuimestre2(x.getCalificacionQuimestre().getNota());
				}
				cc.setPromedio((cc.getQuimestre1()+cc.getQuimestre2())/2);
				cc.setPromedioFinal(cc.getPromedio());
				cc.setLetra(Sistema.numeroAletras(cc.getPromedio()));
				cc.setNota(String.valueOf(cc.getPromedioFinal()));
				InstanciaDAO.cuadroCalificacionDAO.saveorUpdate(cc);
				//Fin proceso
				x.getCalificacionQuimestre().setQuimestre(quimestre);
				x.getCalificacionQuimestre().setAsignatura(asignacionDocente.getAsignatura());
				x.setParcial(parcial);
				InstanciaDAO.calificacionParcialDAO.saveorUpdate(x);
			});
			Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
			buscar();
		}
		if (materia) {
			listaCalificacionParcialCualitativa.forEach(x -> {
				if (x.getCualitativa() == null) {
					Messagebox.show("Hay Valor(es) sin calificar");
					return;
				}
			});
			listaCalificacionParcialCualitativa.forEach(x -> {
				if (x.getId() != null && x.getObservacion() != null && x.getObservacion().getId() != null) {
					InstanciaDAO.observacionDAO.saveorUpdate(x.getObservacion());
				}
				//Inicia proceso para guardar Cuadro Calificacion
				CuadroCalificacion cc = InstanciaDAO.cuadroCalificacionDAO
						.getCuadroCalificacion(x.getCalificacionQuimestreCualitativa().getAsignatura().getId(), x.getCalificacionQuimestreCualitativa().getMatricula().getId());
				if (cc == null) {
					cc = new CuadroCalificacion();
					cc.setAsignatura(x.getCalificacionQuimestreCualitativa().getAsignatura());
					cc.setMatricula(x.getCalificacionQuimestreCualitativa().getMatricula());
				}
				if (x.getCalificacionQuimestreCualitativa().getQuimestre().getId() == 1) {
					cc.setQuimestre1((x.getCalificacionQuimestreCualitativa().getCualitativa2()!=null)?x.getCalificacionQuimestreCualitativa().getCualitativa2().getId():0);
				}
				if (x.getCalificacionQuimestreCualitativa().getQuimestre().getId() == 2) {
					cc.setQuimestre2((x.getCalificacionQuimestreCualitativa().getCualitativa2()!=null)?x.getCalificacionQuimestreCualitativa().getCualitativa2().getId():0);
				}
				double id = Sistema.redondearEntero((cc.getQuimestre1()+cc.getQuimestre2())/2);
				if (id <= 9) {
					id = 9;
				}
				cc.setPromedio(id);
				cc.setPromedioFinal(cc.getPromedio());
				cc.setLetra(Sistema.numeroAletras(cc.getPromedio()));
				cc.setNota(InstanciaDAO.cualitativaDAO.getCualitativa((int) id).getSiglas());
				InstanciaDAO.cuadroCalificacionDAO.saveorUpdate(cc);
				//Fin proceso
				x.getCalificacionQuimestreCualitativa().setQuimestre(quimestre);
				x.getCalificacionQuimestreCualitativa().setAsignatura(asignacionDocente.getAsignatura());
				x.setParcial(parcial);
				InstanciaDAO.calificacionParcialCualitativaDAO.saveorUpdate(x);
			});
			Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
			buscar();
		}
	}

	@NotifyChange({ "listaCalificacionParcialCualitativa", "listaCalificacionParcial", "listaParcial", "parcial",
			"basico", "materia" })
	@Command
	public void selectQuimestre() {
		listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
		parcial = listaParcial.get(0);
		buscar();
	}

	@NotifyChange({ "listaCalificacionParcial", "listaCalificacionParcialCualitativa" })
	@Command
	public void selectParcial() {
		buscar();
	}

	@NotifyChange({ "listaCalificacionParcialCualitativa", "listaCalificacionParcial", "parcial", "basico", "materia" })
	@Command
	public void selectAsignacionDocente() {
		buscar();
	}

	@NotifyChange({ "listaCalificacionParcial", "listaCalificacionParcialCualitativa", "basico", "materia" })
	@Command
	public void buscar() {
		metodoCalificacion(asignacionDocente.getAsignatura());
		if (basico) {
			listaCalificacionParcial = InstanciaDAO.calificacionParcialDAO
					.getListaCalificacionParcialByAsignaturaAndParcial(asignacionDocente.getAsignatura().getId(),
							parcial.getId());
			if (listaCalificacionParcial == null || listaCalificacionParcial.size() == 0) {
				llenarListaAlumno();
			}
		}
		if (materia) {
			listaCalificacionParcialCualitativa = InstanciaDAO.calificacionParcialCualitativaDAO
					.getListaCalificacionParcialCualitativaByAsignaturaAndParcial(
							asignacionDocente.getAsignatura().getId(), parcial.getId());
			if (listaCalificacionParcialCualitativa == null || listaCalificacionParcialCualitativa.size() == 0) {
				llenarListaAlumnoCualitativa();
			}
		}
	}

	@NotifyChange("listaCalificacionParcial")
	@Command
	public void calcularPromedio(@BindingParam("item") CalificacionParcial item) {
		boolean bandera = false;
		if (item.getTai() > 10 || item.getTai() < 0) {
			item.setTai(0);
			bandera = true;
		}
		if (item.getAic() > 10 || item.getAic() < 0) {
			item.setAic(0);
			bandera = true;
		}
		if (item.getAgc() > 10 || item.getAgc() < 0) {
			item.setAgc(0);
			bandera = true;
		}
		if (item.getL() > 10 || item.getL() < 0) {
			item.setL(0);
			bandera = true;
		}
		if (item.getEsumativa() > 10 || item.getEsumativa() < 0) {
			item.setEsumativa(0);
			bandera = true;
		}
		if (bandera) {
			Messagebox.show(Sistema.idioma.getValorInvalido());
			// return;
		}
		item.setPromedio(Sistema
				.redondear((item.getTai() + item.getAic() + item.getAgc() + item.getL() + item.getEsumativa()) / 5));
		Sistema.getListaCualitativaBasico().forEach(x -> {
			if (item.getPromedio() >= x.getValorMinimo() && item.getPromedio() <= x.getValorMaximo()) {
				item.setCualitativa(x);
			}
		});
	}

	@NotifyChange("listaCalificacionParcial")
	@Command
	public void calcularPromedioTodos() {
		listaCalificacionParcial.forEach(x -> {
			x.setPromedio(Sistema.redondear((x.getTai() + x.getAic() + x.getAgc() + x.getL() + x.getEsumativa()) / 5));
			Sistema.getListaCualitativaBasico().forEach(y -> {
				if (x.getPromedio() >= y.getValorMinimo() && x.getPromedio() <= y.getValorMaximo()) {
					x.setCualitativa(y);
				}
			});
		});
	}

	@NotifyChange("listaCalificacionParcial")
	@Command
	public void nuevoCalificacionParcial(@BindingParam("calificacionParcial") CalificacionParcial calificacionParcial) {
		calificacionParcial.setObservacion(new Observacion());

	}

	@NotifyChange({ "listaCalificacionParcialCualitativa" })
	@Command
	public void nuevoCalificacionParcialCualitativa(
			@BindingParam("calificacionParcialCualitativa") CalificacionParcialCualitativa calificacionParcialCualitativa) {
		calificacionParcialCualitativa.setObservacion(new Observacion());
	}

	@NotifyChange({ "listaCalificacionParcial", "listaCalificacionParcialCualitativa", "basico", "materia" })
	@Command
	public void eliminarCalificacionParcial(
			@BindingParam("calificacionParcial") CalificacionParcial calificacionParcial) {
		String msj = "Esta seguro(a) que desea eliminar el registro.";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						if (calificacionParcial.getObservacion().getId() != null) {
							Observacion observacion = calificacionParcial.getObservacion();
							calificacionParcial.setObservacion(null);
							InstanciaDAO.calificacionParcialDAO.saveorUpdate(calificacionParcial);
							int a = InstanciaDAO.observacionDAO.eliminar(observacion);
							if (a == 1) {
								Clients.showNotification("Registro eliminado satisfactoriamente",
										Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassCalificacionParcial.this,
										"listaCalificacionParcial");
							} else {
								Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
										null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassCalificacionParcial.this,
										"listaCalificacionParcial");
							}
						} else {
							calificacionParcial.setObservacion(null);
							BindUtils.postNotifyChange(null, null, ClassCalificacionParcial.this,
									"listaCalificacionParcial");
						}
					}
				});
	}

	@NotifyChange("listaCalificacionParcialCualitativa")
	@Command
	public void eliminarCalificacionParcialCualitativa(
			@BindingParam("calificacionParcialCualitativa") CalificacionParcialCualitativa calificacionParcialCualitativa) {
		String msj = "Esta seguro(a) que desea eliminar el registro.";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						if (calificacionParcialCualitativa.getObservacion().getId() != null) {
							Observacion observacion = calificacionParcialCualitativa.getObservacion();
							calificacionParcialCualitativa.setObservacion(null);
							InstanciaDAO.calificacionParcialCualitativaDAO.saveorUpdate(calificacionParcialCualitativa);
							int a = InstanciaDAO.observacionDAO.eliminar(observacion);
							if (a == 1) {
								Clients.showNotification("Registro eliminado satisfactoriamente",
										Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassCalificacionParcial.this,
										"listaCalificacionParcialCualitativa");
							} else {
								Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
										null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassCalificacionParcial.this,
										"listaCalificacionParcialCualitativa");
							}
						} else {
							calificacionParcialCualitativa.setObservacion(null);
							BindUtils.postNotifyChange(null, null, ClassCalificacionParcial.this,
									"listaCalificacionParcialCualitativa");
						}
					}
				});
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintPrecio() {
		return Sistema.CONSTRAINT_PRECIO;
	}

	public List<CalificacionParcial> getListaCalificacionParcial() {
		return listaCalificacionParcial;
	}

	public void setListaCalificacionParcial(List<CalificacionParcial> listaCalificacionParcial) {
		this.listaCalificacionParcial = listaCalificacionParcial;
	}

	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}

	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}

	public Quimestre getQuimestre() {
		return quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

	public List<Parcial> getListaParcial() {
		return listaParcial;
	}

	public void setListaParcial(List<Parcial> listaParcial) {
		this.listaParcial = listaParcial;
	}

	public Parcial getParcial() {
		return parcial;
	}

	public void setParcial(Parcial parcial) {
		this.parcial = parcial;
	}

	public List<AsignacionDocente> getListaAsignacionDocente() {
		return listaAsignacionDocente;
	}

	public void setListaAsignacionDocente(List<AsignacionDocente> listaAsignacionDocente) {
		this.listaAsignacionDocente = listaAsignacionDocente;
	}

	public AsignacionDocente getAsignacionDocente() {
		return asignacionDocente;
	}

	public void setAsignacionDocente(AsignacionDocente asignacionDocente) {
		this.asignacionDocente = asignacionDocente;
	}

	public List<CalificacionParcialCualitativa> getListaCalificacionParcialCualitativa() {
		return listaCalificacionParcialCualitativa;
	}

	public void setListaCalificacionParcialCualitativa(
			List<CalificacionParcialCualitativa> listaCalificacionParcialCualitativa) {
		this.listaCalificacionParcialCualitativa = listaCalificacionParcialCualitativa;
	}

	public List<Cualitativa> getListaCualitativa() {
		return listaCualitativa;
	}

	public void setListaCualitativa(List<Cualitativa> listaCualitativa) {
		this.listaCualitativa = listaCualitativa;
	}

	public boolean isBasico() {
		return basico;
	}

	public void setBasico(boolean basico) {
		this.basico = basico;
	}

	public boolean isMateria() {
		return materia;
	}

	public void setMateria(boolean materia) {
		this.materia = materia;
	}

}
