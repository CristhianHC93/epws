package ec.com.epws.data.source;

import java.util.ArrayList;
import java.util.List;

import ec.com.epws.entity.Matricula;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class MatriculaDataSource implements JRDataSource {
	private List<Matricula> listaMatricula = new ArrayList<>();
	private int indicePorforma = -1;

	@Override
	public Object getFieldValue(JRField jrf) throws JRException {

		Object valor = null;

		if ("alumno".equals(jrf.getName())) {
			valor = listaMatricula.get(indicePorforma).getAlumno().getNombre()+" "+listaMatricula.get(indicePorforma).getAlumno().getApellido();
		} else if ("representante".equals(jrf.getName())) {
			valor = listaMatricula.get(indicePorforma).getAlumno().getAlumnoFamilias().get(2).getFamilia().getNombre()+" "+listaMatricula.get(indicePorforma).getAlumno().getAlumnoFamilias().get(2).getFamilia().getApellido();
		}else if ("numero".equals(jrf.getName())) {
			valor = listaMatricula.get(indicePorforma).getNumero();
		}else if ("contacto".equals(jrf.getName())) {
			valor = (listaMatricula.get(indicePorforma).getAlumno().getAlumnoFamilias().get(2).getFamilia().getTelefono() != null)?listaMatricula.get(indicePorforma).getAlumno().getAlumnoFamilias().get(2).getFamilia().getTelefono():"";
		}
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indicePorforma < listaMatricula.size();
	}

	public void add(Matricula matricula) {
		this.listaMatricula.add(matricula);
	}

}
