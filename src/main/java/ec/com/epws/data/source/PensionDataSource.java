package ec.com.epws.data.source;

import java.util.ArrayList;
import java.util.List;

import ec.com.epws.entity.Pension;
import ec.com.epws.models.Modelos;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class PensionDataSource implements JRDataSource {
	private List<Modelos<Pension, List<Pension>, Object, Object>> listaPension = new ArrayList<>();
	private int indicePorforma = -1;

	@Override
	public Object getFieldValue(JRField jrf) throws JRException {

		Object valor = null;

		if ("alumno".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT().getMatricula().getAlumno().getNombre()+" "+listaPension.get(indicePorforma).getObjetoT().getMatricula().getAlumno().getApellido();
		} else if ("matricula".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(0).getValorPagado();
		} else if ("abril".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(1).getValorPagado();
		} else if ("mayo".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(2).getValorPagado();
		} else if ("junio".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(3).getValorPagado();
		} else if ("julio".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(4).getValorPagado();
		} else if ("agosto".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(5).getValorPagado();
		} else if ("septiembre".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(6).getValorPagado();
		} else if ("octubre".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(7).getValorPagado();
		} else if ("noviembre".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(8).getValorPagado();
		} else if ("diciembre".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(9).getValorPagado();
		} else if ("enero".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT1().get(10).getValorPagado();
		}
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indicePorforma < listaPension.size();
	}

	public void add(Modelos<Pension, List<Pension>, Object, Object> pension) {
		this.listaPension.add(pension);
	}

}
