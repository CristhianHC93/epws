package ec.com.epws.data.source;

import java.util.ArrayList;
import java.util.List;

import ec.com.epws.entity.Pension;
import ec.com.epws.models.Modelos;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class PensionMesDataSource implements JRDataSource {
	private List<Modelos<Pension, List<Pension>, Object, Object>> listaPension = new ArrayList<>();
	private int indicePorforma = -1;

	@Override
	public Object getFieldValue(JRField jrf) throws JRException {

		Object valor = null;

		if ("alumno".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT().getMatricula().getAlumno().getNombre()+" "+listaPension.get(indicePorforma).getObjetoT().getMatricula().getAlumno().getApellido();
		} else if ("valorPagado".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT().getValorPagado();
		}else if ("curso".equals(jrf.getName())) {
			valor = listaPension.get(indicePorforma).getObjetoT().getMatricula().getCurso().getDescripcion();
		}
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indicePorforma < listaPension.size();
	}

	public void add(Modelos<Pension, List<Pension>, Object, Object> pension) {
		this.listaPension.add(pension);
	}

}
