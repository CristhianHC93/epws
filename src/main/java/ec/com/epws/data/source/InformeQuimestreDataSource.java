package ec.com.epws.data.source;

import java.util.ArrayList;
import java.util.List;

import ec.com.epws.entity.CalificacionQuimestre;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;
import ec.com.epws.models.Modelos;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class InformeQuimestreDataSource implements JRDataSource {
	private List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacionQuimestre = new ArrayList<>();
	private int indicePorforma = -1;

	@Override
	public Object getFieldValue(JRField jrf) throws JRException {

		Object valor = null;

		
//		(each.objetoT != null) ? ((each.objetoT.calificacionParcials.size()>1)?each.objetoT.calificacionParcials.get(1).promedio : ''): ((each.objetoT1.calificacionParcialCualitativas.size()>1)? each.objetoT1.calificacionParcialCualitativas.get(1).cualitativa.descripcion:'')
		
		if ("asignatura".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getAsignatura().getDescripcion()
					: listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getAsignatura().getDescripcion();
		}else if ("alumno".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getMatricula().getAlumno().getNombre()+" "+listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getMatricula().getAlumno().getApellido()
					: listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getMatricula().getAlumno().getNombre()+" "+listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getMatricula().getAlumno().getApellido();
		}		
		else if ("p1".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? ((listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getCalificacionParcials().size() > 0 )?listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getCalificacionParcials().get(0)
							.getPromedio():"")
					: ((listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCalificacionParcialCualitativas().size()>0)? listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCalificacionParcialCualitativas()
							.get(0).getCualitativa().getDescripcion():"");
		} else if ("p2".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? ((listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getCalificacionParcials().size() > 1 )?listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getCalificacionParcials().get(1)
							.getPromedio():"")
					: ((listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCalificacionParcialCualitativas().size()>1)? listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCalificacionParcialCualitativas()
							.get(1).getCualitativa().getDescripcion():"");
		} else if ("p3".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? ((listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getCalificacionParcials().size() > 2 )?listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getCalificacionParcials().get(2)
							.getPromedio():"")
					: ((listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCalificacionParcialCualitativas().size()>2)? listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCalificacionParcialCualitativas()
							.get(2).getCualitativa().getDescripcion():"");
		} else if ("promedio".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getPromedioParciales() : "";
		} else if ("80".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getPromedio80() : 0.0;
		} else if ("examen".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getExamen()
					: (listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCualitativa1()!=null)?listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCualitativa1().getDescripcion():"";
		} else if ("20".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getExamen20() : 0.0;
		} else if ("nota".equals(jrf.getName())) {
			valor = (listaCalificacionQuimestre.get(indicePorforma).getObjetoT() != null)
					? listaCalificacionQuimestre.get(indicePorforma).getObjetoT().getNota()
					: (listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCualitativa2()!=null)?listaCalificacionQuimestre.get(indicePorforma).getObjetoT1().getCualitativa2().getDescripcion():"";
		}
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indicePorforma < listaCalificacionQuimestre.size();
	}

	public void add(Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object> x) {
		this.listaCalificacionQuimestre.add(x);
	}

}
