package ec.com.epws.data.source;

import java.util.ArrayList;
import java.util.List;

import ec.com.epws.entity.CalificacionParcial;
import ec.com.epws.entity.CalificacionParcialCualitativa;
import ec.com.epws.models.Modelos;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class ParcialAsignaturaDataSource implements JRDataSource {
	private List<Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object>> listaCalificacionParcial = new ArrayList<>();
	private int indicePorforma = -1;

	@Override
	public Object getFieldValue(JRField jrf) throws JRException {
		Object valor = null;

		if ("alumno".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ? listaCalificacionParcial.get(indicePorforma).getObjetoT().getCalificacionQuimestre().getMatricula().getAlumno().getNombre()+" "+listaCalificacionParcial.get(indicePorforma).getObjetoT().getCalificacionQuimestre().getMatricula().getAlumno().getApellido()
					:listaCalificacionParcial.get(indicePorforma).getObjetoT1().getCalificacionQuimestreCualitativa().getMatricula().getAlumno().getNombre()+" "+listaCalificacionParcial.get(indicePorforma).getObjetoT1().getCalificacionQuimestreCualitativa().getMatricula().getAlumno().getApellido();
		} else if ("asignatura".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ? listaCalificacionParcial.get(indicePorforma).getObjetoT().getCalificacionQuimestre().getAsignatura().getDescripcion()
					:listaCalificacionParcial.get(indicePorforma).getObjetoT1().getCalificacionQuimestreCualitativa().getAsignatura().getDescripcion();
		} else if ("tai".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ?listaCalificacionParcial.get(indicePorforma).getObjetoT().getTai():0.0;
		}else if ("aic".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ?listaCalificacionParcial.get(indicePorforma).getObjetoT().getAic():0.0;
		}else if ("agc".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ?listaCalificacionParcial.get(indicePorforma).getObjetoT().getAgc():0.0;
		}else if ("l".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ?listaCalificacionParcial.get(indicePorforma).getObjetoT().getL():0.0;
		}else if ("e_sumativa".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ?listaCalificacionParcial.get(indicePorforma).getObjetoT().getEsumativa():0.0;
		}else if ("promedio".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ?listaCalificacionParcial.get(indicePorforma).getObjetoT().getPromedio():0.0;
		}else if ("cualitativa".equals(jrf.getName())) {
			valor = (listaCalificacionParcial.get(indicePorforma).getObjetoT() != null) ?listaCalificacionParcial.get(indicePorforma).getObjetoT().getCualitativa().getSiglas():listaCalificacionParcial.get(indicePorforma).getObjetoT1().getCualitativa().getSiglas();
		}
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indicePorforma < listaCalificacionParcial.size();
	}

	public void add(Modelos<CalificacionParcial, CalificacionParcialCualitativa, Object, Object> calificacionParcial) {
		this.listaCalificacionParcial.add(calificacionParcial);
	}

}
