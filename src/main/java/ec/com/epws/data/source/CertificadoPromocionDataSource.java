package ec.com.epws.data.source;

import java.util.ArrayList;
import java.util.List;

import ec.com.epws.entity.CuadroCalificacion;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRField;

public class CertificadoPromocionDataSource implements JRDataSource {
	private List<CuadroCalificacion> listaCertificadoPromocion = new ArrayList<>();
	private int indicePorforma = -1;

	@Override
	public Object getFieldValue(JRField jrf) throws JRException {

		Object valor = null;

		if ("asignatura".equals(jrf.getName())) {
			valor = listaCertificadoPromocion.get(indicePorforma).getAsignatura().getDescripcion();
		} else if ("nota".equals(jrf.getName())) {
			valor = listaCertificadoPromocion.get(indicePorforma).getNota();
		}else if ("letra".equals(jrf.getName())) {
			valor = listaCertificadoPromocion.get(indicePorforma).getLetra();
		}
		return valor;
	}

	@Override
	public boolean next() throws JRException {
		return ++indicePorforma < listaCertificadoPromocion.size();
	}

	public void add(CuadroCalificacion certificadoPromocion) {
		this.listaCertificadoPromocion.add(certificadoPromocion);
	}

}
