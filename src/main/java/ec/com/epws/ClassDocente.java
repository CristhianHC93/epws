package ec.com.epws;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Docente;
import ec.com.epws.entity.Curso;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassDocente {
	@Wire("#winDocente")
	private Window winDocente;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Docente> listaDocente;
	private Curso curso;
	private String clase;
	private Docente docente;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase,
			@ExecutionArgParam("docente") Docente docente) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "90%";
			this.clase = clase;
		}
		listaDocente = InstanciaDAO.docenteDAO.getListaDocente();
		this.docente = docente;
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formDocenteCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaDocente")
	public void eliminar(@BindingParam("docente") final Docente docente) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + docente.getUsuario().getNombre()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						docente.setEstado(Sistema.getEstado(Sistema.ESTADO_ELIMINADO));
						InstanciaDAO.docenteDAO.saveorUpdate(docente);
						Clients.showNotification("Registro eliminado satisfactoriamente",
								Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
						listaDocente.clear();
						listaDocente = InstanciaDAO.docenteDAO.getListaDocente();
						BindUtils.postNotifyChange(null, null, ClassDocente.this, "listaDocente");
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("docente") Docente docente) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", docente);
		if (clase.equals("AsignacionDocente")) {
			BindUtils.postGlobalCommand(null, null, "cargarTutor", parametros);
		} else {
			BindUtils.postGlobalCommand(null, null, "cargarDocente", parametros);
		}
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaDocente")
	public void refrescarlista() {
		listaDocente = InstanciaDAO.docenteDAO.getListaDocente();
	}

	@Command
	public void editar(@BindingParam("docente") Docente docente) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", docente);
		Executions.createComponents("/vista/formDocenteCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaDocente")
	public void filtrar() {
		listaDocente.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaDocente.addAll(InstanciaDAO.docenteDAO.getListaDocente());
		} else {
			listaDocente.addAll(InstanciaDAO.docenteDAO.getListaDocenteFiltro(datoBusqueda.toUpperCase()));
		}
	}

	@Command
	public void generarExcel(@BindingParam("docente") Docente docente) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("ficha", "FICHA DE DOCENTE");
		mapreporte.put("cedula", docente.getUsuario().getCedula());
		mapreporte.put("nombre", docente.getUsuario().getNombre());
		mapreporte.put("apellido", docente.getUsuario().getApellido());
		mapreporte.put("titulo", docente.getTitulo());
		mapreporte.put("correo_electronico", docente.getUsuario().getEmail());
		mapreporte.put("fecha_nacimiento", new Date());
		mapreporte.put("direccion", docente.getDireccion());
		mapreporte.put("telefono", docente.getCelular());
		mapreporte.put("observacion", (docente.getObservacion()==null)?"":docente.getObservacion());
		mapreporte.put("foto",
				/* (docente.getFoto()==null || docente.getFoto().equals("")? */ Util.getPathRaiz()
						+ "/jasperReport/sin_foto.png"/*
														 * :Util.getPathRaiz()+
														 * docente.getFoto()
														 */);
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		Sistema.reporteEXCEL(Sistema.FICHA_DOCENTE, "ficha Docente", mapreporte, null,null,false);
	}

	@Command
	public void generarPdf(@BindingParam("docente") Docente docente) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("docente", docente);
		Executions.createComponents("/reporte/impresionFichaDocente.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		// List<String> lista = new ArrayList<>();
		// listaCalificacionParcial.forEach(x ->
		// lista.add(x.getCalificacionQuimestre().getAsignatura().getDescripcion()));

		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("ficha", "FICHA DE DOCENTE");
		mapreporte.put("cedula", docente.getUsuario().getCedula());
		mapreporte.put("nombre", docente.getUsuario().getNombre());
		mapreporte.put("apellido", docente.getUsuario().getApellido());
		mapreporte.put("titulo", docente.getTitulo());
		mapreporte.put("correo_electronico", docente.getUsuario().getEmail());
		mapreporte.put("fecha_nacimiento", new Date());
		mapreporte.put("direccion", docente.getDireccion());
		mapreporte.put("telefono", docente.getCelular());
		mapreporte.put("observacion", (docente.getObservacion()==null)?"":docente.getObservacion());
		mapreporte.put("foto", (docente.getFoto() == null || docente.getFoto().equals("")
				? Util.getPathRaiz() + "/jasperReport/sin_foto.png" : Util.getPathRaiz() + docente.getFoto()));
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		iframe.setContent(Sistema.reportePDF(Sistema.FICHA_DOCENTE, mapreporte, null));
	}

	@Command
	public void salir() {
		winDocente.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public Window getWinDocente() {
		return winDocente;
	}

	public void setWinDocente(Window winDocente) {
		this.winDocente = winDocente;
	}

	public List<Docente> getListaDocente() {
		return listaDocente;
	}

	public void setListaDocente(List<Docente> listaDocente) {
		this.listaDocente = listaDocente;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

}
