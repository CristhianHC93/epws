package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassQuimestre {
	@Wire("#winQuimestre")
	private Window winQuimestre;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Quimestre> listaQuimestre;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formQuimestreCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaQuimestre")
	public void eliminar(@BindingParam("quimestre") final Quimestre quimestre) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + quimestre.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.quimestreDAO.eliminar(quimestre.getId());
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaQuimestre.clear();
							listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
							BindUtils.postNotifyChange(null, null, ClassQuimestre.this, "listaQuimestre");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassQuimestre.this, "listaQuimestre");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("quimestre") Quimestre quimestre) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", quimestre);
		BindUtils.postGlobalCommand(null, null, "cargarQuimestre", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaQuimestre")
	public void refrescarlista() {
		listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
	}

	@Command
	public void editar(@BindingParam("quimestre") Quimestre quimestre) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", quimestre);
		Executions.createComponents("/vista/formQuimestreCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaQuimestre")
	public void filtrar() {
		listaQuimestre.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaQuimestre.addAll(InstanciaDAO.quimestreDAO.getListaQuimestre());
		} else {
			for (Quimestre item : InstanciaDAO.quimestreDAO.getListaQuimestre()) {
				if (item.getDescripcion().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaQuimestre.add(item);
				}
			}
		}
	}

	@Command
	public void salir() {
		winQuimestre.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}

	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}
	
	
}
