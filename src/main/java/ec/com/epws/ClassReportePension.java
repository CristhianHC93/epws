package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Mes;
import ec.com.epws.entity.Pension;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.models.Modelos;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassReportePension {

	private List<Modelos<Pension, List<Pension>, Object, Object>> listaPensionPropio = new ArrayList<>();
	private List<Pension> listaPension;

	private List<Curso> listaCurso;
	private Curso curso;

	private boolean obtenMes = false;
	// private boolean obtenCurso = true;

	private Matricula matricula;

	private Double totalRecuadado = 0.0;
	// private List<Estado> listaEstadoPension;
	// private List<Estado> listaEstado;
	// private Estado estado;

	// private boolean obtenMes = false;
	private List<Mes> ListaMes;
	private Mes mes;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista,
			@ExecutionArgParam("lista") List<Modelos<Pension, List<Pension>, Object, Object>> lista,
			@ExecutionArgParam("curso") Curso curso, @ExecutionArgParam("obtenMes") Boolean obtenMes,
			@ExecutionArgParam("mes") Mes mes) {
		Selectors.wireComponents(vista, this, false);
		
		listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
		// listaEstado = Sistema.getListaEstadoPension();
		ListaMes = InstanciaDAO.mesDAO.getListaMes();
		this.curso = (curso == null) ? listaCurso.get(0) : curso;
		this.obtenMes = (obtenMes == null) ? false : obtenMes;
		// estado = listaEstado.get(0);
		this.mes = (mes == null) ? ListaMes.get(0) : mes;
		// listaEstadoPension = Sistema.getListaEstadoPension();
		if (lista != null) {
			listaPensionPropio.addAll(lista);
		}else{
		buscar();
		}
	}

	@NotifyChange({ "listaPensionPropio", "totalRecuadado" })
	@Command
	public void buscar() {
		listaPensionPropio.clear();
		totalRecuadado = 0.0;
		if (matricula != null) {
			if (obtenMes) {
				listaPension = InstanciaDAO.pensionDAO.getListaPensionByMesMatricula(mes.getId(), matricula.getId(),
						Sistema.getAnioLectivoActivoEstatico().getId());
			} else {
				listaPension = InstanciaDAO.pensionDAO.getListaPensionByMesMatricula(1, matricula.getId(),
						Sistema.getAnioLectivoActivoEstatico().getId());
			}
		} else {
			if (obtenMes) {
				listaPension = InstanciaDAO.pensionDAO.getListaPensionByMes(mes.getId(),
						Sistema.getAnioLectivoActivoEstatico().getId());
			} else {
				listaPension = InstanciaDAO.pensionDAO.getListaPensionByMesCurso(1, curso.getId(),
						Sistema.getAnioLectivoActivoEstatico().getId());
			}
		}
		listaPension.forEach(x -> {
			totalRecuadado += x.getValorPagado();
			List<Pension> lista = new ArrayList<>();
			lista = InstanciaDAO.pensionDAO.getListaPensionByMatricula(x.getMatricula().getId());
			listaPensionPropio.add(new Modelos<Pension, List<Pension>, Object, Object>(x, lista, null, null, true));
		});
	}

	@Command
	public void agregarAlumno() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Pension");
		parametros.put("curso", curso.getId());
		Executions.createComponents("/vista/formMatricula.zul", null, parametros);
	}

	@NotifyChange({ "matricula", "listaPensionPropio", "totalRecuadado" })
	@GlobalCommand
	public void cargarAlumno(@BindingParam("objeto") Matricula matricula) {
		this.matricula = matricula;
		buscar();
	}

	@NotifyChange("matricula")
	@Command
	public void limpiar() {
		this.matricula = null;
	}

	@NotifyChange("listaPension")
	@Command
	public void guardar(@BindingParam("item") Pension pension) {
		/*
		 * if (pension.getEstado().getId() != Sistema.ESTADO_CON_DEUDA) {
		 * pension.setValorAdeudado(0); }
		 */
		InstanciaDAO.pensionDAO.saveorUpdate(pension);
		Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
	}

	@NotifyChange("listaPensionPropio")
	@Command
	public void select(@BindingParam("item") Pension pension) {
		if (pension.getEstado().getId() != Sistema.ESTADO_PENSION_DEUDA) {
			// pension.setValorAdeudado(0);
		}
	}

	@NotifyChange("listaPensionPropio")
	@Command
	public void pagarCuota(@BindingParam("item") Pension pension) {
		if (pension.getValorPagado() > pension.getValorReferencia().getValorPension()) {
			pension.setValorPagado(pension.getValorReferencia().getValorPension());
		}
		InstanciaDAO.pensionDAO.saveorUpdate(pension);
	}

	@Command
	public void generarExcel() {
		if (listaPensionPropio.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("valorPension", Sistema.getValorReferencia().getValorPension());
		mapreporte.put("valorMatricula", Sistema.getValorReferencia().getValorMatricula());
		mapreporte.put("mes", mes.getDescripcion());
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		Sistema.reporteEXCEL((obtenMes) ? Sistema.PENSION_MES_JASPER : Sistema.PENSION_JASPER, "Pensiones", mapreporte,
				Sistema.getPensionDataSource(listaPensionPropio, obtenMes),null,true);
	}

	@Command
	public void generarPdf() {
		if (listaPensionPropio.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaPensionPropio);
		parametros.put("curso", curso);
		parametros.put("obtenMes", obtenMes);
		parametros.put("mes", mes);
		Executions.createComponents("/reporte/impresionPension.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {

		// InformacionNegocio in =
		// InstanciaDAO.getInstanciaInformacionNegocioDAO().getInformacionNegocio(1);
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("valorPension", Sistema.getValorReferencia().getValorPension());
		mapreporte.put("valorMatricula", Sistema.getValorReferencia().getValorMatricula());
		mapreporte.put("mes", mes.getDescripcion());
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		iframe.setContent(Sistema.reportePDF((obtenMes) ? Sistema.PENSION_MES_JASPER : Sistema.PENSION_JASPER,
				mapreporte, Sistema.getPensionDataSource(listaPensionPropio, obtenMes)));
	}

	/*
	 * 
	 */

	public int getEstadoCancelado() {
		return Sistema.ESTADO_PENSION_PAGADA;
	}

	public int getEstadoDeuda() {
		return Sistema.ESTADO_PENSION_DEUDA;
	}

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Pension> getListaPension() {
		return listaPension;
	}

	public void setListaPension(List<Pension> listaPension) {
		this.listaPension = listaPension;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}


	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

	public List<Mes> getListaMes() {
		return ListaMes;
	}

	public void setListaMes(List<Mes> listaMes) {
		ListaMes = listaMes;
	}

	public Mes getMes() {
		return mes;
	}

	public void setMes(Mes mes) {
		this.mes = mes;
	}

	public String getConstraintPrecio() {
		return Sistema.CONSTRAINT_PRECIO;
	}

	public List<Modelos<Pension, List<Pension>, Object, Object>> getListaPensionPropio() {
		return listaPensionPropio;
	}

	public void setListaPensionPropio(List<Modelos<Pension, List<Pension>, Object, Object>> listaPensionPropio) {
		this.listaPensionPropio = listaPensionPropio;
	}

	public boolean isObtenMes() {
		return obtenMes;
	}

	public void setObtenMes(boolean obtenMes) {
		this.obtenMes = obtenMes;
	}

	public Double getTotalRecuadado() {
		return totalRecuadado;
	}

	public void setTotalRecuadado(Double totalRecuadado) {
		this.totalRecuadado = totalRecuadado;
	}

}
