package ec.com.epws;

import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.Email;
import ec.com.epws.entity.InformacionNegocio;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.sistema.Sistema;

public class ClassConfiguracion {

	private String btGuardarEditar = Sistema.idioma.getGuardar();
	// Variables para configuracion correo
	private List<Email> listaEmail;
	private Email email;

	private String clave;
	// Variables para configurar datos de negocio
	private InformacionNegocio informacionNegocio;

	private String correoEnviar = "demo@demo.com";

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		if (listaEmail == null) {
			listaEmail = InstanciaDAO.getEmailDAO().getListaEmail();
		}
		if (email == null) {
			email = listaEmail.get(0);
		}
		informacionNegocio = InstanciaDAO.getInformacionNegocioDAO().getInformacionNegocio(1);
	}

	@Command
	@NotifyChange("informacionNegocio")
	public void guardarInformacion() {
		if (informacionNegocio.getId() == null) {
			Messagebox.show("seleccione el registro para modificarlo");
			return;
		}
		InstanciaDAO.getInformacionNegocioDAO().saveorUpdate(informacionNegocio);
		Clients.showNotification("Datos modificados correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
				"middle_center", 1500);
		
	}

	@Command
	@NotifyChange({ "listaEmail", "email" })
	public void guardarEmail() {
		if (email.getId() == null) {
			Messagebox.show("seleccione el registro para modificarlo");
			return;
		}
		Clients.showNotification("Email modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
				"middle_center", 1500);
		InstanciaDAO.getEmailDAO().saveorUpdate(email);
		listaEmail = InstanciaDAO.getEmailDAO().getListaEmail();
		email = listaEmail.get(0);
	}

	@Command
	@NotifyChange({ "clave", "btGuardarEditar" })
	public void desencriptar(@BindingParam("item") Email email) {
		try {
			clave = Sistema.desencriptar(email.getClave());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		btGuardarEditar = Sistema.idioma.getEditar();
	}

	@Command
	public void enviarCorreoPrueba() {
		if (!Sistema.verificarInternet()) {
			Messagebox.show("Imposible enviar correo, Por favor verifique su conexion a internet");
			return;
		}
		try {
			Sistema.enviarCorreoPrueba(correoEnviar, "Prueba", "Mensaje de prueba");
			Messagebox.show("Correo Enviado con Exito");
		} catch (Exception e) {
			Messagebox.show("Imposible enviar Correo");
		}
	}

	public String getBtGuardarEditar() {
		return btGuardarEditar;
	}

	public void setBtGuardarEditar(String btGuardarEditar) {
		this.btGuardarEditar = btGuardarEditar;
	}

	public String getCorreoEnviar() {
		return correoEnviar;
	}

	public void setCorreoEnviar(String correoEnviar) {
		this.correoEnviar = correoEnviar;
	}

	public List<Email> getListaEmail() {
		return listaEmail;
	}

	public void setListaEmail(List<Email> listaEmail) {
		this.listaEmail = listaEmail;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		this.email = email;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public InformacionNegocio getInformacionNegocio() {
		return informacionNegocio;
	}

	public void setInformacionNegocio(InformacionNegocio informacionNegocio) {
		this.informacionNegocio = informacionNegocio;
	}

}
