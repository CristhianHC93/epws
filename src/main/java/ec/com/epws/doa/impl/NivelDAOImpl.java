package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.NivelDAO;
import ec.com.epws.entity.Nivel;

public class NivelDAOImpl extends DAOImplements implements NivelDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Nivel> getListaNivel() {
		return (List<Nivel>) super.getListaRegistro("SELECT n FROM Nivel n");
	}

	@Override
	public Nivel saveorUpdate(Nivel nivel) {
		return (Nivel)super.saveorUpdate(nivel);
	}

	@Override
	public int eliminar(Nivel nivel) {
		return super.eliminar(nivel.getId(), "nivel", "DELETE FROM Nivel where id = :nivel");
	}

}
