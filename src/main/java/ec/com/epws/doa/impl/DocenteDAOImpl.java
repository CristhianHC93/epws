package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.DocenteDAO;
import ec.com.epws.entity.Docente;

public class DocenteDAOImpl extends DAOImplements implements DocenteDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Docente> getListaDocente() {
		return (List<Docente>) super.getListaRegistro("SELECT d FROM Docente d where d.estado.id = 3");
	}

	@Override
	public Docente saveorUpdate(Docente docente) {
		return (Docente) super.saveorUpdate(docente);
	}

	@Override
	public Docente getDocente(int usuario) {
		return (Docente) super.getRegistro(usuario,"usuario","SELECT d FROM Docente d where d.usuario.id = :usuario and d.estado.id = 3");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Docente> getListaDocenteFiltro(String dato) {
		return (List<Docente>) super.getListaRegistroFiltro(dato, "dato",
				"SELECT d FROM Docente d where d.estado.id = 3 and d.usuario.cedula like :dato or d.usuario.nombre like :dato or d.usuario.apellido like :dato");
	}
	

}
