package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.CuadroCalificacionDAO;
import ec.com.epws.entity.CuadroCalificacion;

public class CuadroCalificacionDAOImpl extends DAOImplements implements CuadroCalificacionDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<CuadroCalificacion> getListaCuadroCalificacionByAsignatura(int asignatura) {
		return (List<CuadroCalificacion>) super.getListaRegistro(asignatura, "asignatura",
				"SELECT c FROM CuadroCalificacion c where c.asignatura.id = :asignatura ORDER BY c.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CuadroCalificacion> getListaCuadroCalificacionByMatricula(int matricula) {
		return (List<CuadroCalificacion>) super.getListaRegistro(matricula, "matricula",
				"SELECT c FROM CuadroCalificacion c where c.matricula.id = :matricula ORDER BY c.asignatura.codigoCualitativa.id asc");
	}

	@Override
	public CuadroCalificacion getCuadroCalificacion(int asignatura, int matricula) {
		try {
			return (CuadroCalificacion) super.getRegistro(matricula, "matricula", asignatura, "asignatura",
					"SELECT c FROM CuadroCalificacion c where c.matricula.id = :matricula and c.asignatura.id = :asignatura");
		} catch (Exception e) {
			return null;
		}

	}

	@Override
	public CuadroCalificacion saveorUpdate(CuadroCalificacion cuadroCalificacion) {
		return (CuadroCalificacion) super.saveorUpdate(cuadroCalificacion);
	}

}
