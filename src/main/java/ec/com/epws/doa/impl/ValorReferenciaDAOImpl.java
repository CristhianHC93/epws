package ec.com.epws.doa.impl;

import ec.com.epws.dao.ie.ValorReferenciaDAO;
import ec.com.epws.entity.ValorReferencia;

public class ValorReferenciaDAOImpl extends DAOImplements implements ValorReferenciaDAO{

	@Override
	public ValorReferencia getValorReferencia() {
		return (ValorReferencia) super.getRegistro("SELECT v FROM ValorReferencia v where v.id=1");
	}

	@Override
	public ValorReferencia saveUpdate(ValorReferencia valorReferencia) {
		return (ValorReferencia) super.saveorUpdate(valorReferencia);
	}

}
