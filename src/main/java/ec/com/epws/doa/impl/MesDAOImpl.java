package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.MesDAO;
import ec.com.epws.entity.Mes;

public class MesDAOImpl extends DAOImplements implements MesDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Mes> getListaMes() {
		return (List<Mes>) super.getListaRegistro("SELECT m FROM Mes m order by m.id asc");
	}

	@Override
	public Mes saveorUpdate(Mes mes) {
		return (Mes) super.saveorUpdate(mes);
	}

	@Override
	public int eliminar(Mes mes) {
		return super.eliminar(mes.getId(), "mes", "DELETE FROM Mes where id = :mes");
	}

}
