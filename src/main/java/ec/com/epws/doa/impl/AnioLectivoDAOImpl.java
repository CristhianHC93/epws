package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.AnioLectivoDAO;
import ec.com.epws.entity.AnioLectivo;

public class AnioLectivoDAOImpl extends DAOImplements implements AnioLectivoDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<AnioLectivo> getListaAnioLectivo() {
		return (List<AnioLectivo>) super.getListaRegistro("SELECT a FROM AnioLectivo a order by a.id desc");
	}

	@Override
	public AnioLectivo getAnioLectivoActivo() {
		return (AnioLectivo) super.getRegistro("SELECT a FROM AnioLectivo a where a.estado = TRUE");
	}

	@Override
	public AnioLectivo saveorUpdate(AnioLectivo anioLectivo) {
		return (AnioLectivo) super.saveorUpdate(anioLectivo);
	}

}
