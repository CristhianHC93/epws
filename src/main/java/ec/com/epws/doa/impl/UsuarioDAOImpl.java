package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.UsuarioDAO;
import ec.com.epws.entity.Usuario;

public class UsuarioDAOImpl extends DAOImplements implements UsuarioDAO {

	@Override
	public Usuario saveorUpdate(Usuario usuario) {
		return (Usuario) super.saveorUpdate(usuario);
	}

	@Override
	public Usuario getUsuario(String username, String password) {
		try {
			String sql = "SELECT u FROM Usuario u where u.username = :username and u.password = :password and u.estado = true";
			return (Usuario) super.getRegistro(username, "username", password, "password", sql);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public Usuario getUsuarioRepresentante(String username, String password) {
		try {
			String sql = "SELECT u FROM Usuario u where u.username = :username and u.password = :password and u.estado = true and u.rol.id = 4";
			return (Usuario) super.getRegistro(username, "username", password, "password", sql);
		} catch (Exception e) {
			return null;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> getListaUsuario() {
		return (List<Usuario>) super.getListaRegistro("SELECT u FROM Usuario u");
	}

}
