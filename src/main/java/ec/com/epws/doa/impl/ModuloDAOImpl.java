package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.ModuloDAO;
import ec.com.epws.entity.Modulo;

public class ModuloDAOImpl extends DAOImplements implements ModuloDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Modulo> getListaModulo() {
		return (List<Modulo>) super.getListaRegistro("SELECT m FROM Modulo m order by m.id");
	}

	@Override
	public Modulo saveorUpdate(Modulo modulo) {
		return (Modulo) super.saveorUpdate(modulo);
	}

}
