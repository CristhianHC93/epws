package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.AlumnoDAO;
import ec.com.epws.entity.Alumno;

public class AlumnoDAOImpl extends DAOImplements implements AlumnoDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Alumno> getListaAlumno() {
		return (List<Alumno>) super.getListaRegistro("SELECT a FROM Alumno a where a.estado.id = 3 ORDER BY a.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alumno> getListaAlumno(boolean inscripcion) {
		return (List<Alumno>) super.getListaRegistro(inscripcion, "inscripcion",
				"SELECT a FROM Alumno a where a.inscripcion = :inscripcion and a.estado.id = 3 ORDER BY a.apellido");
	}
	
	@Override
	public Alumno getAlumno(int usuario) {
		return (Alumno) super.getRegistro(usuario, "usuario",
				"SELECT a FROM Alumno a where a.usuario.id = :usuario and a.estado.id = 3");
	}

	@Override
	public Alumno saveorUpdate(Alumno alumno) {
		return (Alumno) super.saveorUpdate(alumno);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Alumno> getListaAlumnoFiltro(String dato) {
		return (List<Alumno>) super.getListaRegistroFiltro(dato, "dato",
				"SELECT a FROM Alumno a where a.estado.id = 3 and a.cedula like :dato or a.nombre like :dato or a.apellido like :dato ORDER BY a.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Alumno> getListaAlumnoFiltroInscripcion(String dato) {
		return (List<Alumno>) super.getListaRegistroFiltro(dato, "dato",
				"SELECT a FROM Alumno a where a.inscripcion = true and a.estado.id = 3 and a.cedula like :dato or c.nombre like :dato or a.apellido like :dato ORDER BY a.apellido");
	}

}
