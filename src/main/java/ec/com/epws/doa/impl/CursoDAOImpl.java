package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.CursoDAO;
import ec.com.epws.entity.Curso;

public class CursoDAOImpl extends DAOImplements implements CursoDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Curso> getListaCurso() {
		return (List<Curso>) super.getListaRegistro("SELECT c FROM Curso c order by c.id");
	}
	
	@Override
	public Curso getCursoById(int id) {
		return (Curso) super.getRegistro(id,"id","SELECT c FROM Curso c where c.id = :id order by c.id");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Curso> getListaCurso(int nivel) {
		return (List<Curso>) super.getListaRegistro(nivel, "nivel", "SELECT c FROM Curso c where c.nivel.id = :nivel order by c.id");
	}

	@Override
	public Curso saveorUpdate(Curso curso) {
		return (Curso) super.saveorUpdate(curso);
	}

	@Override
	public int eliminar(Curso curso) {
		return super.eliminar(curso.getId(), "curso", "DELETE FROM Curso where id = :curso");
	}

}
