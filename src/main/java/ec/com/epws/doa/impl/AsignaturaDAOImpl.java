package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.AsignaturaDAO;
import ec.com.epws.entity.Asignatura;

public class AsignaturaDAOImpl extends DAOImplements implements AsignaturaDAO {

	@Override
	public long countAsignatura(int curso) {
		return super.getCount(curso,"curso",
				"Select count(a) from Asignatura a where a.curso.id = :curso and a.codigoCualitativa.id = 2");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Asignatura> getListaAsignatura() {
		return (List<Asignatura>) super.getListaRegistro("SELECT a FROM Asignatura a");
	}

	// Si el metodo recibe en comportamiento true buscara la lista incluido
	// comportamiento
	@SuppressWarnings("unchecked")
	@Override
	public List<Asignatura> getListaAsignatura(int curso, boolean comportamiento) {
		return (comportamiento)
				? (List<Asignatura>) super.getListaRegistro(curso, "curso",
						"SELECT a FROM Asignatura a where a.curso.id = :curso order by a.id")
				: (List<Asignatura>) super.getListaRegistro(curso, "curso",
						"SELECT a FROM Asignatura a where a.curso.id = :curso and a.codigoCualitativa.id != 4 order by a.id");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Asignatura> getListaAsignaturaBasica(int curso) {
		return (List<Asignatura>) super.getListaRegistro(curso, "curso",
				"SELECT a FROM Asignatura a where a.curso.id = :curso and a.codigoCualitativa.id = 2 order by a.id");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Asignatura> getListaAsignaturaBasicaInicial(int curso) {
		return (List<Asignatura>) super.getListaRegistro(curso, "curso",
				"SELECT a FROM Asignatura a where a.curso.id = :curso and (a.codigoCualitativa.id = 2 or a.codigoCualitativa.id = 1) order by a.id");
	}

	@Override
	public Asignatura saveorUpdate(Asignatura asignatura) {
		return (Asignatura) super.saveorUpdate(asignatura);
	}

	@Override
	public int eliminar(Asignatura asignatura) {
		return super.eliminar(asignatura.getId(), "asignatura", "DELETE FROM Asignatura where id = :asignatura");
	}

}
