package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.CodigoCualitativaDAO;
import ec.com.epws.entity.CodigoCualitativa;

public class CodigoCualitativaDAOImpl extends DAOImplements implements CodigoCualitativaDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<CodigoCualitativa> getListaCodigoCualitativa() {
		return (List<CodigoCualitativa>) super.getListaRegistro("SELECT c FROM CodigoCualitativa c");
	}

	@Override
	public CodigoCualitativa saveUpdate(CodigoCualitativa codigoCualitativa) {
		return (CodigoCualitativa) super.saveorUpdate(codigoCualitativa);
	}

}
