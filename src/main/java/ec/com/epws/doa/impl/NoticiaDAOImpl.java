package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.NoticiaDAO;
import ec.com.epws.entity.Noticia;

public class NoticiaDAOImpl extends DAOImplements implements NoticiaDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Noticia> getListaNoticia() {
		return (List<Noticia>) super.getListaRegistro("SELECT n FROM Noticia n order by n.id desc");
	}

	@Override
	public Noticia saveorUpdate(Noticia noticia) {
		return (Noticia) super.saveorUpdate(noticia);
	}

	@Override
	public int eliminar(Noticia noticia) {
		return super.eliminar(noticia.getId(), "noticia", "DELETE FROM Noticia where id = :noticia");
	}

}
