package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.CualitativaDAO;
import ec.com.epws.entity.Cualitativa;

public class CualitativaDAOImpl extends DAOImplements implements CualitativaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Cualitativa> getListaCualitativa() {
		return (List<Cualitativa>) super.getListaRegistro("SELECT c FROM Cualitativa c order by c.id");
	}
	
	@Override
	public Cualitativa getCualitativa(int id) {
		return (Cualitativa) super.getRegistro(id,"id","SELECT c FROM Cualitativa c where c.id = :id");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Cualitativa> getListaCualitativaByCodigo(int codigo) {
		return (List<Cualitativa>) super.getListaRegistro(codigo, "codigo",
				"SELECT c FROM Cualitativa c where c.codigoCualitativa.id = :codigo order by c.id");
	}

	@Override
	public Cualitativa saveorUpdate(Cualitativa cualitativa) {
		return (Cualitativa) super.saveorUpdate(cualitativa);
	}

	@Override
	public int eliminar(Cualitativa cualitativa) {
		return super.eliminar(cualitativa.getId(), "cualitativa", "DELETE FROM Curso where id = :cualitativa");
	}

}
