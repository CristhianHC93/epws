package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.AlumnoFamiliaDAO;
import ec.com.epws.entity.AlumnoFamilia;

public class AlumnoFamiliaDAOImpl extends DAOImplements implements AlumnoFamiliaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<AlumnoFamilia> getListaAlumno(int alumno) {
		return (List<AlumnoFamilia>) super.getListaRegistro(alumno, "alumno",
				"SELECT a FROM AlumnoFamilia a where a.alumno.id = :alumno ");
	}

	@Override
	public AlumnoFamilia saveorUpdate(AlumnoFamilia alumnoFamilia) {
		return (AlumnoFamilia) super.saveorUpdate(alumnoFamilia);
	}

	@Override
	public AlumnoFamilia getAlumnoFamilia(int alumno) {
		return (AlumnoFamilia) super.getRegistro(alumno, "alumno",
				"SELECT a FROM AlumnoFamilia a where a.alumno.id = :alumno and a.representante = TRUE");
	}

	@Override
	public AlumnoFamilia getAlumnoFamilia(int alumno, int parentesco) {
		return (AlumnoFamilia) super.getRegistro(alumno, "alumno", parentesco, "parentesco",
				"SELECT a FROM AlumnoFamilia a where a.alumno.id = :alumno and a.familia.parentesco.id = :parentesco and a.representante = FALSE");
	}

}
