package ec.com.epws.doa.impl;

import java.util.Date;
import java.util.List;

import ec.com.epws.dao.ie.AsistenciaDAO;
import ec.com.epws.entity.Asistencia;

public class AsistenciaDAOImpl extends DAOImplements implements AsistenciaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Asistencia> getListaAsistencia(int anioLectivo, int curso, Date fecha) {
		return (List<Asistencia>) super.getListaRegistro(anioLectivo, "anioLectivo", curso, "curso", fecha,"fecha",
				"SELECT a FROM Asistencia a where a.curso.id = :curso and a.anioLectivo.id = :anioLectivo and a.fecha = :fecha ORDER BY a.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Asistencia> getListaAsistencia(int anioLectivo, int alumno) {
		return (List<Asistencia>) super.getListaRegistro(anioLectivo, "anioLectivo", alumno, "alumno", 
				"SELECT a FROM Asistencia a where a.alumno.id = :alumno and a.anioLectivo.id = :anioLectivo ORDER BY a.alumno.apellido");
	}

	@Override
	public Asistencia saveorUpdate(Asistencia asistencia) {
		return (Asistencia) super.saveorUpdate(asistencia);
	}

	@Override
	public int eliminar(Asistencia asistencia) {
		return super.eliminar(asistencia.getId(), "asistencia", "DELETE FROM Asistencia where id = :asistencia");
	}

}
