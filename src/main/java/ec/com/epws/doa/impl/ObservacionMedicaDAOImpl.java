package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.ObservacionMedicaDAO;
import ec.com.epws.entity.ObservacionMedica;

public class ObservacionMedicaDAOImpl extends DAOImplements implements ObservacionMedicaDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<ObservacionMedica> getListaObservacionMedica() {
		return (List<ObservacionMedica>) super.getListaRegistro("SELECT o FROM ObservacionMedica o");
	}

	@Override
	public ObservacionMedica saveorUpdate(ObservacionMedica om) {
		return (ObservacionMedica) super.saveorUpdate(om);
	}

}
