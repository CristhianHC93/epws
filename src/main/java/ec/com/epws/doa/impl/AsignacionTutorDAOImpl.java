package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.AsignacionTutorDAO;
import ec.com.epws.entity.AsignacionTutor;

public class AsignacionTutorDAOImpl extends DAOImplements implements AsignacionTutorDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<AsignacionTutor> getListaAsignacionTutor(int anioLectivo) {
		return (List<AsignacionTutor>) super.getListaRegistro(anioLectivo, "anioLectivo",
				"SELECT a FROM AsignacionTutor a where a.anioLectivo.id = :anioLectivo");
	}

	@Override
	public AsignacionTutor saveorUpdate(AsignacionTutor asignacionDocente) {
		return (AsignacionTutor) super.saveorUpdate(asignacionDocente);
	}

	@Override
	public AsignacionTutor getAsignacionTutor(int anioLectivo, int curso) {
		try {
			return (AsignacionTutor) super.getRegistro(anioLectivo, "anioLectivo", curso, "curso",
					"SELECT a FROM AsignacionTutor a where a.curso.id = :curso and a.anioLectivo.id = :anioLectivo");
		} catch (Exception e) {
			return new AsignacionTutor();
		}
	}
	
	@Override
	public AsignacionTutor getAsignacionTutorByDocente(int anioLectivo, int docente) {
		try {
			return (AsignacionTutor) super.getRegistro(anioLectivo, "anioLectivo", docente, "docente",
					"SELECT a FROM AsignacionTutor a where a.docente.id = :docente and a.anioLectivo.id = :anioLectivo");
		} catch (Exception e) {
			return new AsignacionTutor();
		}
	}
}
