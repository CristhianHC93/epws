package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.RolDAO;
import ec.com.epws.entity.Rol;

public class RolDAOImpl extends DAOImplements implements RolDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Rol> getListaRol() {
		return (List<Rol>) super.getListaRegistro("SELECT r FROM Rol r");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Rol> getListaRol(int estado) {
		return (List<Rol>) super.getListaRegistro(estado, "estado", "SELECT r FROM Rol r where r.estado.id = :estado");
	}

	@Override
	public Rol saveorUpdate(Rol rol) {
		return (Rol) super.saveorUpdate(rol);
	}

}
