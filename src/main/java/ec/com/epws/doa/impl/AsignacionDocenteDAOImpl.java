package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.AsignacionDocenteDAO;
import ec.com.epws.entity.AsignacionDocente;

public class AsignacionDocenteDAOImpl extends DAOImplements implements AsignacionDocenteDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<AsignacionDocente> getListaAsignacionDocenteByDocente(int anioLectivo, int docente) {
		return (List<AsignacionDocente>) super.getListaRegistro(anioLectivo, "anioLectivo", docente, "docente",
				"SELECT a FROM AsignacionDocente a where a.anioLectivo.id = :anioLectivo and a.docente.id = :docente");
	}

/*	@SuppressWarnings("unchecked")
	@Override
	public List<AsignacionDocente> getListaAsignacionDocenteByDocenteByNivel12(int anioLectivo, int docente) {
		return (List<AsignacionDocente>) super.getListaRegistro(anioLectivo, "anioLectivo", docente, "docente",
				"SELECT a FROM AsignacionDocente a where a.anioLectivo.id = :anioLectivo and a.docente.id = :docente and a.asignatura.curso.nivel.id != 1");
	}
*/
	@SuppressWarnings("unchecked")
	@Override
	public List<AsignacionDocente> getListaAsignacionDocenteByCurso(int anioLectivo, int curso) {
		return (List<AsignacionDocente>) super.getListaRegistro(anioLectivo, "anioLectivo", curso, "curso",
				"SELECT a FROM AsignacionDocente a where a.anioLectivo.id = :anioLectivo and a.asignatura.curso.id = :curso");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AsignacionDocente> getListaAsignacionDocente(int anioLectivo) {
		return (List<AsignacionDocente>) super.getListaRegistro(anioLectivo, "anioLectivo",
				"SELECT a FROM AsignacionDocente a where a.anioLectivo.id = :anioLectivo ");
	}

	@Override
	public AsignacionDocente getAsignacionDocenteByAsignatura(int anioLectivo, int asignatura) {
		try {
			return (AsignacionDocente) super.getRegistro(anioLectivo, "anioLectivo", asignatura,
					"asignatura",
					"SELECT a FROM AsignacionDocente a where a.anioLectivo.id = :anioLectivo and a.asignatura.id = :asignatura ");
		} catch (Exception e) {
			System.out.println(e.getStackTrace());
			return null;
		}

	}

	@Override
	public AsignacionDocente saveorUpdate(AsignacionDocente asignacionDocente) {
		return (AsignacionDocente) super.saveorUpdate(asignacionDocente);
	}

	@Override
	public int eliminar(AsignacionDocente asignacionDocente) {
		return super.eliminar(asignacionDocente.getId(), "asignacionDocente",
				"DELETE FROM AsignacionDocente where id = :asignacionDocente");
	}

}
