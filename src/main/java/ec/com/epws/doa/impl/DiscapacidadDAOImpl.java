package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.DiscapacidadDAO;
import ec.com.epws.entity.Discapacidad;

public class DiscapacidadDAOImpl extends DAOImplements implements DiscapacidadDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Discapacidad> getListaDiscapacidad() {
		return (List<Discapacidad>) super.getListaRegistro("SELECT v FROM Discapacidad v");
	}

	@Override
	public Discapacidad saveorUpdate(Discapacidad discapacidad) {
		return (Discapacidad) super.saveorUpdate(discapacidad);
	}

	@Override
	public int eliminar(Discapacidad discapacidad) {
		return super.eliminar(discapacidad.getId(), "discapacidad", "DELETE FROM Discapacidad where id = :discapacidad");
	}

}
