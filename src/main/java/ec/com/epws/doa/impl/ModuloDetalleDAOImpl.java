package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.ModuloDetalleDAO;
import ec.com.epws.entity.Modulo;
import ec.com.epws.entity.ModuloDetalle;

public class ModuloDetalleDAOImpl extends DAOImplements implements ModuloDetalleDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<ModuloDetalle> getListaModuloDetallesByModulo(Modulo modulo) {
		int id = modulo.getId();
		String sql = "SELECT m FROM ModuloDetalle m where m.modulo.id = :modulo";
		return (List<ModuloDetalle>) super.getListaRegistro(id, "modulo", sql);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ModuloDetalle> getListaModuloDetallesByModulo(int id) {
//		int id = modulo.getId();
		String sql = "SELECT m FROM ModuloDetalle m where m.modulo.id = :modulo";
		return (List<ModuloDetalle>) super.getListaRegistro(id, "modulo", sql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ModuloDetalle> getListaModuloDetalle() {
		return (List<ModuloDetalle>) super.getListaRegistro("SELECT m FROM ModuloDetalle m");
	}

	@Override
	public ModuloDetalle saveorUpdate(ModuloDetalle moduloDetalle) {
		return (ModuloDetalle) super.saveorUpdate(moduloDetalle);
	}
	
	
	
}
