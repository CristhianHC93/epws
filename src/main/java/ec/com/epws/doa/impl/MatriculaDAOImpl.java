package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.MatriculaDAO;
import ec.com.epws.entity.Matricula;

public class MatriculaDAOImpl extends DAOImplements implements MatriculaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Matricula> getListaMatricula() {
		return (List<Matricula>) super.getListaRegistro("SELECT m FROM Matricula m where m.estado.id = 3 ORDER BY m.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Matricula> getListaMatriculaAnioActivo(int anioLectivo) {
		return (List<Matricula>) super.getListaRegistro(anioLectivo, "anioLectivo",
				"SELECT m FROM Matricula m where m.anioLectivo.id = :anioLectivo and m.estado.id = 3 ORDER BY m.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Matricula> getListaMatriculaAnioActivoByAlumnoByEstado(int anioLectivo,int alumno,int estado){
		return (List<Matricula>) super.getListaRegistro(anioLectivo, "anioLectivo",alumno,"alumno",estado,"estado",
				"SELECT m FROM Matricula m where m.anioLectivo.id = :anioLectivo and m.alumno.id = :alumno and m.estado.id = :estado ORDER BY m.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Matricula> getListaMatriculaAnioActivoFiltro(int anioLectivo,String dato) {
		return (List<Matricula>) super.getListaRegistroFiltro(anioLectivo, "anioLectivo",dato,"dato",
				"SELECT m FROM Matricula m where m.anioLectivo.id = :anioLectivo and m.estado.id = 3 and m.alumno.cedula like :dato or m.alumno.nombre like :dato or m.alumno.apellido like :dato ORDER BY m.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Matricula> getListaMatriculaAnioActivoByCurso(int anioLectivo, int curso) {
		return (List<Matricula>) super.getListaRegistro(anioLectivo, "anioLectivo", curso, "curso",
				"SELECT m FROM Matricula m where m.anioLectivo.id = :anioLectivo and m.curso.id = :curso and m.estado.id = 3 ORDER BY m.alumno.apellido");
	}

	@Override
	public Matricula saveorUpdate(Matricula matricula) {
		return (Matricula) super.saveorUpdate(matricula);
	}

}
