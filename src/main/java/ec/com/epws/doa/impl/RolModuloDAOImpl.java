package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.RolModuloDAO;
import ec.com.epws.entity.Rol;
import ec.com.epws.entity.RolModulo;

public class RolModuloDAOImpl extends DAOImplements implements RolModuloDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<RolModulo> getListaRolModulo(int idRol,int estado) {
		String sql = "SELECT r FROM RolModulo r where r.rol.id = :rol and r.estado = true and r.rol.estado.id = :estado";
		return (List<RolModulo>)super.getListaRegistro(idRol, "rol",estado,"estado", sql);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RolModulo> getListaRolModulo(Rol rol) {
		String sql = "SELECT r FROM RolModulo r where r.rol.id = :rol ";
		return (List<RolModulo>)super.getListaRegistro(rol.getId(), "rol", sql);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RolModulo> getListaRolModulo(int estado) {
		return (List<RolModulo>) super.getListaRegistro(estado,"estado","SELECT r FROM RolModulo r where r.rol.estado.id = :estado order by r.rol.id");
	}

	@Override
	public RolModulo saveorUpdate(RolModulo rolModulo) {
		return (RolModulo) super.saveorUpdate(rolModulo);
	}

}
