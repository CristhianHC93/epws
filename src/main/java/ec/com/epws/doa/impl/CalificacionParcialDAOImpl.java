package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.CalificacionParcialDAO;
import ec.com.epws.entity.CalificacionParcial;

public class CalificacionParcialDAOImpl extends DAOImplements implements CalificacionParcialDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionParcial> getListaCalificacionParcialByAsignaturaAndParcial(int asignatura, int parcial) {
		return (List<CalificacionParcial>) super.getListaRegistro(asignatura, "asignatura", parcial, "parcial",
				"SELECT c FROM CalificacionParcial c where c.calificacionQuimestre.asignatura.id = :asignatura and c.parcial.id = :parcial ORDER BY c.calificacionQuimestre.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionParcial> getListaCalificacionParcialByCalificacionQuimestre(int calificacionQuimestre) {
		return (List<CalificacionParcial>) super.getListaRegistro(calificacionQuimestre, "calificacionQuimestre",
				"SELECT c FROM CalificacionParcial c where c.calificacionQuimestre.id = :calificacionQuimestre ORDER BY c.calificacionQuimestre.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionParcial> getListaCalificacionParcialByMatriculaAndParcial(int matricula, int parcial) {
		return (List<CalificacionParcial>) super.getListaRegistro(matricula, "matricula", parcial, "parcial",
				"SELECT c FROM CalificacionParcial c where c.calificacionQuimestre.matricula.id = :matricula and c.parcial.id = :parcial ORDER BY c.calificacionQuimestre.matricula.alumno.apellido");
	}

	@Override
	public CalificacionParcial getCalificacionParcial(int calificacionParcial,int asignatura){
		try {
			return (CalificacionParcial) super.getRegistro(calificacionParcial, "calificacionParcial",asignatura ,"asignatura","SELECT c FROM CalificacionParcial c where c.id = :calificacionParcial and c.calificacionQuimestre.asignatura.id = :asignatura");			
		} catch (Exception e) {
			return null;
		}

	}
	
	@Override
	public CalificacionParcial saveorUpdate(CalificacionParcial calificacionParcial) {
		return (CalificacionParcial) super.saveorUpdate(calificacionParcial);
	}

}
