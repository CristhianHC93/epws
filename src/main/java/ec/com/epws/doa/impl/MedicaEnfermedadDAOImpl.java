package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.MedicaEnfermedadDAO;
import ec.com.epws.entity.MedicaEnfermedad;

public class MedicaEnfermedadDAOImpl extends DAOImplements implements MedicaEnfermedadDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaEnfermedad> getListaMedicaEnfermedad() {
		return (List<MedicaEnfermedad>) super.getListaRegistro("SELECT m FROM MedicaEnfermedad m");
	}

	@Override
	public MedicaEnfermedad saveorUpdate(MedicaEnfermedad medicaEnfermedad) {
		return (MedicaEnfermedad) super.saveorUpdate(medicaEnfermedad);
	}

	@Override
	public int eliminar(MedicaEnfermedad medicaEnfermedad) {
		return super.eliminar(medicaEnfermedad.getId(), "medicaEnfermedad",
				"DELETE FROM MedicaEnfermedad where id = :medicaEnfermedad");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaEnfermedad> getListaMedicaEnfermedadByEnfermedad(int enfermedad) {
		return (List<MedicaEnfermedad>) super.getListaRegistro(enfermedad, "enfermedad",
				"SELECT m FROM MedicaEnfermedad m where m.enfermedad.id = :enfermedad");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaEnfermedad> getListaMedicaEnfermedadByObservacionMedica(int observacionMedica) {
		return (List<MedicaEnfermedad>) super.getListaRegistro(observacionMedica, "observacionMedica",
				"SELECT m FROM MedicaEnfermedad m where m.observacionMedica.id = :observacionMedica");
	}

}
