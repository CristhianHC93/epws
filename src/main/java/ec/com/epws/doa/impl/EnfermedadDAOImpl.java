package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.EnfermedadDAO;
import ec.com.epws.entity.Enfermedad;

public class EnfermedadDAOImpl extends DAOImplements implements EnfermedadDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Enfermedad> getListaEnfermedad() {
		return (List<Enfermedad>) super.getListaRegistro("SELECT v FROM Enfermedad v");
	}

	@Override
	public Enfermedad saveorUpdate(Enfermedad enfermedad) {
		return (Enfermedad) super.saveorUpdate(enfermedad);
	}

	@Override
	public int eliminar(Enfermedad enfermedad) {
		return super.eliminar(enfermedad.getId(), "enfermedad", "DELETE FROM Enfermedad where id = :enfermedad");
	}

}
