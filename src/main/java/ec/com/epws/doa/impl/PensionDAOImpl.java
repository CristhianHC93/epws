package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.PensionDAO;
import ec.com.epws.entity.Pension;

public class PensionDAOImpl extends DAOImplements implements PensionDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPension() {
		return (List<Pension>) super.getListaRegistro("SELECT p FROM Pension p");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByMatricula(int matricula) {
		return (List<Pension>) super.getListaRegistro(matricula, "matricula",
				"SELECT p FROM Pension p where p.matricula.id = :matricula order by p.id");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByCurso(int curso, int anioLectivo) {
		return (List<Pension>) super.getListaRegistro(curso, "curso", anioLectivo, "anioLectivo",
				"SELECT p FROM Pension p where p.matricula.curso.id = :curso and p.matricula.anioLectivo.id = :anioLectivo order by p.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByAlumno(int alumno, int anioLectivo) {
		return (List<Pension>) super.getListaRegistro(alumno, "alumno", anioLectivo, "anioLectivo",
				"SELECT p FROM Pension p where p.matricula.alumno.id = :alumno and p.matricula.anioLectivo.id = :anioLectivo order by p.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByMes(int mes, int anioLectivo) {
		return (List<Pension>) super.getListaRegistro(mes, "mes", anioLectivo, "anioLectivo",
				"SELECT p FROM Pension p where p.mes.id = :mes and p.matricula.anioLectivo.id = :anioLectivo order by p.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByEstado(int estado, int anioLectivo) {
		return (List<Pension>) super.getListaRegistro(estado, "estado", anioLectivo, "anioLectivo",
				"SELECT p FROM Pension p where p.estado.id = :estado and p.matricula.anioLectivo.id = :anioLectivo order by p.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByMesMatricula(int mes, int matricula, int anioLectivo) {
		return (List<Pension>) super.getListaRegistro(mes, "mes", matricula, "matricula", anioLectivo, "anioLectivo",
				"SELECT p FROM Pension p where p.mes.id = :mes and p.matricula.id = :matricula and p.matricula.anioLectivo.id = :anioLectivo order by p.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByMesCurso(int mes, int curso, int anioLectivo) {
		return (List<Pension>) super.getListaRegistro(mes, "mes", curso, "curso", anioLectivo, "anioLectivo",
				"SELECT p FROM Pension p where p.mes.id = :mes and p.matricula.curso.id = :curso and p.matricula.anioLectivo.id = :anioLectivo and p.matricula.estado.id = 3 order by p.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByEstadoAlumno(int estado, int alumno, int anioLectivo) {
		return (List<Pension>) super.getListaRegistro(estado, "estado", alumno, "alumno", anioLectivo, "anioLectivo",
				"SELECT p FROM Pension p where p.estado.id = :estado and p.matricula.alumno.id = :alumno and p.matricula.anioLectivo.id = :anioLectivo order by p.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Pension> getListaPensionByEstadoCurso(int estado, int curso, int anioLectivo) {
		return (List<Pension>) super.getListaRegistro(estado, "estado", curso, "curso", anioLectivo, "anioLectivo",
				"SELECT p FROM Pension p where p.estado.id = :estado and p.matricula.curso.id = :curso and p.matricula.anioLectivo.id = :anioLectivo order by p.matricula.alumno.apellido");
	}

	@Override
	public Pension saveorUpdate(Pension pension) {
		return (Pension) super.saveorUpdate(pension);
	}

}
