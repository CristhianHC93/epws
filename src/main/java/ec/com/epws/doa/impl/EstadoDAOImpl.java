package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.EstadoDAO;
import ec.com.epws.entity.Estado;

public class EstadoDAOImpl extends DAOImplements implements EstadoDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Estado> getListaEstadoByModulo(int modulo) {
		return (List<Estado>) super.getListaRegistro(modulo, "modulo",
				"SELECT e FROM Estado e where e.codigo = :modulo");
	}

}
