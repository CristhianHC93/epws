package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.CalificacionParcialCualitativaDAO;
import ec.com.epws.entity.CalificacionParcialCualitativa;

public class CalificacionParcialCualitativaDAOImpl extends DAOImplements implements CalificacionParcialCualitativaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionParcialCualitativa> getListaCalificacionParcialCualitativaByAsignaturaAndParcial(
			int asignatura, int parcial) {
		return (List<CalificacionParcialCualitativa>) super.getListaRegistro(asignatura, "asignatura", parcial,
				"parcial",
				"SELECT c FROM CalificacionParcialCualitativa c where c.calificacionQuimestreCualitativa.asignatura.id = :asignatura and c.parcial.id = :parcial ORDER BY c.calificacionQuimestreCualitativa.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionParcialCualitativa> getListaCalificacionParcialCualitativaByCalificacionQuimestre(
			int calificacionQuimestreCualitativa) {
		return (List<CalificacionParcialCualitativa>) super.getListaRegistro(calificacionQuimestreCualitativa,
				"calificacionQuimestreCualitativa",
				"SELECT c FROM CalificacionParcialCualitativa c where c.calificacionQuimestreCualitativa.id = :calificacionQuimestreCualitativa ORDER BY c.calificacionQuimestreCualitativa.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionParcialCualitativa> getListaCalificacionParcialCualitativaByMatriculaAndParcial(
			int matricula, int parcial) {
		return (List<CalificacionParcialCualitativa>) super.getListaRegistro(matricula, "matricula", parcial, "parcial",
				"SELECT c FROM CalificacionParcialCualitativa c where c.calificacionQuimestreCualitativa.matricula.id = :matricula and c.parcial.id = :parcial ORDER BY c.calificacionQuimestreCualitativa.matricula.alumno.apellido");
	}

	@Override
	public CalificacionParcialCualitativa getCalificacionParcialCualitatival(int calificacionParcialCualitativa,int asignatura) {
		try {
			return (CalificacionParcialCualitativa) super.getRegistro(calificacionParcialCualitativa,
					"calificacionParcialCualitativa",asignatura,"asignatura",
					"SELECT c FROM CalificacionParcialCualitativa c where c.id = :calificacionParcialCualitativa and c.calificacionQuimestreCualitativa.asignatura.id = :asignatura");	
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public CalificacionParcialCualitativa saveorUpdate(CalificacionParcialCualitativa calificacionParcialCualitativa) {
		return (CalificacionParcialCualitativa) super.saveorUpdate(calificacionParcialCualitativa);
	}

}
