package ec.com.epws.doa.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import ec.com.epws.jpa.JPAPersistenceManager;

public class DAOImplements {
	private EntityManagerFactory emf = null;

	public DAOImplements() {
		this.emf = JPAPersistenceManager.getInstance().getEntityManagerFactory();
	}

	protected EntityManager getEntityManager() {
		return emf.createEntityManager();
	}

	public Object saveorUpdate(Object Object) {
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		Object ObjectDevuelto = null;
		try {
			t.begin();
			ObjectDevuelto = em.merge(Object);
			t.commit();
		} catch (RuntimeException e) {
			if (t.isActive()) {
				System.out.println(e.getMessage());
				t.rollback();
			}
		} finally {
			em.close();
		}
		return ObjectDevuelto;
	}

	public int eliminar(int parametro, String var, String sql) {
		EntityManager em = getEntityManager();
		EntityTransaction t = em.getTransaction();
		int enteroDevuelto = 0;
		try {
			t.begin();
			Query query = em.createQuery(sql);
			query.setParameter(var, parametro);
			query.executeUpdate();
			t.commit();
			enteroDevuelto = 1;
		} catch (RuntimeException e) {
			if (t.isActive()) {
				t.rollback();
				System.out.println(e.getMessage() + "+++" + e.getStackTrace());
				enteroDevuelto = 2;
			}
		} finally {
			em.close();
		}
		return enteroDevuelto;
	}

	public long getCount(String sql) {
		long a;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		a = (long)query.getSingleResult();
		em.close();
		return a;
	}
	
	public long getCount(int parametro, String var,String sql) {
		long a;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		a = (long)query.getSingleResult();
		em.close();
		return a;
	}
	
	public List<?> getListaRegistro(int parametro, String var, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(int parametro, String var, Date fechaIni, String fechaInicial, Date fechaFin,
			String fechaFinal, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(fechaInicial, fechaIni);
		query.setParameter(fechaFinal, fechaFin);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public Object getRegistro(int parametro, String var, String sql) {
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		Object object = query.getSingleResult();
		em.close();
		return object;
	}

	public Object getRegistro(int parametro, String var, int parametro2, String var2, String sql) {
		try {
			EntityManager em = getEntityManager();
			Query query = em.createQuery(sql);
			query.setParameter(var, parametro);
			query.setParameter(var2, parametro2);
			Object object = query.getSingleResult();
			em.close();
			return object;
		} catch (Exception e) {
			return null;
		}

	}

	public Object getRegistro(int parametro, String var, int parametro2, String var2, int parametro3, String var3,
			String sql) {
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		query.setParameter(var3, parametro3);
		Object object = query.getSingleResult();
		em.close();
		return object;
	}

	public Object getRegistro(String sql) {
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		Object object = query.getSingleResult();
		em.close();
		return object;
	}

	public Object getRegistro(String parametro, String var, String sql) {
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		Object object = query.getSingleResult();
		em.close();
		return object;
	}

	public Object getRegistro(String parametro, String var, int parametro2, String var2, String sql) {
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		Object object = query.getSingleResult();
		em.close();
		return object;
	}

	public Object getRegistro(String parametro, String var, String parametro2, String var2, String sql) {
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		Object object = query.getSingleResult();
		em.close();
		return object;
	}

	public List<?> getListaRegistro(int parametro, String var, int parametro2, String var2, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(int parametro, String var, int parametro2, String var2, Date fecha, String fecha1,
			String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		query.setParameter(fecha1, fecha);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(int parametro, String var, int parametro2, String var2, int parametro3, String var3,
			String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, parametro2);
		query.setParameter(var3, parametro3);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(boolean parametro, String var, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		lista = query.getResultList();
		em.close();
		return lista;
	}

	public List<?> getListaRegistro(String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		lista = query.getResultList();
		em.close();
		return lista;
	}
	
	public List<?> getListaRegistroFiltro(String parametro, String var, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, '%'+parametro+'%');
		lista = query.getResultList();
		em.close();
		return lista;
	}
	
	public List<?> getListaRegistroFiltro(int parametro,String var,String dato, String var2, String sql) {
		List<?> lista;
		EntityManager em = getEntityManager();
		Query query = em.createQuery(sql);
		query.setParameter(var, parametro);
		query.setParameter(var2, '%'+dato+'%');
		lista = query.getResultList();
		em.close();
		return lista;
	}

}
