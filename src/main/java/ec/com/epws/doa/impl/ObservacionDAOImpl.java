package ec.com.epws.doa.impl;

import ec.com.epws.dao.ie.ObservacionDAO;
import ec.com.epws.entity.Observacion;

public class ObservacionDAOImpl extends DAOImplements implements ObservacionDAO {
	
	@Override
	public Observacion saveorUpdate(Observacion observacion) {
		return (Observacion) super.saveorUpdate(observacion);
	}

	@Override
	public int eliminar(Observacion observacion) {
		return super.eliminar(observacion.getId(), "observacion", "DELETE FROM Observacion where id = :observacion");
	}

}
