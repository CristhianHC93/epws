package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.QuimestreDAO;
import ec.com.epws.entity.Quimestre;

public class QuimestreDAOImpl extends DAOImplements implements QuimestreDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Quimestre> getListaQuimestre() {
		return (List<Quimestre>) super.getListaRegistro("SELECT q FROM Quimestre q");
	}

	@Override
	public Quimestre saveorUpdate(Quimestre quimestre) {
		return (Quimestre) super.saveorUpdate(quimestre);
	}

	@Override
	public int eliminar(int quimestre) {
		return super.eliminar(quimestre, "quimestre", "DELETE FROM Quimestre where id = :quimestre");
	}

}
