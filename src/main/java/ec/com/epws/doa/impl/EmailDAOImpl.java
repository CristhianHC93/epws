package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.EmailDAO;
import ec.com.epws.entity.Email;

public class EmailDAOImpl extends DAOImplements implements EmailDAO {

	List<Email> listaEmail;

	@Override
	public Email saveorUpdate(Email email) {
		return (Email) super.saveorUpdate(email);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Email> getListaEmail() {
		if (listaEmail == null) {
			listaEmail = (List<Email>) super.getListaRegistro("SELECT e FROM Email e");
		}
		return listaEmail;
	}

	@Override
	public Email getEmail(int id) {
		// TODO Auto-generated method stub
		return (Email) super.getRegistro(id, "id", "SELECT e FROM Email e where e.id = :id");
	}

}
