package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.FamiliaDAO;
import ec.com.epws.entity.Familia;

public class FamiliaDAOImpl extends DAOImplements implements FamiliaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Familia> getListaFamilia() {
		return (List<Familia>) super.getListaRegistro("SELECT f FROM Familia f");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Familia> getListaFamilia(int parentesco) {
		return (List<Familia>) super.getListaRegistro(parentesco, "parentesco",
				"SELECT f FROM Familia f where f.parentesco.id = :parentesco");
	}

	@Override
	public Familia saveorUpdate(Familia familia) {
		return (Familia) super.saveorUpdate(familia);
	}

	@Override
	public int eliminar(Familia familia) {
		return super.eliminar(familia.getId(), "id", "DELETE FROM Familia where id = :id");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Familia> getListaFamiliaFiltro(String dato) {
		return (List<Familia>) super.getListaRegistroFiltro(dato, "dato",
				"SELECT f FROM Familia f where f.cedula like :dato or f.nombre like :dato or f.apellido like :dato");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Familia> getListaFamiliaParentescoFiltro(int parentesco,String dato) {
		return (List<Familia>) super.getListaRegistroFiltro(parentesco,"parentesco",dato, "dato",
				"SELECT f FROM Familia f where f.parentesco.id = :parentesco and f.cedula like :dato or f.nombre like :dato or f.apellido like :dato");
	}

}
