package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.MedicaVacunaDAO;
import ec.com.epws.entity.MedicaVacuna;

public class MedicaVacunaDAOImpl extends DAOImplements implements MedicaVacunaDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaVacuna> getListaMedicaVacuna() {
		return (List<MedicaVacuna>) super.getListaRegistro("SELECT m FROM MedicaVacuna m");
	}

	@Override
	public MedicaVacuna saveorUpdate(MedicaVacuna medicaVacuna) {
		return (MedicaVacuna) super.saveorUpdate(medicaVacuna);
	}

	@Override
	public int eliminar(MedicaVacuna medicaVacuna) {
		return super.eliminar(medicaVacuna.getId(), "medicaVacuna", "DELETE FROM MedicaVacuna where id = :medicaVacuna");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaVacuna> getListaMedicaVacunaByVacuna(int vacuna) {
		return (List<MedicaVacuna>) super.getListaRegistro(vacuna,"vacuna", "SELECT m FROM MedicaVacuna m where m.vacuna.id = :vacuna");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaVacuna> getListaMedicaVacunaByObservacionMedica(int observacionMedica) {
		return (List<MedicaVacuna>) super.getListaRegistro(observacionMedica, "observacionMedica",
				"SELECT m FROM MedicaVacuna m where m.observacionMedica.id = :observacionMedica");
	}

}
