package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.VacunaDAO;
import ec.com.epws.entity.Vacuna;

public class VacunaDAOImpl extends DAOImplements implements VacunaDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Vacuna> getListaVacuna() {
		return (List<Vacuna>) super.getListaRegistro("SELECT v FROM Vacuna v");
	}

	@Override
	public Vacuna saveorUpdate(Vacuna vacuna) {
		return (Vacuna) super.saveorUpdate(vacuna);
	}

	@Override
	public int eliminar(Vacuna vacuna) {
		return super.eliminar(vacuna.getId(), "vacuna", "DELETE FROM Vacuna where id = :vacuna");
	}

}
