package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.ParcialDAO;
import ec.com.epws.entity.Parcial;

public class ParcialDAOImpl extends DAOImplements implements ParcialDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Parcial> getListaParcial() {
		return (List<Parcial>) super.getListaRegistro("SELECT p FROM Parcial p");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Parcial> getListaParcial(int quimestre) {
		return (List<Parcial>) super.getListaRegistro(quimestre, "quimestre",
				"SELECT p FROM Parcial p where p.quimestre.id = :quimestre");
	}

	@Override
	public Parcial saveorUpdate(Parcial parcial) {
		return (Parcial) super.saveorUpdate(parcial);
	}

	@Override
	public int eliminar(int parcial) {
		return super.eliminar(parcial, "parcial", "DELETE FROM Parcial where id = :parcial");
	}

}
