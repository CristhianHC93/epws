package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.InformacionNegocioDAO;
import ec.com.epws.entity.InformacionNegocio;

public class InformacionNegocioDAOImpl extends DAOImplements implements InformacionNegocioDAO{
	List<InformacionNegocio> listaInformacionNegocio;

	@Override
	public InformacionNegocio saveorUpdate(InformacionNegocio informacionNegocio) {
		return (InformacionNegocio) super.saveorUpdate(informacionNegocio);
	}

	@Override
	public InformacionNegocio getInformacionNegocio(int id) {
		return (InformacionNegocio) super.getRegistro(id, "id", "SELECT e FROM InformacionNegocio e where e.id = :id");
	}
}
