package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.CalificacionQuimestreDAO;
import ec.com.epws.entity.CalificacionQuimestre;

public class CalificacionQuimestreDAOImpl extends DAOImplements implements CalificacionQuimestreDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestre> getListaCalificacionQuimestreByAsignaturaAndQuimestre(int asignatura,
			int quimestre) {
		return (List<CalificacionQuimestre>) super.getListaRegistro(asignatura, "asignatura", quimestre, "quimestre",
				"SELECT c FROM CalificacionQuimestre c where c.asignatura.id = :asignatura and c.quimestre.id = :quimestre ORDER BY c.matricula.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestre> getListaCalificacionQuimestreByMatriculaByAsignatura(int matricula,int asignatura) {
		return (List<CalificacionQuimestre>) super.getListaRegistro(matricula, "matricula", asignatura, "asignatura",
				"SELECT c FROM CalificacionQuimestre c where c.matricula.id = :matricula and c.asignatura.id = :asignatura ORDER BY c.matricula.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestre> getListaCalificacionQuimestreByMatriculaQuimestre(int matricula,
			int quimestre) {
		return (List<CalificacionQuimestre>) super.getListaRegistro(matricula, "matricula", quimestre, "quimestre",
				"SELECT c FROM CalificacionQuimestre c where c.matricula.id = :matricula and c.quimestre.id = :quimestre ORDER BY c.matricula.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestre> getListaCalificacionQuimestreByAsignatura(int asignatura) {
		return (List<CalificacionQuimestre>) super.getListaRegistro(asignatura, "asignatura",
				"SELECT c FROM CalificacionQuimestre c where c.asignatura.id = :asignatura ORDER BY c.matricula.alumno.apellido");
	}

	@Override
	public CalificacionQuimestre getCalificacionQuimestre(int calificacionQuimestre,int asignatura){
		try {
			return (CalificacionQuimestre) super.getRegistro(calificacionQuimestre, "calificacionQuimestre", asignatura, "asignatura", 
					"SELECT c FROM CalificacionQuimestre c where c.id = :calificacionQuimestre and c.asignatura.id = :asignatura");
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public CalificacionQuimestre saveorUpdate(CalificacionQuimestre calificacionQuimestre) {
		return (CalificacionQuimestre) super.saveorUpdate(calificacionQuimestre);
	}

}
