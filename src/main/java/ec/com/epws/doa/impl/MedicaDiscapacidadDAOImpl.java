package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.MedicaDiscapacidadDAO;
import ec.com.epws.entity.MedicaDiscapacidad;

public class MedicaDiscapacidadDAOImpl  extends DAOImplements implements MedicaDiscapacidadDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaDiscapacidad> getListaMedicaDiscapacidad() {
		return (List<MedicaDiscapacidad>) super.getListaRegistro("SELECT m FROM MedicaDiscapacidad m");
	}

	@Override
	public MedicaDiscapacidad saveorUpdate(MedicaDiscapacidad medicaDiscapacidad) {
		return (MedicaDiscapacidad) super.saveorUpdate(medicaDiscapacidad);
	}

	@Override
	public int eliminar(MedicaDiscapacidad medicaDiscapacidad) {
		return super.eliminar(medicaDiscapacidad.getId(), "medicaDiscapacidad", "DELETE FROM MedicaDiscapacidad where id = :medicaDiscapacidad");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaDiscapacidad> getListaMedicaDiscapacidadByDiscapacidad(int discapacidad) {
		return (List<MedicaDiscapacidad>) super.getListaRegistro(discapacidad,"discapacidad", "SELECT m FROM MedicaDiscapacidad m where m.discapacidad.id = :discapacidad");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MedicaDiscapacidad> getListaMedicaDiscapacidadByObservacionMedica(int observacionMedica) {
		return (List<MedicaDiscapacidad>) super.getListaRegistro(observacionMedica, "observacionMedica",
				"SELECT m FROM MedicaDiscapacidad m where m.observacionMedica.id = :observacionMedica");
	}

}
