package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.ParentescoDAO;
import ec.com.epws.entity.Parentesco;

public class ParentescoDAOImpl extends DAOImplements implements ParentescoDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<Parentesco> getListaParentesco() {
		return (List<Parentesco>) super.getListaRegistro("SELECT p FROM Parentesco p");
	}
	
	@Override
	public Parentesco getParentesco(int id) {
		return (Parentesco) super.getRegistro(id,"id","SELECT p FROM Parentesco p where p.id = :id");
	}

	@Override
	public Parentesco saveorUpdate(Parentesco parentesco) {
		return (Parentesco) super.saveorUpdate(parentesco);
	}

	@Override
	public int eliminar(Parentesco parentesco) {
		return super.eliminar(parentesco.getId(), "id", "DELETE FROM Parentesco as p where p.id = :id");
	}

}
