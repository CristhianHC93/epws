package ec.com.epws.doa.impl;

import java.util.List;

import ec.com.epws.dao.ie.CalificacionQuimestreCualitativaDAO;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;

public class CalificacionQuimestreCualitativaDAOImpl extends DAOImplements
		implements CalificacionQuimestreCualitativaDAO {

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestreCualitativa> getListaCalificacionQuimestreCualitativaByAsignaturaAndQuimestre(
			int asignatura, int quimestre) {
		return (List<CalificacionQuimestreCualitativa>) super.getListaRegistro(asignatura, "asignatura", quimestre,
				"quimestre",
				"SELECT c FROM CalificacionQuimestreCualitativa c where c.asignatura.id = :asignatura and c.quimestre.id = :quimestre ORDER BY c.matricula.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestreCualitativa> getListaCalificacionQuimestreCualitativaByMatricula(int matricula,int asignatura) {
		return (List<CalificacionQuimestreCualitativa>) super.getListaRegistro(matricula, "matricula", asignatura, "asignatura", 
				"SELECT c FROM CalificacionQuimestreCualitativa c where c.matricula.id = :matricula and c.asignatura.id = :asignatura ORDER BY c.matricula.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestreCualitativa> getListaCalificacionQuimestreCualitativaByMatriculaQuimestre(
			int matricula, int quimestre) {
		return (List<CalificacionQuimestreCualitativa>) super.getListaRegistro(matricula, "matricula", quimestre,
				"quimestre",
				"SELECT c FROM CalificacionQuimestreCualitativa c where c.matricula.id = :matricula and c.quimestre.id = :quimestre and c.asignatura.codigoCualitativa.id != 4 ORDER BY c.matricula.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestreCualitativa> getListaCalificacionQuimestreCualitativaByMatriculaQuimestreAll(
			int matricula, int quimestre) {
		return (List<CalificacionQuimestreCualitativa>) super.getListaRegistro(matricula, "matricula", quimestre,
				"quimestre",
				"SELECT c FROM CalificacionQuimestreCualitativa c where c.matricula.id = :matricula and c.quimestre.id = :quimestre ORDER BY c.matricula.alumno.apellido");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestreCualitativa> getListaCalificacionQuimestreCualitativaByMatriculaQuimestreComportamiento(
			int matricula, int quimestre) {
		return (List<CalificacionQuimestreCualitativa>) super.getListaRegistro(matricula, "matricula", quimestre,
				"quimestre",
				"SELECT c FROM CalificacionQuimestreCualitativa c where c.matricula.id = :matricula and c.quimestre.id = :quimestre and c.asignatura.codigoCualitativa.id == 4 ORDER BY c.matricula.alumno.apellido");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CalificacionQuimestreCualitativa> getListaCalificacionQuimestreCualitativaByAsignatura(int asignatura) {
		return (List<CalificacionQuimestreCualitativa>) super.getListaRegistro(asignatura, "asignatura",
				"SELECT c FROM CalificacionQuimestreCualitativa c where c.asignatura.id = :asignatura ORDER BY c.matricula.alumno.apellido");
	}
	
	@Override
	public CalificacionQuimestreCualitativa getCalificacionQuimestreCualitativa(int calificacionQuimestreCualitativa,int asignatura){
		try {
			return (CalificacionQuimestreCualitativa) super.getRegistro(calificacionQuimestreCualitativa, "calificacionQuimestreCualitativa", asignatura, "asignatura", 
					"SELECT c FROM CalificacionQuimestreCualitativa c where c.id = :calificacionQuimestreCualitativa and c.asignatura.id = :asignatura");
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public CalificacionQuimestreCualitativa getCalificacionQuimestreCualitativaComportamiento(int matricula,int quimestre){
		try {
			return (CalificacionQuimestreCualitativa) super.getRegistro(matricula, "matricula", quimestre, "quimestre", 
					"SELECT c FROM CalificacionQuimestreCualitativa c where c.matricula.id = :matricula and c.quimestre.id = :quimestre and c.asignatura.codigoCualitativa.id = 4");
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public CalificacionQuimestreCualitativa saveorUpdate(
			CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		return (CalificacionQuimestreCualitativa) super.saveorUpdate(calificacionQuimestreCualitativa);
	}

}
