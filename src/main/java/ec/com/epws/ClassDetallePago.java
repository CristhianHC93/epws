package ec.com.epws;

import java.util.HashMap;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Pension;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassDetallePago {
	@Wire("#winDetallePagoCE")
	private Window winDetallePagoCE;

	private Pension pension;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("pension") Pension pension,
			@ExecutionArgParam("pension1") Pension pension1) {
		Selectors.wireComponents(view, this, false);
		if (pension1 == null) {
			this.pension = pension;
			if (pension.getEstado().getId() == Sistema.ESTADO_PENSION_PAGADA) {
				this.pension.setDetalle("PAGO DE " + pension.getMes().getDescripcion() + " REALIZADO POR ");
			}
			if (pension.getEstado().getId() == Sistema.ESTADO_PENSION_DEUDA) {
				this.pension.setDetalle("ABONO DE " + pension.getMes().getDescripcion() + " REALIZADO POR ");
			}
		} else {
			this.pension = pension1;
		}

	}

	@Command
	@NotifyChange("pension")
	public void guardarEditar() {
		pension = InstanciaDAO.pensionDAO.saveorUpdate(pension);
	}

	@Command
	@NotifyChange("pension")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("detalle")) {
			pension.setDetalle(cadena.toUpperCase());
		}
	}

	@Command
	public void generarPdf() {
		guardarEditar();
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("pension1", pension);
		Executions.createComponents("/reporte/impresionFichaDetallePago.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("ficha", "DETALLE DE PAGO");
		mapreporte.put("nombre", pension.getMatricula().getAlumno().getApellido() + " "
				+ pension.getMatricula().getAlumno().getNombre());
		mapreporte.put("monto", String.valueOf(pension.getValorPagado()));
		mapreporte.put("detalle", pension.getDetalle());
		mapreporte.put("me", Util.getPathRaiz() + Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz() + Sistema.LOGO);
		mapreporte.put("fecha", "Manta, " + Sistema.getFechaActual());
		iframe.setContent(Sistema.reportePDF(Sistema.FICHA_DETALLE_PAGO, mapreporte, null));
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winDetallePagoCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Pension getPension() {
		return pension;
	}

	public void setPension(Pension pension) {
		this.pension = pension;
	}

}
