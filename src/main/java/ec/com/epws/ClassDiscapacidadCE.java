package ec.com.epws;

import java.io.File;
import java.util.concurrent.ThreadLocalRandom;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Discapacidad;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassDiscapacidadCE {
	@Wire("#winDiscapacidadCE")
	private Window winDiscapacidadCE;

	private Discapacidad discapacidad;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Discapacidad discapacidad) {
		Selectors.wireComponents(view, this, false);
		this.discapacidad = (discapacidad == null) ? new Discapacidad() : discapacidad;
	}

	@Command
	@NotifyChange("discapacidad")
	public void guardarEditar() {
		bandera = true;
		if (discapacidad.getId() == null) {
			InstanciaDAO.discapacidadDAO.saveorUpdate(discapacidad);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			discapacidad = new Discapacidad();
		} else {
			InstanciaDAO.discapacidadDAO.saveorUpdate(discapacidad);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("discapacidad")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			discapacidad.setDescripcion((cadena.toUpperCase()));
		}
	}


	@NotifyChange("discapacidad")
	@Command
	public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {
		
		Media media;
		media = event.getMedia();
		int aleatorio = ThreadLocalRandom.current().nextInt(1, 100000);
		if (Util.uploadFile(media, String.valueOf(aleatorio), Sistema.carpetaCertificadoMedico)) {
			new File(Util.getPathRaiz() + discapacidad.getCertificado()).delete();
			discapacidad.setCertificado("/" + Sistema.carpetaCertificadoMedico + "/" +  String.valueOf(aleatorio));
		}
	}
	
	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winDiscapacidadCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Discapacidad getDiscapacidad() {
		return discapacidad;
	}

	public void setDiscapacidad(Discapacidad discapacidad) {
		this.discapacidad = discapacidad;
	}

}
