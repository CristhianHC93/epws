package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Familia;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassFamilia {
	@Wire("#winFamilia")
	private Window winFamilia;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private Integer parentesco = 0;
	private List<Familia> listaFamilia;
	// private FamiliaDAO familiaDAO = InstanciaDAO.familiaDAO;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase,
			@ExecutionArgParam("parentesco") Integer parentesco) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "90%";
		}
		this.parentesco = parentesco;
		listaFamilia = getListaFamilia(this.parentesco);
	}

	private List<Familia> getListaFamilia(Integer parentesco) {
		return (parentesco == null || parentesco == Sistema.PARENTESCO_REPRESENTANTE)
				? InstanciaDAO.getFamiliaDAO().getListaFamilia()
				: InstanciaDAO.getFamiliaDAO().getListaFamilia(parentesco);
	}

	@Command
	public void nuevo() {
		Map<String, Object> parameters = new HashMap<String, Object>();
		if (parentesco != null) {
			parameters.put("parentesco", parentesco);
		}
		Executions.createComponents("/vista/formFamiliaCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaFamilia")
	public void deleteFamilia(@BindingParam("familia") final Familia familia) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + familia.getNombre()+ " "+familia.getApellido()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.getFamiliaDAO().eliminar(familia);
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaFamilia.clear();
							listaFamilia = getListaFamilia(this.parentesco);
							BindUtils.postNotifyChange(null, null, ClassFamilia.this, "listaFamilia");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassFamilia.this, "listaFamilia");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("familia") Familia familia) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", familia);
		parametros.put("parentesco", parentesco);
		BindUtils.postGlobalCommand(null, null, "cargarFamilia", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaFamilia")
	public void refrescarlista() {
		listaFamilia = getListaFamilia(this.parentesco);
	}

	@Command
	public void editar(@BindingParam("familia") Familia familia) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", familia);
		if (parentesco != null) {
			parameters.put("parentesco", parentesco);
		}
		Executions.createComponents("/vista/formFamiliaCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaFamilia")
	public void filtrar() {
		listaFamilia.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaFamilia.addAll(getListaFamilia(this.parentesco));
		} else {
			listaFamilia = (parentesco == null || parentesco == Sistema.PARENTESCO_REPRESENTANTE)
			? InstanciaDAO.getFamiliaDAO().getListaFamiliaFiltro(datoBusqueda.toUpperCase())
			: InstanciaDAO.getFamiliaDAO().getListaFamiliaParentescoFiltro(this.parentesco, datoBusqueda.toUpperCase());
		}
	}

	@Command
	public void salir() {
		winFamilia.detach();
	}
	/*
	 * Metodos Get AND Set
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Familia> getListaFamilia() {
		return listaFamilia;
	}

	public void setListaFamilia(List<Familia> listaFamilia) {
		this.listaFamilia = listaFamilia;
	}

}
