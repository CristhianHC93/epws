package ec.com.epws;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.AsignacionDocente;
import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.CalificacionQuimestre;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;
import ec.com.epws.entity.CuadroCalificacion;
import ec.com.epws.entity.Cualitativa;
import ec.com.epws.entity.Docente;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Observacion;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassCalificacionQuimestre {

	private List<CalificacionQuimestre> listaCalificacionQuimestre = new ArrayList<>();
	private List<CalificacionQuimestreCualitativa> listaCalificacionQuimestreCualitativa = new ArrayList<>();

	private List<Cualitativa> listaCualitativaInicial = Sistema.getListaCualitativaInicial();
	private List<Cualitativa> listaCualitativaMateria = Sistema.getListaCualitativaMateria();

	private List<Quimestre> listaQuimestre;
	private Quimestre quimestre;
	private List<AsignacionDocente> listaAsignacionDocente;
	private AsignacionDocente asignacionDocente;

	/*
	 * Variables para vistas de calificaciones Cualitativas o Cuantitativas
	 */

	private boolean inicial;
	private boolean basico;
	private boolean materia;

	double promedio = 0.0;
	double promedioCualitativa = 0.0;
	private List<Matricula> listaMatricula = new ArrayList<>();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista) {
		Selectors.wireComponents(vista, this, false);
		if (Sistema.getUsuario().getRol().getId() != 2) {
			Messagebox.show("Usuario no es Docente");
			return;
		}
		Docente docente = InstanciaDAO.docenteDAO.getDocente(Sistema.getUsuario().getId());
		listaAsignacionDocente = InstanciaDAO.asignacionDocenteDAO
				.getListaAsignacionDocenteByDocente(Sistema.getAnioLectivoActivoEstatico().getId(), docente.getId());
		if (listaAsignacionDocente == null || listaAsignacionDocente.size() <= 0) {
			Messagebox.show("Docente no tiene asignaturas asignadas");
			return;
		}
		asignacionDocente = listaAsignacionDocente.get(0);
		listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
		quimestre = listaQuimestre.get(0);
		buscar();
	}

	private void metodoCalificacion(Asignatura asignatura) {
		if (asignatura.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_INICIAL) {
			inicial = true;
			basico = false;
			materia = false;
		}
		if (asignatura.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_BASICO) {
			inicial = false;
			basico = true;
			materia = false;
		}
		if (asignatura.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_MATERIA) {
			inicial = false;
			basico = false;
			materia = true;
		}
	}

	private void llenarListaAlumno() {

	}

	private void llenarListaCualitativa() {
		listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(
				Sistema.getAnioLectivoActivoEstatico().getId(), asignacionDocente.getAsignatura().getCurso().getId());
		listaMatricula.forEach(x -> {
			CalificacionQuimestreCualitativa cqc = new CalificacionQuimestreCualitativa();
			cqc.setMatricula(x);
			listaCalificacionQuimestreCualitativa.add(cqc);
		});
	}

	boolean bandera = false;

	@NotifyChange({ "listaCalificacionQuimestreCualitativa", "listaCalificacionQuimestre", "quimestre", "inicial",
			"basico", "materia", "listaCualitativa" })
	@Command
	public void guardar() {
		if (inicial) {
			listaCalificacionQuimestreCualitativa.forEach(x -> {
				if (x.getCualitativa1() == null) {
					bandera = true;
				}
			});
			if (bandera) {
				bandera = false;
				Messagebox.show("Existen Alumnos sin Calificar");
				return;
			}
			listaCalificacionQuimestreCualitativa.forEach(x -> {
				if (x.getId() != null && x.getObservacion() != null && x.getObservacion().getId() != null) {
					InstanciaDAO.observacionDAO.saveorUpdate(x.getObservacion());
				}				
				//Inicia proceso para guardar Cuadro Calificacion
				CuadroCalificacion cc = InstanciaDAO.cuadroCalificacionDAO
						.getCuadroCalificacion(x.getAsignatura().getId(), x.getMatricula().getId());
				if (cc == null) {
					cc = new CuadroCalificacion();
					cc.setAsignatura(x.getAsignatura());
					cc.setMatricula(x.getMatricula());
				}
				if (x.getQuimestre().getId() == 1) {
					cc.setQuimestre1(x.getCualitativa1().getId());
				}
				if (x.getQuimestre().getId() == 2) {
					cc.setQuimestre2(x.getCualitativa1().getId());
				}
				double id = Sistema.redondearEntero((cc.getQuimestre1()+cc.getQuimestre2())/2);
				if (id <= 5) {
					id = 5;
				}
				cc.setPromedio(id);
				cc.setPromedioFinal(cc.getPromedio());
				cc.setLetra(Sistema.numeroAletras(cc.getPromedio()));
				cc.setNota(InstanciaDAO.cualitativaDAO.getCualitativa((int) id).getSiglas());
				InstanciaDAO.cuadroCalificacionDAO.saveorUpdate(cc);
				//Fin proceso
				x.setAsignatura(asignacionDocente.getAsignatura());
				x.setCualitativa2(x.getCualitativa1());
				x.setQuimestre(quimestre);
				InstanciaDAO.calificacionQuimestreCualitativaDAO.saveorUpdate(x);
			});
			Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
			buscar();
		}
		if (basico) {
			listaCalificacionQuimestre.forEach(x -> {
				if (x.getExamen() == 0) {
					bandera = true;
				}
			});
			if (bandera) {
				bandera = false;
				Messagebox.show("Existen Alumnos sin Calificar");
				return;
			}
			calcularPromedioTodos();
			listaCalificacionQuimestre.forEach(x -> {
				if (x.getId() != null && x.getObservacion() != null && x.getObservacion().getId() != null) {
					InstanciaDAO.observacionDAO.saveorUpdate(x.getObservacion());
				}
				//Inicia proceso para guardar Cuadro Calificacion
				CuadroCalificacion cc = InstanciaDAO.cuadroCalificacionDAO
						.getCuadroCalificacion(x.getAsignatura().getId(), x.getMatricula().getId());
				if (cc == null) {
					cc = new CuadroCalificacion();
					cc.setAsignatura(x.getAsignatura());
					cc.setMatricula(x.getMatricula());
				}
				if (x.getQuimestre().getId() == 1) {
					cc.setQuimestre1(x.getNota());
				}
				if (x.getQuimestre().getId() == 2) {
					cc.setQuimestre2(x.getNota());
				}
				cc.setPromedio((cc.getQuimestre1()+cc.getQuimestre2())/2);
				cc.setPromedioFinal(cc.getPromedio());
				cc.setLetra(Sistema.numeroAletras(cc.getPromedio()));
				cc.setNota(String.valueOf(cc.getPromedioFinal()));
				InstanciaDAO.cuadroCalificacionDAO.saveorUpdate(cc);
				//Fin proceso
				InstanciaDAO.calificacionQuimestreDAO.saveorUpdate(x);
			});
			Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
			buscar();
		}
		if (materia) {
			listaCalificacionQuimestreCualitativa.forEach(x -> {
				if (x.getCualitativa1() == null) {
					bandera = true;
				}
			});
			if (bandera) {
				bandera = false;
				Messagebox.show("Existen Alumnos sin Calificar");
				return;
			}
			listaCalificacionQuimestreCualitativa.forEach(x -> {
				if (x.getId() != null && x.getObservacion() != null && x.getObservacion().getId() != null) {
					InstanciaDAO.observacionDAO.saveorUpdate(x.getObservacion());
				}
				//Inicia proceso para guardar Cuadro Calificacion
				CuadroCalificacion cc = InstanciaDAO.cuadroCalificacionDAO
						.getCuadroCalificacion(x.getAsignatura().getId(), x.getMatricula().getId());
				if (cc == null) {
					cc = new CuadroCalificacion();
					cc.setAsignatura(x.getAsignatura());
					cc.setMatricula(x.getMatricula());
				}
				if (x.getQuimestre().getId() == 1) {
					cc.setQuimestre1(x.getCualitativa2().getId());
				}
				if (x.getQuimestre().getId() == 2) {
					cc.setQuimestre2(x.getCualitativa2().getId());
				}
				double id = Sistema.redondearEntero((cc.getQuimestre1()+cc.getQuimestre2())/2);
				if (id <= 9) {
					id = 9;
				}
				cc.setPromedio(id);
				cc.setPromedioFinal(cc.getPromedio());
				cc.setLetra(Sistema.numeroAletras(cc.getPromedio()));
				cc.setNota(InstanciaDAO.cualitativaDAO.getCualitativa((int) id).getSiglas());
				InstanciaDAO.cuadroCalificacionDAO.saveorUpdate(cc);
				//Fin proceso				
				InstanciaDAO.calificacionQuimestreCualitativaDAO.saveorUpdate(x);
			});
			Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
			buscar();
		}
	}

	@NotifyChange({ "listaCalificacionQuimestre", "listaCalificacionQuimestreCualitativa", "listaCualitativa" })
	@Command
	public void selectQuimestre() {
		buscar();
	}

	@NotifyChange({ "listaCalificacionQuimestreCualitativa", "listaCalificacionQuimestre", "materia", "basico",
			"inicial" })
	@Command
	public void selectAsignacionDocente() {
		buscar();
	}

	@NotifyChange({ "listaCalificacionQuimestreCualitativa", "listaCalificacionQuimestre", "materia", "basico",
			"inicial" })
	@Command
	public void buscar() {
		metodoCalificacion(asignacionDocente.getAsignatura());
		if (basico) {
			listaCalificacionQuimestre = InstanciaDAO.calificacionQuimestreDAO
					.getListaCalificacionQuimestreByAsignaturaAndQuimestre(asignacionDocente.getAsignatura().getId(),
							quimestre.getId());
			if (listaCalificacionQuimestre == null || listaCalificacionQuimestre.size() == 0) {
				llenarListaAlumno();
			} else {
				listaCalificacionQuimestre.forEach(x -> {
					x.setCalificacionParcials(InstanciaDAO.calificacionParcialDAO
							.getListaCalificacionParcialByCalificacionQuimestre(x.getId()));
					promedio = 0.0;
					InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByCalificacionQuimestre(x.getId())
							.forEach(y -> {
								promedio += y.getPromedio();
							});
					x.setPromedioParciales(Sistema.redondear(promedio / 3));
					x.setPromedio80(Sistema.redondear(x.getPromedioParciales() * 80 / 100));
				});
			}
		}
		if (inicial || materia) {
			listaCalificacionQuimestreCualitativa = InstanciaDAO.calificacionQuimestreCualitativaDAO
					.getListaCalificacionQuimestreCualitativaByAsignaturaAndQuimestre(
							asignacionDocente.getAsignatura().getId(), quimestre.getId());
			if (listaCalificacionQuimestreCualitativa == null || listaCalificacionQuimestreCualitativa.size() == 0) {
				llenarListaCualitativa();
			} else {
				listaCalificacionQuimestreCualitativa.forEach(x -> {
					x.setCalificacionParcialCualitativas(InstanciaDAO.calificacionParcialCualitativaDAO
							.getListaCalificacionParcialCualitativaByCalificacionQuimestre(x.getId()));
				});
			}
		}
	}

	@NotifyChange("listaCalificacionQuimestre")
	@Command
	public void calcularPromedio(@BindingParam("item") CalificacionQuimestre item,
			@BindingParam("valor") Double valor) {
		if (item.getExamen() > 10 || item.getExamen() < 0) {
			item.setExamen(0.0);
			Messagebox.show(Sistema.idioma.getValorInvalido());
		}
		item.setExamen20(Sistema.redondear(item.getExamen() * 20 / 100));
		item.setNota(Sistema.redondear(item.getPromedio80() + item.getExamen20()));
	}

	@NotifyChange("listaCalificacionQuimestreCualitativa")
	@Command
	public void calcularPromedioCualitativaMateria(@BindingParam("item") CalificacionQuimestreCualitativa cqc) {
		promedioCualitativa = 0.0;
		InstanciaDAO.calificacionParcialCualitativaDAO
				.getListaCalificacionParcialCualitativaByCalificacionQuimestre(cqc.getId()).forEach(x -> {
					promedioCualitativa = promedioCualitativa + x.getCualitativa().getId();
				});
		promedioCualitativa = promedioCualitativa + cqc.getCualitativa1().getId();
		promedioCualitativa = promedioCualitativa / 4;
		double id = Sistema.redondearEntero(promedioCualitativa);
		if (id <= 9) {
			id = 9;
		}
		cqc.setCualitativa2(InstanciaDAO.cualitativaDAO.getCualitativa((int) id));
	}

	@NotifyChange("listaCalificacionQuimestre")
	@Command
	public void calcularPromedioTodos() {
		listaCalificacionQuimestre.forEach(x -> {
			calcularPromedio(x, x.getExamen());
		});
	}

	@NotifyChange("listaCalificacionQuimestre")
	@Command
	public void nuevoCalificacionQuimestre(
			@BindingParam("calificacionQuimestre") CalificacionQuimestre calificacionQuimestre) {
		calificacionQuimestre.setObservacion(new Observacion());

	}

	@NotifyChange({ "listaCalificacionQuimestreCualitativa" })
	@Command
	public void nuevoCalificacionQuimestreCualitativa(
			@BindingParam("calificacionQuimestreCualitativa") CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		calificacionQuimestreCualitativa.setObservacion(new Observacion());
	}

	@NotifyChange({ "listaCalificacionQuimestre", "listaCalificacionQuimestreCualitativa", "basico", "materia" })
	@Command
	public void eliminarCalificacionQuimestre(
			@BindingParam("calificacionQuimestre") CalificacionQuimestre calificacionQuimestre) {
		String msj = "Esta seguro(a) que desea eliminar el registro.";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						if (calificacionQuimestre.getObservacion().getId() != null) {
							Observacion observacion = calificacionQuimestre.getObservacion();
							calificacionQuimestre.setObservacion(null);
							InstanciaDAO.calificacionQuimestreDAO.saveorUpdate(calificacionQuimestre);
							int a = InstanciaDAO.observacionDAO.eliminar(observacion);
							if (a == 1) {
								Clients.showNotification("Registro eliminado satisfactoriamente",
										Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassCalificacionQuimestre.this,
										"listaCalificacionQuimestre");
							} else {
								Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
										null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassCalificacionQuimestre.this,
										"listaCalificacionQuimestre");
							}
						} else {
							calificacionQuimestre.setObservacion(null);
							BindUtils.postNotifyChange(null, null, ClassCalificacionQuimestre.this,
									"listaCalificacionQuimestre");
						}
					}
				});
	}

	@NotifyChange("listaCalificacionQuimestreCualitativa")
	@Command
	public void eliminarCalificacionQuimestreCualitativa(
			@BindingParam("calificacionQuimestreCualitativa") CalificacionQuimestreCualitativa calificacionQuimestreCualitativa) {
		String msj = "Esta seguro(a) que desea eliminar el registro.";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						if (calificacionQuimestreCualitativa.getObservacion().getId() != null) {
							Observacion observacion = calificacionQuimestreCualitativa.getObservacion();
							calificacionQuimestreCualitativa.setObservacion(null);
							InstanciaDAO.calificacionQuimestreCualitativaDAO
									.saveorUpdate(calificacionQuimestreCualitativa);
							int a = InstanciaDAO.observacionDAO.eliminar(observacion);
							if (a == 1) {
								Clients.showNotification("Registro eliminado satisfactoriamente",
										Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassCalificacionQuimestre.this,
										"listaCalificacionQuimestreCualitativa");
							} else {
								Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
										null, "top_center", 2000);
								BindUtils.postNotifyChange(null, null, ClassCalificacionQuimestre.this,
										"listaCalificacionQuimestreCualitativa");
							}
						} else {
							calificacionQuimestreCualitativa.setObservacion(null);
							BindUtils.postNotifyChange(null, null, ClassCalificacionQuimestre.this,
									"listaCalificacionQuimestreCualitativa");
						}
					}
				});
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintPrecio() {
		return Sistema.CONSTRAINT_PRECIO;
	}

	public List<CalificacionQuimestre> getListaCalificacionQuimestre() {
		return listaCalificacionQuimestre;
	}

	public void setListaCalificacionQuimestre(List<CalificacionQuimestre> listaCalificacionQuimestre) {
		this.listaCalificacionQuimestre = listaCalificacionQuimestre;
	}

	public List<CalificacionQuimestreCualitativa> getListaCalificacionQuimestreCualitativa() {
		return listaCalificacionQuimestreCualitativa;
	}

	public void setListaCalificacionQuimestreCualitativa(
			List<CalificacionQuimestreCualitativa> listaCalificacionQuimestreCualitativa) {
		this.listaCalificacionQuimestreCualitativa = listaCalificacionQuimestreCualitativa;
	}

	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}

	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}

	public Quimestre getQuimestre() {
		return quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

	public List<AsignacionDocente> getListaAsignacionDocente() {
		return listaAsignacionDocente;
	}

	public void setListaAsignacionDocente(List<AsignacionDocente> listaAsignacionDocente) {
		this.listaAsignacionDocente = listaAsignacionDocente;
	}

	public AsignacionDocente getAsignacionDocente() {
		return asignacionDocente;
	}

	public void setAsignacionDocente(AsignacionDocente asignacionDocente) {
		this.asignacionDocente = asignacionDocente;
	}

	public boolean isInicial() {
		return inicial;
	}

	public void setInicial(boolean inicial) {
		this.inicial = inicial;
	}

	public boolean isBasico() {
		return basico;
	}

	public void setBasico(boolean basico) {
		this.basico = basico;
	}

	public boolean isMateria() {
		return materia;
	}

	public void setMateria(boolean materia) {
		this.materia = materia;
	}

	public List<Cualitativa> getListaCualitativaInicial() {
		return listaCualitativaInicial;
	}

	public void setListaCualitativaInicial(List<Cualitativa> listaCualitativaInicial) {
		this.listaCualitativaInicial = listaCualitativaInicial;
	}

	public List<Cualitativa> getListaCualitativaMateria() {
		return listaCualitativaMateria;
	}

	public void setListaCualitativaMateria(List<Cualitativa> listaCualitativaMateria) {
		this.listaCualitativaMateria = listaCualitativaMateria;
	}

}
