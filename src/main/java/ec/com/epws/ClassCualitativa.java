package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Cualitativa;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassCualitativa {
	@Wire("#winCualitativa")
	private Window winCualitativa;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Cualitativa> listaCualitativa;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaCualitativa = InstanciaDAO.cualitativaDAO.getListaCualitativa();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formCualitativaCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaCualitativa")
	public void eliminar(@BindingParam("cualitativa") final Cualitativa cualitativa) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + cualitativa.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.cualitativaDAO.eliminar(cualitativa);
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaCualitativa.clear();
							listaCualitativa = InstanciaDAO.cualitativaDAO.getListaCualitativa();
							BindUtils.postNotifyChange(null, null, ClassCualitativa.this, "listaCualitativa");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassCualitativa.this, "listaCualitativa");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("cualitativa") Cualitativa cualitativa) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", cualitativa);
		BindUtils.postGlobalCommand(null, null, "cargarCualitativa", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaCualitativa")
	public void refrescarlista() {
		listaCualitativa = InstanciaDAO.cualitativaDAO.getListaCualitativa();
	}

	@Command
	public void editar(@BindingParam("cualitativa") Cualitativa cualitativa) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", cualitativa);
		Executions.createComponents("/vista/formCualitativaCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaCualitativa")
	public void filtrar() {
		listaCualitativa.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaCualitativa.addAll(InstanciaDAO.cualitativaDAO.getListaCualitativa());
		} else {
			for (Cualitativa item : InstanciaDAO.cualitativaDAO.getListaCualitativa()) {
				if (item.getDescripcion().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0
						|| item.getSiglas().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase())== 0) {
					listaCualitativa.add(item);
				}
			}
		}
	}

	@Command
	public void salir() {
		winCualitativa.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Cualitativa> getListaCualitativa() {
		return listaCualitativa;
	}

	public void setListaCualitativa(List<Cualitativa> listaCualitativa) {
		this.listaCualitativa = listaCualitativa;
	}
	
	
}
