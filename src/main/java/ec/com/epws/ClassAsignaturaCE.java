package ec.com.epws;

import java.util.List;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.CodigoCualitativa;
import ec.com.epws.entity.Curso;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassAsignaturaCE {
	@Wire("#winAsignaturaCE")
	private Window winAsignaturaCE;

	private Asignatura asignatura;
	private List<CodigoCualitativa> listaCodigoCualitativa;
	private CodigoCualitativa codigoCualitativa;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Asignatura asignatura, @ExecutionArgParam("curso") Curso curso) {
		Selectors.wireComponents(view, this, false);
		listaCodigoCualitativa = Sistema.getListaCodigoCualitativa();
		if (asignatura == null) {
			this.asignatura = new Asignatura();
			this.asignatura.setCurso(curso);
			if (curso.getNivel().getId() == Sistema.NIVEL_INICIAL) {
				codigoCualitativa = listaCodigoCualitativa.get(Sistema.FILTRO_CUALITATIVO_INICIAL - 1);
			}
			if (curso.getNivel().getId() != Sistema.NIVEL_INICIAL) {
				codigoCualitativa = listaCodigoCualitativa.get(Sistema.FILTRO_CUALITATIVO_BASICO - 1);
			}
		} else {
			this.asignatura = asignatura;
			codigoCualitativa = listaCodigoCualitativa.get(asignatura.getCodigoCualitativa().getId() - 1);
		}
	}

	@Command
	@NotifyChange("asignatura")
	public void guardarEditar() {
		bandera = true;
		asignatura.setCodigoCualitativa(codigoCualitativa);
		if (asignatura.getId() == null) {
			InstanciaDAO.asignaturaDAO.saveorUpdate(asignatura);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			Curso curso = asignatura.getCurso();
			asignatura = new Asignatura();
			asignatura.setCurso(curso);
		} else {
			InstanciaDAO.asignaturaDAO.saveorUpdate(asignatura);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("asignatura")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			asignatura.setDescripcion((cadena.toUpperCase()));
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winAsignaturaCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintEntero() {
		return Sistema.CONSTRAINT_ENTERO;
	}

	public String getConstraintEnteroVacio() {
		return Sistema.CONSTRAINT_ENTERO_VACIO;
	}

	public String getConstraintLetra() {
		return Sistema.CONSTRAINT_LETRA;
	}

	public String getConstraintLetraVacio() {
		return Sistema.CONSTRAINT_LETRA_VACIO;
	}

	public Asignatura getAsignatura() {
		return asignatura;
	}

	public void setAsignatura(Asignatura asignatura) {
		this.asignatura = asignatura;
	}

	public List<CodigoCualitativa> getListaCodigoCualitativa() {
		return listaCodigoCualitativa;
	}

	public void setListaCodigoCualitativa(List<CodigoCualitativa> listaCodigoCualitativa) {
		this.listaCodigoCualitativa = listaCodigoCualitativa;
	}

	public CodigoCualitativa getCodigoCualitativa() {
		return codigoCualitativa;
	}

	public void setCodigoCualitativa(CodigoCualitativa codigoCualitativa) {
		this.codigoCualitativa = codigoCualitativa;
	}

}
