package ec.com.epws;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.AnioLectivo;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassAnioLectivoCE {
	@Wire("#winAnioLectivoCE")
	private Window winAnioLectivoCE;
	private AnioLectivo anioLectivo;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") AnioLectivo anioLectivo) {
		Selectors.wireComponents(view, this, false);
		this.anioLectivo = (anioLectivo == null) ? new AnioLectivo() : anioLectivo;
	}

	@Command
	@NotifyChange("anioLectivo")
	public void guardarEditar() {
		bandera = true;
		if (anioLectivo.getId() == null) {
			anioLectivo.setEstado(false);
			InstanciaDAO.anioLectivoDAO.saveorUpdate(anioLectivo);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			anioLectivo = new AnioLectivo();
		} else {
			InstanciaDAO.anioLectivoDAO.saveorUpdate(anioLectivo);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("anioLectivo")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			anioLectivo.setDescripcion((cadena.toUpperCase()));
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winAnioLectivoCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Window getWinAnioLectivoCE() {
		return winAnioLectivoCE;
	}

	public void setWinAnioLectivoCE(Window winAnioLectivoCE) {
		this.winAnioLectivoCE = winAnioLectivoCE;
	}

	public AnioLectivo getAnioLectivo() {
		return anioLectivo;
	}

	public void setAnioLectivo(AnioLectivo anioLectivo) {
		this.anioLectivo = anioLectivo;
	}

}
