package ec.com.epws;

import java.io.File;
import java.util.concurrent.ThreadLocalRandom;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Docente;
import ec.com.epws.entity.Rol;
import ec.com.epws.entity.Usuario;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassDocenteCE {
	@Wire("#winDocenteCE")
	private Window winDocenteCE;

	private Docente docente;
	// private Usuario usuario;

	private String password;
	private String passwordRepetir;
	private String claveRepetida;
	boolean validarRepetido = false;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Docente docente) {
		Selectors.wireComponents(view, this, false);
		this.docente = (docente == null) ? new Docente() : docente;
		this.docente.setUsuario((docente == null) ? new Usuario() : docente.getUsuario());
		desencriptar(this.docente.getUsuario());
	}

	@Command
	@NotifyChange({ "docente", "usuario", "passwordRepetir", "password" })
	public void guardarEditar() {
		validarPassword();
		if (!validarRepetido) {
			Messagebox.show("Claves no coinciden");
			return;
		}
		if (Sistema.validarRepetido(InstanciaDAO.usuarioDAO.getListaUsuario(), docente.getUsuario())) {
			return;
		}
		if (!Sistema.validarCedula(docente.getUsuario().getCedula())) {
			return;
		}
		bandera = true;
		docente.getUsuario().setPassword(Sistema.Encriptar(password));
		if (docente.getId() == null) {
			docente.setEstado(Sistema.getEstado(Sistema.ESTADO_INGRESADO));
			Rol rol = new Rol();
			rol.setId(Sistema.ID_ROL_DOCENTE);
			docente.getUsuario().setRol(rol);
			docente.getUsuario().setCargo("Docente");
			docente.getUsuario().setFuncion("Docente");
			docente.getUsuario().setEstado(true);
			// docente.setUsuario(usuario);
			InstanciaDAO.docenteDAO.saveorUpdate(docente);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			docente = new Docente();
			docente.setUsuario(new Usuario());
			password = "";
			passwordRepetir = "";
			// usuario = new Usuario();
		} else {
			InstanciaDAO.usuarioDAO.saveorUpdate(docente.getUsuario());
			InstanciaDAO.docenteDAO.saveorUpdate(docente);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("usuario")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("nombre")) {
			docente.getUsuario().setNombre((cadena.toUpperCase()));
		}
		if (campo.equals("apellido")) {
			docente.getUsuario().setApellido((cadena.toUpperCase()));
		}
	}

	public void desencriptar(Usuario usuario) {
		try {
			password = Sistema.desencriptar(usuario.getPassword());
			passwordRepetir = password;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@NotifyChange("claveRepetida")
	@Command
	public void validarPassword() {
		if (passwordRepetir != null && password != null && !passwordRepetir.trim().equals("")
				&& !password.trim().equals("")) {
			if (!password.trim().equals(passwordRepetir.trim())) {
				validarRepetido = false;
				claveRepetida = "Claves no coinciden";
			} else {
				validarRepetido = true;
				claveRepetida = "";
			}
		}
	}
	
	@NotifyChange("docente")
	@Command
	public void uploadFile(@ContextParam(ContextType.TRIGGER_EVENT) UploadEvent event) {
		if (docente.getUsuario()==null || docente.getUsuario().getCedula() == null || docente.getUsuario().getCedula().equals("")) {
			Messagebox.show("Es necesario ingresar cedula");
			return;
		}
		String cedulaProvicional = docente.getUsuario().getCedula();
		if (Sistema.validarRepetido(InstanciaDAO.usuarioDAO.getListaUsuario(), docente.getUsuario())) {
			docente.getUsuario().setCedula(cedulaProvicional);
			return;
		}
		Media media;
		media = event.getMedia();
		int aleatorio = ThreadLocalRandom.current().nextInt(1, 100000);
		if (Util.uploadFile(media, docente.getUsuario().getCedula() + aleatorio, Sistema.carpetaFotoDocente)) {
			new File(Util.getPathRaiz() + docente.getFoto()).delete();
			docente.setFoto("/" + Sistema.carpetaFotoDocente + "/" + docente.getUsuario().getCedula() + aleatorio);
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winDocenteCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getConstraintEntero() {
		return Sistema.CONSTRAINT_ENTERO;
	}

	public String getConstraintEnteroVacio() {
		return Sistema.CONSTRAINT_ENTERO_VACIO;
	}

	public String getConstraintLetra() {
		return Sistema.CONSTRAINT_LETRA;
	}

	public String getConstraintLetraVacio() {
		return Sistema.CONSTRAINT_LETRA_VACIO;
	}
	
	public String getConstraintCorreo() {
		return Sistema.CONSTRAINT_CORREO;
	}

	public Docente getDocente() {
		return docente;
	}

	public void setDocente(Docente docente) {
		this.docente = docente;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordRepetir() {
		return passwordRepetir;
	}

	public void setPasswordRepetir(String passwordRepetir) {
		this.passwordRepetir = passwordRepetir;
	}

	public String getClaveRepetida() {
		return claveRepetida;
	}

	public void setClaveRepetida(String claveRepetida) {
		this.claveRepetida = claveRepetida;
	}

	/*
	 * public Usuario getUsuario() { return usuario; }
	 * 
	 * public void setUsuario(Usuario usuario) { this.usuario = usuario; }
	 */

}
