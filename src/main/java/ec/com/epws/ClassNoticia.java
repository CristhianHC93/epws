package ec.com.epws;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Noticia;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassNoticia {
	@Wire("#winNoticia")
	private Window winNoticia;

	private String dimensionPantalla;
	private String estadoPantalla;
	private boolean btAceptar;
	private String datoBusqueda;
	private List<Noticia> listaNoticia;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("clase") String clase) {
		Selectors.wireComponents(vista, this, false);
		if (clase == null) {
			clase = "this";
		}
		if (clase.equals("this")) {
			estadoPantalla = "embedded";
			btAceptar = false;
			dimensionPantalla = "100%";
		}
		if (!clase.equals("this")) {
			estadoPantalla = "modal";
			btAceptar = true;
			dimensionPantalla = "85%";
		}
		listaNoticia = InstanciaDAO.noticiaDAO.getListaNoticia();
	}

	@Command
	public void nuevo() {
		Executions.createComponents("/vista/formNoticiaCE.zul", null, null);
	}

	@Command
	@NotifyChange("listaNoticia")
	public void eliminar(@BindingParam("noticia") final Noticia noticia) {
		String msj = "Esta seguro(a) que desea eliminar el registro \"" + noticia.getDescripcion()
				+ "\" esta accion no podra deshacerse si se confirma";
		Messagebox.show(msj, "Confirm", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				(EventListener<Event>) event -> {
					if (((Integer) event.getData()).intValue() == Messagebox.OK) {
						int a = InstanciaDAO.noticiaDAO.eliminar(noticia);
						if (a == 1) {
							Clients.showNotification("Registro eliminado satisfactoriamente",
									Clients.NOTIFICATION_TYPE_INFO, null, "top_center", 2000);
							listaNoticia.clear();
							listaNoticia = InstanciaDAO.noticiaDAO.getListaNoticia();
							BindUtils.postNotifyChange(null, null, ClassNoticia.this, "listaNoticia");
						} else {
							Clients.showNotification("Imposible Eliminar registro", Clients.NOTIFICATION_TYPE_ERROR,
									null, "top_center", 2000);
							BindUtils.postNotifyChange(null, null, ClassNoticia.this, "listaNoticia");
						}
					}
				});
	}

	@Command
	public void aceptar(@BindingParam("noticia") Noticia noticia) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("objeto", noticia);
		BindUtils.postGlobalCommand(null, null, "cargarNoticia", parametros);
		salir();
	}

	@GlobalCommand
	@NotifyChange("listaNoticia")
	public void refrescarlista() {
		listaNoticia = InstanciaDAO.noticiaDAO.getListaNoticia();
	}

	@Command
	public void editar(@BindingParam("noticia") Noticia noticia) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("objeto", noticia);
		Executions.createComponents("/vista/formNoticiaCE.zul", null, parameters);
	}

	@Command
	@NotifyChange("listaNoticia")
	public void filtrar() {
		listaNoticia.clear();
		if (datoBusqueda == null || datoBusqueda.trim().equals("")) {
			listaNoticia.addAll(InstanciaDAO.noticiaDAO.getListaNoticia());
		} else {
			for (Noticia item : InstanciaDAO.noticiaDAO.getListaNoticia()) {
				if (item.getTitulo().trim().toLowerCase().indexOf(datoBusqueda.trim().toLowerCase()) == 0) {
					listaNoticia.add(item);
				}
			}
		}
	}

	@Command
	public void salir() {
		winNoticia.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public String getDimensionPantalla() {
		return dimensionPantalla;
	}

	public void setDimensionPantalla(String dimensionPantalla) {
		this.dimensionPantalla = dimensionPantalla;
	}

	public String getEstadoPantalla() {
		return estadoPantalla;
	}

	public void setEstadoPantalla(String estadoPantalla) {
		this.estadoPantalla = estadoPantalla;
	}

	public boolean isBtAceptar() {
		return btAceptar;
	}

	public void setBtAceptar(boolean btAceptar) {
		this.btAceptar = btAceptar;
	}

	public String getDatoBusqueda() {
		return datoBusqueda;
	}

	public void setDatoBusqueda(String datoBusqueda) {
		this.datoBusqueda = datoBusqueda;
	}

	public List<Noticia> getListaNoticia() {
		return listaNoticia;
	}

	public void setListaNoticia(List<Noticia> listaNoticia) {
		this.listaNoticia = listaNoticia;
	}
	
	
}
