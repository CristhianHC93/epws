package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.MedicaVacuna;

public interface MedicaVacunaDAO {
	public List<MedicaVacuna> getListaMedicaVacuna();
	public List<MedicaVacuna> getListaMedicaVacunaByVacuna(int vacuna);
	public List<MedicaVacuna> getListaMedicaVacunaByObservacionMedica(int observacionMedica);
	public MedicaVacuna saveorUpdate(MedicaVacuna medicaVacuna);
	public int eliminar(MedicaVacuna medicaVacuna);

}
