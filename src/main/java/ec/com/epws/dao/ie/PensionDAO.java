package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Pension;

public interface PensionDAO {
	public List<Pension> getListaPension();
	public List<Pension> getListaPensionByMatricula(int matricula);
	public List<Pension> getListaPensionByCurso(int curso,int anioLectivo);
	public List<Pension> getListaPensionByAlumno(int alumno,int anioLectivo);
	public List<Pension> getListaPensionByMes(int mes,int anioLectivo);
	public List<Pension> getListaPensionByEstado(int estado,int anioLectivo);
	public List<Pension> getListaPensionByMesMatricula(int mes,int matricula,int anioLectivo);
	public List<Pension> getListaPensionByMesCurso(int mes,int curso,int anioLectivo);
	public List<Pension> getListaPensionByEstadoAlumno(int estado,int alumno,int anioLectivo);
	public List<Pension> getListaPensionByEstadoCurso(int estado,int curso,int anioLectivo);
	public Pension saveorUpdate(Pension pension);
}
