package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Docente;

public interface DocenteDAO {
	public List<Docente> getListaDocente();
	public Docente getDocente(int usuario);
	public Docente saveorUpdate(Docente docente);
	public List<Docente> getListaDocenteFiltro(String dato);
}
