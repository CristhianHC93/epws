package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.CalificacionParcialCualitativa;

public interface CalificacionParcialCualitativaDAO {

	public List<CalificacionParcialCualitativa> getListaCalificacionParcialCualitativaByAsignaturaAndParcial(int asignatura,int parcial);
	public List<CalificacionParcialCualitativa> getListaCalificacionParcialCualitativaByCalificacionQuimestre(int calificacionQuimestreCualitativa);
	public List<CalificacionParcialCualitativa> getListaCalificacionParcialCualitativaByMatriculaAndParcial(int matricula, int parcial);
	public CalificacionParcialCualitativa getCalificacionParcialCualitatival(int calificacionParcialCualitativa,int asignatura);
	public CalificacionParcialCualitativa saveorUpdate(CalificacionParcialCualitativa calificacionParcialCualitativa);
}
