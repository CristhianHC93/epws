package ec.com.epws.dao.ie;

import ec.com.epws.entity.ValorReferencia;

public interface ValorReferenciaDAO {

	public ValorReferencia getValorReferencia();
	public ValorReferencia saveUpdate(ValorReferencia valorReferencia);
}
