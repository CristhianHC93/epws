package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Matricula;

public interface MatriculaDAO {
	public List<Matricula> getListaMatricula();
	public List<Matricula> getListaMatriculaAnioActivo(int anioLectivo);
	public List<Matricula> getListaMatriculaAnioActivoByAlumnoByEstado(int anioLectivo,int alumno,int estado);
	public List<Matricula> getListaMatriculaAnioActivoFiltro(int anioLectivo,String dato);
	public List<Matricula> getListaMatriculaAnioActivoByCurso(int anioLectivo,int curso);
	public Matricula saveorUpdate(Matricula matricula);
}
