package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Discapacidad;

public interface DiscapacidadDAO {
	public List<Discapacidad> getListaDiscapacidad();
	public Discapacidad saveorUpdate(Discapacidad discapacidad);
	public int eliminar(Discapacidad discapacidad);
}
