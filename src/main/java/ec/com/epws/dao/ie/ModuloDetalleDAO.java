package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Modulo;
import ec.com.epws.entity.ModuloDetalle;

public interface ModuloDetalleDAO {
	public List<ModuloDetalle> getListaModuloDetallesByModulo(Modulo modulo);
	public List<ModuloDetalle> getListaModuloDetallesByModulo(int modulo);
	public List<ModuloDetalle> getListaModuloDetalle();
	public ModuloDetalle saveorUpdate(ModuloDetalle moduloDetalle);
}
