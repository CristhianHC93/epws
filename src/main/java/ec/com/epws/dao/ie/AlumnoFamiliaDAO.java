package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.AlumnoFamilia;

public interface AlumnoFamiliaDAO {
//	public List<AlumnoFamilia> getListaAlumno();
	public AlumnoFamilia getAlumnoFamilia(int alumno);
	public AlumnoFamilia getAlumnoFamilia(int alumno,int familia);
	public List<AlumnoFamilia> getListaAlumno(int alumno);
	public AlumnoFamilia saveorUpdate(AlumnoFamilia alumno);
}
