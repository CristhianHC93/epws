package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.AsignacionDocente;

public interface AsignacionDocenteDAO {
	public List<AsignacionDocente> getListaAsignacionDocenteByDocente(int anoiLectivo,int docente);
//	public List<AsignacionDocente> getListaAsignacionDocenteByDocenteByNivel12(int anioLectivo, int docente);
	public List<AsignacionDocente> getListaAsignacionDocenteByCurso(int anoiLectivo,int curso);
	public AsignacionDocente getAsignacionDocenteByAsignatura(int anoiLectivo,int asignatura);
	public List<AsignacionDocente> getListaAsignacionDocente(int anioLectivo);
	public AsignacionDocente saveorUpdate(AsignacionDocente asignacionDocente);
	public int eliminar(AsignacionDocente asignacionDocente);
}
