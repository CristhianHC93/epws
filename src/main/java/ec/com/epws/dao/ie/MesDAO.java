package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Mes;

public interface MesDAO {
	public List<Mes> getListaMes();
	public Mes saveorUpdate(Mes mes);
	public int eliminar(Mes mes);
}
