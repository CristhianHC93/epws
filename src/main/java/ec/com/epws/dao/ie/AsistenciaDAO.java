package ec.com.epws.dao.ie;

import java.util.Date;
import java.util.List;

import ec.com.epws.entity.Asistencia;

public interface AsistenciaDAO {
	public List<Asistencia> getListaAsistencia(int anoiLectivo,int curso,Date fecha);
	public List<Asistencia> getListaAsistencia(int anioLectivo, int alumno);
	public Asistencia saveorUpdate(Asistencia asistencia);
	public int eliminar(Asistencia asistencia);
}
