package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Estado;

public interface EstadoDAO {
	public List<Estado> getListaEstadoByModulo(int modulo);
}
