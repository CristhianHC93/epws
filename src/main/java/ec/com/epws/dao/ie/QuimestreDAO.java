package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Quimestre;

public interface QuimestreDAO {

	public List<Quimestre> getListaQuimestre();
	public Quimestre saveorUpdate(Quimestre quimestre);
	public int eliminar(int quimestre);
}
