package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Parcial;

public interface ParcialDAO {

	public List<Parcial> getListaParcial();
	public List<Parcial> getListaParcial(int quimestre);
	public Parcial saveorUpdate (Parcial parcial);
	public int eliminar(int parcial);
}
