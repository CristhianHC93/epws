package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Noticia;

public interface NoticiaDAO {
	public List<Noticia> getListaNoticia();
	public Noticia saveorUpdate(Noticia noticia);
	public int eliminar(Noticia noticia);
}
