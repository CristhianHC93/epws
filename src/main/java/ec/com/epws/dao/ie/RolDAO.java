package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Rol;

public interface RolDAO {

	public List<Rol> getListaRol();
	public List<Rol> getListaRol(int estado);
	public Rol saveorUpdate(Rol rol);
}
