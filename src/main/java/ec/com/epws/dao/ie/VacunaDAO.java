package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Vacuna;

public interface VacunaDAO {
	public List<Vacuna> getListaVacuna();
	public Vacuna saveorUpdate(Vacuna vacuna);
	public int eliminar(Vacuna vacuna);

}
