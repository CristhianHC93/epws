package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.AnioLectivo;

public interface AnioLectivoDAO {
	public List<AnioLectivo> getListaAnioLectivo();
	public AnioLectivo getAnioLectivoActivo();
	public AnioLectivo saveorUpdate(AnioLectivo anioLectivo);
}
