package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.CuadroCalificacion;

public interface CuadroCalificacionDAO {
//	public List<CuadroCalificacion> getListaCuadroCalificacionByAsignaturaAndMatricual(int asignatura,int matricula);
	public List<CuadroCalificacion> getListaCuadroCalificacionByAsignatura(int asignatura);
	public List<CuadroCalificacion> getListaCuadroCalificacionByMatricula(int matricula);
	public CuadroCalificacion getCuadroCalificacion(int asignatura,int matricula);
	public CuadroCalificacion saveorUpdate(CuadroCalificacion cuadroCalificacion);
}
