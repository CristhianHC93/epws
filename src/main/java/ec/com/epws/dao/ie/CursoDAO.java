package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Curso;

public interface CursoDAO {
	public List<Curso> getListaCurso();
	public Curso getCursoById(int id);
	public List<Curso> getListaCurso(int nivel);
	public Curso saveorUpdate(Curso curso);
	public int eliminar(Curso curso);
}
