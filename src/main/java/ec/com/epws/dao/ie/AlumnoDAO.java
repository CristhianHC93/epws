package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Alumno;

public interface AlumnoDAO {
	public Alumno getAlumno(int usuario);
	public List<Alumno> getListaAlumno();
	public List<Alumno> getListaAlumno(boolean inscripcion);
	public List<Alumno> getListaAlumnoFiltro(String dato);
	public List<Alumno> getListaAlumnoFiltroInscripcion(String dato);
	public Alumno saveorUpdate(Alumno alumno);
}
