package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Enfermedad;

public interface EnfermedadDAO {

	public List<Enfermedad> getListaEnfermedad();
	public Enfermedad saveorUpdate(Enfermedad enfermedad);
	public int eliminar(Enfermedad enfermedad);
}
