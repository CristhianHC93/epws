package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Usuario;

public interface UsuarioDAO {

	public Usuario saveorUpdate(Usuario Usuario);
	public Usuario getUsuario(String username, String password);
	public Usuario getUsuarioRepresentante(String username, String password);
	public List<Usuario> getListaUsuario();
}
