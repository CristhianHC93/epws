package ec.com.epws.dao.ie;

import ec.com.epws.entity.Observacion;

public interface ObservacionDAO {
	public Observacion saveorUpdate(Observacion observacion);
	public int eliminar(Observacion observacion);
}
