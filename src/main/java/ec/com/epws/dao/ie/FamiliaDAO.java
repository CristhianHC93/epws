package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Familia;

public interface FamiliaDAO {
	public List<Familia> getListaFamilia();
	public List<Familia> getListaFamilia(int parentesco);
	public Familia saveorUpdate(Familia familia);
	public int eliminar(Familia familia);
	public List<Familia> getListaFamiliaFiltro(String dato);
	public List<Familia> getListaFamiliaParentescoFiltro(int parentesco,String dato);
}
