package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Nivel;

public interface NivelDAO {
	public List<Nivel> getListaNivel();
	public Nivel saveorUpdate(Nivel nivel);
	public int eliminar(Nivel nivel);
}
