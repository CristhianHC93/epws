package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Parentesco;

public interface ParentescoDAO {
	public List<Parentesco> getListaParentesco();
	public Parentesco getParentesco(int id);
	public Parentesco saveorUpdate(Parentesco parentesco);
	public int eliminar(Parentesco parentesco);
}
