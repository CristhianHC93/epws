package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Asignatura;

public interface AsignaturaDAO{
	public long countAsignatura(int curso);
	public List<Asignatura> getListaAsignatura();
	public List<Asignatura> getListaAsignatura(int curso,boolean comportamiento);
	public List<Asignatura> getListaAsignaturaBasica(int curso);
	public List<Asignatura> getListaAsignaturaBasicaInicial(int curso);
	public Asignatura saveorUpdate(Asignatura asignatura);
	public int eliminar(Asignatura asignatura);
}
