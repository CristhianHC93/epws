package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Modulo;

public interface ModuloDAO {
	public List<Modulo> getListaModulo();
	public Modulo saveorUpdate(Modulo modulo);
}
