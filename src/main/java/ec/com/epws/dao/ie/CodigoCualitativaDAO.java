package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.CodigoCualitativa;

public interface CodigoCualitativaDAO {

	public List<CodigoCualitativa> getListaCodigoCualitativa();
	public CodigoCualitativa saveUpdate(CodigoCualitativa codigoCualitativa);
}
