package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Cualitativa;

public interface CualitativaDAO {

	public List<Cualitativa> getListaCualitativa();
	public Cualitativa getCualitativa(int id);
	public List<Cualitativa> getListaCualitativaByCodigo(int codigo);
	public Cualitativa saveorUpdate(Cualitativa cualitativa);
	public int eliminar(Cualitativa cualitativa);
}
