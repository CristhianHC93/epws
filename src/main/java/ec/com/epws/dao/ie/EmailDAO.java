package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.Email;

public interface EmailDAO {
	public Email saveorUpdate(Email email);
	public List<Email> getListaEmail();
	public Email getEmail(int id);
}
