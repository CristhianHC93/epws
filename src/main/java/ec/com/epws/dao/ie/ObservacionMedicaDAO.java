package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.ObservacionMedica;

public interface ObservacionMedicaDAO {
	public List<ObservacionMedica> getListaObservacionMedica();
	public ObservacionMedica saveorUpdate(ObservacionMedica om);
}
