package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.CalificacionParcial;

public interface CalificacionParcialDAO {

	public List<CalificacionParcial> getListaCalificacionParcialByAsignaturaAndParcial(int asignatura,int parcial);
	public List<CalificacionParcial> getListaCalificacionParcialByCalificacionQuimestre(int calificacionQuimestre);
	public List<CalificacionParcial> getListaCalificacionParcialByMatriculaAndParcial(int matricula, int parcial);
	public CalificacionParcial getCalificacionParcial(int calificacionParcial,int asignatura);
	public CalificacionParcial saveorUpdate(CalificacionParcial calificacionParcial);
}
