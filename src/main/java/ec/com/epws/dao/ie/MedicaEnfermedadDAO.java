package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.MedicaEnfermedad;

public interface MedicaEnfermedadDAO {
	public List<MedicaEnfermedad> getListaMedicaEnfermedad();
	public List<MedicaEnfermedad> getListaMedicaEnfermedadByEnfermedad(int enfermedad);
	public List<MedicaEnfermedad> getListaMedicaEnfermedadByObservacionMedica(int observacionMedica);
	public MedicaEnfermedad saveorUpdate(MedicaEnfermedad medicaEnfermedad);
	public int eliminar(MedicaEnfermedad medicaEnfermedad);
}
