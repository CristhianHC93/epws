package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.AsignacionTutor;

public interface AsignacionTutorDAO {
	public List<AsignacionTutor> getListaAsignacionTutor(int anioLectivo);
	public AsignacionTutor getAsignacionTutor(int anioLectivo,int curso);
	public AsignacionTutor getAsignacionTutorByDocente(int anioLectivo, int docente);
	public AsignacionTutor saveorUpdate(AsignacionTutor asignacionDocente);
}
