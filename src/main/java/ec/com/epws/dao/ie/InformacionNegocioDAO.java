package ec.com.epws.dao.ie;

import ec.com.epws.entity.InformacionNegocio;

public interface InformacionNegocioDAO {
	public InformacionNegocio saveorUpdate(InformacionNegocio informacionNegocio);
	public InformacionNegocio getInformacionNegocio(int id);
}
