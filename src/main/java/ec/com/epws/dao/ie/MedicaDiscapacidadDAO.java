package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.MedicaDiscapacidad;

public interface MedicaDiscapacidadDAO {
	public List<MedicaDiscapacidad> getListaMedicaDiscapacidad();
	public List<MedicaDiscapacidad> getListaMedicaDiscapacidadByDiscapacidad(int discapacidad);
	public List<MedicaDiscapacidad> getListaMedicaDiscapacidadByObservacionMedica(int observacionMedica);
	public MedicaDiscapacidad saveorUpdate(MedicaDiscapacidad medicaDiscapacidad);
	public int eliminar(MedicaDiscapacidad medicaDiscapacidad);
}
