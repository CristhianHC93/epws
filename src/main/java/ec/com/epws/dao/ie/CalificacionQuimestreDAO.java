package ec.com.epws.dao.ie;

import java.util.List;

import ec.com.epws.entity.CalificacionQuimestre;

public interface CalificacionQuimestreDAO {

	public List<CalificacionQuimestre> getListaCalificacionQuimestreByAsignaturaAndQuimestre(int asignatura,int quimestre);
	public List<CalificacionQuimestre> getListaCalificacionQuimestreByMatriculaByAsignatura(int matricula,int asignatura);
	public List<CalificacionQuimestre> getListaCalificacionQuimestreByMatriculaQuimestre(int matricula,int quimestre);
	public List<CalificacionQuimestre> getListaCalificacionQuimestreByAsignatura(int asignatura);
	public CalificacionQuimestre getCalificacionQuimestre(int calificacionQuimestre,int asignatura);
	public CalificacionQuimestre saveorUpdate(CalificacionQuimestre calificacionQuimestre);
}
