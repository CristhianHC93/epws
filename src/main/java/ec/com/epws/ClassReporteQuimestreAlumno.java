package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.GlobalCommand;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.CalificacionQuimestre;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;
import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Parcial;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.models.Modelos;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassReporteQuimestreAlumno {

	private List<Curso> listaCurso;
	private Curso curso;
	private List<Quimestre> listaQuimestre;
	private Quimestre quimestre;
	private List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacionQuimestre = new ArrayList<>();

	private Matricula matricula;
	private List<Matricula> listaMatricula = new ArrayList<>();

	private List<Asignatura> listaAsignatura;
	double p1 = 0.0, p2 = 0.0, p3 = 0.0, promedio = 0.0, ocho_cero = 0.0, examen = 0.0, dos_cero = 0.0, nota = 0.0;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista, @ExecutionArgParam("curso") Curso curso,
			@ExecutionArgParam("quimestre") Quimestre quimestre,@ExecutionArgParam("matricula") Matricula matricula,
			@ExecutionArgParam("lista") List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacionQuimestre,
			@ExecutionArgParam("listaMatricula") List<Matricula> listaMatricula) {
		Selectors.wireComponents(vista, this, false);
		if (quimestre != null) {
			this.curso = curso;
			this.quimestre = quimestre;
			this.matricula =matricula;
			this.listaMatricula = listaMatricula;
			if (listaCalificacionQuimestre != null) {
				this.listaCalificacionQuimestre = listaCalificacionQuimestre;
			}
		} else {
			listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
			this.curso = listaCurso.get(0);
			listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
			this.quimestre = listaQuimestre.get(0);
		}
	}

	@NotifyChange("listaCalificacionQuimestre")
	@Command
	public List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> buscar(Matricula matricula) {
		if (matricula != null) {
			listaCalificacionQuimestre.clear();
			List<CalificacionQuimestre> listaQuimestre = InstanciaDAO.calificacionQuimestreDAO
					.getListaCalificacionQuimestreByMatriculaQuimestre(matricula.getId(), quimestre.getId());
			List<CalificacionQuimestreCualitativa> listaQuimestreCualitativa = InstanciaDAO.calificacionQuimestreCualitativaDAO
					.getListaCalificacionQuimestreCualitativaByMatriculaQuimestre(matricula.getId(), quimestre.getId());
			listaQuimestre.forEach(x -> {
				x.setCalificacionParcials(InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByCalificacionQuimestre(x.getId()));				
				listaCalificacionQuimestre
					.add(new Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>(x, null,
							null, null, true));});
			listaQuimestreCualitativa.forEach(x -> {
				x.setCalificacionParcialCualitativas(InstanciaDAO.calificacionParcialCualitativaDAO.getListaCalificacionParcialCualitativaByCalificacionQuimestre(x.getId()));
				listaCalificacionQuimestre
					.add(new Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>(null, x,
							null, null, true));});
		}
		return listaCalificacionQuimestre;
	}

	@Command
	public void agregarAlumno() {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("clase", "Pension");
		parametros.put("curso", curso.getId());
		Executions.createComponents("/vista/formMatricula.zul", null, parametros);
	}

	@NotifyChange({ "matricula", "listaCalificacionQuimestre" })
	@GlobalCommand
	public void cargarAlumno(@BindingParam("objeto") Matricula matricula) {
		this.matricula = matricula;
		buscar(matricula);
	}

	@NotifyChange({ "matricula", "listaCalificacionQuimestre" })
	@Command
	public void selectCurso(){
		matricula = null;
		listaCalificacionQuimestre.clear();
	}
	
	@NotifyChange({ "matricula", "listaCalificacionQuimestre" })
	@Command
	public void selectQuimestre(){
		if (matricula != null) {
			buscar(matricula);
		}
	}
	
	@Command
	public void generarExcel() {
		if (listaCalificacionQuimestre.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		CalificacionQuimestreCualitativa c =InstanciaDAO.calificacionQuimestreCualitativaDAO.getCalificacionQuimestreCualitativaComportamiento(listaCalificacionQuimestre.get(0).getObjetoT().getMatricula().getId(), quimestre.getId());
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("lista", CalcularTotal(listaCalificacionQuimestre));
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("p1", p1);
		mapreporte.put("p2", p2);
		mapreporte.put("p3", p3);
		mapreporte.put("promedio", promedio);
		mapreporte.put("examen", examen);
		mapreporte.put("80", ocho_cero);
		mapreporte.put("20", dos_cero);
		mapreporte.put("nota", nota);
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		mapreporte.put("quimestre",quimestre.getDescripcion());
		mapreporte.put("alumno", matricula.getAlumno().getNombre()+" "+matricula.getAlumno().getApellido());
		mapreporte.put("comportamiento", (c== null)?"":(c.getCualitativa2() == null)?"":c.getCualitativa2().getSiglas());
		Sistema.reporteEXCEL(Sistema.QUIMESTRE_ALUMNO_JASPER, "Informe Quimestre", mapreporte,
				Sistema.getInformeCalificacionQuimestreDataSource(listaCalificacionQuimestre),null,true);
	}

	@Command
	public void generarPdf() {
		if (listaCalificacionQuimestre.size() == 0) {
			Messagebox.show(Sistema.idioma.getNoExisteInformacion());
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaCalificacionQuimestre);
		parametros.put("curso", curso);
		parametros.put("quimestre", quimestre);
		parametros.put("matricula", matricula);
		Executions.createComponents("/reporte/impresionQuimestreAlumno.zul", null, parametros);
	}
	
	@Command
	public void generarPdfTodo() {
		List<Matricula> listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());		
		if (listaMatricula.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("listaMatricula", listaMatricula);
		parametros.put("curso", curso);
		parametros.put("quimestre", quimestre);
		parametros.put("matricula", matricula);
		Executions.createComponents("/reporte/impresionQuimestreAlumno.zul", null, parametros);
	}

	AMedia amedia;
	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		mapreporte.put("quimestre",quimestre.getDescripcion());
		if (listaMatricula != null) {
			List<AMedia> lista = new ArrayList<>();
			listaMatricula.forEach(x -> {
				CalificacionQuimestreCualitativa c =InstanciaDAO.calificacionQuimestreCualitativaDAO.getCalificacionQuimestreCualitativaComportamiento(x.getId(), quimestre.getId());
				mapreporte.put("lista", CalcularTotal(buscar(x)));
				mapreporte.put("p1", p1);
				mapreporte.put("p2", p2);
				mapreporte.put("p3", p3);
				mapreporte.put("promedio", promedio);
				mapreporte.put("examen", examen);
				mapreporte.put("80", ocho_cero);
				mapreporte.put("20", dos_cero);
				mapreporte.put("nota", nota);
				mapreporte.put("alumno", x.getAlumno().getNombre()+" "+x.getAlumno().getApellido());
				mapreporte.put("comportamiento", (c== null)?"":(c.getCualitativa2() == null)?"":c.getCualitativa2().getSiglas());
				amedia = Sistema.reportePDF(Sistema.QUIMESTRE_ALUMNO_JASPER, mapreporte,
						Sistema.getInformeCalificacionQuimestreDataSource(buscar(x)));
				lista.add(amedia);
			});
			iframe.setContent(Sistema.reportePDF(lista));
		}else{
			CalificacionQuimestreCualitativa c =InstanciaDAO.calificacionQuimestreCualitativaDAO.getCalificacionQuimestreCualitativaComportamiento(matricula.getId(), quimestre.getId());
			mapreporte.put("lista", CalcularTotal(listaCalificacionQuimestre));
			mapreporte.put("p1", p1);
			mapreporte.put("p2", p2);
			mapreporte.put("p3", p3);
			mapreporte.put("promedio", promedio);
			mapreporte.put("examen", examen);
			mapreporte.put("80", ocho_cero);
			mapreporte.put("20", dos_cero);
			mapreporte.put("nota", nota);
			mapreporte.put("alumno", matricula.getAlumno().getNombre()+" "+matricula.getAlumno().getApellido());
			mapreporte.put("comportamiento", (c== null)?"":(c.getCualitativa2() == null)?"":c.getCualitativa2().getSiglas());
			iframe.setContent(Sistema.reportePDF(Sistema.QUIMESTRE_ALUMNO_JASPER, mapreporte,
					Sistema.getInformeCalificacionQuimestreDataSource(listaCalificacionQuimestre)));
		}
	}

	private List<String> CalcularTotal(List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacionQuimestre) {
		List<String> lista = new ArrayList<>();
		List<Parcial> listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
		listaParcial.forEach(x -> lista.add(x.getDescripcion()));
		p1 = 0.0;
		p2 = 0.0;
		p3 = 0.0;
		promedio = 0.0;
		ocho_cero = 0.0;
		examen = 0.0;
		dos_cero = 0.0;
		nota = 0.0;
		listaAsignatura = InstanciaDAO.asignaturaDAO.getListaAsignaturaBasica(curso.getId());
		listaCalificacionQuimestre.forEach(x -> {
			if (x.getObjetoT() != null) {
				if (x.getObjetoT().getCalificacionParcials().size() > 0) {
					p1 += x.getObjetoT().getCalificacionParcials().get(0).getPromedio();
				}
				if (x.getObjetoT().getCalificacionParcials().size() > 1) {
					p2 += x.getObjetoT().getCalificacionParcials().get(1).getPromedio();
				}
				if (x.getObjetoT().getCalificacionParcials().size() > 2) {
					p3 += x.getObjetoT().getCalificacionParcials().get(2).getPromedio();
				}
				promedio += x.getObjetoT().getPromedioParciales();
				examen += x.getObjetoT().getExamen();
				nota += x.getObjetoT().getNota();
			}
		});
		p1 = Sistema.redondear(p1 / listaAsignatura.size());
		p2 = Sistema.redondear(p2 / listaAsignatura.size());
		p3 = Sistema.redondear(p3 / listaAsignatura.size());
		promedio = Sistema.redondear(promedio / listaAsignatura.size());
		examen = Sistema.redondear(examen / listaAsignatura.size());
		nota = Sistema.redondear(nota / listaAsignatura.size());
		ocho_cero = Sistema.redondear(promedio * 80 / 100);
		dos_cero = Sistema.redondear(examen * 20 / 100);
		return lista;
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}

	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}

	public Quimestre getQuimestre() {
		return quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

	public List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> getListaCalificacionQuimestre() {
		return listaCalificacionQuimestre;
	}

	public void setListaCalificacionQuimestre(
			List<Modelos<CalificacionQuimestre, CalificacionQuimestreCualitativa, Object, Object>> listaCalificacionQuimestre) {
		this.listaCalificacionQuimestre = listaCalificacionQuimestre;
	}

	public Matricula getMatricula() {
		return matricula;
	}

	public void setMatricula(Matricula matricula) {
		this.matricula = matricula;
	}

}
