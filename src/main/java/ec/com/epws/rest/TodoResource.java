package ec.com.epws.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import ec.com.epws.entity.Alumno;
import ec.com.epws.entity.AsignacionDocente;
import ec.com.epws.entity.AsignacionTutor;
import ec.com.epws.entity.Asistencia;
import ec.com.epws.entity.CalificacionParcial;
import ec.com.epws.entity.CalificacionParcialCualitativa;
import ec.com.epws.entity.CalificacionQuimestre;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Noticia;
import ec.com.epws.entity.Parcial;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.entity.Usuario;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.rest.modelo.IdBody;
import ec.com.epws.rest.modelo.ModeloAsignatura;
import ec.com.epws.rest.modelo.ModeloAsistencia;
import ec.com.epws.rest.modelo.ModeloCalificacionParcial;
import ec.com.epws.rest.modelo.ModeloCalificacionQuimestre;
import ec.com.epws.sistema.Sistema;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;

@Path("/todo")
public class TodoResource {
    // This method is called if XMLis request
	
    @GET
    @Path("/noticia")
    @Produces( { MediaType.APPLICATION_JSON })
    public List<Noticia> getListaNoticia() throws Exception {
    	List<Noticia> list = InstanciaDAO.noticiaDAO.getListaNoticia();
    	list.forEach(x -> {
    		try{
    		URL url = new URL(Sistema.URL+x.getImagen());
            String inStreamGuess = url.toString();
            InputStream inStreamConvert = url.openStream();
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            String contentType = URLConnection.guessContentTypeFromName(inStreamGuess);
            if (null != contentType)
            {
              byte[] chunk = new byte[4096];
              int bytesRead;
              while ((bytesRead = inStreamConvert.read(chunk)) > 0)
              {
                os.write(chunk, 0, bytesRead);
              }
              os.flush();
              x.setImagen(Base64.getEncoder().encodeToString(os.toByteArray()));
            }} catch (Exception e) {
				// TODO: handle exception
			
            }
    	});
        return list;
    }
    
    @GET
    @Path("/parcial")
    @Produces( { MediaType.APPLICATION_JSON })
    public List<Parcial> getListaParcial() {
    	List<Parcial> listaParcial = InstanciaDAO.parcialDAO.getListaParcial();
    	List<Parcial> lista = new ArrayList<>();
    	listaParcial.forEach(x -> {
    		Parcial p = new Parcial();
    		p.setDescripcion(x.getDescripcion());
    		p.setId(x.getId());
    		lista.add(p);
    	});
        return lista;
    }
    
    @GET
    @Path("/quimestre")
    @Produces( { MediaType.APPLICATION_JSON })
    public List<Quimestre> getListaQuimestre() {
    	List<Quimestre> listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
    	List<Quimestre> lista = new ArrayList<>();
    	listaQuimestre.forEach(x -> {
    		Quimestre q = new Quimestre();
    		q.setDescripcion(x.getDescripcion());
    		q.setId(x.getId());
    		lista.add(q);
    	});
        return lista;
    }
    
    @POST
    @Path("/login")
    @Produces( {  MediaType.APPLICATION_JSON })
    @Consumes( { MediaType.APPLICATION_JSON })
    public Usuario login(Usuario usuario){
    	Usuario user  = InstanciaDAO.usuarioDAO.getUsuarioRepresentante(usuario.getUsername(), Sistema.Encriptar(usuario.getPassword()));
    	if (user == null) {
    		return null;
		}else{
			Usuario u = new Usuario();
			u.setId(user.getId());
			u.setUsername(user.getUsername());
			u.setPassword(user.getPassword());
			u.setApellido(user.getApellido());
			u.setCedula(user.getCedula());
			u.setNombre(user.getNombre());
			u.setCargo(user.getCargo());
			u.setEmail(user.getEmail());
			u.setEstado(user.getEstado());
			u.setToken(user.getToken());
			return u;
		}
    }
    
    @POST
    @Path("/asistencia")
    @Produces( {  MediaType.APPLICATION_JSON })
    @Consumes( { MediaType.APPLICATION_JSON })
    public List<ModeloAsistencia> getAsistencia(Usuario usuario){
    	Alumno alumno = InstanciaDAO.getAlumnoDAO().getAlumno(usuario.getId());
    	List<Asistencia> listaAsistencia = InstanciaDAO.asistenciaDAO.getListaAsistencia(Sistema.getAnioLectivoActivoEstatico().getId(), alumno.getId());
    	List<ModeloAsistencia> modelo = new ArrayList<>();
    	listaAsistencia.forEach(x -> {
    		DateFormat fecha = new SimpleDateFormat("yyyy/MM/dd");
    		String convertido = fecha.format(x.getFecha());
    		ModeloAsistencia m =  new ModeloAsistencia(convertido,x.getAlumno().getNombre()+" "+x.getAlumno().getApellido(),  x.getAnioLectivo().getDescripcion(), x.getCurso().getDescripcion(), x.getEstado().getDescripcion(), (x.getObservacion() != null) ? x.getObservacion().getDescripcion():"");
    		modelo.add(m);
     	});
    	return modelo;
    }
    
    @POST
    @Path("/asignatura_parcial")
    @Produces( {  MediaType.APPLICATION_JSON })
    @Consumes( { MediaType.APPLICATION_JSON })
    public List<ModeloAsignatura> getAsignaturaParcial(IdBody idBody ){
    	Alumno alumno = InstanciaDAO.getAlumnoDAO().getAlumno(idBody.getId());
    	List<Matricula> listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByAlumnoByEstado(Sistema.getAnioLectivoActivoEstatico().getId(), alumno.getId(), Sistema.ESTADO_INGRESADO);    	
    	List<CalificacionParcial> listaCalificacionParcial = InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByMatriculaAndParcial(listaMatricula.get(0).getId(), idBody.getIdSecundario());
    	List<CalificacionParcialCualitativa> listaCalificacionParcialCualitativa =InstanciaDAO.calificacionParcialCualitativaDAO.getListaCalificacionParcialCualitativaByMatriculaAndParcial(listaMatricula.get(0).getId(), idBody.getIdSecundario());    	
    	List<ModeloAsignatura> lista = new ArrayList<>();
    	listaCalificacionParcial.forEach(x -> {
    		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), x.getCalificacionQuimestre().getAsignatura().getId());
    		ModeloAsignatura a = new ModeloAsignatura(x.getId(),x.getCalificacionQuimestre().getAsignatura().getId(),x.getCalificacionQuimestre().getAsignatura().getDescripcion(),x.getCalificacionQuimestre().getAsignatura().getCurso().getDescripcion(),ad.getDocente().getUsuario().getNombre()+" "+ad.getDocente().getUsuario().getApellido(),String.valueOf(x.getPromedio()),ad.getDocente().getCelular(),ad.getDocente().getUsuario().getEmail());
    		lista.add(a);
     	});
    	listaCalificacionParcialCualitativa.forEach(x -> {
    		if (x.getCalificacionQuimestreCualitativa().getAsignatura().getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_COMPORTAMIENTO) {
				AsignacionTutor at = InstanciaDAO.asignacionTutorDAO.getAsignacionTutorByDocente(Sistema.getAnioLectivoActivoEstatico().getId(), x.getCalificacionQuimestreCualitativa().getAsignatura().getCurso().getId());
	    		ModeloAsignatura a = new ModeloAsignatura(x.getId(),x.getCalificacionQuimestreCualitativa().getAsignatura().getId(),x.getCalificacionQuimestreCualitativa().getAsignatura().getDescripcion(),x.getCalificacionQuimestreCualitativa().getAsignatura().getCurso().getDescripcion(),at.getDocente().getUsuario().getNombre(),String.valueOf(x.getCualitativa().getDescripcion()),at.getDocente().getCelular(),at.getDocente().getUsuario().getEmail());
	    		lista.add(a);
			}else{
    		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), x.getCalificacionQuimestreCualitativa().getAsignatura().getId());
    		ModeloAsignatura a = new ModeloAsignatura(x.getId(),x.getCalificacionQuimestreCualitativa().getAsignatura().getId(),x.getCalificacionQuimestreCualitativa().getAsignatura().getDescripcion(),x.getCalificacionQuimestreCualitativa().getAsignatura().getCurso().getDescripcion(),ad.getDocente().getUsuario().getNombre(),String.valueOf(x.getCualitativa().getDescripcion()),ad.getDocente().getCelular(),ad.getDocente().getUsuario().getEmail());
    		lista.add(a);
    		}
    	});
    	return lista;
    }
    
    @POST
    @Path("/asignatura_quimestre")
    @Produces( {  MediaType.APPLICATION_JSON })
    @Consumes( { MediaType.APPLICATION_JSON })
    public List<ModeloAsignatura> getAsignaturaQuimestre(IdBody idBody ){
    	Alumno alumno = InstanciaDAO.getAlumnoDAO().getAlumno(idBody.getId());
    	List<Matricula> listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByAlumnoByEstado(Sistema.getAnioLectivoActivoEstatico().getId(), alumno.getId(), Sistema.ESTADO_INGRESADO);    	
    	List<CalificacionQuimestre> listaCalificacionQuimestre = InstanciaDAO.calificacionQuimestreDAO.getListaCalificacionQuimestreByMatriculaQuimestre(listaMatricula.get(0).getId(), idBody.getIdSecundario());
    	List<CalificacionQuimestreCualitativa> listaCalificacionQuimestreCualitativa =InstanciaDAO.calificacionQuimestreCualitativaDAO.getListaCalificacionQuimestreCualitativaByMatriculaQuimestreAll(listaMatricula.get(0).getId(), idBody.getIdSecundario());
    	
    	List<ModeloAsignatura> lista = new ArrayList<>();
    	listaCalificacionQuimestre.forEach(x -> {
    		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), x.getAsignatura().getId());
    		ModeloAsignatura a = new ModeloAsignatura(x.getId(),x.getAsignatura().getId(),x.getAsignatura().getDescripcion(),x.getAsignatura().getCurso().getDescripcion(),"",String.valueOf(x.getNota()),ad.getDocente().getCelular(),ad.getDocente().getUsuario().getEmail());
    		lista.add(a);
     	});
    	listaCalificacionQuimestreCualitativa.forEach(x -> {
    		if (x.getAsignatura().getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_COMPORTAMIENTO) {
				AsignacionTutor at = InstanciaDAO.asignacionTutorDAO.getAsignacionTutorByDocente(Sistema.getAnioLectivoActivoEstatico().getId(), x.getAsignatura().getCurso().getId());
	    		ModeloAsignatura a = new ModeloAsignatura(x.getId(),x.getAsignatura().getId(),x.getAsignatura().getDescripcion(),x.getAsignatura().getCurso().getDescripcion(),at.getDocente().getUsuario().getNombre(),(x.getCualitativa2()== null)?"":x.getCualitativa2().getDescripcion(),at.getDocente().getCelular(),at.getDocente().getUsuario().getEmail());
	    		lista.add(a);
			}else{
    		AsignacionDocente ad = InstanciaDAO.asignacionDocenteDAO.getAsignacionDocenteByAsignatura(Sistema.getAnioLectivoActivoEstatico().getId(), x.getAsignatura().getId());
    		ModeloAsignatura a = new ModeloAsignatura(x.getId(),x.getAsignatura().getId(),x.getAsignatura().getDescripcion(),x.getAsignatura().getCurso().getDescripcion(),"",String.valueOf(x.getCualitativa2().getDescripcion()),ad.getDocente().getCelular(),ad.getDocente().getUsuario().getEmail());
    		lista.add(a);
    		}
    	});
    	return lista;
    }
    
    @POST
    @Path("/nota_parcial")
    @Produces( {  MediaType.APPLICATION_JSON })
    @Consumes( { MediaType.APPLICATION_JSON })
    public ModeloCalificacionParcial getNotaParcial(IdBody idBody ){
    	CalificacionParcial cp = InstanciaDAO.calificacionParcialDAO.getCalificacionParcial(idBody.getId(),idBody.getIdSecundario());
    	ModeloCalificacionParcial mcp = null; 
    	if (cp !=null) {
			mcp = new ModeloCalificacionParcial(cp.getId(), String.valueOf(cp.getTai()), String.valueOf(cp.getAic()),String.valueOf(cp.getAgc()), String.valueOf(cp.getL()), String.valueOf(cp.getEsumativa()), String.valueOf(cp.getPromedio()), String.valueOf(cp.getCualitativa().getDescripcion()),(cp.getObservacion() != null)?cp.getObservacion().getDescripcion():null);
		}
    	CalificacionParcialCualitativa cpc = InstanciaDAO.calificacionParcialCualitativaDAO.getCalificacionParcialCualitatival(idBody.getId(),idBody.getIdSecundario());
    	if (cpc != null) {
			mcp =new ModeloCalificacionParcial(cpc.getId(), null, null, null, null, null, null, cpc.getCualitativa().getDescripcion(),(cpc.getObservacion()!= null)?cpc.getObservacion().getDescripcion():null);
		}
    	return mcp;
    }
    
    @POST
    @Path("/nota_quimestre")
    @Produces( {  MediaType.APPLICATION_JSON })
    @Consumes( { MediaType.APPLICATION_JSON })
    public ModeloCalificacionQuimestre getNotaQuimestre(IdBody idBody ){
    	CalificacionQuimestre cq = InstanciaDAO.calificacionQuimestreDAO.getCalificacionQuimestre(idBody.getId(),idBody.getIdSecundario());
    	ModeloCalificacionQuimestre mcq = null;
    	if (cq !=null) {
    		cq.setCalificacionParcials(InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByCalificacionQuimestre(cq.getId()));
    		mcq = new ModeloCalificacionQuimestre(cq.getId(), 
    				(cq.getCalificacionParcials().size()>0) ? String.valueOf(cq.getCalificacionParcials().get(0).getPromedio()):"",
    				(cq.getCalificacionParcials().size()>1) ? String.valueOf(InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByCalificacionQuimestre(cq.getId()).get(1).getPromedio()):"",
    				(cq.getCalificacionParcials().size()>2) ? String.valueOf(InstanciaDAO.calificacionParcialDAO.getListaCalificacionParcialByCalificacionQuimestre(cq.getId()).get(2).getPromedio()):"", 
    				String.valueOf(cq.getPromedioParciales()), String.valueOf(cq.getPromedio80()), String.valueOf(cq.getExamen()), String.valueOf(cq.getExamen20()), String.valueOf(cq.getNota()),(cq.getObservacion() != null)?cq.getObservacion().getDescripcion():null);
		}
    	CalificacionQuimestreCualitativa cqc = InstanciaDAO.calificacionQuimestreCualitativaDAO.getCalificacionQuimestreCualitativa(idBody.getId(),idBody.getIdSecundario());
    	if (cqc != null) {
    		mcq = new ModeloCalificacionQuimestre(cqc.getId(), 
    				(cqc.getCalificacionParcialCualitativas().size()>0) ? cqc.getCalificacionParcialCualitativas().get(0).getCualitativa().getDescripcion():null,
    				(cqc.getCalificacionParcialCualitativas().size()>1) ? cqc.getCalificacionParcialCualitativas().get(1).getCualitativa().getDescripcion():null, 
    				(cqc.getCalificacionParcialCualitativas().size()>2) ? cqc.getCalificacionParcialCualitativas().get(2).getCualitativa().getDescripcion():null, 
    				null, null, (cqc.getCualitativa1()==null)?"":cqc.getCualitativa1().getDescripcion(), null, (cqc.getCualitativa2()==null)?"":cqc.getCualitativa2().getDescripcion(), (cqc.getObservacion()!= null)?cqc.getObservacion().getDescripcion():null);
		}
    	return mcq;
    }
}