package ec.com.epws.rest.modelo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ModeloCalificacionParcial {

	private Integer id;
	private String tai;
	private String aic;
	private String agc;
	private String l;
	private String esumativa;
	private String promedio;
	private String cualitativa;
	private String observacion;

	public ModeloCalificacionParcial(Integer id, String tai, String aic, String agc, String l, String esumativa,
			String promedio, String cualitativa, String observacion) {
		this.id = id;
		this.tai = tai;
		this.aic = aic;
		this.agc = agc;
		this.l = l;
		this.esumativa = esumativa;
		this.promedio = promedio;
		this.cualitativa = cualitativa;
		this.observacion = observacion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTai() {
		return tai;
	}

	public void setTai(String tai) {
		this.tai = tai;
	}

	public String getAic() {
		return aic;
	}

	public void setAic(String aic) {
		this.aic = aic;
	}

	public String getAgc() {
		return agc;
	}

	public void setAgc(String agc) {
		this.agc = agc;
	}

	public String getL() {
		return l;
	}

	public void setL(String l) {
		this.l = l;
	}

	public String getEsumativa() {
		return esumativa;
	}

	public void setEsumativa(String esumativa) {
		this.esumativa = esumativa;
	}

	public String getPromedio() {
		return promedio;
	}

	public void setPromedio(String promedio) {
		this.promedio = promedio;
	}

	public String getCualitativa() {
		return cualitativa;
	}

	public void setCualitativa(String cualitativa) {
		this.cualitativa = cualitativa;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

}
