package ec.com.epws.rest.modelo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ModeloAsistencia {

	private String fecha;
	private String alumno;
	private String anioLectivo;
	private String curso;
	private String estado;
	private String observacion;

	public ModeloAsistencia(String fecha, String alumno, String anioLectivo, String curso, String estado,
			String observacion) {
		this.fecha = fecha;
		this.alumno = alumno;
		this.anioLectivo = anioLectivo;
		this.curso = curso;
		this.estado = estado;
		this.observacion = observacion;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getAlumno() {
		return alumno;
	}

	public void setAlumno(String alumno) {
		this.alumno = alumno;
	}

	public String getAnioLectivo() {
		return anioLectivo;
	}

	public void setAnioLectivo(String anioLectivo) {
		this.anioLectivo = anioLectivo;
	}

	public String getCurso() {
		return curso;
	}

	public void setCurso(String curso) {
		this.curso = curso;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

}
