package ec.com.epws.rest.modelo;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class IdBody {
	private Integer id;
	private Integer idSecundario;

	public IdBody() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdSecundario() {
		return idSecundario;
	}

	public void setIdSecundario(Integer idSecundario) {
		this.idSecundario = idSecundario;
	}

}
