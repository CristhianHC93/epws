package ec.com.epws;

import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.Modulo;
import ec.com.epws.entity.ModuloDetalle;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.sistema.Sistema;

public class ClassModuloDetalle {

	private String btGuardarEditar = Sistema.idioma.getGuardar();
	private List<ModuloDetalle> listaModuloDetalle;
	private ModuloDetalle moduloDetalle;
	private List<Modulo> listaModulo;

	// private Estado estado = InstanciaEntity.getInstanciaEstado();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		listaModuloDetalle = InstanciaDAO.moduloDetalleDAO.getListaModuloDetalle();
		listaModulo = InstanciaDAO.getModuloDAO().getListaModulo();
		moduloDetalle = new ModuloDetalle();
	}

	@Command
	@NotifyChange("listaModuloDetalle")
	public void guardarEditar() {
		if (moduloDetalle.getModulo() == null) {
			Messagebox.show("Debe asignar un Modulo ");
			return;
		}
		if (moduloDetalle.getRuta() == null || moduloDetalle.getRuta().equals("") || moduloDetalle.getNombre() == null
				|| moduloDetalle.getNombre().equals("")) {
			Messagebox.show("Falto ingresar datos");
			return;
		}

		if (moduloDetalle.getId() == null) {
			listaModuloDetalle = InstanciaDAO.moduloDetalleDAO.getListaModuloDetalle();
			for (ModuloDetalle moduloDetalle : listaModuloDetalle) {
				if (moduloDetalle.getNombre().trim().equals(this.moduloDetalle.getNombre().trim())) {
					Messagebox.show("El Nombre ya ha sido asignado");
					return;
				}
			}
			listaModuloDetalle.clear();
			InstanciaDAO.moduloDetalleDAO.saveorUpdate(moduloDetalle);
			Clients.showNotification("ModuloDetalle ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		} else {
			InstanciaDAO.moduloDetalleDAO.saveorUpdate(moduloDetalle);
			Clients.showNotification("ModuloDetalle Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
		listaModuloDetalle = InstanciaDAO.moduloDetalleDAO.getListaModuloDetalle();
	}

	@Command
	@NotifyChange({ "listaModuloDetalle", "moduloDetalle", "btGuardarEditar" })
	public void nuevo() {
		moduloDetalle = new ModuloDetalle();
		btGuardarEditar = Sistema.idioma.getGuardar();
	}

	@Command
	@NotifyChange("btGuardarEditar")
	public void select() {
		btGuardarEditar = Sistema.idioma.getEditar();
	}

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	/*
	 * METODOS GET AND SET
	 */

	@Command
	public void check(@BindingParam("item") ModuloDetalle moduloDetalle) {
		InstanciaDAO.moduloDetalleDAO.saveorUpdate(moduloDetalle);
	}

	public List<ModuloDetalle> getListaModuloDetalle() {
		return listaModuloDetalle;
	}

	public void setListaModuloDetalle(List<ModuloDetalle> listaModuloDetalle) {
		this.listaModuloDetalle = listaModuloDetalle;
	}

	public ModuloDetalle getModuloDetalle() {
		return moduloDetalle;
	}

	public void setModuloDetalle(ModuloDetalle moduloDetalle) {
		this.moduloDetalle = moduloDetalle;
	}

	public List<Modulo> getListaModulo() {
		return listaModulo;
	}

	public void setListaModulo(List<Modulo> listaModulo) {
		this.listaModulo = listaModulo;
	}

	public String getBtGuardarEditar() {
		return btGuardarEditar;
	}

	public void setBtGuardarEditar(String btGuardarEditar) {
		this.btGuardarEditar = btGuardarEditar;
	}

}
