package ec.com.epws.factory;

import ec.com.epws.dao.ie.AlumnoDAO;
import ec.com.epws.dao.ie.AlumnoFamiliaDAO;
import ec.com.epws.dao.ie.AnioLectivoDAO;
import ec.com.epws.dao.ie.AsignacionDocenteDAO;
import ec.com.epws.dao.ie.AsignacionTutorDAO;
import ec.com.epws.dao.ie.AsignaturaDAO;
import ec.com.epws.dao.ie.AsistenciaDAO;
import ec.com.epws.dao.ie.CalificacionParcialCualitativaDAO;
import ec.com.epws.dao.ie.CalificacionParcialDAO;
import ec.com.epws.dao.ie.CalificacionQuimestreCualitativaDAO;
import ec.com.epws.dao.ie.CalificacionQuimestreDAO;
import ec.com.epws.dao.ie.CodigoCualitativaDAO;
import ec.com.epws.dao.ie.CuadroCalificacionDAO;
import ec.com.epws.dao.ie.CualitativaDAO;
import ec.com.epws.dao.ie.CursoDAO;
import ec.com.epws.dao.ie.DiscapacidadDAO;
import ec.com.epws.dao.ie.DocenteDAO;
import ec.com.epws.dao.ie.EmailDAO;
import ec.com.epws.dao.ie.EnfermedadDAO;
import ec.com.epws.dao.ie.EstadoDAO;
import ec.com.epws.dao.ie.FamiliaDAO;
import ec.com.epws.dao.ie.InformacionNegocioDAO;
import ec.com.epws.dao.ie.MatriculaDAO;
import ec.com.epws.dao.ie.MedicaDiscapacidadDAO;
import ec.com.epws.dao.ie.MedicaEnfermedadDAO;
import ec.com.epws.dao.ie.MedicaVacunaDAO;
import ec.com.epws.dao.ie.MesDAO;
import ec.com.epws.dao.ie.ModuloDAO;
import ec.com.epws.dao.ie.ModuloDetalleDAO;
import ec.com.epws.dao.ie.NivelDAO;
import ec.com.epws.dao.ie.NoticiaDAO;
import ec.com.epws.dao.ie.ObservacionDAO;
import ec.com.epws.dao.ie.ObservacionMedicaDAO;
import ec.com.epws.dao.ie.ParcialDAO;
import ec.com.epws.dao.ie.ParentescoDAO;
import ec.com.epws.dao.ie.PensionDAO;
import ec.com.epws.dao.ie.QuimestreDAO;
import ec.com.epws.dao.ie.RolDAO;
import ec.com.epws.dao.ie.RolModuloDAO;
import ec.com.epws.dao.ie.UsuarioDAO;
import ec.com.epws.dao.ie.VacunaDAO;
import ec.com.epws.dao.ie.ValorReferenciaDAO;
import ec.com.epws.doa.impl.AlumnoDAOImpl;
import ec.com.epws.doa.impl.AlumnoFamiliaDAOImpl;
import ec.com.epws.doa.impl.AnioLectivoDAOImpl;
import ec.com.epws.doa.impl.AsignacionDocenteDAOImpl;
import ec.com.epws.doa.impl.AsignacionTutorDAOImpl;
import ec.com.epws.doa.impl.AsignaturaDAOImpl;
import ec.com.epws.doa.impl.AsistenciaDAOImpl;
import ec.com.epws.doa.impl.CalificacionParcialCualitativaDAOImpl;
import ec.com.epws.doa.impl.CalificacionParcialDAOImpl;
import ec.com.epws.doa.impl.CalificacionQuimestreCualitativaDAOImpl;
import ec.com.epws.doa.impl.CalificacionQuimestreDAOImpl;
import ec.com.epws.doa.impl.CodigoCualitativaDAOImpl;
import ec.com.epws.doa.impl.CuadroCalificacionDAOImpl;
import ec.com.epws.doa.impl.CualitativaDAOImpl;
import ec.com.epws.doa.impl.CursoDAOImpl;
import ec.com.epws.doa.impl.DiscapacidadDAOImpl;
import ec.com.epws.doa.impl.DocenteDAOImpl;
import ec.com.epws.doa.impl.EmailDAOImpl;
import ec.com.epws.doa.impl.EnfermedadDAOImpl;
import ec.com.epws.doa.impl.EstadoDAOImpl;
import ec.com.epws.doa.impl.FamiliaDAOImpl;
import ec.com.epws.doa.impl.InformacionNegocioDAOImpl;
import ec.com.epws.doa.impl.MatriculaDAOImpl;
import ec.com.epws.doa.impl.MedicaDiscapacidadDAOImpl;
import ec.com.epws.doa.impl.MedicaEnfermedadDAOImpl;
import ec.com.epws.doa.impl.MedicaVacunaDAOImpl;
import ec.com.epws.doa.impl.MesDAOImpl;
import ec.com.epws.doa.impl.ModuloDAOImpl;
import ec.com.epws.doa.impl.ModuloDetalleDAOImpl;
import ec.com.epws.doa.impl.NivelDAOImpl;
import ec.com.epws.doa.impl.NoticiaDAOImpl;
import ec.com.epws.doa.impl.ObservacionDAOImpl;
import ec.com.epws.doa.impl.ObservacionMedicaDAOImpl;
import ec.com.epws.doa.impl.ParcialDAOImpl;
import ec.com.epws.doa.impl.ParentescoDAOImpl;
import ec.com.epws.doa.impl.PensionDAOImpl;
import ec.com.epws.doa.impl.QuimestreDAOImpl;
import ec.com.epws.doa.impl.RolDAOImpl;
import ec.com.epws.doa.impl.RolModuloDAOImpl;
import ec.com.epws.doa.impl.UsuarioDAOImpl;
import ec.com.epws.doa.impl.VacunaDAOImpl;
import ec.com.epws.doa.impl.ValorReferenciaDAOImpl;

public class InstanciaDAO {

	public static final UsuarioDAO usuarioDAO=new UsuarioDAOImpl();;
	private static EstadoDAO estadoDAO;
	private static RolModuloDAO rolModuloDAO;
	private static ModuloDAO moduloDAO;
	public final static ModuloDetalleDAO moduloDetalleDAO = new ModuloDetalleDAOImpl();
	private static RolDAO rolDAO;
	private static EmailDAO emailDAO;
	private static InformacionNegocioDAO informacionNegocioDAO;
	private static AlumnoDAO alumnoDAO;
	private static AlumnoFamiliaDAO alumnoFamiliaDAO;
	private static FamiliaDAO familiaDAO;
	private static ParentescoDAO parentescoDAO;
	private static CursoDAO cursoDAO;
	private static NivelDAO nivelDAO;
	private static ObservacionMedicaDAO observacionMedicaDAO;
	public static final MatriculaDAO matriculaDAO = new MatriculaDAOImpl();
	public static final AnioLectivoDAO anioLectivoDAO = new AnioLectivoDAOImpl();
	public static final AsignaturaDAO asignaturaDAO = new AsignaturaDAOImpl();
	public static final DocenteDAO docenteDAO = new DocenteDAOImpl();
	public static final MesDAO mesDAO = new MesDAOImpl();
	public static final PensionDAO pensionDAO = new PensionDAOImpl();
	public static final AsignacionDocenteDAO asignacionDocenteDAO = new AsignacionDocenteDAOImpl();
	public static final AsignacionTutorDAO asignacionTutorDAO = new AsignacionTutorDAOImpl();
	public static final AsistenciaDAO asistenciaDAO = new AsistenciaDAOImpl();
	public static final ParcialDAO parcialDAO = new ParcialDAOImpl();
	public static final QuimestreDAO quimestreDAO = new QuimestreDAOImpl();
	public static final CalificacionParcialDAO calificacionParcialDAO = new CalificacionParcialDAOImpl();
	public static final CalificacionQuimestreDAO calificacionQuimestreDAO = new CalificacionQuimestreDAOImpl();
	public static final CualitativaDAO cualitativaDAO = new CualitativaDAOImpl();
	public static final CodigoCualitativaDAO codigoCualitativaDAO = new CodigoCualitativaDAOImpl(); 
	public static final CalificacionParcialCualitativaDAO calificacionParcialCualitativaDAO = new CalificacionParcialCualitativaDAOImpl();
	public static final CalificacionQuimestreCualitativaDAO calificacionQuimestreCualitativaDAO = new CalificacionQuimestreCualitativaDAOImpl();
	public static final ValorReferenciaDAO valorReferenciaDAO = new ValorReferenciaDAOImpl();
	public static final VacunaDAO vacunaDAO = new VacunaDAOImpl();
	public static final EnfermedadDAO enfermedadDAO = new EnfermedadDAOImpl();
	public static final DiscapacidadDAO discapacidadDAO = new DiscapacidadDAOImpl();
	public static final MedicaVacunaDAO medicaVacunaDAO = new MedicaVacunaDAOImpl();
	public static final MedicaEnfermedadDAO medicaEnfermedadDAO = new MedicaEnfermedadDAOImpl();
	public static final MedicaDiscapacidadDAO medicaDiscapacidadDAO = new MedicaDiscapacidadDAOImpl();
	public static final NoticiaDAO noticiaDAO = new NoticiaDAOImpl();
	public static final ObservacionDAO observacionDAO = new ObservacionDAOImpl();
	public static final CuadroCalificacionDAO cuadroCalificacionDAO = new CuadroCalificacionDAOImpl();
	
	private InstanciaDAO() {
	}

/*	public static UsuarioDAO getUsuarioDAO() {
		if (usuarioDAO == null) {
			usuarioDAO = new UsuarioDAOImpl();
		}
		return usuarioDAO;
	}*/

	public static EstadoDAO getEstadoDAO() {
		if (estadoDAO == null) {
			estadoDAO = new EstadoDAOImpl();
		}
		return estadoDAO;
	}

	public static RolModuloDAO getRolModuloDAO() {
		if (rolModuloDAO == null) {
			rolModuloDAO = new RolModuloDAOImpl();
		}
		return rolModuloDAO;
	}

	public static ModuloDAO getModuloDAO() {
		if (moduloDAO == null) {
			moduloDAO = new ModuloDAOImpl();
		}
		return moduloDAO;
	}

	public static RolDAO getRolDAO() {
		if (rolDAO == null) {
			rolDAO = new RolDAOImpl();
		}
		return rolDAO;
	}

	public static EmailDAO getEmailDAO() {
		if (emailDAO == null) {
			emailDAO = new EmailDAOImpl();
		}
		return emailDAO;
	}

	public static InformacionNegocioDAO getInformacionNegocioDAO() {
		if (informacionNegocioDAO == null) {
			informacionNegocioDAO = new InformacionNegocioDAOImpl();
		}
		return informacionNegocioDAO;
	}

	public static AlumnoDAO getAlumnoDAO() {
		if (alumnoDAO == null) {
			alumnoDAO = new AlumnoDAOImpl();
		}
		return alumnoDAO;
	}

	public static AlumnoFamiliaDAO getAlumnoFamiliaDAO() {
		if (alumnoFamiliaDAO == null) {
			alumnoFamiliaDAO = new AlumnoFamiliaDAOImpl();
		}
		return alumnoFamiliaDAO;
	}

	public static FamiliaDAO getFamiliaDAO() {
		if (familiaDAO == null) {
			familiaDAO = new FamiliaDAOImpl();
		}
		return familiaDAO;
	}

	public static ParentescoDAO getParentescoDAO() {
		if (parentescoDAO == null) {
			parentescoDAO = new ParentescoDAOImpl();
		}
		return parentescoDAO;
	}

	public static CursoDAO getCursoDAO() {
		if (cursoDAO == null) {
			cursoDAO = new CursoDAOImpl();
		}
		return cursoDAO;
	}

	public static NivelDAO getNivelDAO() {
		if (nivelDAO == null) {
			nivelDAO = new NivelDAOImpl();
		}
		return nivelDAO;
	}

	public static ObservacionMedicaDAO getObservacionMedicaDAO() {
		if (observacionMedicaDAO == null) {
			observacionMedicaDAO = new ObservacionMedicaDAOImpl();
		}
		return observacionMedicaDAO;
	}

}
