package ec.com.epws.factory;

import ec.com.epws.entity.Estado;

public class InstanciaEntity {

	public static Estado estado;

	private InstanciaEntity() {
	}

	public static Estado getInstanciaEstado() {
		if (estado == null) {
			estado = new Estado();
		}
		return estado;
	}


}
