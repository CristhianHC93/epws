package ec.com.epws.factory;

import ec.com.epws.idioma.Ingles;
import ec.com.epws.idioma.Latino;

public class InstanciaIdioma {
	private InstanciaIdioma(){}

	public static Latino latino;
	public static Ingles ingles;

	public static Latino getLatino() {
		if (latino == null) {
			latino = new Latino();
		}
		return latino;
	}
	
	public static Ingles getIngles() {
		if (ingles == null) {
			ingles = new Ingles();
		}
		return ingles;
	}

}
