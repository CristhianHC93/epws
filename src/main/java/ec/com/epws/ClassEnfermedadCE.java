package ec.com.epws;

import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Window;

import ec.com.epws.entity.Enfermedad;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassEnfermedadCE {
	@Wire("#winEnfermedadCE")
	private Window winEnfermedadCE;

	private Enfermedad enfermedad;

	boolean bandera = false;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view,
			@ExecutionArgParam("objeto") Enfermedad enfermedad) {
		Selectors.wireComponents(view, this, false);
		this.enfermedad = (enfermedad == null) ? new Enfermedad() : enfermedad;
	}

	@Command
	@NotifyChange("enfermedad")
	public void guardarEditar() {
		bandera = true;
		if (enfermedad.getId() == null) {
			InstanciaDAO.enfermedadDAO.saveorUpdate(enfermedad);
			Clients.showNotification("Dato ingresado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
			enfermedad = new Enfermedad();
		} else {
			InstanciaDAO.enfermedadDAO.saveorUpdate(enfermedad);
			Clients.showNotification("Dato Modificado correctamente", Clients.NOTIFICATION_TYPE_INFO, null,
					"middle_center", 1500);
		}
	}

	@Command
	@NotifyChange("enfermedad")
	public void mayusculas(@BindingParam("cadena") String cadena, @BindingParam("campo") String campo) {
		if (campo.equals("descripcion")) {
			enfermedad.setDescripcion((cadena.toUpperCase()));
		}
	}

	@Command
	public void salir() {
		if (bandera) {
			BindUtils.postGlobalCommand(null, null, "refrescarlista", null);
		}
		winEnfermedadCE.detach();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public Enfermedad getEnfermedad() {
		return enfermedad;
	}

	public void setEnfermedad(Enfermedad enfermedad) {
		this.enfermedad = enfermedad;
	}

}
