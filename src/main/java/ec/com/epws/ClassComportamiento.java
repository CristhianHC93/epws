package ec.com.epws;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.AsignacionTutor;
import ec.com.epws.entity.Asignatura;
import ec.com.epws.entity.CalificacionParcialCualitativa;
import ec.com.epws.entity.CalificacionQuimestreCualitativa;
import ec.com.epws.entity.CuadroCalificacion;
import ec.com.epws.entity.Cualitativa;
import ec.com.epws.entity.Docente;
import ec.com.epws.entity.Matricula;
import ec.com.epws.entity.Parcial;
import ec.com.epws.entity.Quimestre;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;

public class ClassComportamiento {

	private List<CalificacionParcialCualitativa> listaComportamiento = new ArrayList<>();
	private List<CalificacionQuimestreCualitativa> listaCalificacionQuimestreCualitativa = new ArrayList<>();
	private List<Matricula> listaMatricula = new ArrayList<>();
	private List<Cualitativa> listaCualitativa = Sistema.getListaCualitativaComportamiento();

	AsignacionTutor tutor;
	private Asignatura comportamiento;

	private List<Quimestre> listaQuimestre;
	private Quimestre quimestre;
	private List<Parcial> listaParcial;
	private Parcial parcial;

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component vista) {
		Selectors.wireComponents(vista, this, false);
		if (Sistema.getUsuario().getRol().getId() != 2) {
			Messagebox.show("Usuario no es Docente");
			return;
		}
		Docente docente = InstanciaDAO.docenteDAO.getDocente(Sistema.getUsuario().getId());
		tutor = InstanciaDAO.asignacionTutorDAO
				.getAsignacionTutorByDocente(Sistema.getAnioLectivoActivoEstatico().getId(), docente.getId());
		if (tutor != null && tutor.getCurso() != null) {
			InstanciaDAO.asignaturaDAO.getListaAsignatura(tutor.getCurso().getId(), true).forEach(x -> {
				if (x.getCodigoCualitativa().getId() == Sistema.FILTRO_CUALITATIVO_COMPORTAMIENTO) {
					comportamiento = x;
				}
			});
		}
		if (comportamiento == null) {
			Messagebox.show("No tiene listado para asignar comportamiento");
			return;
		}
		listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(
				Sistema.getAnioLectivoActivoEstatico().getId(), comportamiento.getCurso().getId());
		listaQuimestre = InstanciaDAO.quimestreDAO.getListaQuimestre();
		quimestre = listaQuimestre.get(0);
		listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
		parcial = listaParcial.get(0);
		buscar();
	}

	@NotifyChange("listaComportamiento")
	@Command
	public void buscar() {
		listaComportamiento = InstanciaDAO.calificacionParcialCualitativaDAO
				.getListaCalificacionParcialCualitativaByAsignaturaAndParcial(comportamiento.getId(), parcial.getId());
		if (listaComportamiento == null || listaComportamiento.size() == 0) {
			llenarListaAlumno();
		}
	}

	public void llenarListaAlumno() {
		listaComportamiento.clear();
		listaCalificacionQuimestreCualitativa.clear();
		List<CalificacionQuimestreCualitativa> pro = InstanciaDAO.calificacionQuimestreCualitativaDAO
				.getListaCalificacionQuimestreCualitativaByAsignaturaAndQuimestre(comportamiento.getId(),
						quimestre.getId());
		if (pro == null || pro.size() == 0) {
			listaMatricula.forEach(x -> {
				CalificacionQuimestreCualitativa calificacionQuimestreCualitativa = new CalificacionQuimestreCualitativa();
				calificacionQuimestreCualitativa.setMatricula(x);
				listaCalificacionQuimestreCualitativa.add(calificacionQuimestreCualitativa);
			});
		} else {
			listaCalificacionQuimestreCualitativa.addAll(pro);
		}

		listaCalificacionQuimestreCualitativa.forEach(x -> {
			CalificacionParcialCualitativa calificacionParcialCualitativa = new CalificacionParcialCualitativa();
			calificacionParcialCualitativa.setCalificacionQuimestreCualitativa(x);
			calificacionParcialCualitativa.setParcial(parcial);
			// calificacionParcialCualitativa.setCualitativa(listaCualitativa.get(0));
			listaComportamiento.add(calificacionParcialCualitativa);
		});
	}

	double promedioCualitativa = 0.0;
	boolean val = false;

	@NotifyChange({ "listaComportamiento", "parcial" })
	@Command
	public void guardar() {
		listaComportamiento.forEach(x -> {
			if (x.getCualitativa() == null) {
				val = true;
			}
		});

		if (val) {
			Messagebox.show("Hay Valor(es) sin calificar");
			val = false;
			return;
		}
		listaComportamiento.forEach(x -> {
			promedioCualitativa = 0.0;
			if (x.getCalificacionQuimestreCualitativa().getId() != null) {
				List<CalificacionParcialCualitativa> lis = InstanciaDAO.calificacionParcialCualitativaDAO
						.getListaCalificacionParcialCualitativaByCalificacionQuimestre(
								x.getCalificacionQuimestreCualitativa().getId());
				lis.forEach(y -> {
					promedioCualitativa = promedioCualitativa + y.getCualitativa().getId();
				});
			} else {
				promedioCualitativa = x.getCualitativa().getId();
			}
			promedioCualitativa = promedioCualitativa / 3;
			double id = Sistema.redondearEntero(promedioCualitativa);
			if (id <= 14) {
				id = 14;
			}
			x.getCalificacionQuimestreCualitativa()
					.setCualitativa2(InstanciaDAO.cualitativaDAO.getCualitativa((int) id));

			x.getCalificacionQuimestreCualitativa().setQuimestre(quimestre);
			x.getCalificacionQuimestreCualitativa().setAsignatura(comportamiento);
			x.setParcial(parcial);
			
			//Inicia proceso para guardar Cuadro Calificacion
			CuadroCalificacion cc = InstanciaDAO.cuadroCalificacionDAO
					.getCuadroCalificacion(x.getCalificacionQuimestreCualitativa().getAsignatura().getId(), x.getCalificacionQuimestreCualitativa().getMatricula().getId());
			if (cc == null) {
				cc = new CuadroCalificacion();
				cc.setAsignatura(x.getCalificacionQuimestreCualitativa().getAsignatura());
				cc.setMatricula(x.getCalificacionQuimestreCualitativa().getMatricula());
			}
			if (x.getCalificacionQuimestreCualitativa().getQuimestre().getId() == 1) {
				cc.setQuimestre1(x.getCalificacionQuimestreCualitativa().getCualitativa2().getId());
			}
			if (x.getCalificacionQuimestreCualitativa().getQuimestre().getId() == 2) {
				cc.setQuimestre2(x.getCalificacionQuimestreCualitativa().getCualitativa2().getId());
			}
			double notaFinal = Sistema.redondearEntero((cc.getQuimestre1()+cc.getQuimestre2())/2);
			if (notaFinal <= 14) {
				notaFinal = 14;
			}
			cc.setPromedio(id);
			cc.setPromedioFinal(cc.getPromedio());
			cc.setLetra(Sistema.numeroAletras(cc.getPromedio()));
			cc.setNota(InstanciaDAO.cualitativaDAO.getCualitativa((int) id).getSiglas());
			InstanciaDAO.cuadroCalificacionDAO.saveorUpdate(cc);
			//Fin proceso
			
			InstanciaDAO.calificacionParcialCualitativaDAO.saveorUpdate(x);
			// InstanciaDAO.calificacionQuimestreCualitativaDAO.saveorUpdate(x.getCalificacionQuimestreCualitativa());
		});
		Clients.showNotification("Ingresado Correcto", Clients.NOTIFICATION_TYPE_INFO, null, "middle_center", 1500);
		buscar();
	}

	@NotifyChange({ "listaComportamiento", "listaParcial", "parcial" })
	@Command
	public void selectQuimestre() {
		listaParcial = InstanciaDAO.parcialDAO.getListaParcial(quimestre.getId());
		parcial = listaParcial.get(0);
		buscar();
	}

	@NotifyChange({ "listaComportamiento", "listaParcial", "parcial" })
	@Command
	public void selectParcial() {
		buscar();
	}

	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}

	public List<CalificacionParcialCualitativa> getListaComportamiento() {
		return listaComportamiento;
	}

	public void setListaComportamiento(List<CalificacionParcialCualitativa> listaComportamiento) {
		this.listaComportamiento = listaComportamiento;
	}

	public List<Cualitativa> getListaCualitativa() {
		return listaCualitativa;
	}

	public void setListaCualitativa(List<Cualitativa> listaCualitativa) {
		this.listaCualitativa = listaCualitativa;
	}

	public Asignatura getComportamiento() {
		return comportamiento;
	}

	public void setComportamiento(Asignatura comportamiento) {
		this.comportamiento = comportamiento;
	}

	public List<Quimestre> getListaQuimestre() {
		return listaQuimestre;
	}

	public void setListaQuimestre(List<Quimestre> listaQuimestre) {
		this.listaQuimestre = listaQuimestre;
	}

	public Quimestre getQuimestre() {
		return quimestre;
	}

	public void setQuimestre(Quimestre quimestre) {
		this.quimestre = quimestre;
	}

	public List<Parcial> getListaParcial() {
		return listaParcial;
	}

	public void setListaParcial(List<Parcial> listaParcial) {
		this.listaParcial = listaParcial;
	}

	public Parcial getParcial() {
		return parcial;
	}

	public void setParcial(Parcial parcial) {
		this.parcial = parcial;
	}

}
