package ec.com.epws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Messagebox;

import ec.com.epws.entity.Curso;
import ec.com.epws.entity.Matricula;
import ec.com.epws.factory.InstanciaDAO;
import ec.com.epws.idioma.Idioma;
import ec.com.epws.sistema.Sistema;
import ec.com.epws.sistema.Util;

public class ClassReporteMatricula {

	private List<Curso> listaCurso;
	private Curso curso;

	private List<Matricula> listaMatricula = new ArrayList<>();

	@Init
	public void initSetup(@ContextParam(ContextType.VIEW) Component view, @ExecutionArgParam("curso") Curso curso,
			@ExecutionArgParam("lista") List<Matricula> lista) {
		Selectors.wireComponents(view, this, false);
		listaCurso = InstanciaDAO.getCursoDAO().getListaCurso();
		this.curso = (curso == null) ? listaCurso.get(0) : curso;
		if (lista != null) {
			listaMatricula.addAll(lista);
		}else{
		buscar();}
		}
	
	@NotifyChange("listaMatricula")
	@Command
	public void buscar(){
		listaMatricula = InstanciaDAO.matriculaDAO.getListaMatriculaAnioActivoByCurso(Sistema.getAnioLectivoActivoEstatico().getId(), curso.getId());		
	}
	
	@Command
	public void generarExcel() {
		if (listaMatricula.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		Sistema.reporteEXCEL(Sistema.MATRICULA_JASPER, "Matricula", mapreporte,
				Sistema.getMatriculaDataSource(listaMatricula),null,false);
	}

	@Command
	public void generarPdf() {
		if (listaMatricula.size() == 0) {
			Messagebox.show("No existe informacion para el reporte");
			return;
		}
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("lista", listaMatricula);
		parametros.put("curso", curso);
		Executions.createComponents("/reporte/impresionMatricula.zul", null, parametros);
	}

	@Command
	public void showReport(@BindingParam("item") Iframe iframe) {

		Map<String, Object> mapreporte = new HashMap<String, Object>();
		mapreporte.put("escuela", Sistema.getInformacionNegocio().getNombre());
		mapreporte.put("codigo", Sistema.getInformacionNegocio().getCodigo());
		mapreporte.put("anioLectivo", Sistema.getAnioLectivoActivoEstatico().getDescripcion());
		mapreporte.put("curso", curso.getDescripcion());
		mapreporte.put("me", Util.getPathRaiz()+Sistema.ME);
		mapreporte.put("logo", Util.getPathRaiz()+Sistema.LOGO);
		iframe.setContent(Sistema.reportePDF(Sistema.MATRICULA_JASPER,
				mapreporte, Sistema.getMatriculaDataSource(listaMatricula)));
	}
	
	/*
	 * 
	 */

	public Idioma getIdioma() {
		return Sistema.idioma;
	}
	
	public String getConstraintPrecio() {
		return Sistema.CONSTRAINT_PRECIO;
	}
	
	public List<Curso> getListaCurso() {
		return listaCurso;
	}

	public void setListaCurso(List<Curso> listaCurso) {
		this.listaCurso = listaCurso;
	}

	public Curso getCurso() {
		return curso;
	}

	public void setCurso(Curso curso) {
		this.curso = curso;
	}

	public List<Matricula> getListaMatricula() {
		return listaMatricula;
	}

	public void setListaMatricula(List<Matricula> listaMatricula) {
		this.listaMatricula = listaMatricula;
	}
	
	
}
